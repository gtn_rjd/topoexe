object AboutBox: TAboutBox
  Left = 671
  Top = 297
  BorderStyle = bsDialog
  Caption = 'About'
  ClientHeight = 250
  ClientWidth = 297
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 8
    Top = 8
    Width = 281
    Height = 203
    BevelInner = bvRaised
    BevelOuter = bvLowered
    ParentColor = True
    TabOrder = 0
    object ProgramIcon: TImage
      Left = 8
      Top = 8
      Width = 32
      Height = 32
      AutoSize = True
      Picture.Data = {
        055449636F6E0000010001002020100000000000E80200001600000028000000
        2000000040000000010004000000000080020000000000000000000000000000
        0000000000000000000080000080000000808000800000008000800080800000
        C0C0C000808080000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000
        FFFFFF0000E00000000000000000000000000000000E00000000000000000000
        000000000000E000000000000000000A0000000000000AAAAAAAAAAAAAAAAAAA
        AA00000000000A00000000000000000A0000000000000A000000000000000000
        0000000000000A0000000000000000000000000000000A000000000000000000
        0000000000000A0000000000000000000000000000000A000000000000000000
        0000000000000A0000000000000000000000000000000A000000000000000000
        0000000000000A0000000000000000000000000000000A000000000000000000
        0000000000000A0000000000000000000000000000000A000000000000000000
        0000B00000000A000000B0B000000000000000000000AAA00000BB0000000000
        0000000000000A000000BBB0B000000000B00B0000000A0000000BBBBB0BB00B
        B00000000000000000000BBBBBBB00000000000000000000000000BBBBBB00BB
        BB000000000000000000000BBBBBBBBBB00000000000000000000000BB00BBBB
        0B0000000000000000000000000BBBB0B000000000000000000000000B9BBB0B
        B0000000000000000000000BBBB900BBBBB00000000000000000000BB0BB00BB
        BBB0000000000000000000000BB00BBBBBB0B00000000000000000000BB00B0B
        BBBB00000000000000000000000000000BBBBBB0000000000000000000000000
        000BBB0000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000}
      Stretch = True
      IsControl = True
    end
    object ProductName: TLabel
      Left = 126
      Top = 17
      Width = 29
      Height = 13
      Alignment = taCenter
      AutoSize = False
      Caption = 'TOpO'
      IsControl = True
    end
    object Version: TLabel
      Left = 108
      Top = 72
      Width = 64
      Height = 13
      Alignment = taCenter
      AutoSize = False
      Caption = 'Version  0.4'
      IsControl = True
    end
    object Copyright: TLabel
      Left = 63
      Top = 158
      Width = 155
      Height = 13
      Alignment = taCenter
      AutoSize = False
      Caption = 'Shelepin brothers  1995-2015  '
      IsControl = True
    end
    object Comments: TLabel
      Left = 42
      Top = 38
      Width = 195
      Height = 26
      Alignment = taCenter
      AutoSize = False
      Caption = 'General purpose cave program'
      WordWrap = True
      IsControl = True
    end
    object Label1: TLabel
      Left = 103
      Top = 120
      Width = 74
      Height = 13
      Alignment = taCenter
      AutoSize = False
      Caption = '  Created by '
    end
    object Label2: TLabel
      Left = 84
      Top = 136
      Width = 112
      Height = 13
      Alignment = taCenter
      AutoSize = False
      Caption = 'An. Verchenko  2006-08'
    end
    object Label3: TLabel
      Left = 81
      Top = 179
      Width = 119
      Height = 13
      Alignment = taCenter
      AutoSize = False
      Caption = 'P.Koveshnikov 2015-16'
    end
  end
  object OKButton: TButton
    Left = 112
    Top = 217
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 1
    OnClick = OKButtonClick
  end
end
