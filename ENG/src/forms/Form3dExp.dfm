object Exp3dForm: TExp3dForm
  Left = 257
  Top = 175
  BorderStyle = bsDialog
  Caption = 'TOpO - 3D Export'
  ClientHeight = 328
  ClientWidth = 371
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object PageCtr: TPageControl
    Left = 0
    Top = 0
    Width = 371
    Height = 273
    ActivePage = TabSheet1
    Align = alCustom
    Anchors = [akLeft, akTop, akRight]
    MultiLine = True
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = 'vrml'
      object Label1: TLabel
        Left = 336
        Top = 280
        Width = 32
        Height = 13
        Caption = 'Label1'
      end
      object Options: TGroupBox
        Left = 16
        Top = 16
        Width = 329
        Height = 209
        Caption = 'Options'
        TabOrder = 0
        object Label2: TLabel
          Left = 160
          Top = 48
          Width = 23
          Height = 13
          Caption = 'Background'
        end
        object Label3: TLabel
          Left = 16
          Top = 24
          Width = 86
          Height = 13
          Caption = 'Show:'
          WordWrap = True
        end
        object Label4: TLabel
          Left = 160
          Top = 24
          Width = 28
          Height = 13
          Caption = 'Color:'
        end
        object Label5: TLabel
          Left = 160
          Top = 80
          Width = 32
          Height = 13
          Caption = 'Lines'
        end
        object BgColor: TComboBox
          Left = 232
          Top = 40
          Width = 81
          Height = 21
          TabOrder = 0
          Text = 'Black'
          Items.Strings = (
            'White'
            'Grey'
            'Black')
        end
        object CheckSurface: TCheckBox
          Left = 16
          Top = 48
          Width = 97
          Height = 17
          Caption = 'Surface legs'
          Checked = True
          State = cbChecked
          TabOrder = 1
        end
        object CheckDuplicate: TCheckBox
          Left = 16
          Top = 80
          Width = 145
          Height = 17
          Caption = 'Duplicate legs'
          Checked = True
          State = cbChecked
          TabOrder = 2
        end
        object vrml_lines: TComboBox
          Left = 232
          Top = 72
          Width = 81
          Height = 21
          TabOrder = 3
          Text = 'Colored'
          Items.Strings = (
            'Colored'
            'Black')
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'txt'
      ImageIndex = 1
      object OptionsTxt: TGroupBox
        Left = 16
        Top = 16
        Width = 329
        Height = 209
        Caption = 'Options'
        TabOrder = 0
        object Txt_Statistics: TCheckBox
          Left = 16
          Top = 32
          Width = 153
          Height = 17
          Caption = 'Statistics'
          Checked = True
          State = cbChecked
          TabOrder = 0
        end
        object Txt_Surveys: TCheckBox
          Left = 16
          Top = 56
          Width = 113
          Height = 17
          Caption = 'Surveys'
          Checked = True
          State = cbChecked
          TabOrder = 1
        end
        object Txt_Loops: TCheckBox
          Left = 16
          Top = 80
          Width = 169
          Height = 17
          Caption = 'Loops'
          Checked = True
          State = cbChecked
          TabOrder = 2
        end
        object Txt_Stations: TCheckBox
          Left = 16
          Top = 104
          Width = 97
          Height = 17
          Caption = 'Stations'
          Checked = True
          State = cbChecked
          TabOrder = 3
          OnClick = Txt_StationsClick
        end
        object Txt_Surface: TCheckBox
          Left = 32
          Top = 128
          Width = 97
          Height = 17
          Caption = 'Surface '
          TabOrder = 4
        end
        object Txt_Duplicate: TCheckBox
          Left = 32
          Top = 152
          Width = 193
          Height = 17
          Caption = 'Duplicate'
          TabOrder = 5
        end
      end
    end
  end
  object Cancel: TButton
    Left = 280
    Top = 288
    Width = 75
    Height = 25
    Caption = 'Cancel'
    TabOrder = 1
    OnClick = CancelClick
  end
  object OK: TButton
    Left = 184
    Top = 288
    Width = 75
    Height = 25
    Caption = 'OK'
    TabOrder = 2
    OnClick = OKClick
  end
end
