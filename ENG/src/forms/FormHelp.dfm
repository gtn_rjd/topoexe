object HelpForm: THelpForm
  Left = 336
  Top = 298
  Caption = 'T0p0 Help'
  ClientHeight = 488
  ClientWidth = 676
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Icon.Data = {
    0000010001002020100000000000E80200001600000028000000200000004000
    0000010004000000000080020000000000000000000000000000000000000000
    0000000080000080000000808000800000008000800080800000C0C0C0008080
    80000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00AAE0
    00000000000000000000000000000AAE000000000000000000000000000000AA
    E000000000000000000AA0000000000AAAAAAAAAAAAAAAAAAAAAAAA000000000
    AAAAAAAAAAAAAAAAAAAAAAA000000000AA00000000000000000AA00000000000
    AA000000000000000000000000000000AA000000000000000000000000000000
    AA000000000000000000000000000000AA000000000000000000000000000000
    AA000000000000000000000000000000AA000000000000000000000000000000
    AA000000000000000000000000000000AA000000000000000000000000000000
    AA000000000000000000000000000000AA0000000000000000000000B0000000
    AA000000B0B000000000000000000000AA000000BB0000000000000000000000
    AA000000BBB0B000000000B00B000000AA0000000BBBBB0BB00BB00000000000
    AA0000000BBBBBBB0000000000000000AA00000000BBBBBB00BBBB0000000000
    AA000000000BBBBBBBBBB00000000000AA0000000000BB00BBBB0B000000000A
    AAA000000000000BBBB0B0000000000AAAA0000000000B9BBB0BB00000000000
    AA000000000BBBB900BBBBB000000000AA000000000BB0BB00BBBBB000000000
    0000000000000BB00BBBBBB0B00000000000000000000BB00B0BBBBB00000000
    000000000000000000000BBBBBB0000000000000000000000000000BBB000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    000000000000000000000000000000000000000000000000000000000000}
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 676
    Height = 488
    ActivePage = TabSheet9
    Align = alClient
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = []
    MultiLine = True
    ParentFont = False
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = 'General'
      object Memo2: TMemo
        Left = 0
        Top = 0
        Width = 668
        Height = 436
        Align = alClient
        BorderStyle = bsNone
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = True
        ParentFont = False
        ReadOnly = True
        ScrollBars = ssVertical
        TabOrder = 0
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Data'
      ImageIndex = 1
      object Memo3: TMemo
        Left = 0
        Top = 0
        Width = 668
        Height = 436
        Align = alClient
        BorderStyle = bsNone
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = True
        ParentFont = False
        ReadOnly = True
        ScrollBars = ssVertical
        TabOrder = 0
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'Commands'
      ImageIndex = 2
      object Memo4: TMemo
        Left = 0
        Top = 0
        Width = 668
        Height = 436
        Align = alClient
        BorderStyle = bsNone
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Fixedsys'
        Font.Style = []
        ParentColor = True
        ParentFont = False
        ReadOnly = True
        ScrollBars = ssVertical
        TabOrder = 0
      end
    end
    object TabSheet4: TTabSheet
      Caption = 'Calibration'
      ImageIndex = 3
      object Memo5: TMemo
        Left = 0
        Top = 0
        Width = 668
        Height = 436
        Align = alClient
        BorderStyle = bsNone
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Fixedsys'
        Font.Style = []
        ParentColor = True
        ParentFont = False
        ReadOnly = True
        ScrollBars = ssVertical
        TabOrder = 0
      end
    end
    object TabSheet5: TTabSheet
      Caption = 'View    '
      ImageIndex = 4
      object Memo6: TMemo
        Left = 0
        Top = 0
        Width = 668
        Height = 436
        Align = alClient
        BorderStyle = bsNone
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = True
        ParentFont = False
        ReadOnly = True
        ScrollBars = ssVertical
        TabOrder = 0
      end
    end
    object TabSheet6: TTabSheet
      Caption = 'Print | Export'
      ImageIndex = 5
      object Memo7: TMemo
        Left = 0
        Top = 0
        Width = 668
        Height = 436
        Align = alClient
        BorderStyle = bsNone
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = True
        ParentFont = False
        ReadOnly = True
        ScrollBars = ssVertical
        TabOrder = 0
      end
    end
    object TabSheet7: TTabSheet
      Caption = 'Loops'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = []
      ImageIndex = 6
      ParentFont = False
      ParentShowHint = False
      ShowHint = False
      object Memo8: TMemo
        Left = 0
        Top = 0
        Width = 668
        Height = 436
        Align = alClient
        BorderStyle = bsNone
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = True
        ParentFont = False
        ReadOnly = True
        ScrollBars = ssVertical
        TabOrder = 0
      end
    end
    object TabSheet8: TTabSheet
      Caption = 'Ext. Profile'
      ImageIndex = 7
      object Memo9: TMemo
        Left = 0
        Top = 0
        Width = 668
        Height = 436
        Align = alClient
        BorderStyle = bsNone
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        Lines.Strings = (
          '')
        ParentColor = True
        ParentFont = False
        ReadOnly = True
        ScrollBars = ssVertical
        TabOrder = 0
      end
    end
    object TabSheet9: TTabSheet
      Caption = 'Surf. Geometry'
      ImageIndex = 8
      object Memo1: TMemo
        Left = 0
        Top = 0
        Width = 668
        Height = 436
        Align = alClient
        BorderStyle = bsNone
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        Lines.Strings = (
          '')
        ParentColor = True
        ParentFont = False
        ReadOnly = True
        ScrollBars = ssVertical
        TabOrder = 0
      end
    end
    object TabSheet10: TTabSheet
      Caption = 'Walls'
      ImageIndex = 9
      object Memo10: TMemo
        Left = 0
        Top = 0
        Width = 668
        Height = 436
        Align = alClient
        BorderStyle = bsNone
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = True
        ParentFont = False
        ReadOnly = True
        ScrollBars = ssVertical
        TabOrder = 0
      end
    end
  end
end
