object PrnForm: TPrnForm
  Left = 500
  Top = 149
  BorderStyle = bsDialog
  Caption = 'TOpO '#1055#1077#1095#1072#1090#1100' / 2D '#1069#1082#1089#1087#1086#1088#1090
  ClientHeight = 540
  ClientWidth = 627
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Icon.Data = {
    0000010001002020100000000000E80200001600000028000000200000004000
    0000010004000000000080020000000000000000000000000000000000000000
    0000000080000080000000808000800000008000800080800000C0C0C0008080
    80000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00AAE0
    00000000000000000000000000000AAE000000000000000000000000000000AA
    E000000000000000000AA0000000000AAAAAAAAAAAAAAAAAAAAAAAA000000000
    AAAAAAAAAAAAAAAAAAAAAAA000000000AA00000000000000000AA00000000000
    AA000000000000000000000000000000AA000000000000000000000000000000
    AA000000000000000000000000000000AA000000000000000000000000000000
    AA000000000000000000000000000000AA000000000000000000000000000000
    AA000000000000000000000000000000AA000000000000000000000000000000
    AA000000000000000000000000000000AA0000000000000000000000B0000000
    AA000000B0B000000000000000000000AA000000BB0000000000000000000000
    AA000000BBB0B000000000B00B000000AA0000000BBBBB0BB00BB00000000000
    AA0000000BBBBBBB0000000000000000AA00000000BBBBBB00BBBB0000000000
    AA000000000BBBBBBBBBB00000000000AA0000000000BB00BBBB0B000000000A
    AAA000000000000BBBB0B0000000000AAAA0000000000B9BBB0BB00000000000
    AA000000000BBBB900BBBBB000000000AA000000000BB0BB00BBBBB000000000
    0000000000000BB00BBBBBB0B00000000000000000000BB00B0BBBBB00000000
    000000000000000000000BBBBBB0000000000000000000000000000BBB000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    000000000000000000000000000000000000000000000000000000000000}
  OldCreateOrder = True
  OnActivate = FormActivate
  OnCloseQuery = FormCloseQuery
  PixelsPerInch = 96
  TextHeight = 13
  object PaintBox1: TPaintBox
    Left = 8
    Top = 85
    Width = 457
    Height = 346
    OnMouseDown = PaintBox1MouseDown
    OnPaint = PaintBox1Paint
  end
  object SpeedButton1: TSpeedButton
    Left = 355
    Top = 451
    Width = 94
    Height = 33
    Caption = #1057#1087#1088#1072#1074#1082#1072
    Glyph.Data = {
      F6000000424DF600000000000000760000002800000010000000100000000100
      0400000000008000000000000000000000001000000000000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
      777777777770007777777777770BB00777777777770BB0077777777777700777
      77777777770B007777777777770B007777777777770B007777777777770BB007
      777777770000BB0077777770B0000BB007777770B00000B007777770BB000BB0
      077777770BBBBB00777777777000000777777777777777777777}
    OnClick = SpeedButton1Click
  end
  object GroupBox1: TGroupBox
    Left = 472
    Top = 140
    Width = 153
    Height = 249
    Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1072
    TabOrder = 1
    object Label2: TLabel
      Left = 128
      Top = 16
      Width = 17
      Height = 13
      Caption = 'sm.'
    end
    object LabelPrintScale: TLabel
      Left = 19
      Top = 206
      Width = 18
      Height = 13
      Caption = '000'
    end
    object Label1: TLabel
      Left = 9
      Top = 193
      Width = 86
      Height = 13
      Caption = 'Print scale:'
    end
    object CheckBox1: TCheckBox
      Left = 8
      Top = 16
      Width = 85
      Height = 17
      Caption = 'Grid step'
      TabOrder = 0
    end
    object EditGridScale: TEdit
      Left = 96
      Top = 8
      Width = 25
      Height = 21
      MaxLength = 3
      TabOrder = 1
      Text = '1'
    end
    object CheckBox2: TCheckBox
      Left = 8
      Top = 32
      Width = 65
      Height = 17
      Caption = 'Border'
      TabOrder = 2
    end
    object CheckBox3: TCheckBox
      Left = 8
      Top = 64
      Width = 65
      Height = 17
      Caption = 'Stations'
      Checked = True
      State = cbChecked
      TabOrder = 3
      OnClick = CheckBox3Click
    end
    object CheckBox4: TCheckBox
      Left = 8
      Top = 128
      Width = 89
      Height = 17
      Caption = 'Surface'
      TabOrder = 4
      OnClick = CheckBox4Click
    end
    object CheckBox5: TCheckBox
      Left = 8
      Top = 96
      Width = 105
      Height = 17
      Caption = 'Bifurcation labels'
      TabOrder = 5
      OnClick = CheckBox5Click
    end
    object CheckBox6: TCheckBox
      Left = 8
      Top = 48
      Width = 97
      Height = 17
      Caption = 'Thread'
      Checked = True
      State = cbChecked
      TabOrder = 6
      OnClick = CheckBox6Click
    end
    object CheckBox7: TCheckBox
      Left = 8
      Top = 112
      Width = 97
      Height = 17
      Caption = 'Comments'
      TabOrder = 7
    end
    object CheckBox8: TCheckBox
      Left = 8
      Top = 80
      Width = 81
      Height = 17
      Caption = 'All Stations'
      TabOrder = 8
      OnClick = CheckBox8Click
    end
    object CheckBox9: TCheckBox
      Left = 8
      Top = 144
      Width = 97
      Height = 17
      Caption = 'Duplicated survies'
      TabOrder = 9
      OnClick = CheckBox9Click
    end
    object ButtonScale: TButton
      Left = 9
      Top = 220
      Width = 137
      Height = 25
      Caption = 'Set zoom...'
      TabOrder = 10
      OnClick = ButtonScaleClick
    end
    object CheckBox10: TCheckBox
      Left = 8
      Top = 160
      Width = 73
      Height = 17
      Caption = 'LRUD'
      TabOrder = 11
    end
    object CheckBox11: TCheckBox
      Left = 8
      Top = 176
      Width = 121
      Height = 17
      Caption = 'Cave outline'
      Checked = True
      State = cbChecked
      TabOrder = 12
      OnClick = CheckBox4Click
    end
  end
  object GroupBox5: TGroupBox
    Left = 328
    Top = 42
    Width = 297
    Height = 41
    TabOrder = 4
    object Label8: TLabel
      Left = 8
      Top = 7
      Width = 82
      Height = 30
      Caption = #1056#1072#1079#1088#1077#1096#1077#1085#1080#1077' '#1087#1088#1080' '#1101#1082#1089#1087#1086#1088#1090#1077
      WordWrap = True
    end
    object Label9: TLabel
      Left = 136
      Top = 16
      Width = 14
      Height = 13
      Caption = 'dpi'
    end
    object EditBitmapResolution: TEdit
      Left = 96
      Top = 10
      Width = 33
      Height = 21
      ParentColor = True
      TabOrder = 0
      Text = '200'
    end
    object SaveButton: TButton
      Left = 177
      Top = 8
      Width = 113
      Height = 25
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100' '#1074' .bmp'
      TabOrder = 1
      OnClick = SaveButtonClick
    end
  end
  object GroupBox4: TGroupBox
    Left = 328
    Top = 1
    Width = 297
    Height = 41
    TabOrder = 3
    object Label10: TLabel
      Left = 8
      Top = 11
      Width = 66
      Height = 26
      Caption = #1056#1072#1079#1088#1077#1096#1077#1085#1080#1077' '#1087#1088#1080' '#1087#1077#1095#1072#1090#1080
      WordWrap = True
    end
    object Label11: TLabel
      Left = 136
      Top = 16
      Width = 14
      Height = 13
      Caption = 'dpi'
    end
    object PrintButton: TButton
      Left = 176
      Top = 9
      Width = 113
      Height = 25
      Caption = #1055#1077#1095#1072#1090#1100
      TabOrder = 0
      OnClick = PrintButtonClick
    end
    object EditPrinterResolution: TEdit
      Left = 95
      Top = 11
      Width = 33
      Height = 21
      MaxLength = 4
      ParentColor = True
      TabOrder = 1
      Text = '600'
    end
  end
  object Edit2: TEdit
    Left = 152
    Top = 96
    Width = 57
    Height = 21
    Color = clSilver
    ReadOnly = True
    TabOrder = 0
    Visible = False
  end
  object GroupBox2: TGroupBox
    Left = 472
    Top = 84
    Width = 153
    Height = 57
    Caption = #1054#1088#1080#1077#1085#1090#1072#1094#1080#1103' '#1083#1080#1089#1090#1072
    TabOrder = 2
    object RadioButton1: TRadioButton
      Left = 8
      Top = 16
      Width = 89
      Height = 17
      Caption = #1055#1086#1088#1090#1088#1077#1085#1072#1103
      Checked = True
      TabOrder = 0
      TabStop = True
      OnClick = RadioButton1Click
    end
    object RadioButton2: TRadioButton
      Left = 8
      Top = 32
      Width = 81
      Height = 17
      Caption = #1040#1083#1100#1073#1086#1084#1085#1072#1103
      TabOrder = 1
      OnClick = RadioButton2Click
    end
  end
  object PSBox: TGroupBox
    Left = 8
    Top = 436
    Width = 329
    Height = 103
    Caption = #1069#1082#1089#1087#1086#1088#1090' '#1074' '#1074#1077#1082#1090#1086#1088#1085#1099#1081' '#1092#1086#1088#1084#1072#1090' (PostScript)'
    TabOrder = 5
    object Label12: TLabel
      Left = 104
      Top = 45
      Width = 9
      Height = 13
      Caption = 'pt'
    end
    object psFontsize: TLabel
      Left = 20
      Top = 45
      Width = 34
      Height = 13
      Alignment = taRightJustify
      Caption = #1064#1088#1080#1092#1090
    end
    object psLinewidth: TLabel
      Left = -17
      Top = 65
      Width = 71
      Height = 27
      Alignment = taRightJustify
      Caption = #1058#1086#1083#1097#1080#1085#1072' '#1083#1080#1085#1080#1081
      WordWrap = True
    end
    object Label14: TLabel
      Left = 104
      Top = 69
      Width = 9
      Height = 17
      Caption = 'pt'
    end
    object Label13: TLabel
      Left = 216
      Top = 45
      Width = 9
      Height = 13
      Caption = 'pt'
    end
    object psRadius: TLabel
      Left = 133
      Top = 45
      Width = 36
      Height = 13
      Alignment = taRightJustify
      Caption = #1056#1072#1076#1080#1091#1089
    end
    object Label15: TLabel
      Left = 136
      Top = 23
      Width = 85
      Height = 13
      Alignment = taRightJustify
      Caption = #1050#1088#1091#1078#1082#1080' '#1087#1080#1082#1077#1090#1086#1074':'
    end
    object Label16: TLabel
      Left = 216
      Top = 69
      Width = 9
      Height = 17
      Caption = 'pt'
    end
    object Label17: TLabel
      Left = 129
      Top = 73
      Width = 40
      Height = 13
      Alignment = taRightJustify
      Caption = #1058#1086#1083#1097#1080#1072
    end
    object ButtonPSExp: TButton
      Left = 244
      Top = 67
      Width = 75
      Height = 25
      Caption = #1069#1082#1089#1087#1086#1088#1090
      TabOrder = 0
      OnClick = ButtonPSExpClick
    end
    object Edit1: TEdit
      Left = 64
      Top = 41
      Width = 33
      Height = 21
      TabOrder = 1
      Text = '8'
    end
    object Edit7: TEdit
      Left = 64
      Top = 69
      Width = 33
      Height = 21
      TabOrder = 2
      Text = '0.4'
    end
    object psColor: TCheckBox
      Left = 244
      Top = 43
      Width = 57
      Height = 17
      Caption = #1062#1074#1077#1090
      Checked = True
      State = cbChecked
      TabOrder = 3
    end
    object Edit8: TEdit
      Left = 176
      Top = 41
      Width = 33
      Height = 21
      TabOrder = 4
      Text = '1'
    end
    object Edit9: TEdit
      Left = 176
      Top = 69
      Width = 33
      Height = 21
      TabOrder = 5
      Text = '0.2'
    end
  end
  object GroupBox6: TGroupBox
    Left = 472
    Top = 391
    Width = 153
    Height = 146
    TabOrder = 6
    object Label3: TLabel
      Left = 5
      Top = 8
      Width = 130
      Height = 26
      Caption = #1057#1084#1077#1097#1077#1085#1080#1077' '#1086#1090#1085#1086#1089#1080#1090#1077#1083#1100#1085#1086' '#1082#1088#1072#1103' '#1083#1080#1089#1090#1072' ('#1089#1084')'
      WordWrap = True
    end
    object Label4: TLabel
      Left = 39
      Top = 68
      Width = 10
      Height = 13
      Caption = 'X:'
    end
    object Label5: TLabel
      Left = 40
      Top = 91
      Width = 10
      Height = 13
      Caption = 'Y:'
    end
    object ButtonShiftLeft: TButton
      Left = 3
      Top = 73
      Width = 33
      Height = 25
      Caption = '<'
      TabOrder = 0
      OnClick = ButtonShiftLeftClick
    end
    object ButtonShiftUp: TButton
      Left = 59
      Top = 38
      Width = 33
      Height = 24
      Caption = '^'
      TabOrder = 1
      OnClick = ButtonShiftUpClick
    end
    object ButtonShiftDown: TButton
      Left = 61
      Top = 112
      Width = 33
      Height = 25
      Caption = 'v'
      TabOrder = 2
      OnClick = ButtonShiftDownClick
    end
    object ButtonShiftRight: TButton
      Left = 116
      Top = 71
      Width = 33
      Height = 25
      Caption = '>'
      TabOrder = 3
      OnClick = ButtonShiftRightClick
    end
    object Edit_ShiftX: TEdit
      Left = 52
      Top = 63
      Width = 63
      Height = 21
      TabOrder = 4
      OnKeyDown = Edit_ShiftXKeyDown
    end
    object Edit_ShiftY: TEdit
      Left = 53
      Top = 86
      Width = 62
      Height = 21
      TabOrder = 5
      OnKeyDown = Edit_ShiftYKeyDown
    end
  end
  object BitBtnClose: TBitBtn
    Left = 355
    Top = 495
    Width = 94
    Height = 33
    Caption = #1042#1099#1093#1086#1076
    TabOrder = 7
    OnClick = BitBtnCloseClick
  end
  object RadioGroupProjectionMode: TRadioGroup
    Left = 6
    Top = 1
    Width = 153
    Height = 82
    Caption = 'Projection kind'
    ItemIndex = 0
    Items.Strings = (
      'Plan'
      'Vert. projection'
      'Ext. elevation'
      'Custom projection')
    TabOrder = 8
    OnClick = RadioGroupProjectionModeClick
  end
  object BitBtnRazvMirrow: TBitBtn
    Left = 94
    Top = 47
    Width = 35
    Height = 17
    Caption = '<->'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 9
    OnClick = BitBtnRazvMirrowClick
  end
  object GroupBox3: TGroupBox
    Left = 162
    Top = 2
    Width = 161
    Height = 81
    Caption = 'Angles'
    TabOrder = 10
    object Label6: TLabel
      Left = 8
      Top = 24
      Width = 91
      Height = 13
      Caption = #1043#1086#1088'. '#1087#1086#1074#1086#1088#1086#1090' ('#1060#1080')'
    end
    object Label7: TLabel
      Left = 8
      Top = 48
      Width = 101
      Height = 13
      Caption = #1042#1077#1088#1090'. '#1087#1086#1074#1086#1088#1086#1090'('#1058#1077#1090#1072')'
    end
    object PhiEdit: TEdit
      Left = 112
      Top = 16
      Width = 41
      Height = 21
      MaxLength = 4
      TabOrder = 0
      Text = '0'
      OnKeyDown = PhiEditKeyDown
    end
    object ThetaE: TEdit
      Left = 112
      Top = 40
      Width = 41
      Height = 21
      MaxLength = 4
      TabOrder = 1
      Text = '0'
      OnKeyDown = ThetaEKeyDown
    end
  end
  object PrintDialog1: TPrintDialog
    Left = 112
    Top = 96
  end
  object SaveDialog1: TSaveDialog
    DefaultExt = '.ps'
    Filter = 'PostScript (*.ps)|*.ps'
    Left = 72
    Top = 96
  end
end
