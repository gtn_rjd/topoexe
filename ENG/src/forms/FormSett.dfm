object SettForm: TSettForm
  Left = 98
  Top = 145
  Caption = 'ToPo - Settings'
  ClientHeight = 200
  ClientWidth = 240
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object VStep: TLabel
    Left = 8
    Top = 140
    Width = 161
    Height = 13
    Alignment = taRightJustify
    Caption = 'Vertical step, degrees'
  end
  object OK: TButton
    Left = 76
    Top = 167
    Width = 75
    Height = 25
    Caption = 'OK'
    TabOrder = 0
    OnClick = OKClick
  end
  object Cancel: TButton
    Left = 157
    Top = 167
    Width = 75
    Height = 25
    Caption = 'Cancel'
    TabOrder = 1
    OnClick = CancelClick
  end
  object Color: TGroupBox
    Left = 8
    Top = 8
    Width = 225
    Height = 121
    Caption = 'Color'
    TabOrder = 2
    object BackGround: TLabel
      Left = 8
      Top = 24
      Width = 23
      Height = 13
      Caption = 'Background'
    end
    object BgColor: TComboBox
      Left = 88
      Top = 21
      Width = 121
      Height = 21
      TabOrder = 0
      Text = 'Black'
      Items.Strings = (
        'Black'
        'White'
        'Silver'
        'Cream')
    end
    object WallsColor: TRadioGroup
      Left = 0
      Top = 48
      Width = 97
      Height = 65
      Caption = 'Walls'
      ItemIndex = 0
      Items.Strings = (
        'grey'
        'line color')
      TabOrder = 1
    end
  end
  object Language: TRadioGroup
    Left = 264
    Top = 24
    Width = 113
    Height = 73
    Caption = 'Language'
    ItemIndex = 0
    Items.Strings = (
      'English'
      'Russian')
    TabOrder = 3
    Visible = False
    OnClick = LanguageClick
  end
  object VertStep: TEdit
    Left = 183
    Top = 137
    Width = 49
    Height = 21
    TabOrder = 4
    Text = '2'
  end
end
