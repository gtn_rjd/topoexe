object TrackForm: TTrackForm
  Left = 0
  Top = 0
  Caption = #1042#1099#1073#1086#1088' '#1090#1088#1077#1082#1072
  ClientHeight = 350
  ClientWidth = 214
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 9
    Top = 5
    Width = 43
    Height = 13
    Caption = #1055#1080#1082#1077#1090#1099':'
  end
  object ListBox1: TListBox
    Left = 8
    Top = 24
    Width = 121
    Height = 97
    ItemHeight = 13
    TabOrder = 0
  end
  object Button1: TButton
    Left = 135
    Top = 24
    Width = 75
    Height = 25
    Caption = #1059#1076#1072#1083#1080#1090#1100
    TabOrder = 1
    OnClick = Button1Click
  end
  object GroupBox1: TGroupBox
    Left = 6
    Top = 127
    Width = 202
    Height = 122
    Caption = #1061#1072#1088#1088#1072#1082#1090#1077#1088#1080#1089#1090#1080#1082#1080' '#1090#1088#1077#1082#1072
    TabOrder = 2
    object Label2: TLabel
      Left = 11
      Top = 24
      Width = 38
      Height = 13
      Caption = #1044#1083#1080#1085#1085#1072
    end
    object Label3: TLabel
      Left = 11
      Top = 40
      Width = 107
      Height = 13
      Caption = #1057#1091#1084#1084#1072#1088#1085#1099#1081' '#1087#1077#1088#1077#1087#1072#1076':'
    end
    object Label4: TLabel
      Left = 11
      Top = 88
      Width = 116
      Height = 13
      Caption = #1055#1077#1088#1077#1084#1077#1097#1077#1085#1080#1077' '#1074' '#1087#1083#1072#1085#1077':'
    end
    object Label5: TLabel
      Left = 11
      Top = 56
      Width = 60
      Height = 13
      Caption = #1040#1084#1087#1083#1080#1090#1091#1076#1072':'
    end
    object Label6: TLabel
      Left = 11
      Top = 72
      Width = 84
      Height = 13
      Caption = #1044#1083#1080#1085#1085#1072' '#1074' '#1087#1083#1072#1085#1077':'
    end
    object Label7: TLabel
      Left = 185
      Top = 24
      Width = 6
      Height = 13
      Alignment = taRightJustify
      Caption = '0'
    end
    object Label8: TLabel
      Left = 185
      Top = 40
      Width = 6
      Height = 13
      Alignment = taRightJustify
      Caption = '0'
    end
    object Label9: TLabel
      Left = 134
      Top = 88
      Width = 57
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = '0'
    end
    object Label10: TLabel
      Left = 185
      Top = 72
      Width = 6
      Height = 13
      Alignment = taRightJustify
      Caption = '0'
    end
    object Label11: TLabel
      Left = 185
      Top = 56
      Width = 6
      Height = 13
      Alignment = taRightJustify
      Caption = '0'
    end
    object Label12: TLabel
      Left = 11
      Top = 103
      Width = 47
      Height = 13
      Caption = #1055#1080#1082#1077#1090#1086#1074':'
    end
    object Label13: TLabel
      Left = 134
      Top = 103
      Width = 57
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = '0'
    end
  end
  object GroupBox2: TGroupBox
    Left = 6
    Top = 255
    Width = 202
    Height = 90
    Caption = #1069#1082#1089#1087#1086#1088#1090' '#1090#1088#1077#1082#1072
    TabOrder = 3
    object Label14: TLabel
      Left = 11
      Top = 15
      Width = 59
      Height = 13
      Caption = #1048#1084#1103'  '#1090#1088#1077#1082#1072':'
    end
    object Button3: TButton
      Left = 6
      Top = 57
      Width = 100
      Height = 25
      Caption = #1069#1082#1089#1087#1086#1088#1090
      TabOrder = 0
      OnClick = Button3Click
    end
    object Edit2: TEdit
      Left = 6
      Top = 30
      Width = 185
      Height = 21
      TabOrder = 1
      Text = 'Edit1'
    end
  end
  object SaveDialog1: TSaveDialog
    DefaultExt = '.gpx'
    Filter = 'GPX|*.gpx'
    Title = 'Save track'
    Left = 152
    Top = 72
  end
end
