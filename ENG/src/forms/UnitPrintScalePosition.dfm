object FormPrintScalePosition: TFormPrintScalePosition
  Left = 395
  Top = 193
  BorderStyle = bsDialog
  Caption = #1052#1072#1089#1096#1090#1072#1073
  ClientHeight = 199
  ClientWidth = 404
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  OnActivate = FormActivate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 144
    Width = 404
    Height = 55
    Align = alBottom
    TabOrder = 0
    object BitBtnOK: TBitBtn
      Left = 234
      Top = 19
      Width = 75
      Height = 25
      Kind = bkOK
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BitBtnOKClick
    end
    object BitBtnCancel: TBitBtn
      Left = 322
      Top = 19
      Width = 75
      Height = 25
      Kind = bkCancel
      NumGlyphs = 2
      TabOrder = 1
      OnClick = BitBtnCancelClick
    end
  end
  object GroupBox1: TGroupBox
    Left = 8
    Top = 32
    Width = 345
    Height = 81
    Caption = #1052#1072#1089#1096#1090#1072#1073' '#1087#1077#1095#1072#1090#1080' '#1080#1083#1080' '#1101#1082#1089#1087#1086#1088#1090#1072
    TabOrder = 1
    object Label1: TLabel
      Left = 8
      Top = 40
      Width = 9
      Height = 13
      Caption = '1:'
    end
    object Label2: TLabel
      Left = 144
      Top = 40
      Width = 29
      Height = 13
      Caption = '1 '#1089#1084'='
    end
    object Label3: TLabel
      Left = 298
      Top = 39
      Width = 37
      Height = 13
      Caption = #1084#1077#1090#1088#1086#1074
    end
    object EditScale: TEdit
      Left = 24
      Top = 35
      Width = 105
      Height = 21
      TabOrder = 0
      OnChange = EditScaleChange
    end
    object EditPrintedScale: TEdit
      Left = 181
      Top = 35
      Width = 113
      Height = 21
      TabOrder = 1
      OnChange = EditPrintedScaleChange
    end
  end
end
