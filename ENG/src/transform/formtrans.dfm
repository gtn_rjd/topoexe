object TransForm: TTransForm
  Left = 655
  Top = 123
  VertScrollBar.Visible = False
  Caption = 'TOpO Builder'
  ClientHeight = 520
  ClientWidth = 581
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Icon.Data = {
    0000010001001010100000000000280100001600000028000000100000002000
    00000100040000000000C0000000000000000000000000000000000000000000
    0000000080000080000000808000800000008000800080800000C0C0C0008080
    80000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF000000
    0000000000000000000BB0000000000000BBBB00000000000BBBBBB000000000
    0BBBBBB0000000000BBBBBB0000000000BB00BB0000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    000000000000000000000000000000000000000000000000000000000000FE7F
    0000FC3F0000F81F0000F00F0000F00F0000F00F0000F00F0000F00F0000FE7F
    0000FE7F0000FE7F0000FE7F0000FE7F0000FE7F0000FE7F0000FE7F0000}
  OldCreateOrder = False
  OnCloseQuery = FormCloseQuery
  PixelsPerInch = 96
  TextHeight = 13
  object Memo1: TMemo
    Left = 0
    Top = 0
    Width = 581
    Height = 443
    Align = alClient
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'System'
    Font.Style = [fsBold]
    ParentColor = True
    ParentFont = False
    ReadOnly = True
    ScrollBars = ssBoth
    TabOrder = 0
    OnKeyDown = Memo1KeyDown
  end
  object Panel1: TPanel
    Left = 0
    Top = 443
    Width = 581
    Height = 77
    Align = alBottom
    TabOrder = 1
    DesignSize = (
      581
      77)
    object Label1: TLabel
      Left = 468
      Top = 61
      Width = 82
      Height = 13
      Anchors = [akLeft, akBottom]
      Caption = #1087#1088#1086#1089#1084#1086#1090#1088' '#1082#1086#1083#1077#1094
      Visible = False
    end
    object Button1: TButton
      Left = 12
      Top = 25
      Width = 75
      Height = 33
      Anchors = [akLeft, akBottom]
      Caption = #1042#1099#1093#1086#1076
      TabOrder = 0
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 92
      Top = 25
      Width = 77
      Height = 33
      Anchors = [akLeft, akBottom]
      Caption = #1055#1088#1072#1074#1080#1090#1100
      TabOrder = 1
      WordWrap = True
      OnClick = Button2Click
    end
    object BitBtn1: TBitBtn
      Left = 175
      Top = 25
      Width = 126
      Height = 33
      Anchors = [akLeft, akBottom]
      Caption = 'OK'
      TabOrder = 2
      OnClick = BitBtn1Click
    end
    object RadioGroup1: TRadioGroup
      Left = 308
      Top = 9
      Width = 153
      Height = 49
      Anchors = [akLeft, akBottom]
      ItemIndex = 0
      Items.Strings = (
        #1086#1073#1088#1072#1073#1072#1090#1099#1074#1072#1090#1100' '#1082#1086#1083#1100#1094#1072
        #1085#1077' '#1086#1073#1088#1072#1073#1072#1090#1099#1074#1072#1090#1100)
      TabOrder = 3
    end
    object BitBtn2: TBitBtn
      Left = 468
      Top = 33
      Width = 81
      Height = 25
      Anchors = [akLeft, akBottom]
      Caption = 'O'
      TabOrder = 4
      Visible = False
      OnClick = BitBtn2Click
    end
  end
end
