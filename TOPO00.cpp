//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include <tchar.h>
USEFORM("src\forms\About.cpp", AboutBox);
USEFORM("src\forms\Form3dExp.cpp", Exp3dForm);
USEFORM("src\forms\FormInsSurv.cpp", InsSurvForm);
USEFORM("src\forms\FORMINFO.CPP", InfoForm);
USEFORM("src\forms\FormHelp.cpp", HelpForm);
USEFORM("src\forms\FormEdit.cpp", EditForm);
USEFORM("src\forms\FormPrn.cpp", PrnForm);
USEFORM("src\transform\formtrans.cpp", TransForm);
USEFORM("src\forms\TOpO.cpp", MainForm1);
USEFORM("src\forms\FORMVIEW.CPP", ViewForm);
USEFORM("src\forms\FormTrack.cpp", TrackForm);
USEFORM("src\forms\FormSett.cpp", SettForm);
USEFORM("src\forms\UnitInsertNewPiket.cpp", FormInsertNewPiket);
USEFORM("src\forms\UnitPrintScalePosition.cpp", FormPrintScalePosition);
//---------------------------------------------------------------------------
int WINAPI _tWinMain(HINSTANCE inst, HINSTANCE, LPTSTR, int)
{
        try
        {
                 Application->Initialize();
                 Application->Title = "TOpO";
                 Application->CreateForm(__classid(TMainForm1), &MainForm1);
		Application->CreateForm(__classid(TAboutBox), &AboutBox);
		Application->CreateForm(__classid(THelpForm), &HelpForm);
		Application->CreateForm(__classid(TInfoForm), &InfoForm);
		Application->CreateForm(__classid(TEditForm), &EditForm);
		Application->CreateForm(__classid(TPrnForm), &PrnForm);
		Application->CreateForm(__classid(TInsSurvForm), &InsSurvForm);
		Application->CreateForm(__classid(TExp3dForm), &Exp3dForm);
		Application->CreateForm(__classid(TSettForm), &SettForm);
		Application->CreateForm(__classid(TTransForm), &TransForm);
		Application->CreateForm(__classid(TTrackForm), &TrackForm);
		Application->Run();
        }
        catch (Exception &exception)
        {
                 Application->ShowException(&exception);
        }
        return 0;
}
//---------------------------------------------------------------------------

