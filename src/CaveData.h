#pragma once

#include "CMTypes.h"
#include <vector>


// ������������ ���� ��� �������� �� ���� � OgreRenderer
#define OGRE_IMPORT_AXIS_X(__x__) (__x__)
#define OGRE_IMPORT_AXIS_Y(__y__) (__y__)
#define OGRE_IMPORT_AXIS_Z(__z__) (-__z__)

#define OGRE_EXPORT_AXIS_X(__x__) (__x__)
#define OGRE_EXPORT_AXIS_Y(__y__) (__y__)
#define OGRE_EXPORT_AXIS_Z(__z__) (-__z__)

using CM::FakeWallsMode;
using CM::WallsPropagateMode;
using CM::WallsShadowMode;
using CM::WallsBlowMode;
using CM::WallsTrianglesBlowMode;
using CM::WallsTrianglesBlowStrength;
using CM::WallsSurfaceMode;
using CM::WallsSegmentTriangulationMode;
using CM::AmbientLightStrength;
using CM::WallColoringMode;
using CM::RenderSimpleMode;
using CM::CaveViewPrefs;
using CM::V2;
using CM::V3;

enum P3DPriz {
    PRIZ_NONE ,
    PRIZ_BRANCH = 1,
    PRIZ_BEGIN = 2,
    PRIZ_SURFACE = 4, 
    PRIZ_SIPHON = 8, 
    PRIZ_ENTRY = 16, 
    PRIZ_RIVER = 32,     
    PRIZ_DUPLICATE = 64, 
    PRIZ_WALLS_FROM = 128, 
    PRIZ_Z_SURVEY = 256,
    PRIZ_Z_TURN = 512,   
    PRIZ_Z_SURVEY_FAKE = 1024, // ����� - �������������� � ���������� ���-��� ������
    PRIZ_ONLY_CONVEX_WALLS = 2048, //����������� ������������ ������������� �������� �����
	PRIZ_LR_NORMAL = 4096,
    PRIZ_NO_FAKE_WALLS = 8192
 
};
  
enum ColorNum {
    COLOR_BLACK = 0, 
    COLOR_BLUE = 1, 
    COLOR_GREEN = 2,   
    COLOR_AQUA = 3,
    COLOR_RED = 4,
    COLOR_PURPULE = 5,
    COLOR_SOFT_RED = 6,
    COLOR_LIGHT_GRAY = 7,
    COLOR_DARK_GRAY = 8,
    COLOR_PURE_BLUE = 9,
    COLOR_LIME = 10,     
    COLOR_KHAKI = 11,
    COLOR_RED_AGAIN = 12,
    COLOR_FUCHSIA = 13,     
    COLOR_YELLOW = 14,
    COLOR_WHITE = 15,
    COLOR_ORANGE = 16
};

struct P3D {
    P3D():
    p3did(++p3dIdCounter)
    { }
    
	V3 pos {0, 0, 0};
	long XY {0}; // �-���������
	P3DPriz priz {PRIZ_NONE} ; // ��. �������� �����
	unsigned char col {COLOR_GREEN}; // ����
	int left {0}; // ������
	int right {0};
	float sin_c {0}; // ���� ��� ������
	float cos_c {0};
	int up {0}; // ������
	int down {0};
	int vet {0}; // ����� �����
	int srv {0}; // ����� ������
	int line {0}; // ����� ������ � ����� ������
	int com {0}; // ����� �����������
	int met {0}; // ����� �����
    int p3did {0}; // ���������� id ������� P3D 
    static int p3dIdCounter;
};

struct W3D {
	V3 pos;
	// long XY;              // �-���������
	unsigned char type; // (lrud,s),Srv,line
   //	unsigned char col; // ����
	int srv = 0; // ����� ������
	int line = 0; // ����� ������ � ����� ������
	int met = 0; // ����� �����
	int pik = 0; // ����� ������� ������ � ������ ������
	int com = 0; // ����� �����������
	int col = 0;
	bool ignoreAt3d = false;
};

typedef std::vector<std::pair<int,int> > EquatesArrayType;
