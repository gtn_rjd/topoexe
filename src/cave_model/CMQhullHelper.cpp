#include "CMQhullHelper.h"


#include "libqhullcpp/Qhull.h" 
//#include "libqhullcpp/RboxPoints.h" 
#include "libqhullcpp/QhullFacetList.h"
#include "libqhullcpp/QhullFacetSet.h"
#include "libqhullcpp/QhullVertexSet.h"  
#include "libqhullcpp/QhullVertex.h"
#include "libqhullcpp/QhullPoint.h"
#include "libqhullcpp/QhullRidge.h"
//#include "libqhullcpp/QhullRidgeSet.h"
#include <type_traits>        
#include "wykobi_wrap.h"
#include "CMDebug.h"
#include "CMLog.h"           
#include "CMtext.h"       
#include "CMTypes.h"        
#include "CMAssertions.h"
#include "wykobi_algorithm.hpp"

using namespace orgQhull;
using namespace std;  
using namespace wykobi;

#include "CMLog.h"


void test () {
//    {
////        auto p3 = degenerate_point3d<float>();
////        if (p3 == degenerate_point3d<float>()) {
////            int i = 0;
////        }
//    }
//    {
//        auto p3 = make_plane(make_point(1.0, 1.0, -100.0), make_point(1.0, -1.0, -100.0), make_point(-1.0, 1.0, -100.0));
//        LOG(p3.normal.x << " " << p3.normal.y << " " << p3.normal.z << " " << p3.constant);
//        auto r3 = make_ray(make_point(0.0, 0.0, 0.0), make_vector(0.0, 0.0, -1.0));
//        auto intp = intersection_point(r3, p3);
//        LOG(intp.x << " " << intp.y << " " << intp.z);
//        
//    }
//   {   
////       V3 c = ( V3(1000, 0, -200) + V3(1000, 300, -0) + V3(1000, 2.236068010330200195f, 4.47213602066040039f))/3 - V3(1000, 0, 0);
////       c.normalise();
////       auto r3 = make_ray(make_point(1000.0f, 0.0f, -0.0f), make_vector<float>( c.x, c.y, c.z));  
//       auto r3 = make_ray(make_point(1000.0f, 0.0f, -0.0f), make_vector<float>( 0.0f, 0.839616578589404f, -0.543179529214627f));
//       auto t3 = make_triangle(make_point(0.0f, 0.0f, -0.0f), make_point(1000.0f, 0.0f, -200.0f), make_point(1000.0f, 300.0f, -0.0f));
//       if (intersect(r3, t3)) {
//           cout << "intersect";
//       } else {
//           cout << "not intersect";
//       }
//   }

   {
//Name	Value
//triangle	{ ????, { { 0, 0, -0 }, { 1000, 0, -200 }, { 1000, 300, -0 } } }
//ray	{ { 1000, 0, -0 }, { 0, 0.839616578589404, -0.543179529214627 } }



       T3 t3 = makeT3(V3(0, 0, 0), V3(1000, 0, -200), V3(1000, 300, 0));
       R3 r3 = makeR3(V3(1000, 0, 0), V3(1000,  0.8396165,  -0.5431795));
    //   P3 p3 = intersection_point(r3, t3);
       if (intersect(r3, t3)) {
           int i = 0;
       } else {
           int j = 0;
       }
   }
//    RboxPoints rbox;
//    rbox.append  appendRandomPoints("100");
//    PointCoordinates points;
//    points.setDimension(3);
//    points.append({0.0, 0.0, 0.0});
//    points.append({1.0, 0.0, 0.0});
//    points.append({0.0, 0.0, 1.0});
//    float points[] =
//    {0.0, 0.0, 0.0
//    ,1.0, 0.0, 0.0
//    ,0.0, 0.0, 1.0
//    ,0.0, 1.0, 0.0
//    ,0.0, 1.0, 1.0
//    ,1.0, 1.0, 0.0
//    ,1.0, 0.0, 1.0};
//    Qhull qhull("comment",
//                3,
//                7,
//                points,
//                "d Qbb QJ");
////                  "d Qbb Qt");
////    qhull.runQhull("", pints);
////    
//    QhullFacetList facets = qhull.facetList();
//    QhullFacet facet = facets.front();
//    QhullVertexSet vts = facet.vertices();
//    QhullVertex v = vts.front();
//    QhullPoint  p = v.point();
//    LOG(facets);
}

//bool trueIntersect(const L3& l3, const T3& t3) {
//    P3 pt = intersection_point(l3, t3);
//    return true;
//}     
    template<typename T >void strangelog(const T& t) {
        LOG(t);
    }
        
namespace CM {
    void debugDrawTriangle(V3 a, V3 b, V3 c) {     
        V3 ccc = (a + b + c)/3;

		V3 cc((rand()%255), (rand()%255), (rand()%255));
        cc = 2 * cc / (cc.x + cc.y + cc.z);
        CM::Color col (cc.x, cc.y, cc.z);
                           
        V3 va = a + (ccc - a) * 0.2;
        V3 vb = b + (ccc - b) * 0.2;
        V3 vc = c + (ccc - c) * 0.2;
            
		DEBUG_DRAW_C(va, vb, col); 
        DEBUG_DRAW_C(vb, vc, col); 
        DEBUG_DRAW_C(vc, va, col); 
    }

    void debugDrawTetraider(V3 a, V3 b, V3 c, V3 d) {     
        V3 center = a + b + c + d;
                 
        V3 shift =  center / 4 * 0.25 ;
        
        V3 cc((rand()%255), (rand()%255), (rand()%255)); 
        cc = 2 * cc / (cc.x + cc.y + cc.z);
        CM::Color col (cc.x, cc.y, cc.z);
                                  
        DEBUG_DRAW_C(a + shift, b + shift, col); 
        DEBUG_DRAW_C(a + shift, c + shift, col);
        DEBUG_DRAW_C(a + shift, d + shift, col);
                
        DEBUG_DRAW_C(b + shift, c + shift, col);
        DEBUG_DRAW_C(b + shift, d + shift, col);

        DEBUG_DRAW_C(c + shift, d + shift, col);

    }

    void CM::makeCloudTriangulation(const std::vector<const V3*>& points, std::list<DelaunayTriangle>& output) {
        output.clear();

        std::vector<V3> pontsValues(points.size());     
        auto transFunc = [](const V3* v){ return *v; };
		std::transform(points.begin(), points.end(), pontsValues.begin(), transFunc);
        static_assert(is_same<realT, Ogre::Real>::value, "Qhull::realT type is not same as Ogre::Real");
        static_assert(3 * sizeof(float) == sizeof(decltype(pontsValues)::value_type), "Ogre::Vector3 size is not same as 3 Qhull::realT");
                
        Qhull qhull("",
                    3,
                    pontsValues.size(),
                    (float*)pontsValues.data(),
                    "d Qbb Qt");
        QhullFacetList facets = qhull.facetList();
        strangelog(facets); 
        for (const QhullFacet& facet : facets) {                 
            if (!facet.isGood()) continue;
//            facet.isTopOrient
            QhullVertexSet vts = facet.vertices();

            if (vts[0].point().id() >= points.size()) continue;
            if (vts[1].point().id() >= points.size()) continue;
            if (vts[2].point().id() >= points.size()) continue;
            if (vts[3].point().id() >= points.size()) continue;
                
            output.push_back({points[vts[0].point().id()],
                              points[vts[1].point().id()],
                              points[vts[2].point().id()]});
                              
            output.push_back({points[vts[0].point().id()],
                              points[vts[2].point().id()],
                              points[vts[3].point().id()]});

            output.push_back({points[vts[0].point().id()],
                              points[vts[1].point().id()],
                              points[vts[3].point().id()]});

            output.push_back({points[vts[1].point().id()],
                              points[vts[2].point().id()],
                              points[vts[3].point().id()]});

            //debugDrawTetraider(*points[vts[0].point().id()], *points[vts[1].point().id()], *points[vts[2].point().id()], *points[vts[3].point().id()]);
            
        }

        auto sortFunc = [](const DelaunayTriangle& tr1, const DelaunayTriangle& tr2) {
            if (tr1.a != tr2.a) return tr1.a < tr2.a;
            else if (tr1.b != tr2.b) return tr1.b < tr2.b;
            else if (tr1.c != tr2.c) return tr1.c < tr2.c;
            else return false;
        };

        output.sort(sortFunc);

        auto compareFunc = [](const DelaunayTriangle& tr1, const DelaunayTriangle& tr2) {
           return tr1.hasVertice(tr2.a) && tr1.hasVertice(tr2.b) && tr1.hasVertice(tr2.c);
        };      
        output.erase(unique(output.begin(), output.end(), compareFunc), output.end());
    }
        
    void makeHullFromCloudTriangulation(const V3& surveyPoint, const std::vector<const V3*>& delaunayVerts, std::list<DelaunayTriangle>& triangels, const std::set<const V3*>& tabooNeigbors) {
        LOG("\t\tmake hull ");         
        P3 surveyP3 = V3toP3(surveyPoint);
        

        auto trgIt = triangels.begin();        
        while(trgIt != triangels.end()) {
            const DelaunayTriangle& trg = *trgIt;
            T3 t3 = makeT3(*trg.a, *trg.b, *trg.c);

            // ������� ��� ����� ������������ ������ �� ������ � �������  
            bool removeTriangle = false;
            for (const V3* v : delaunayVerts) {
                if (trg.hasVertice(v)) continue;  
                R3 r3 = makeR3(surveyPoint, *v);
                if (intersect(r3, t3)) {
                    removeTriangle = true;
                    break;
                }
            }   

            // ������� ��� ����� ���������� ����� 0
//            if (!removeTriangle) {
//                if (P3toV3(closest_point_on_triangle_from_point(t3, surveyP3)).distance(surveyPoint) < 0.000001 * PointsInMeter) { 
//                    removeTriangle = true;                    
//                }
//            }
            
            if (removeTriangle) {     
                LOG("\t\t\tS remove " << *trgIt);  
                trgIt = triangels.erase(trgIt); 
            }
            else trgIt++;
        }
        
        // ������� ��� ����� ���������� ��� �������� ������
        trgIt = triangels.begin();
        while(trgIt != triangels.end()) { 
            const DelaunayTriangle& trg = *trgIt;
            if (tabooNeigbors.count(trg.a) + tabooNeigbors.count(trg.b) + tabooNeigbors.count(trg.c) > 1) {                
                LOG("\t\t\tT remove " << *trgIt);  
                trgIt = triangels.erase(trgIt); 
            } else {
                trgIt++; 
            }
        }

        // ������� ��� �� ������� �����
        trgIt = triangels.begin();
        while(trgIt != triangels.end()) {
            const DelaunayTriangle& trg = *trgIt;
            V3 trgCenter = trg.getCenter();
            R3 r3 = makeR3(trgCenter, trgCenter + (trgCenter - surveyPoint));
            bool removeTriangle = false;
            for (const DelaunayTriangle& trg2 : triangels) {
                if (&trg == &trg2) continue;
                T3 t3 = makeT3(*trg2.a, *trg2.b, *trg2.c);
                plane3d p3 = make_plane(t3);
                
                P3 planePt = intersection_point(r3, p3);
                if (planePt.x == infinity<float>()) continue;
                
                P3 triagPt = closest_point_on_triangle_from_point(t3, planePt);
                if (triagPt.x == infinity<float>()) continue;
                if (P3toV3(planePt).distance(P3toV3(triagPt)) < 0.0001) {  
                    removeTriangle = true;
                    break;   
                }
            } 

            if (removeTriangle) {
                LOG("\t\t\tI remove " << *trgIt);  
                trgIt = triangels.erase(trgIt); 
            } else trgIt++;
        }
    }

	void debugDrawTriangles(const std::list<DelaunayTriangle>& trgs, V3 zero) {
		for(const auto& trg : trgs) {
//			debugDrawTetraider(*trg.a, *trg.b, *trg.c, zero);
			debugDrawTriangle(*trg.a, *trg.b, *trg.c);
//			debugDrawTriangle(zero, *trg.b, *trg.c);
//			debugDrawTriangle(*trg.a, zero, *trg.c);
//			debugDrawTriangle(*trg.a, *trg.b, zero);
        }
    }

    void makePointsUniq(std::vector<const V3*>& points) {
        auto sortFunc = [](const V3* w1, const V3* w2) {
            if (w1->x != w2->x) return w1->x < w2->x;
            else if (w1->y != w2->y) return w1->y < w2->y;
            else if (w1->z != w2->z) return w1->z < w2->z;  
            else return false;
        };
        std::sort(points.begin(), points.end(), sortFunc);

        auto uniqFunc = [](const V3* w1, const V3* w2) {
            return w1->distance(*w2) < FLT_MIN * 100;
        };
        auto it = std::unique(points.begin(), points.end(), uniqFunc);
        points.resize(points.size() - (points.end() - it));        
    }

    void makePointsUniq(std::vector<V3>& points) {
        auto sortFunc = [](const V3& w1, const V3& w2) {
            if (w1.x != w2.x) return w1.x < w2.x;
            else if (w1.y != w2.y) return w1.y < w2.y;
            else if (w1.z != w2.z) return w1.z < w2.z;  
            else return false;
        };
        std::sort(points.begin(), points.end(), sortFunc);

        auto uniqFunc = [](const V3& w1, const V3& w2) {
            return w1.distance(w2) < FLT_MIN * 100;
        };
        auto it = std::unique(points.begin(), points.end(), uniqFunc);
        points.resize(points.size() - (points.end() - it));        
    }
 
    bool isConvexHullIncludePoint(int targetPointIdx, const std::vector<V3>& points) {
        std::vector<int> hullPoints = getConvexHullPoints(points);
        return find(hullPoints.begin(), hullPoints.end(), targetPointIdx) != hullPoints.end();
    }

	std::vector<int> getConvexHullPoints( const std::vector<V3>& points) {
		static_assert(is_same<realT, Ogre::Real>::value, "Qhull::realT type is not same as Ogre::Real");
        static_assert(3 * sizeof(float) == sizeof(remove_reference<decltype(points)>::type::value_type), "Ogre::Vector3 size is not same as 3 Qhull::realT");

        std::vector<int> res;
                
        Qhull qhull("",
                    3,
                    points.size(),
                    (float*)points.data(),
                    " Qt Qc");

        QhullFacetList facets = qhull.facetList();
        //strangelog(qhull.outputQhull());
        for (const QhullFacet& facet : facets) {                 
            if (facet.isGood()) {
            
                QhullPointSet pts = facet.coplanarPoints(); 
                for(const QhullPoint& p : pts) {
                    res.push_back(p.id());
                }
                QhullVertexSet vts = facet.vertices(); 
                for(const QhullVertex& v : vts) {
                    res.push_back(v.point().id());
                }
//                debugDrawTriangle(points[vts[0].point().id()], points[vts[1].point().id()], points[vts[2].point().id()]);
            }
        }

        std::sort(res.begin(), res.end());
        res.resize(std::unique(res.begin(), res.end()) - res.begin());
        
        return res;
    }

	void isFacetXYCorner(const QhullVertex& v0, const QhullVertex& v1) {
	 //	QhullVertexSet vts = facet.vertices();
	}

	std::vector<std::pair<int, int>> makeHullXYProjection(V3 zero, const std::vector<V3>& points) {

		LOG("\t\tmakeHullXYProjection\n " << points);
		if (points.empty() || points.size() == 1) {
			return {};
		} else if (points.size() == 2) {
			return {{0, 1}};
		} else if (points.size() == 3) {
			return {{0, 1}, {1, 2}};
		} else {
			std::vector<std::pair<int, int>> result;
			
			std::vector<V2> spherePoints(points.size());
			std::transform(points.begin(), points.end(), spherePoints.begin(), &xyV3);

			V2 xyZero = xyV3(zero);
			
			static_assert(is_same<realT, Ogre::Real>::value, "Qhull::realT type is not same as Ogre::Real");
			static_assert(2 * sizeof(realT) == sizeof(decltype(spherePoints)::value_type), "Ogre::Vector3 size is not same as 3 Qhull::realT");

			try {
				Qhull qhull("", 2, spherePoints.size(), (float*)spherePoints.data(), " Qt Qc");
				QhullFacetList facets = qhull.facetList();

				strangelog(facets);
				std::set<ridgeT*> seenRidges;
				for (const QhullFacet& facet : facets) {
					if (facet.isGood()) {
						qh_makeridges(qhull.qh(), facet.getFacetT());
						QhullRidgeSet ridges = facet.ridges();
						for (const QhullRidge& ridge : ridges) {
							if (seenRidges.count(ridge.getRidgeT()) == 0) {
								LOG(ridge.vertices().size());
								strangelog(ridge.vertices());
								AssertAction(ridge.vertices().size() == 2, continue);
								AssertAction(ridge.bottomFacet().isValid(), continue);
								AssertAction(ridge.topFacet().isValid(), continue);

								QhullPoint f1qp = ridge.topFacet().getCenter();
								QhullPoint f2qp = ridge.bottomFacet().getCenter();
								QhullPoint aqp = ridge.vertices()[0].point();
								QhullPoint bqp = ridge.vertices()[1].point();

								V2 f1(f1qp.coordinates()[0], f1qp.coordinates()[1]);
								V2 f2(f2qp.coordinates()[0], f2qp.coordinates()[1]);
								V2 a(aqp.coordinates()[0], aqp.coordinates()[1]);
								V2 b(bqp.coordinates()[0], bqp.coordinates()[1]);

								if(((b-a).dotProduct(f1-a) > 0) == ((b-a).dotProduct(f2-a) > 0)) {
									int idx1 = ridge.vertices()[0].point().id();
									int idx2 = ridge.vertices()[1].point().id();
									if ((xyV3(points[idx1]) - xyZero).dotProduct(xyV3(points[idx2]) - xyZero) < 0 ) {
										std::swap(idx1, idx2); 
									}
									result.push_back(make_pair(idx1, idx2));			
								}
									
								seenRidges.insert(ridge.getRidgeT());	
							}
						}
					}
				}

			} catch (...) {

			}

			LOG("\t\t " << result);
			return result;
		}
	}

	std::vector<int> makeHullXYProjectionByWykobi(V3 zero, const std::vector<V3>& points) {
		LOG("\t\tmakeHullXYProjectionByWykobi\n " << points);

		std::vector<P2> spherePoints(points.size());
		auto p2FtomV3Func = [](const V3& v3) {
			return make_point(v3.x, v3.y);
        };
		std::transform(points.begin(), points.end(), spherePoints.begin(), p2FtomV3Func);
		//LOG("\t\tspherePoints\n " << spherePoints);

		std::vector<P2> hull;
		wykobi::algorithm::convex_hull_graham_scan<P2>(spherePoints.begin(), spherePoints.end(), std::back_inserter(hull));

		//LOG("\t\thull\n " << hull);

		Assert(spherePoints.size() == points.size());
		std::vector<int> res;
		for(const P2& p : hull) {
			//LOG("\t\tp\n " << p);
			auto findRes = std::find(spherePoints.begin(), spherePoints.end(), p);
			AssertAction(findRes != spherePoints.end(), continue);
			res.push_back(std::distance(spherePoints.begin(), findRes));
			findRes->x = infinity<float>();
			findRes->y = infinity<float>();
		}

		LOG("\t\t\t " << res);
        return res;
	}

	void makeDelaunaySphereTriangulation(V3 zero, const std::vector<const V3*>& points, std::list<DelaunayTriangle>& output, bool debug) {
		if (points.size() <= 4) {
            if (points.size() >= 3) {
                output.push_back({points[0],
                                  points[1],
                                  points[2]});   

                if (points.size() == 4) {
                    output.push_back({points[3],
                                      points[1],
                                      points[2]});    
                    output.push_back({points[0],
                                      points[3],
                                      points[2]});   
                    output.push_back({points[0],
                                      points[1],
                                      points[3]});   
				}
            }       
        } else {
            std::vector<V3> pointsValues(points.size());
            auto transFunc = [&zero](const V3* v){ 
                return (*v - zero).normalisedCopy() * 100;     
            };               
            std::transform(points.begin(), points.end(), pointsValues.begin(), transFunc);

			static_assert(is_same<realT, Ogre::Real>::value, "Qhull::realT type is not same as Ogre::Real");
            static_assert(3 * sizeof(realT) == sizeof(decltype(pointsValues)::value_type), "Ogre::Vector3 size is not same as 3 Qhull::realT");

            LOG("\t\ttriangulate sphere");
            LOG(pointsValues);

//			for (const auto& v : pointsValues) {
//				DEBUG_DRAW(zero + V3(1,1,1), zero + v + V3(1,1,1));    
//			}
			
            try {
						 
				Qhull qhull("", 3, pointsValues.size(), (float*)pointsValues.data(), "Gridges Qt Qc");
				QhullFacetList facets = qhull.facetList();
				strangelog(facets);
				for (const QhullFacet& facet : facets) {
					if (facet.isGood()) {
						QhullVertexSet vts = facet.vertices();
						if (facet.coplanarPoints().size() == 0) {  
							output.push_back({points[vts[0].point().id()],
											  points[vts[1].point().id()],
											  points[vts[2].point().id()]});

						} else {
							Assert(facet.coplanarPoints().size() == 1);
							int addpId = facet.coplanarPoints()[0].id();    
							
							output.push_back({points[addpId],
											  points[vts[1].point().id()],
											  points[vts[2].point().id()]});  
											  
							output.push_back({points[vts[0].point().id()],
											  points[addpId],
											  points[vts[2].point().id()]});
											  
							output.push_back({points[vts[0].point().id()],
											  points[vts[1].point().id()],
											  points[addpId]});
						}
						
						if (debug) {
				//                    debugDrawTriangle(zero + pointsValues[vts[0].point().id()] / 1 ,
				//                                      zero + pointsValues[vts[1].point().id()] / 1,
				//                                      zero + pointsValues[vts[2].point().id()] / 1);
							debugDrawTriangle(*points[vts[0].point().id()] / 1 ,
											  *points[vts[1].point().id()] / 1,
											  *points[vts[2].point().id()] / 1);
						}
					}
				}

			} catch (...) {
				auto p = make_plane(V3toP3(*points[0]), V3toP3(*points[1]), V3toP3(*points[2]));
				bool allOnPlane = true;
				for (int i = 3; i < points.size(); i++) {
					if (distance(V3toP3(*points[i]), p) > HundredthOfMeter) {
					    allOnPlane = false;
						break;
                    }
                }
				
				Assert(allOnPlane);
			}
        }
    }

	void makeConvexHullTriangulation(V3 shift, const std::vector<const V3*>& points, std::list<DelaunayTriangle>& output) {
		output.clear();

		std::vector<V3> pointsValues(points.size());
		auto transFunc = [&shift](const V3* v){ return *v - shift; };
        std::transform(points.begin(), points.end(), pointsValues.begin(), transFunc);

		static_assert(is_same<realT, Ogre::Real>::value, "Qhull::realT type is not same as Ogre::Real");
		static_assert(3 * sizeof(realT) == sizeof(remove_reference<decltype(pointsValues)>::type::value_type), "Ogre::Vector3 size is not same as 3 Qhull::realT");

//		LOG(points);
//		LOG(pointsValues);

		try {
			Qhull qhull("", 3, pointsValues.size(), (float*)pointsValues.data(), " QJ");

			QhullFacetList facets = qhull.facetList();
			strangelog(facets);

			for (const QhullFacet& facet : facets) {
				if (facet.isGood()) {
					QhullVertexSet vts = facet.vertices();
					output.push_back({points.at(vts[0].point().id()),
									  points.at(vts[1].point().id()),
									  points.at(vts[2].point().id())});

		//				debugDrawTriangle(zero + pointsValues[vts[0].point().id()],
		//								  zero + pointsValues[vts[1].point().id()],
		//								  zero + pointsValues[vts[2].point().id()]);
		//				Assert(facet.coplanarPoints().size() == 0);
				}
			}
		} catch (const QhullError& err) {
			LOG("makeConvexHullTriangulation: error " << err.what());
			Assert(false);
		} catch (...) {
			LOG("makeConvexHullTriangulation unknown error");
			Assert(false);
		}
	}

	void makeDelaunayStrainHullTriangulation(V3 zero, const std::vector<const V3*>& points, std::list<DelaunayTriangle>& output) {
		std::vector<V3> pointsValues(points.size());
		auto transFunc = [&zero](const V3* v){ return *v - zero; };
		std::transform(points.begin(), points.end(), pointsValues.begin(), transFunc);

		static_assert(is_same<realT, Ogre::Real>::value, "Qhull::realT type is not same as Ogre::Real");
		static_assert(3 * sizeof(realT) == sizeof(remove_reference<decltype(pointsValues)>::type::value_type), "Ogre::Vector3 size is not same as 3 Qhull::realT");

        Qhull qhull("", 3, pointsValues.size(), (float*)pointsValues.data(), " Qt Qc");

        QhullFacetList facets = qhull.facetList();
        strangelog(facets);
                   
        std::set<int> innerVerts;
        int counter = 0;
        std::generate_n(std::inserter(innerVerts, innerVerts.end()), points.size(), [&counter]() { return counter++; });

        for (const QhullFacet& facet : facets) {                 
            if (facet.isGood()) {
                QhullVertexSet vts = facet.vertices(); 
                for(const QhullVertex& v : vts) {
                    innerVerts.erase(v.point().id());
                }        
            }
        }
        
        Qhull qhullsec;
        if (false) {//!innerVerts.empty()) {
            for (int idx : innerVerts) {
                R3 r3 = makeR3(V3(0, 0, 0), pointsValues[idx]);
                for (const QhullFacet& facet : facets) {                 
                    if (facet.isGood()) {
                        QhullVertexSet vts = facet.vertices();
                        T3 t3 = makeT3(pointsValues[vts[0].point().id()],
                                       pointsValues[vts[1].point().id()],
                                       pointsValues[vts[2].point().id()]);
                        P3 p = intersection_point(r3, t3);
                        if (p.x != infinity<float>()) {
                            pointsValues[idx] = P3toV3(p) * 1.1;   
                            break;     
                        }
                    }
                }
            } 

            qhullsec.runQhull("", 3, pointsValues.size(), (float*)pointsValues.data(), " Qt Qc");
            facets = qhullsec.facetList();  
            strangelog(facets);
        }

        for (const QhullFacet& facet : facets) {                 
            if (facet.isGood()) {     
                QhullVertexSet vts = facet.vertices(); 
                output.push_back({points.at(vts[0].point().id()),
                                  points.at(vts[1].point().id()),
                                  points.at(vts[2].point().id())});

//				debugDrawTriangle(zero + pointsValues[vts[0].point().id()],
//								  zero + pointsValues[vts[1].point().id()],
//								  zero + pointsValues[vts[2].point().id()]);
//				Assert(facet.coplanarPoints().size() == 0);
            }
        }
    }

        
        
//        std::vector<int> hullPoints = getConvexHullPoints(points);
//
//        if (hullPoints.size() < points.size()) {
//            set<int> hullPointsSet(hullPoints.begin(), hullPoints.end());
//            for (int i = 0; i < points.size(); i++) {
//                if (hullPointsSet.count) {
//                
//                }
//            }
//        }

    
    bool isCoplanar(const std::vector<V3>& points) {
        for (int i = 0; i < ((int)points.size())-3; i++) {
            if (!robust_coplanar(V3toP3(points[i]),
                                 V3toP3(points[i+1]),
                                 V3toP3(points[i+2]),
                                 V3toP3(points[i+3]),
                                 0.1f)) {
                return false;            
            }
        }
        return true;
    }
    std::set<const V3*> removeFacesWithTabooNeigbors(std::list<DelaunayTriangle>& triangels, const std::set<const V3*>& tabooNeigbors) {
        std::set<const V3*> detectedNeigbors;
        // ������� ��� ����� ���������� ��� �������� ������
        auto trgIt = triangels.begin();
        while(trgIt != triangels.end()) { 
            const DelaunayTriangle& trg = *trgIt;
            bool a = tabooNeigbors.count(trg.a);
            bool b = tabooNeigbors.count(trg.b);
            bool c = tabooNeigbors.count(trg.c);
            if (a + b + c > 1) {                
                if (a) detectedNeigbors.insert(trg.a); 
                if (b) detectedNeigbors.insert(trg.b); 
                if (c) detectedNeigbors.insert(trg.c); 
                trgIt = triangels.erase(trgIt); 
            } else {
                trgIt++; 
            }
        }
        return detectedNeigbors;
	}

	std::vector<int> makeConvexHull2d (const std::vector<V2>& points) {
		if (points.size() == 3) {
			return {0, 1, 2};
		} else if (points.size() == 2) {
			return {0, 1};
		} else if (points.size() == 1) {
			return {};
		}

		static_assert(is_same<realT, Ogre::Real>::value, "Qhull::realT type is not same as Ogre::Real");
		static_assert(2 * sizeof(float) == sizeof(remove_reference<decltype(points)>::type::value_type), "Ogre::Vector2 size is not same as  Qhull::realT");

		LOG(points);

        Qhull qhull("",
					2,
					points.size(),
                    (float*)points.data(),
					" QJ Fx ");
		QhullFacetList facets = qhull.facetList();
		strangelog(facets);

		std::vector<int> result;

		if (facets.count() >= 3) {
			QhullFacet firstFacet = facets.front();
			QhullFacet facet = firstFacet;
			int infinieLoopCounter = points.size() * 2;
			do {
				if (facet.isTopOrient()) {
					result.push_back(facet.vertices()[0].point().id());
					facet = facet.neighborFacets()[0];
				} else {
					result.push_back(facet.vertices()[1].point().id());
					facet = facet.neighborFacets()[1];
				}
				infinieLoopCounter--;
			} while(firstFacet != facet && infinieLoopCounter > 0);
		}

		return result;
	}

	void makeEdgesFromTriags(std::list<DelaunayTriangle>& triags, std::map<SortablePair<const V3*>, DelaunayEdge>& edges) {
		auto addEdge = [&](const V3* a, const V3* b, DelaunayTriangle* tr) {
			auto it = edges.find(SortablePair<const V3*>(a, b));
			if (it == edges.end()) {
				it = edges.emplace_hint(it, SortablePair<const V3*>(a, b), DelaunayEdge(a, b));
			} else {
				AssertReturn(!it->second.tr2, return);
			}
			AssertReturn(it != edges.end(), return);
			it->second.addTriag(tr);
		};

		for (auto& tr : triags) {
			addEdge(tr.a, tr.b, &tr);
			addEdge(tr.b, tr.c, &tr);
			addEdge(tr.c, tr.a, &tr);
		}

		for (const auto& edge : edges) {
			AssertReturn(edge.second.tr1, return);
			AssertReturn(edge.second.tr2, return);
			AssertReturn(edge.second.a, return);
			AssertReturn(edge.second.b, return);
			AssertReturn(edge.second.c0, return);
			AssertReturn(edge.second.c1, return);
		}
//		for (i = 0; i < 10; i++) {
//
//		}
	}

	void postprocessTriags(V3 zero, std::list<DelaunayTriangle>& triags) {
		static int numAccepted = 0;
		static int numTotal = 0;
		numTotal++;
		
		if (!isPointInHull(zero, triags)) return;
		numAccepted++;
//		V3 shift(1000,1000,-500);
//		V3 A = Vector3(174.762, 302.693, -18.3175) + shift; //s
//
//		V3 B = Vector3(-651.894, -48.1837, -165.376) + shift;
//		V3 C = Vector3(145.221, 317.023, -70.7076) + shift;
//
//		V3 D = Vector3(185.229, 322.646, -85.0197) + shift;
//		V3 E = Vector3(300.204, 14.3144, -105.626) + shift;//s
//                                                            
//		V3 F = Vector3(-534.792, -1146.8, -339.051) + shift;
//		V3 G = Vector3(263.561, -42.042, -89.2795) + shift;//s
//		
//		V3 O = V3::ZERO + shift;
//
//		//DEBUG_DRAW_C(O + (O-A) * 100, A + (A-O) * 100, Color::Green);
//		DEBUG_DRAW_C(O, A, Color::Green);
//		DEBUG_DRAW_C(A, B, Color::Blue);
//		DEBUG_DRAW_C(A, C, Color::Blue);
//		DEBUG_DRAW_C(A, D, Color::Blue);
//		DEBUG_DRAW_C(A, E, Color::Blue); 
//		DEBUG_DRAW_C(A, F, Color::Blue);
//		DEBUG_DRAW_C(A, G, Color::Blue);
//
//		DEBUG_DRAW_C(B, C, Color::Red);
//		DEBUG_DRAW_C(D, E, Color::Red);
//		DEBUG_DRAW_C(C, D, Color::Red);  
//		DEBUG_DRAW_C(B, F, Color::Red); 
//		DEBUG_DRAW_C(G, E, Color::Red); 
//		DEBUG_DRAW_C(G, F, Color::Red);
//
////		DEBUG_DRAW_C(E, C, Color::Red);
////		DEBUG_DRAW_C(B, C, Color::Red);
////		DEBUG_DRAW_C(E, D, Color::Red);
////		DEBUG_DRAW_C(D, B, Color::Red);
//
//		V4 zeroInABC0C1 = toBaricentric(O, A, E, C, D);
//		if (zeroInABC0C1.x >= 0 && zeroInABC0C1.y >= 0 && zeroInABC0C1.z >= 0 && zeroInABC0C1.w >= 0) {
//			return;
//		}
//
//		return;

//		float ra = 0.0f;
//		float dstDa = 2.864814;
//		V3 O(1/3.0f, 1/3.0f, 1/3.0f);
//		V3 A;
//		V3 B;
//		V3 C;
//		float ratio = 0;
//		do {
//			ratio += 0.0001;
//			A = V3::UNIT_X * ratio + O * (1.0f - ratio);
//			B = V3::UNIT_Y * ratio + O * (1.0f - ratio);
//			C = V3::UNIT_Z * ratio + O * (1.0f - ratio);
//
//            ra = 0.0f;
//			ra += A.angleBetween(B).valueRadians();
//			ra += B.angleBetween(C).valueRadians();
//			ra += C.angleBetween(A).valueRadians();
//		} while(ra < dstDa);
//
//		float sa = 0;
//		sa += solidAngle(V3::ZERO, A, B, O);
//		sa += solidAngle(V3::ZERO, B, C, O);
//		sa += solidAngle(V3::ZERO, C, A, O);
//
//		float n = 3;
//		double r = O.distance(A);
//		double h = O.distance(V3::ZERO);
//		float san = 2 * M_PI - 2 * n * atan(tan(M_PI/n)/sqrt(1+(r*r/(h*h))));
//
//        float ran = pyramidRoundAngle(san, n);
//        LOG(ran	);
//		int i = 0;
//        float ra =
//		V3 p = (V3(1, 0, 0) + V3(0, 1, 0) + V3(0, 0, 1)) / 3.0f;
//		float r = p.distance(V3(1, 0, 0));
//		float h = p.distance(V3(0, 0, 0));
//
//		float n = 3;
//		float sa = 2 * M_PI - 2 * n * atan(tan(M_PI/n) / sqrt(1 + (r*r)/(h*h)));
//		LOG(sa);
//		double ra = pyramidRoundAngle(2*M_PI/4, 3);
//		double oac = atan(sqrt(ra));

//		LOG(ra);
//		LOG(oac);
//
//		float rb = V3(1, 0, 0).angleBetween(p).valueRadians();
//		LOG(rb);

//		return;

//		int n = 400;
//		float sa = 0;
//		for (int i = 0; i < n; i++) {
//			float a  =  i   * 2 * M_PI / n;
//			float na = (i+1) * 2 * M_PI / n;
//			LOG("a: " << a / M_PI);
//			LOG("na: " << na / M_PI);
//
//			V3 v0(sin(a), cos(a), 0.5);
//			V3 v1(sin(na), cos(na), 0.5);
//
//			float da = v0.angleBetween(v1).valueRadians();
//			LOG("da: " << da / M_PI);
//
//			sa += da;
//			LOG("sa: " << sa / M_PI);
//		}
//
//		float ssa = 2 * M_PI * 1 / V3(1, 0, 0.5).length();
//		LOG("ssa: " << ssa / M_PI);

		// ���� ������� ������ = 2 * M_PI * ������ ��������� / ������ ������
		// ���� �������� n ������� ��������:
		// tana2 = (ctan((tetta/2 - M_PI)/n)*tan(M_PI/2))^2 - 1
		// a = 2 * n * asin(sqrt(tana2) * sin(M_PI/n)/sqrt(tana2+1))

//		return;
	   // return;

//		V3 a = {0, 0, 0};
//		V3 b = {-100, 50, 18};
//		V3 c = {-100, -50, 0};
//		V3 d = {100, 0, 0};
//		V3 e = {30, 0, -100};
//		V3 z = {10, 0, -10};
//
//		std::vector<std::vector<V3>> sap {
//			{a, b, d},
//			{a, c, d},
//			{a, b, e},
//			{a, c, e},
//		};
//
//		float s = 0;
//		for (const auto& v : sap) {
//			s += solidAngle(v[0], v[1], v[2], z);
//		}
//
//		LOG(s);

		if (triags.empty()) return;

		std::map<V3Pair, DelaunayEdge> edges;
		makeEdgesFromTriags(triags, edges);

		std::map<const V3*, float> verticeSolidAngleDeviation;
		for (const auto& tr : triags) {
			verticeSolidAngleDeviation[tr.a] += solidAngle(*tr.a, *tr.b, *tr.c, zero);
			verticeSolidAngleDeviation[tr.b] += solidAngle(*tr.b, *tr.a, *tr.c, zero);
			verticeSolidAngleDeviation[tr.c] += solidAngle(*tr.c, *tr.a, *tr.b, zero);
		}
		for (auto& vsa : verticeSolidAngleDeviation) {
			vsa.second -= 2 * M_PI;
		}

//		{
			std::map<const V3*, float> verticeRoundAngleDeviation;
			std::map<const V3*, float> verticeRoundAngle;
			std::map<const V3*, int> verticeEdgesNum;
			for (const auto& tr : triags) {
				verticeRoundAngle[tr.a] += (*tr.a - *tr.b).angleBetween(*tr.a - *tr.c).valueRadians();
				verticeRoundAngle[tr.b] += (*tr.b - *tr.a).angleBetween(*tr.b - *tr.c).valueRadians();
				verticeRoundAngle[tr.c] += (*tr.c - *tr.a).angleBetween(*tr.c - *tr.b).valueRadians();

				verticeEdgesNum[tr.a]++;
				verticeEdgesNum[tr.b]++;
				verticeEdgesNum[tr.c]++;
			}
			for (auto& vra : verticeRoundAngle) {
				float sa = verticeSolidAngleDeviation[vra.first] + 2 * M_PI;
				float n = verticeEdgesNum[vra.first];
				float d = vra.second - pyramidRoundAngle(sa, n);
				verticeRoundAngleDeviation[vra.first] = d;

				if (d < 0) {
					LOG(d);
					for (const auto& tr : triags) {
						auto f = [&](const V3* a, const V3* b, const V3* c) {
							LOG("a: "<< a << " " << *a - zero);
							LOG("b: "<< b << " " << *b - zero);
							LOG("c: "<< c << " " << *c - zero);
							LOG("ra " << (*a - *b).angleBetween(*a - *c).valueRadians());
							LOG("sa " << solidAngle(*a, *b, *c, zero));

						};

						if (vra.first == tr.a) f(tr.a, tr.b, tr.c);
						if (vra.first == tr.b) f(tr.b, tr.c, tr.a);
						if (vra.first == tr.c) f(tr.c, tr.a, tr.b);
					}
				}
				
				Assert(d >= 0);

				
				
//				float sa = vsa.second + 2 * M_PI;
//				if (sa > 2*M_PI) sa = 4*M_PI - sa;
//
//				float dd = pyramidRoundAngle(sa, verticeEdgesNum[vsa.first]);
//
//				V3 p = *vsa.first;
//			  //	p.z = 300;
//				p += V3(5, 5, 0);
//
//				float d = verticeRoundAngle[vsa.first];
//				DEBUG_DRAW_C(p, p + V3(0, 0, -100 * (d-dd)), Color::Red/* + Color::Blue*/);
//	//
	//			DEBUG_DRAW_C(p, p + V3(0, 0, -100 * dd), /*Color::Red +*/ Color::Blue);
			}
//		}

		auto verticeSolidAngleDeviationOrig = verticeSolidAngleDeviation;

		auto checkVerticeSolidAngleDeviation = [&](const V3* v) {
			float sum = 0;
			for (const auto& tr : triags) {
				if (v == tr.a) sum += solidAngle(*tr.a, *tr.b, *tr.c, zero);
				if (v == tr.b) sum += solidAngle(*tr.b, *tr.a, *tr.c, zero);
				if (v == tr.c) sum += solidAngle(*tr.c, *tr.a, *tr.b, zero);
			}
			sum -= 2 * M_PI;

			//LOG(v << " " << verticeSolidAngleDeviation[v] << " : " << sum);
			Assert(abs(sum - verticeSolidAngleDeviation[v]) < 0.00001);
		};

		auto checkVerticeRoundAngleDeviation = [&](const V3* v) {
			float sum = 0;
			float n = 0;
			for (const auto& tr : triags) {
				if (v == tr.a) sum += (*tr.a - *tr.b).angleBetween(*tr.a - *tr.c).valueRadians();
				if (v == tr.b) sum += (*tr.b - *tr.c).angleBetween(*tr.b - *tr.a).valueRadians();
				if (v == tr.c) sum += (*tr.c - *tr.a).angleBetween(*tr.c - *tr.b).valueRadians();

				if (v == tr.a) n++;
				if (v == tr.b) n++;
				if (v == tr.c) n++;
			}

			float s = verticeSolidAngleDeviation[v] + 2*M_PI;
			float d = sum - pyramidRoundAngle(s, n);
			Assert(d >= -0.001);

			LOG(v << " " << verticeRoundAngle[v] << " : " << sum);
			LOG(v << " " << verticeEdgesNum[v] << " : " << n);
			LOG(v << " " << verticeRoundAngleDeviation[v] << " : " << d << " " << d - verticeRoundAngleDeviation[v]);

			Assert(abs(sum - verticeRoundAngle[v]) < 0.001);
			Assert(n == verticeEdgesNum[v]);
			Assert(abs(d - verticeRoundAngleDeviation[v]) < 0.001);

//			int& n = verticeEdgesNum[a];
//				float& angle = verticeRoundAngle[a];
//				n += nChange;
//				angle += angleChange;
//
//				float sa = verticeSolidAngleDeviation[edge.a] + 2 * M_PI;
//				verticeRoundAngleDeviation[edge.a] = angle - pyramidRoundAngle(sa, n);
		};


		auto solidAngleSegmentChange = [&](const V3* a, const V3* b, const V3* c0 , const V3* c1) {
			return -solidAngle(*a, *b, *c0, zero) -solidAngle(*a, *b, *c1, zero) +solidAngle(*a, *c0, *c1, zero);
		};
		auto roundAngleSegmentChange = [&](const V3* a, const V3* b, const V3* c0 , const V3* c1) {
			float res = (-(*b-*a).angleBetween(*c0-*a) -(*b-*a).angleBetween(*c1-*a) +(*c0-*a).angleBetween(*c1-*a)).valueRadians();
			Assert(res <= 0.0001);
			return res;
		};
//		auto roundAngleDeviation = [&](const V3* a, float solidAngleChange, int edgesNumChange, float roundAngleChange) {
//			float sa = verticeSolidAngleDeviation[a] + 2*M_PI + solidAngleChange;
//			int n = verticeEdgesNum[a] + edgesNumChange;
//			return  pyramidRoundAngle(sa, n);
//		};

		auto swapEgde = [&](DelaunayEdge& edge) {
			LOG("\nswap:" << edge.a<< " " << edge.b);
			//a-c0
			//b-c1
			if (edge.tr2->a == edge.a) { edge.tr2->a = edge.c0; }
			else if (edge.tr2->b == edge.a) { edge.tr2->b = edge.c0; }
			else if (edge.tr2->c == edge.a) { edge.tr2->c = edge.c0; }
			else Assert(false);
			*edge.tr2 = DelaunayTriangle(edge.tr2->a, edge.tr2->b, edge.tr2->c);

			if (edge.tr1->a == edge.b) { edge.tr1->a = edge.c1; }
			else if (edge.tr1->b == edge.b) { edge.tr1->b = edge.c1; }
			else if (edge.tr1->c == edge.b) { edge.tr1->c = edge.c1; }
			else Assert(false);
			*edge.tr1 = DelaunayTriangle(edge.tr1->a, edge.tr1->b, edge.tr1->c);

	//			DelaunayEdge edgec0c1(edge.c0, edge.c1);
	//			edgec0c1.addTriag(origEdge.tr1);
	//			edgec0c1.addTriag(origEdge.tr2);
	//
	//			edges.erase(SortablePair<const V3*>(origEdge.a, origEdge.b));
	//			edges.erase(SortablePair<const V3*>(origEdge.a, origEdge.c0));

			{
				auto edgeC0A = edges.find(V3Pair(edge.c0, edge.a));
				AssertAction(edgeC0A != edges.end(), return);
				edgeC0A->second.updateC();
			}
			{
				auto edgeC1A = edges.find(V3Pair(edge.c1, edge.a));
				AssertAction(edgeC1A != edges.end(), return);
				if (edgeC1A->second.tr1 == edge.tr2) {
					edgeC1A->second.tr1 = edge.tr1;
					edgeC1A->second.updateC();
				} else if (edgeC1A->second.tr2 == edge.tr2) {
					edgeC1A->second.tr2 = edge.tr1;
					edgeC1A->second.updateC();
				} else Assert(false);
			}
			{
				auto edgeC0B = edges.find(SortablePair<const V3*>(edge.c0, edge.b));
				AssertAction(edgeC0B != edges.end(), return);
				if (edgeC0B->second.tr1 == edge.tr1) {
					edgeC0B->second.tr1 = edge.tr2;
					edgeC0B->second.updateC();
				} else if (edgeC0B->second.tr2 == edge.tr1) {
					edgeC0B->second.tr2 = edge.tr2;
					edgeC0B->second.updateC();
				} else Assert(false);
			}
			{
				auto edgeC1B = edges.find(SortablePair<const V3*>(edge.c1, edge.b));
				AssertAction(edgeC1B != edges.end(), return);
				edgeC1B->second.updateC();
			}


			edges.at(V3Pair(edge.a, edge.c0)).swappable = DelaunayEdge::UNKNOWN;
			edges.at(V3Pair(edge.a, edge.c1)).swappable = DelaunayEdge::UNKNOWN;
			edges.at(V3Pair(edge.b, edge.c0)).swappable = DelaunayEdge::UNKNOWN;
			edges.at(V3Pair(edge.b, edge.c1)).swappable = DelaunayEdge::UNKNOWN;
			edge.swappable = DelaunayEdge::UNKNOWN;

			verticeSolidAngleDeviation[edge.a] += (+solidAngleSegmentChange(edge.a, edge.b, edge.c0, edge.c1));
			verticeSolidAngleDeviation[edge.b] += (+solidAngleSegmentChange(edge.b, edge.a, edge.c0, edge.c1));
			verticeSolidAngleDeviation[edge.c0] += (-solidAngleSegmentChange(edge.c0, edge.c1, edge.a, edge.b));
			verticeSolidAngleDeviation[edge.c1] += (-solidAngleSegmentChange(edge.c1, edge.c0, edge.a, edge.b));

			checkVerticeSolidAngleDeviation(edge.a);
			checkVerticeSolidAngleDeviation(edge.b);
			checkVerticeSolidAngleDeviation(edge.c0);
			checkVerticeSolidAngleDeviation(edge.c1);

			auto updateRoundAngle = [&](const V3* a, int nChange, float angleChange) {
				int& n = verticeEdgesNum[a];
				float& angle = verticeRoundAngle[a];
				n += nChange;
				angle += angleChange;

				float sa = verticeSolidAngleDeviation[a] + 2 * M_PI;
				float d = angle - pyramidRoundAngle(sa, n);
				verticeRoundAngleDeviation[a] = d;

			   	if (d < 0) {
					for (const auto& tr : triags) {
						auto f = [&](const V3* a, const V3* b, const V3* c) {
							LOG("a: "<< a << " " << *a - zero);
							LOG("b: "<< b << " " << *b - zero);
							LOG("c: "<< c << " " << *c - zero);
							LOG("ra " << (*a - *b).angleBetween(*a - *c).valueRadians());
							LOG("sa " << solidAngle(*a, *b, *c, zero));

						};

						if (a == tr.a) f(tr.a, tr.b, tr.c);
						if (a == tr.b) f(tr.b, tr.c, tr.a);
						if (a == tr.c) f(tr.c, tr.a, tr.b);
					}
				}

				Assert(d >= -0.001);
			};

			updateRoundAngle(edge.a, -1, roundAngleSegmentChange(edge.a, edge.b, edge.c0, edge.c1));
			updateRoundAngle(edge.b, -1, roundAngleSegmentChange(edge.b, edge.a, edge.c0, edge.c1));
			updateRoundAngle(edge.c0, 1, -roundAngleSegmentChange(edge.c0, edge.c1, edge.a, edge.b));
			updateRoundAngle(edge.c1, 1, -roundAngleSegmentChange(edge.c1, edge.c0, edge.a, edge.b));

			checkVerticeRoundAngleDeviation(edge.a);
			checkVerticeRoundAngleDeviation(edge.b);
			checkVerticeRoundAngleDeviation(edge.c0);
			checkVerticeRoundAngleDeviation(edge.c1);

			std::swap(edge.a, edge.c0);
			std::swap(edge.b, edge.c1);
			edge.updateC();


			auto it = edges.find(V3Pair(edge.a, edge.b));
			if (it == edges.end()) {
				edges.emplace_hint(it, V3Pair(edge.a, edge.b), edge);
			} else {
				Assert(false);
				it->second = edge;
			}
			edges.erase(V3Pair(edge.c0, edge.c1));
		};

		auto checkEdges = [&]() {
			for (const auto& tr : triags) {
				Assert(edges.count(V3Pair(tr.a, tr.b)) == 1);
				Assert(edges.count(V3Pair(tr.b, tr.c)) == 1);
				Assert(edges.count(V3Pair(tr.c, tr.a)) == 1);
			}

			for (auto& edge : edges) {
				AssertReturn(edge.second.tr1, return);
				AssertReturn(edge.second.tr2, return);
				AssertReturn(edge.second.a, return);
				AssertReturn(edge.second.b, return);
				AssertReturn(edge.second.c0, return);
				AssertReturn(edge.second.c1, return);

				//edge.second.swappable = DelaunayEdge::UNKNOWN;
			}

//			decltype(verticeSolidAngleDeviation) vt;;
//			for (const auto& tr : triags) {
//				vt[tr.a] += solidAngle(*tr.a, *tr.b, *tr.c, zero);
//				vt[tr.b] += solidAngle(*tr.b, *tr.a, *tr.c, zero);
//				vt[tr.c] += solidAngle(*tr.c, *tr.a, *tr.b, zero);
//			}
//
//			for (auto& vsa : vt) {
//				vsa.second -= 2 * M_PI;
//				LOG(vsa.first << " " << verticeSolidAngleDeviation[vsa.first] << " : " << vsa.second);
//				Assert(abs(verticeSolidAngleDeviation[vsa.first] - vsa.second) < 0.00001);
//			}
		};

		auto isSwappableEdge = [&](const DelaunayEdge& edge) {
			if (edges.count(V3Pair(edge.c0, edge.c1)) == 1) {
				return false;
			}

			float curSq0 = trinagleSquare(*edge.a, *edge.b, *edge.c0);
			float curSq1 = trinagleSquare(*edge.a, *edge.b, *edge.c1);
			float revSq0 = trinagleSquare(*edge.c0, *edge.c1, *edge.a);
			float revSq1 = trinagleSquare(*edge.c0, *edge.c1, *edge.b);
			if (curSq0 == 0 || curSq1 == 0 || revSq0 == 0 || revSq1 == 0) {
				return false;
			}

			auto p0 = make_plane(V3toP3(*edge.a), V3toP3(*edge.b), V3toP3(*edge.c0));
			float p0c1d = abs(distance(V3toP3(*edge.c1), p0));   
			float p0zerod = abs(distance(V3toP3(zero), p0));
			if (p0c1d + p0zerod < 2*HundredthOfMeter ) {
				return false;
			} else {
				V4 zeroInABC0C1 = toBaricentric(zero, *edge.a, *edge.b, *edge.c0, *edge.c1);
				AssertAction(zeroInABC0C1 != V4::ZERO, return false);
				LOG(zeroInABC0C1);
				if (zeroInABC0C1.x >= -0.001 && 
					zeroInABC0C1.y >= -0.001 && 
					zeroInABC0C1.z >= -0.001 && 
					zeroInABC0C1.w >= -0.001) {
					return false;
				}
			}
			auto p1 = make_plane(V3toP3(*edge.a), V3toP3(*edge.b), V3toP3(*edge.c1));  
			auto p2 = make_plane(V3toP3(*edge.c0), V3toP3(*edge.c1), V3toP3(*edge.a));
			auto p3 = make_plane(V3toP3(*edge.c0), V3toP3(*edge.c1), V3toP3(*edge.b));
			if (abs(distance(V3toP3(zero), p0)) < HundredthOfMeter
			 || abs(distance(V3toP3(zero), p1)) < HundredthOfMeter
			 || abs(distance(V3toP3(zero), p2)) < HundredthOfMeter
			 || abs(distance(V3toP3(zero), p3)) < HundredthOfMeter) {
			 return false;
			}

			auto plane = make_plane(V3toP3(zero), V3toP3(*edge.c0), V3toP3(*edge.c1));
			auto segment = make_segment(V3toP3(*edge.a), V3toP3(*edge.b));
			V3 ip = P3toV3(intersection_point(segment, plane));
			bool sect = intersect(segment, plane);
//			Assert(sect == (ip.x != infinity<float>() && ip.distance(*edge.c0) > 1 && ip.distance(*edge.c1) > 1));
			return sect;
		};

		auto updateIsSwappable = [&]() {
			LOG("update is swappable:");
			for (auto& edgePair : edges) {
				auto& edge = edgePair.second;

//				if ((edges.count(V3Pair(edge.c0, edge.c1)) == 1) && (edge.swappable == DelaunayEdge::YES)) {
//					edge.swappable = DelaunayEdge::UNKNOWN;
//				}
//
//				if (edge.swappable == DelaunayEdge::UNKNOWN) {
					auto sw = isSwappableEdge(edge) ? (DelaunayEdge::YES) : (DelaunayEdge::NO);
					Assert(edge.swappable == DelaunayEdge::UNKNOWN || edge.swappable == sw); 
					edge.swappable = sw;

					if (edge.swappable == DelaunayEdge::YES) {
						auto solidAngleDeviationChange = [&](const V3* v, float change) {
							float aSA = verticeSolidAngleDeviation[v];
							float aSAmod = aSA + change;
							return pow(abs(aSA), 1.7) - pow(abs(aSAmod), 1.7);
						};
						bool swapForDebug = false;
						auto roundAngleDeviationChange = [&](const V3* v, float solidAngleChange, int edgesNumChange, float roundAngleChange) {
//							checkVerticeRoundAngleDeviation(v);
//							checkVerticeSolidAngleDeviation(v);

							float rad = verticeRoundAngleDeviation[v];

							float raHaveMod = verticeRoundAngle[v] + roundAngleChange;
							float saMod = verticeSolidAngleDeviation[v] + 2*M_PI + solidAngleChange;
							int edgesNumMod = verticeEdgesNum[v] + edgesNumChange;
							float raDstMod = pyramidRoundAngle(saMod, edgesNumMod);
							float radMod = raHaveMod - raDstMod;
							Assert(radMod >= -0.001);
							if (radMod < 0) {
								LOG(raHaveMod);
								LOG(raDstMod);
								LOG(radMod);
								auto f = [&](const V3* a, const V3* b, const V3* c) {
									LOG("a: "<< a << " " << *a - zero);
									LOG("b: "<< b << " " << *b - zero);
									LOG("c: "<< c << " " << *c - zero);
									LOG("ra " << (*a - *b).angleBetween(*a - *c).valueRadians()); 
									LOG("sa " << solidAngle(*a, *b, *c, zero));
							
								};
								for (const auto& tr : triags) {

									if (v == tr.a) f(tr.a, tr.b, tr.c);
									if (v == tr.b) f(tr.b, tr.c, tr.a);
									if (v == tr.c) f(tr.c, tr.a, tr.b);
								}
								swapForDebug = true;
							}

							return rad - radMod;
						};

						float solidChange = 0;
						float roundChange = 0;

						float aSolidChange = +solidAngleSegmentChange(edge.a, edge.b, edge.c0, edge.c1);
						solidChange += solidAngleDeviationChange(edge.a, aSolidChange);
						roundChange += roundAngleDeviationChange(edge.a, aSolidChange, -1, roundAngleSegmentChange(edge.a, edge.b, edge.c0, edge.c1));

						float bSolidChange = +solidAngleSegmentChange(edge.b, edge.a, edge.c0, edge.c1);
						solidChange += solidAngleDeviationChange(edge.b, bSolidChange);
						roundChange += roundAngleDeviationChange(edge.b, bSolidChange, -1, roundAngleSegmentChange(edge.b, edge.a, edge.c0, edge.c1));

						float c0SolidChange = -solidAngleSegmentChange(edge.c0, edge.c1, edge.a, edge.b);
						solidChange += solidAngleDeviationChange(edge.c0, c0SolidChange);
						roundChange += roundAngleDeviationChange(edge.c0, c0SolidChange, +1, -roundAngleSegmentChange(edge.c0, edge.c1, edge.a, edge.b));

						float c1SolidChange = -solidAngleSegmentChange(edge.c1, edge.c0, edge.a, edge.b);
						solidChange += solidAngleDeviationChange(edge.c1, c1SolidChange);
						roundChange += roundAngleDeviationChange(edge.c1, c1SolidChange, +1, -roundAngleSegmentChange(edge.c1, edge.c0, edge.a, edge.b));

						edge.solidAngleDeviationChange = solidChange;
						edge.roundAngleDeviationChange = roundChange;
                                        
//						if (swapForDebug) swapEgde(edge);

//						V3 p = (*edge.a + *edge.b) / 2;
//						p.z = + 500;
//						DEBUG_DRAW_C(p, p + V3(0, 0, -100 * diff), Color::White);
//
//
//						{
//							V3 pa = ((*edge.a-p).normalisedCopy() * 20 + p);
//							pa.z = + 500;
//							float d = solidAngleDeviationChange(edge.a, +solidAngleSegmentChange(edge.a, edge.b, edge.c0, edge.c1));
//							DEBUG_DRAW_C(pa, pa + V3(0, 0, -100 * d), Color::Red);
//						}
//						{
//							V3 pa = ((*edge.b-p).normalisedCopy() * 20 + p);
//							pa.z = + 500;
//							float d = solidAngleDeviationChange(edge.b, +solidAngleSegmentChange(edge.b, edge.a, edge.c0, edge.c1));
//							DEBUG_DRAW_C(pa, pa + V3(0, 0, -100 * d), Color::Green);
//						}
//
//						{
//							V3 pa = ((*edge.c0-p).normalisedCopy() * 20 + p);
//							pa.z = + 500;
//							float d = solidAngleDeviationChange(edge.c0, -solidAngleSegmentChange(edge.c0, edge.c1, edge.a, edge.b));
//							DEBUG_DRAW_C(pa, pa + V3(0, 0, -100 * d), Color::Blue);
//						}
//
//						{
//							V3 pa = ((*edge.c1-p).normalisedCopy() * 20 + p);
//							pa.z = + 500;
//							float d = solidAngleDeviationChange(edge.c1, -solidAngleSegmentChange(edge.c1, edge.c0, edge.a, edge.b));
//							DEBUG_DRAW_C(pa, pa + V3(0, 0, -100 * d), Color::Green +  Color::Red);
//						}

					} else {
						edge.solidAngleDeviationChange = 0.0f;
						edge.roundAngleDeviationChange = 0.0f;
					}

//					V3 p = (*edge.a + *edge.b) / 2;
//					p.z = + 500;
//
//					DEBUG_DRAW_C(p, p + V3(0, 0, -100 * edge.solidAngleDeviationChange), Color::Green);
//					LOG(" \t" << edge.a << " " << edge.b << " " << edge.solidAngleDeviationChange);
//				}
			}
		};

		auto compareEdgePairs = [](const std::pair<V3Pair, DelaunayEdge>& e0, const std::pair<V3Pair, DelaunayEdge>& e1) {
//			return e0.second.solidAngleDeviationChange < e1.second.solidAngleDeviationChange;
			return e0.second.roundAngleDeviationChange < e1.second.roundAngleDeviationChange;
		};
		
		for (int i = 0; ; i++) {
//			LOG("\nedges:");
//			for (auto& edge : edges) {
//				LOG("key:" << edge.first.first << " " << edge.first.second);
//				LOG("\ta:" << edge.second.a);
//				LOG("\tb:" << edge.second.b);
//				LOG("\tc0:" << edge.second.c0);
//				LOG("\tc1:" << edge.second.c1);
//				LOG("\ttr1:" << edge.second.tr1);
//				LOG("\ttr2:" << edge.second.tr2);
//			}
            
			updateIsSwappable();
//			return;
			auto maxIt = std::max_element(edges.begin(), edges.end(), compareEdgePairs);
			AssertAction(maxIt != edges.end(), break);
			LOG("\nmax:" << maxIt->second.a<< " " << maxIt->second.b << " " << maxIt->second.roundAngleDeviationChange);
			if (maxIt->second.swappable == DelaunayEdge::YES && maxIt->second.roundAngleDeviationChange > 0) {
				swapEgde(maxIt->second);
				checkEdges();
			} else {
                break;
            }

			if (i > edges.size() * 2) {
				Assert(false);
				break;
			}
		}
	}

	bool isPointInHull(V3 point, const std::list<DelaunayTriangle>& triags) {
		int intersectionsCountY {0};
		int intersectionsCountX {0}; 
		int intersectionsCountZ {0};
		ray<float, 3> xray = make_ray(V3toP3(point), vector3d<float>(1, 0.1, 0.1));
		ray<float, 3> yray = make_ray(V3toP3(point), vector3d<float>(-0.1, 1, -0.1)); 
		ray<float, 3> zray = make_ray(V3toP3(point), vector3d<float>(-0.1, 0.1, 1));
		
		for (const auto& triag : triags) {
			auto tr = make_triangle(V3toP3(*triag.a), V3toP3(*triag.b), V3toP3(*triag.c));
										  
			V3 cl = P3toV3(closest_point_on_triangle_from_point(tr, V3toP3(point)));
			if (cl.distance(point) < HundredthOfMeter * 0.1) {
				return false;
			}

			if (intersect(xray, tr)) intersectionsCountX++; 
			if (intersect(yray, tr)) intersectionsCountY++;  
			if (intersect(zray, tr)) intersectionsCountZ++;


		}
                                          
		Assert((intersectionsCountX % 2 == intersectionsCountY % 2) && (intersectionsCountY % 2 == intersectionsCountZ % 2));
		
		intersectionsCountX = intersectionsCountX % 2;
		intersectionsCountY = intersectionsCountY % 2;
		intersectionsCountZ = intersectionsCountZ % 2;		

		return (intersectionsCountX + intersectionsCountY + intersectionsCountZ) >= 2;
	}

	bool sortChain(std::vector<DelaunayTriangleExt>& triags) {
		int end = ((int)triags.size())-1;
		for (int i = 0; i <= end-1; ) {
			auto& iTr = triags[i];
			bool found = false;
			for (int j = i+1; j <= end; j++) {
				auto& jTr = triags[j];
				if (!iTr.cFromA && jTr.hasVertice(iTr.a) && jTr.hasVertice(iTr.c)) {
//					LOG(iTr.a);
//					LOG(iTr.b);
//					LOG(iTr.c);
//					LOG(jTr.a);
//					LOG(jTr.b);
//					LOG(jTr.c);
					if (jTr.c == iTr.a ) {
						std::swap(jTr.c, jTr.a);
					} else if (jTr.c == iTr.c ) {
						std::swap(jTr.c, jTr.b);
					}
					Assert(iTr.a == jTr.a && iTr.c == jTr.b);
					found = true;;
				} else if (iTr.cFromA && jTr.hasVertice(iTr.b) && jTr.hasVertice(iTr.c)) {
//					LOG(iTr.a);
//					LOG(iTr.b);
//					LOG(iTr.c);
//					LOG(jTr.a);
//					LOG(jTr.b);
//					LOG(jTr.c);
					if (jTr.c == iTr.b ) {
						std::swap(jTr.c, jTr.b);
					} else if (jTr.c == iTr.c ) {
						std::swap(jTr.c, jTr.a);
					}
					Assert(iTr.b == jTr.b && iTr.c == jTr.a);
					found = true;               
				}

				if (found) {
					std::swap(triags[i+1], triags[j]);
					Assert(triags[i].a == triags[i+1].a && triags[i].c == triags[i+1].b
						|| triags[i].b == triags[i+1].b && triags[i].c == triags[i+1].a);
					break;
				}
			}

			if (!found) return false;
			i++;

//			if (!found) {
//				if (end > 0) {
//					if (i > 1) --i;
//					std::swap(iTr, triags[end]);
//					end--;
//				} else {
//					Assert(false);
//					break;
//				}
//			} else {
//				i++;
//            }
		}
//		Assert(end+1 == triags.size());
//		triags.resize(end+1);
		return true;//end+1 == triags.size();
	}
}


