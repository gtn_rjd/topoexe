#pragma once
#include "CMTypes.h"
#include "CMPiket.h" 
#include <cmath>     
#include <ostream>   
#include <algorithm>
#include "CMAssertions.h"
#include "CMHelpers.cpp"

void test ();

namespace CM {

struct DelaunayTriangle {
	DelaunayTriangle() { }
	DelaunayTriangle(const V3* a, const V3* b, const V3* c)
	: a(std::min(std::min(a, b), c))
	, c(std::max(std::max(a, b), c)) {
		if (a > b && a < c || a < b && a > c) this->b = a;
		else if (b > a && b < c || b < a && b > c) this->b = b;
		else this->b = c;
	}

    bool hasVertice(const V3* v) const { return a == v || b == v || c == v; }
    bool isNull() const { return hasVertice(nullptr); }
    const V3* getNextVert(const V3* v) const { 
        if (a == v) return b; 
        if (b == v) return c; 
        if (c == v) return a;
        return nullptr;
    }
    const V3* getPrevVert(const V3* v) const { 
		if (a == v) return c;
        if (b == v) return a; 
        if (c == v) return b;
        return nullptr;
    }
	const V3* getContrVert(const V3* v1, const V3* v2) const {
		if (v1 > v2) std::swap(v1, v2);
		if (a == v1) {
			if (b == v2) return c;
			else if (c == v2) return b;
			else return nullptr;
		} else if (b == v1 && c == v2) {
			return a;
		} else
            return nullptr;
	}

    V3 getCenter() const {
        if (isNull()) return V3(0, 0, 0);
        else return (*a + *b + *c)/3;
    }

	const V3* a {nullptr};
    const V3* b {nullptr};
    const V3* c {nullptr};
};

bool operator== (const DelaunayTriangle& tr0, const DelaunayTriangle& tr1) {
	return (tr0.a == tr1.a && tr0.b == tr1.b && tr0.c == tr1.c);
}

bool operator< (const DelaunayTriangle& tr0, const DelaunayTriangle& tr1) {
	if (tr0.a != tr1.a) return tr0.a < tr1.a;
	else if (tr0.b != tr1.b) return tr0.b < tr1.b;
	else if (tr0.c != tr1.c) return tr0.c < tr1.c;
	return false;
}

std::ostream& operator<< (std::ostream& s, const DelaunayTriangle& tr) {
	return s << "{" << tr.a << "," << tr.b << "," << tr.c << "}";
}

struct DelaunayTriangleExt : public DelaunayTriangle {
	DelaunayTriangleExt() { }
    bool cFromA {false};
};

//struct Simplex3d {
//    V3 a {0,0,0,0};
//    V3 b {0,0,0,0};
//    V3 c {0,0,0,0};
//    V3 d {0,0,0,0};
//};

typedef SortablePair<const V3*>  V3Pair;

struct DelaunayEdge {
	DelaunayEdge(const V3* a, const V3* b)
	: a(std::min(a, b))
	, b(std::max(a, b)) {

	}

	V3Pair getKey() const {
		return V3Pair(a, b);
	}

	void updateC() {
		AssertAction(tr1, return);
		AssertAction(tr2, return);

		c0 = tr1->getContrVert(a, b);
		c1 = tr2->getContrVert(a, b);

		AssertReturn(tr1, return);
		AssertReturn(tr2, return);
		AssertReturn(a, return);
		AssertReturn(b, return);
		AssertReturn(c0, return);
		AssertReturn(c1, return);
	}

	void addTriag(DelaunayTriangle* tr) {
		if (!tr1) tr1 = tr;
		else if (!tr2) {
			tr2 = tr;
			c0 = tr1->getContrVert(a, b);
			c1 = tr2->getContrVert(a, b);

		} else Assert(false);
	}

	enum {
		UNKNOWN = 0,
		NO = 1,
		YES = 2,
	} swappable { UNKNOWN };

	float solidAngleDeviationChange {0.0f};
	float roundAngleDeviationChange {0.0f};

	const V3* a {nullptr};
	const V3* b {nullptr};
	const V3* c0 {nullptr};
	const V3* c1 {nullptr};
	DelaunayTriangle* tr1 {nullptr};
	DelaunayTriangle* tr2 {nullptr};
};

bool operator< (const DelaunayEdge& t1, const DelaunayEdge& t2 ) {
	if (t1.a == t2.a) return t1.b < t2.b;
	else return t1.a < t2.a;
}

void makeCloudTriangulation(const std::vector<const V3*>& points, std::list<DelaunayTriangle>& output);
void makeHullFromCloudTriangulation(const V3& surveyPoint, const std::vector<const V3*>& delaunayVerts, std::list<DelaunayTriangle>& triangels, const std::set<const V3*>& tabooNeigbors);

void debugDrawTriangles(const std::list<DelaunayTriangle>& trgs, V3 zero = V3::ZERO);

void makePointsUniq(std::vector<const V3*>& points);
void makePointsUniq(std::vector<V3>& points);

std::vector<int> getConvexHullPoints(const std::vector<V3>& points);
bool isConvexHullIncludePoint(int targetPointIdx, const std::vector<V3>& points);
bool isCoplanar(const std::vector<V3>& points);

void makeDelaunaySphereTriangulation(V3 zero, const std::vector<const V3*>& points, std::list<DelaunayTriangle>& output, bool debug);
void makeDelaunayStrainHullTriangulation(V3 zero, const std::vector<const V3*>& points, std::list<DelaunayTriangle>& output);
void makeConvexHullTriangulation(V3 shift, const std::vector<const V3*>& points, std::list<DelaunayTriangle>& output);
void makeEdgesFromTriags(std::list<DelaunayTriangle>& triags, std::map<SortablePair<const V3*>, DelaunayEdge>& edges);
void postprocessTriags(V3 zero, std::list<DelaunayTriangle>& triags);

std::set<const V3*> removeFacesWithTabooNeigbors(std::list<DelaunayTriangle>& triangels, const std::set<const V3*>& tabooNeigbors);

std::vector<int> makeConvexHull2d (const std::vector<V2>& points);

// DOES NOT WORK
std::vector<std::pair<int, int>> makeHullXYProjectionByQull(V3 zero, const std::vector<V3>& points);

std::vector<int> makeHullXYProjectionByWykobi(V3 zero, const std::vector<V3>& points);

bool isPointInHull(V3 point, const std::list<DelaunayTriangle>& triags);

bool sortChain(std::vector<DelaunayTriangleExt>& triags);

}
