#pragma once
#include "assert.h" 
//#include "yprintf.h"  
#include <string>      
#include <sstream>
#include <csignal>


#ifdef _DEBUG 

#define AssertReturn(var, exec) \
if(!(var)) { \
_asm {int 3};\
exec; \
}\

#else

#define AssertReturn(var, exec) \
if(!(var)) { \
exec; \
}\

#endif 


#define Assert(var) AssertReturn(var, ; )
#define AssertAction(var, exec) AssertReturn(var, exec)

