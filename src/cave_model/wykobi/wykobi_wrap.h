//---------------------------------------------------------------------------

#pragma once
#include "OgreVector3.h"
#include "OgreVector2.h"
#include "wykobi.hpp"
                          
typedef Ogre::Vector3 V3;
typedef Ogre::Vector2 V2;  

typedef wykobi::plane<float, 3> plane3d;
typedef wykobi::line<float,3> line3df;
typedef wykobi::point3d<float> P3;
typedef wykobi::point2d<float> P2;
typedef wykobi::segment<float,2> S2;
typedef wykobi::line<float,3> L3;
typedef wykobi::line<float,2> L2;
typedef wykobi::ray<float,3> R3;
typedef wykobi::triangle<float,3> T3;  
typedef wykobi::polygon<float,2> POLY2;


inline P3 V3toP3 (const V3& v3) {
    return wykobi::make_point<float>(v3.x, v3.y, v3.z);
}  

inline P2 V2toP2 (const V2& v2) {
    return wykobi::make_point<float>(v2.x, v2.y);
}

inline Ogre::Vector3 P3toV3 (const P3& p3) {
    return Ogre::Vector3(p3.x, p3.y, p3.z);
}

inline L3 makeL3 (const V3& a, const V3& b) {
    L3 l3;
    l3[0] = V3toP3(a);
    l3[1] = V3toP3(b);
    return l3;
}

inline T3 makeT3 (const V3& a, const V3& b, const V3& c) {
    T3 t3;
    t3[0] = V3toP3(a);
    t3[1] = V3toP3(b);
    t3[2] = V3toP3(c);
    return t3;
}

inline R3 makeR3 (const V3& origin, const V3& otherPoint) {
    R3 r3;
    r3.origin = V3toP3(origin);
    V3 d = otherPoint-origin;
    d.normalise(); 
    r3.direction = wykobi::vector3d<float>(d.x, d.y, d.z);
    return r3;
}

namespace std {
	std::ostream& operator<< (std::ostream& s, const P2& p) {
		s << "{" ;
		s << p.x;
		s << ", ";
		s << p.y;
		s << "}" ;
		return s ;
	}
}

inline wykobi::cubic_bezier<float,2> makeCubicBezier(const V2& h, const V2& i, const V2& j, const V2& k, float controlStrength = 0.2f) {
	V2 ic = i;
	V2 jc = j;
	if (i != j) {
		float ijdst = i.distance(j);

		V2 hi = (h == i) ? V2::ZERO : (i-h).normalisedCopy();
		V2 kj = (k == j) ? V2::ZERO : (j-k).normalisedCopy();

		ic = i + ijdst * (hi + (j-i).normalisedCopy()).normalisedCopy() * controlStrength;
		jc = j + ijdst * (kj + (i-j).normalisedCopy()).normalisedCopy() * controlStrength;
	}

	wykobi::cubic_bezier<float,2> res;
	res[0] = wykobi::make_point(i.x, i.y);
	res[1] = wykobi::make_point(ic.x, ic.y);
	res[2] = wykobi::make_point(jc.x, jc.y);
	res[3] = wykobi::make_point(j.x, j.y);
	return res;
}

namespace wykobi {
	template <typename T>
	inline wykobi::point2d<float> closest_point_on_bezier_from_line(const wykobi::cubic_bezier<T,2>& bezier,
																	const wykobi::line<T,2>& line,
																	const std::size_t& steps)
	{
	  typedef point2d<T> point_type;

	  T smallest_distance = +infinity<T>();

	  point_type closest_points_summ = degenerate_point2d<T>();
	  int num_closest_points = 0;

	  std::vector<point_type> point_list;
	  point_list.reserve(steps);

	  wykobi::generate_bezier(bezier, std::back_inserter(point_list), steps);

	  for (std::size_t i = 0; i < point_list.size(); ++i)
	  {
		 point_type current_point = closest_point_on_line_from_point(line, point_list[i]);

		 const T current_distance = distance(current_point, point_list[i]);

		 if (current_distance < smallest_distance) {
			closest_points_summ = point_list[i];
			smallest_distance = current_distance;
			num_closest_points = 1;
		 } else if (current_distance == smallest_distance) {
			closest_points_summ.x += point_list[i].x;
			closest_points_summ.y += point_list[i].y;
			num_closest_points++;
		 }
	  }

	  if(num_closest_points > 0) {
		 closest_points_summ.x /= num_closest_points;
		 closest_points_summ.y /= num_closest_points;
	  }

	  return closest_points_summ;
	}
}
