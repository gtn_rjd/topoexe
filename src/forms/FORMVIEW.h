﻿//---------------------------------------------------------------------------
#ifndef FormViewH
#define FormViewH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>

#include <ddraw.h>
//---------------------------------------------------------------------------
class TViewForm : public TForm
{
__published:	// IDE-managed Components
        void __fastcall FormActivate(TObject *Sender);
        void __fastcall FormKeyPress(TObject *Sender, char &Key);
private:	// User declarations
        IDirectDraw* DirectDraw;
        IDirectDrawSurface* PrimaryCanvas;
        IDirectDrawSurface* AttachedCanvas;        
public:		// User declarations
        __fastcall TViewForm(TComponent* Owner);
        void __fastcall InitializeDirectDraw();
        void __fastcall DisplayError(wchar_t* error);
        void __fastcall SetupPrimaryCanvas();
        void __fastcall TViewForm::DrawLines();        
};
//---------------------------------------------------------------------------
extern PACKAGE TViewForm *ViewForm;
//---------------------------------------------------------------------------
#endif
