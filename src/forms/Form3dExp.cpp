﻿//---------------------------------------------------------------------------

#include <vcl.h>
#include <math.h>
#include <stdio.h>
#include <string>
#include <iostream>
#include <fstream>
#pragma hdrstop

#include "formtrans.h"
#include "Form3dExp.h"
#include "TOpO.h"
#include "vrml.h"
#include "x3d.h"
#include "OgreCMCaveModel.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TExp3dForm *Exp3dForm;

//TStringList* wrList;      // текст дл¤ сохранени¤ в vrml
//TStringList* txtList;     // текст дл¤ сохранени¤ в txt

//---------------------------------------------------------------------------
__fastcall TExp3dForm::TExp3dForm(TComponent* Owner)
        : TForm(Owner)
{
 if(Txt_Stations->Checked) {Txt_Surface->Enabled=true; Txt_Duplicate->Enabled=true;}
  else {Txt_Surface->Checked=false; Txt_Duplicate->Checked=false;
        Txt_Surface->Enabled=false; Txt_Duplicate->Enabled=false;}
}
//---------------------------------------------------------------------------
void __fastcall TExp3dForm::Txt_StationsClick(TObject *Sender)
{
 if(Txt_Stations->Checked) {Txt_Surface->Enabled=true; Txt_Duplicate->Enabled=true;}
  else {Txt_Surface->Checked=false; Txt_Duplicate->Checked=false;
        Txt_Surface->Enabled=false; Txt_Duplicate->Enabled=false;}
}
//---------------------------------------------------------------------------
void __fastcall TExp3dForm::CancelClick(TObject *Sender)
{
 Exp3dForm->Close();
}
//--------------------------- Color conversion ------------------------------
const wchar_t* wrColor(int oldColor) // аналогична psColor
{
  switch(oldColor){
     case 0: return L"0 0 0";         //clBlack;
     case 1: return L"0 0 1";         //clBlue;
     case 2: return L"0 0.66 0";       //clGreen;
     case 3: return L"0 1 1";         //clAqua;
     case 4: return L"0.9 0.17 0.17"; //clRed;
     case 5: return L"0.83 0 0.83";   //clPurple;  Magenta
     case 6: return L"0.94 0.5 0.5";  //(TColor)0x008080F0;  brown
     case 7: return L"0.9 0.9 0.9";   //clLtGray;
     case 8: return L"0.66 0.66 0.66";//clDkGray;
     case 9: return L"0 0.66 1";      //(TColor)0x00FFAA00;  светло-синий
     case 10: return L"0 1 0";        //clLime;
     case 11: return L"0.83 0.83 0.17";//(TColor)0x0020C0C0;  //khaki защитный
     case 12: return L"1 0 0";        //(TColor)0x000000FF;  ¤рко-красный
     case 13: return L"1 0 1";        //clFuchsia;
     case 14: return L"1 1 0";        //clYellow;
     case 15: return L"1 1 1";        //clWhite;
     case 16: return L"0.94 0.7 0.17";//(TColor)0x002AAFF 0; //оранжевый
//     clCream  clGray  clMaroon clSkyBlue clMoneyGreen (TColor)0x00FF7070;
   default: return L"0 0 0";
   }
}
//-----------------------------------------------------------------------------

void TExp3dForm::MinMax3D(void)
{
  xmin=xmax=P[0].pos.x; ymin=ymax=P[0].pos.y; zmin=zmax=P[0].pos.z;
  for(unsigned int s=1; s < MainForm1->Razmer; ++s)
   {
    if ( P[s].pos.x < xmin )  xmin=P[s].pos.x;
    if ( P[s].pos.x > xmax )  xmax=P[s].pos.x;
    if ( P[s].pos.y < ymin )  ymin=P[s].pos.y;
    if ( P[s].pos.y > ymax )  ymax=P[s].pos.y;
    if ( P[s].pos.z < zmin )  zmin=P[s].pos.z;
    if ( P[s].pos.z > zmax )  zmax=P[s].pos.z;
   }
  xmin=xmin/100;   xmax=xmax/100;
  ymin=ymin/100;   ymax=ymax/100;
  zmin=zmin/100;   zmax=zmax/100;
}
//---------------------------------------------------------------------------
//void TExp3dForm::wrEndPath(int a)
//{
// UnicodeString Astr=L"";
// wrList->Add(L"   ]");
// wrList->Add(L"}");
// wrList->Add(L"   coordIndex [ ");
// for(int s=0; s < a; ++s)
//  Astr = Astr + UnicodeString(s) + L",L";
// Astr = Astr + L"-1,L";
// wrList->Add(Astr);
// wrList->Add(L"   ]");
// wrList->Add(L"}");
// wrList->Add(L"}");
// wrList->Add(L"");
//}
//---------------------------------------------------------------------------
//void TExp3dForm::wrVPoint(void)
//{
// wrList->Add(L"Viewpoint {");
// wrList->Add(L"   fieldOfView 0.42");
// wrList->Add(L"   jump TRUE");
//}
//---------------------------------------------------------------------------
void TExp3dForm::WriteVrml(void) {
	int  wrLineColor=2;
	bool colorFlag=0;
	wchar_t buffer[60];
	int stations=0;

	//--- Save ---
	UnicodeString wrFile = ExtractFileName(MainForm1->CurrFileName);

	SaveDialog1->FileName = (wrFile + L".wrl").c_str();
	if(!SaveDialog1->Execute())
		return;
	wrFile = SaveDialog1->FileName;

	MinMax3D();

	std::wfstream f;
	f.open(wrFile.c_str(), std::ios_base::trunc | std::ios_base::out);

	if (!f.is_open()) return;

	vrmlPreambula(f);

	auto wrNewPath = [](std::wostream& strm, int Ncol, bool colorFlag) {
		const wchar_t* col = nullptr;
		if((Exp3dForm->vrml_lines->ItemIndex<=0)&&(colorFlag)) {
			col = wrColor(Ncol);
		} else {
			col = L"0.0 0.0 0.0";
		}
		vrmlStartPath(strm, col);
	};

	for(unsigned int s=0; s < MainForm1->Razmer; ++s)
	if( !((P[s].priz & PRIZ_DUPLICATE) && !(CheckDuplicate->State == cbChecked)) &&
		!((P[s].priz & PRIZ_SURFACE)&& !(CheckSurface->State == cbChecked))   )
	 {
	  if(  (P[s].priz & PRIZ_BRANCH)||(P[s].col!=P[s-1].col)||     //newVetka, color
		   ((P[s].priz & PRIZ_SURFACE)!=(P[s-1].priz & PRIZ_SURFACE))||        //surf
		   ((P[s].priz & PRIZ_DUPLICATE)!=(P[s-1].priz & PRIZ_DUPLICATE)) )      //dupl
	   {
		if(s==0)       // 0-й пикет
		{
		 wrLineColor = P[0].col; colorFlag=1;
		 if(P[0].priz & PRIZ_SURFACE) wrLineColor=MainForm1->sur_col;
		 if(P[0].priz & PRIZ_DUPLICATE) wrLineColor=MainForm1->dup_col;
		 wrNewPath(f, wrLineColor, colorFlag); colorFlag=0;
		}
		else          // новый цвет
		{
		 if((P[s].col!=P[s-1].col)|| (P[s].priz & 1) ||
		   ((P[s].priz & PRIZ_SURFACE)!=(P[s-1].priz & PRIZ_SURFACE))||((P[s].priz & PRIZ_DUPLICATE)!=(P[s-1].priz & PRIZ_DUPLICATE)) )
		 {
		  colorFlag=1;
		  wrLineColor=P[s].col;
		  if(P[s].priz & PRIZ_SURFACE) wrLineColor=MainForm1->sur_col;
		  if(P[s].priz & PRIZ_DUPLICATE) wrLineColor=MainForm1->dup_col;
		  if(stations!=0) vrmlEndPath(f, stations);
		  stations=0;
		  wrNewPath(f, wrLineColor, colorFlag); colorFlag=0;
		  if( !(P[s].priz & PRIZ_BRANCH) ) {
		   vrmlPoint(f, P[s-1].pos/100.0f);
		   stations++;
		  }
		 }
		}
	   }
	   vrmlPoint(f, P[s].pos/100.0f);
	   stations++;
	}
	if(stations!=0) vrmlEndPath(f, stations);

	if (CheckBox1->Checked) {
		OgreCaveDataModel* model = new OgreCaveDataModel(P, size, W, MainForm1->WRazmer, Equates);
		model->setOutputType(OutputType::OT_WALL);
		auto cvp = model->getCaveViewPrefs();
		cvp.wallsTrianglesBlowMode = CM::WTBM_NONE;
		cvp.wallsTrianglesBlowStrength = CM::WTBS_LOW;
		cvp.wallColoringMode = CM::WCM_SMOOTH;
		model->setCaveViewPrefs(cvp);
		model->init();

		const std::vector<CM::OutputPoly>& polygons = model->getOutputPoly(OutputType::OT_WALL);
		std::vector<V3> points;
		std::vector<int> idx;
		std::vector<CM::Color> color;

		auto checkAdd = [&](V3 v, const CM::Color& c) {
			v = model->verticePosToPiketPos(v) / 100;
			for (int i = 0; i < points.size(); i++) {
				if (points[i] == v) {
					idx.push_back(i);
                    return;
				}
			}
			points.push_back(v);
			idx.push_back(points.size()-1);
			color.push_back(c);
		} ;

		int i = 0;
		for(const auto& poly : polygons) {
			checkAdd(poly.a, poly.ca);
			checkAdd(poly.c, poly.cb);
			checkAdd(poly.b, poly.cc);
		}
		vrmlPoly(f, color, points, idx);
//		for(const auto& poly : polygons) {
//			vrmlPoly(f, (poly.ca+poly.cb+poly.cc)/3
//			, model->verticePosToPiketPos(poly.a) / 100
//			, model->verticePosToPiketPos(poly.b) / 100
//			, model->verticePosToPiketPos(poly.c) / 100);
//		}
		delete model;
	}

	f.flush();

	//--- рамка ---
	vrmlAABB(f, V3(xmin, ymin, zmin), V3(xmax, ymax, zmax));
	f.flush();

	//--- View points ---
	vrmlViewPoints(f, V3(xmin, ymin, zmin), V3(xmax, ymax, zmax));
	f.flush();

	vrmlNavigation(f, (fabs(xmax-xmin)+ fabs(ymax-ymin) + fabs(zmax-zmin))/30 +1);
	f.flush();
	/* wrList->Add(L" WorldInfo {");
	wrList->Add(L"info \"Generated by TOpO\" ");
	wrList->Add(L"title \" cave \" ");
	wrList->Add(L"}"); */

	if(BgColor->ItemIndex==0) vrmlBackground(f, 1, 1, 1);
	if(BgColor->ItemIndex==1) vrmlBackground(f, 0.7, 0.7, 0.7);
	if(BgColor->ItemIndex==2 || BgColor->ItemIndex==-1) vrmlBackground(f, 0, 0, 0);

	f << L"\n";
    f.flush();
	f.close();

	Application->MessageBox(L"Save finished", L"TOpO: Message", MB_OK);
}
  //////////////////////save x3d//////////////////////////
 void TExp3dForm::Writex3d(void) {
	int  x3dLineColor=2;
	bool colorFlag=0;
	wchar_t buffer[60];
	int stations=0;

	//--- Save ---
	UnicodeString wrFile = ExtractFileName(MainForm1->CurrFileName);

	SaveDialog1->FileName = (wrFile + L".x3d").c_str();
	if(!SaveDialog1->Execute())
		return;
	wrFile = SaveDialog1->FileName;

	MinMax3D();

	std::wfstream f;
	f.open(wrFile.c_str(), std::ios_base::trunc | std::ios_base::out);

	if (!f.is_open()) return;

	x3dPreambula(f);

	auto x3dNewPath = [](std::wostream& strm, int Ncol, bool colorFlag) {
		const wchar_t* col = nullptr;
		if((Exp3dForm->vrml_lines->ItemIndex<=0)&&(colorFlag)) {
			col = wrColor(Ncol);
		} else {
			col = L"0.0 0.0 0.0";
		}
		x3dStartPath(strm, col);
	};
	int n=0;
    unsigned int s=0;
	for(unsigned int s=0; s < MainForm1->Razmer; ++s)   {

	if( !((P[s].priz & PRIZ_DUPLICATE) && !(CheckDuplicate->State == cbChecked)) &&
		!((P[s].priz & PRIZ_SURFACE)&& !(CheckSurface->State == cbChecked))   )
	 {
	  if(  (P[s].priz & PRIZ_BRANCH)||(P[s].col!=P[s-1].col)||     //newVetka, color
		   ((P[s].priz & PRIZ_SURFACE)!=(P[s-1].priz & PRIZ_SURFACE))||        //surf
		   ((P[s].priz & PRIZ_DUPLICATE)!=(P[s-1].priz & PRIZ_DUPLICATE)) )      //dupl
	   {
		if(s==0)       // 0-й пикет
		{
		 x3dLineColor = P[0].col; colorFlag=1;
		 if(P[0].priz & PRIZ_SURFACE) x3dLineColor=MainForm1->sur_col;
		 if(P[0].priz & PRIZ_DUPLICATE) x3dLineColor=MainForm1->dup_col;
		 x3dNewPath(f, x3dLineColor, colorFlag); colorFlag=0;
		}
		else          // новый цвет
		{
		 if((P[s].col!=P[s-1].col)|| (P[s].priz & 1) ||
		   ((P[s].priz & PRIZ_SURFACE)!=(P[s-1].priz & PRIZ_SURFACE))||((P[s].priz & PRIZ_DUPLICATE)!=(P[s-1].priz & PRIZ_DUPLICATE)) )
		 {
		  colorFlag=1;
		  x3dLineColor=P[s].col;
		  if(P[s].priz & PRIZ_SURFACE) x3dLineColor=MainForm1->sur_col;
		  if(P[s].priz & PRIZ_DUPLICATE) x3dLineColor=MainForm1->dup_col;
		  if(stations!=0){ x3dEndPath(f, stations);
		  int n=s;
		  for( s=(n-stations); s < n ; ++s)                              //////           //вывод¤тс¤ все индексы кроме последнего
          if( !((P[s].priz & PRIZ_DUPLICATE) && !(CheckDuplicate->State == cbChecked)) &&
		!((P[s].priz & PRIZ_SURFACE)&& !(CheckSurface->State == cbChecked))   )
	 {
	  if(  (P[s].priz & PRIZ_BRANCH)||(P[s].col!=P[s-1].col)||     //newVetka, color
		   ((P[s].priz & PRIZ_SURFACE)!=(P[s-1].priz & PRIZ_SURFACE))||        //surf
		   ((P[s].priz & PRIZ_DUPLICATE)!=(P[s-1].priz & PRIZ_DUPLICATE)) )      //dupl
	   {
		if(s==0)       // 0-й пикет
		{
		 //wrLineColor = P[0].col; colorFlag=1;
		 //if(P[0].priz & PRIZ_SURFACE) wrLineColor=MainForm1->sur_col;
		 //if(P[0].priz & PRIZ_DUPLICATE) wrLineColor=MainForm1->dup_col;
		 //wrNewPath(f, wrLineColor, colorFlag); colorFlag=0;
		}
		else          // новый цвет
		{
		 if((P[s].col!=P[s-1].col)|| (P[s].priz & 1) ||
		   ((P[s].priz & PRIZ_SURFACE)!=(P[s-1].priz & PRIZ_SURFACE))||((P[s].priz & PRIZ_DUPLICATE)!=(P[s-1].priz & PRIZ_DUPLICATE)) )
		 {
		  //colorFlag=1;
		  //wrLineColor=P[s].col;
		  //if(P[s].priz & PRIZ_SURFACE) wrLineColor=MainForm1->sur_col;
		  //if(P[s].priz & PRIZ_DUPLICATE) wrLineColor=MainForm1->dup_col;
		  //if(stations!=0) vrmlEndPath(f, stations);
		  //stations=0;
		  //wrNewPath(f, wrLineColor, colorFlag); colorFlag=0;
		  if( !(P[s].priz & PRIZ_BRANCH) ) {

		   //vrmlPoint(f, P[s-1].pos/100.0f);
                                                                                  // возможно нужно дл¤ колец
		   //stations++;
		  }
		 }
		}
	   }
	   x3dPoint(f, P[s].pos/100.0f);
	   //stations++;
	}
		  }
		   x3dcoord(f)	;								  //вывод¤тс¤ все индексы кроме последнего
		  stations=0;
		  x3dNewPath(f, x3dLineColor, colorFlag); colorFlag=0;
		  if( !(P[s].priz & PRIZ_BRANCH) ) {
		  // vrmlPoint(f, P[s-1].pos/100.0f);                                            // координаты вроде только колец
		   stations++;
		  }
		 }
		}
	   }
	  // vrmlPoint(f, P[s].pos/100.0f);                                              // все координаты
	   stations++;
	  int n=s;
	}
	}
	int size = MainForm1->Razmer;
		   if(stations!=0){ x3dEndPath(f, stations);
			for(unsigned int s=(size - stations); s < MainForm1->Razmer; ++s) {                             //////           //вывод¤тс¤ все индексы кроме последнего
		  x3dPoint(f, P[s].pos/100.0f);
		  }
	   x3dcoord(f) ;
	   //stations++;
	}



   /*	if( !((P[s].priz & PRIZ_DUPLICATE) && !(CheckDuplicate->State == cbChecked)) &&
		!((P[s].priz & PRIZ_SURFACE)&& !(CheckSurface->State == cbChecked))   )
	 {
	  if(  (P[s].priz & PRIZ_BRANCH)||(P[s].col!=P[s-1].col)||     //newVetka, color
		   ((P[s].priz & PRIZ_SURFACE)!=(P[s-1].priz & PRIZ_SURFACE))||        //surf
		   ((P[s].priz & PRIZ_DUPLICATE)!=(P[s-1].priz & PRIZ_DUPLICATE)) )      //dupl
	   {
		if(s==0)       // 0-й пикет
		{
		 //wrLineColor = P[0].col; colorFlag=1;
		 //if(P[0].priz & PRIZ_SURFACE) wrLineColor=MainForm1->sur_col;
		 //if(P[0].priz & PRIZ_DUPLICATE) wrLineColor=MainForm1->dup_col;
		 //wrNewPath(f, wrLineColor, colorFlag); colorFlag=0;
		}
		else          // новый цвет
		{
		 if((P[s].col!=P[s-1].col)|| (P[s].priz & 1) ||
		   ((P[s].priz & PRIZ_SURFACE)!=(P[s-1].priz & PRIZ_SURFACE))||((P[s].priz & PRIZ_DUPLICATE)!=(P[s-1].priz & PRIZ_DUPLICATE)) )
		 {
		  //colorFlag=1;
		  //wrLineColor=P[s].col;
		  //if(P[s].priz & PRIZ_SURFACE) wrLineColor=MainForm1->sur_col;
		  //if(P[s].priz & PRIZ_DUPLICATE) wrLineColor=MainForm1->dup_col;
		  //if(stations!=0) vrmlEndPath(f, stations);
		  //stations=0;
		  //wrNewPath(f, wrLineColor, colorFlag); colorFlag=0;
		  if( !(P[s].priz & PRIZ_BRANCH) ) {
		   vrmlPoint(f, P[s-1].pos/100.0f);
		   //stations++;
		  }
		 }
		}
	   }
	   vrmlPoint(f, P[s].pos/100.0f);
	   //stations++;
	}
	}                                                                                  // конец for
	if(stations!=0) vrmlEndPath(f, stations);     /*                                   //последний индекс


																							//////////////////////////

	 /* if( !((P[s].priz & PRIZ_DUPLICATE) && !(CheckDuplicate->State == cbChecked)) &&
		!((P[s].priz & PRIZ_SURFACE)&& !(CheckSurface->State == cbChecked))   )
	 {
	  if(  (P[s].priz & PRIZ_BRANCH)||(P[s].col!=P[s-1].col)||     //newVetka, color
		   ((P[s].priz & PRIZ_SURFACE)!=(P[s-1].priz & PRIZ_SURFACE))||        //surf
		   ((P[s].priz & PRIZ_DUPLICATE)!=(P[s-1].priz & PRIZ_DUPLICATE)) )      //dupl
	   {
		if(s==0)       // 0-й пикет
		{
		 wrLineColor = P[0].col; colorFlag=1;
		 if(P[0].priz & PRIZ_SURFACE) wrLineColor=MainForm1->sur_col;
		 if(P[0].priz & PRIZ_DUPLICATE) wrLineColor=MainForm1->dup_col;
		 wrNewPath(f, wrLineColor, colorFlag); colorFlag=0;
		}
		else          // новый цвет
		{
		 if((P[s].col!=P[s-1].col)|| (P[s].priz & 1) ||
		   ((P[s].priz & PRIZ_SURFACE)!=(P[s-1].priz & PRIZ_SURFACE))||((P[s].priz & PRIZ_DUPLICATE)!=(P[s-1].priz & PRIZ_DUPLICATE)) )
		 {
		  colorFlag=1;
		  wrLineColor=P[s].col;
		  if(P[s].priz & PRIZ_SURFACE) wrLineColor=MainForm1->sur_col;
		  if(P[s].priz & PRIZ_DUPLICATE) wrLineColor=MainForm1->dup_col;
		  if(stations!=0) vrmlEndPath(f, stations);                           //вывод¤тс¤ все индексы кроме последнего
		  stations=0;
		  wrNewPath(f, wrLineColor, colorFlag); colorFlag=0;
		  if( !(P[s].priz & PRIZ_BRANCH) ) {
		   //vrmlPoint(f, P[s-1].pos/100.0f);                          // координаты вроде только колец
		   stations++;
		  }
		 }
		}
	   }
	   //vrmlPoint(f, P[s].pos/100.0f);                            // все координаты
	   stations++;
	}
	if(stations!=0) vrmlEndPath(f, stations);    */               // последний индекс

	if (CheckBox1->Checked) {
		OgreCaveDataModel* model = new OgreCaveDataModel(P, size, W, MainForm1->WRazmer, Equates);
		model->setOutputType(OutputType::OT_WALL);
		auto cvp = model->getCaveViewPrefs();
		cvp.wallsTrianglesBlowMode = CM::WTBM_NONE;
		cvp.wallsTrianglesBlowStrength = CM::WTBS_LOW;
		cvp.wallColoringMode = CM::WCM_SMOOTH;
		model->setCaveViewPrefs(cvp);
		model->init();

		const std::vector<CM::OutputPoly>& polygons = model->getOutputPoly(OutputType::OT_WALL);
		std::vector<V3> points;
		std::vector<int> idx;
		std::vector<CM::Color> color;

		auto checkAdd = [&](V3 v, const CM::Color& c) {
			v = model->verticePosToPiketPos(v) / 100;
			for (int i = 0; i < points.size(); i++) {
				if (points[i] == v) {
					idx.push_back(i);
					return;
				}
			}
			points.push_back(v);
			idx.push_back(points.size()-1);
			color.push_back(c);
		} ;

		int i = 0;
		for(const auto& poly : polygons) {
			checkAdd(poly.a, poly.ca);
			checkAdd(poly.c, poly.cb);
			checkAdd(poly.b, poly.cc);
		}
//		x3dPoly(f, color, points, idx);
		for(const auto& poly : polygons) {
			x3dPoly(f, (poly.ca+poly.cb+poly.cc)/3
			, model->verticePosToPiketPos(poly.a) / 100
			, model->verticePosToPiketPos(poly.b) / 100
			, model->verticePosToPiketPos(poly.c) / 100);
		}
		delete model;
	}

	f.flush();

	//--- рамка ---
	x3dAABB(f, V3(xmin, ymin, zmin), V3(xmax, ymax, zmax));
	f.flush();

	//--- View points ---
	x3dViewPoints(f, V3(xmin, ymin, zmin), V3(xmax, ymax, zmax));
	f.flush();

	x3dNavigation(f, (fabs(xmax-xmin)+ fabs(ymax-ymin) + fabs(zmax-zmin))/30 +1);
	f.flush();
	/* wrList->Add(L" WorldInfo {");
	wrList->Add(L"info \"Generated by TOpO\" ");
	wrList->Add(L"title \" cave \" ");
	wrList->Add(L"}"); */

	if(BgColor->ItemIndex==0) x3dBackground(f, 1, 1, 1);
	if(BgColor->ItemIndex==1) x3dBackground(f, 0.7, 0.7, 0.7);
	if(BgColor->ItemIndex==2 || BgColor->ItemIndex==-1) x3dBackground(f, 0, 0, 0);

	f << L"\n";
    f.flush();
	f.close();

	Application->MessageBox(L"Save finished", L"TOpO: Message", MB_OK);
}


  /////////////////////////////////////////////////////////
//------------------------------- save text ---------------------------------
void TExp3dForm::WriteTxt(void)
{
 txtList = new TStringList();
 UnicodeString InfoStr;
 wchar_t  buffer[100];
 int mode = 0;
 if(Txt_Statistics->Checked) {mode=mode+1;}
 if(Txt_Surveys->Checked) {mode=mode+2;};
 MainForm1->AboutCave(mode);
 if(Txt_Loops->Checked) {txtList->Add(L"\r\n"); txtList->Add(TransForm->txtLoops);}
 if(Txt_Stations->Checked)
 {
  txtList->Add(L"\r\n");
  txtList->Add(L"S - номер съемки, B - номер ветки \r\n");
  txtList->Add(L"### \r\n");
  for(unsigned int s=0; s<MainForm1->Razmer; ++s)
  {
  if( !( (P[s].priz & PRIZ_SURFACE) && !(Txt_Surface->Checked) ) &&
      !( (P[s].priz & PRIZ_DUPLICATE) && !(Txt_Duplicate->Checked) ))
   {
   swprintf(buffer,L"% 8.2f N ",(P[s].pos.x/100.0));
   swprintf(buffer+wcslen(buffer),L"% 8.2f E ",(P[s].pos.y/100.0));
   swprintf(buffer+wcslen(buffer),L"% 8.2f H" ,(P[s].pos.z/100.0));
   swprintf(buffer+wcslen(buffer),L"  ");
   InfoStr = buffer;
   InfoStr = InfoStr + L" S" +  UnicodeString(P[s].srv);
   InfoStr = InfoStr + L" B" +  UnicodeString(P[s].vet);
   if(P[s].met>0)                // показ меток
     InfoStr = InfoStr + L" " + StationList->Strings[P[s].met];
    else
     InfoStr = InfoStr + L" - ";
   if(P[s].priz & PRIZ_ENTRY) InfoStr += L" #ent";
    if(P[s].priz & PRIZ_SIPHON) InfoStr += L" #sump";
   if(P[s].com>0)                // показ комментари¤
     InfoStr = InfoStr + L" #com " + UnicodeString(MainForm1->Name(Comments, P[s].com).c_str() );
   txtList->Add(InfoStr);
   InfoStr = L"";
   }
  }
 }

//--- Save ---
 UnicodeString txtFile = ExtractFileName(MainForm1->CurrFileName);
 txtFile.Delete( txtFile.LastDelimiter(L"."), txtFile.Length()+1-txtFile.LastDelimiter(L"."));

 UnicodeString message = L"Data will be save as " + txtFile+ L".txt";
 int button = Application->MessageBox(message.c_str(), L"TOpO: Message", MB_OKCANCEL);
 if (button != IDCANCEL) txtList->SaveToFile(txtFile+".txt");

 if(txtList!=NULL) delete txtList;
}

//---------------------------------------------------------------------------
void __fastcall TExp3dForm::OKClick(TObject *Sender)
{
 if(PageCtr->ActivePage==TabSheet1) {
 if(ComboBox3->ItemIndex==0) WriteVrml();
 if(ComboBox3->ItemIndex==1) Writex3d();
 }
 if(PageCtr->ActivePage==TabSheet2) WriteTxt();

}
//---------------------------------------------------------------------------



