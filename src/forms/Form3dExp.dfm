object Exp3dForm: TExp3dForm
  AlignWithMargins = True
  Left = 500
  Top = 500
  Margins.Left = 100
  Margins.Top = 100
  Align = alCustom
  BorderStyle = bsDialog
  Caption = 'TOpO - 3D Export'
  ClientHeight = 328
  ClientWidth = 371
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesigned
  PixelsPerInch = 96
  TextHeight = 13
  object Cancel: TButton
    Left = 280
    Top = 288
    Width = 75
    Height = 25
    Caption = #1054#1090#1084#1077#1085#1072
    TabOrder = 0
    OnClick = CancelClick
  end
  object OK: TButton
    Left = 184
    Top = 288
    Width = 75
    Height = 25
    Caption = #1069#1082#1089#1087#1086#1088#1090
    TabOrder = 1
    OnClick = OKClick
  end
  object PageCtr: TPageControl
    Left = 0
    Top = 0
    Width = 371
    Height = 273
    ActivePage = TabSheet1
    Align = alCustom
    Anchors = [akLeft, akTop, akRight]
    MultiLine = True
    TabOrder = 2
    object TabSheet1: TTabSheet
      Caption = 'vrml/ x3d'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Label1: TLabel
        Left = 336
        Top = 280
        Width = 32
        Height = 13
        Caption = 'Label1'
      end
      object Options: TGroupBox
        Left = 16
        Top = 16
        Width = 329
        Height = 209
        Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1080
        TabOrder = 0
        object Label2: TLabel
          Left = 160
          Top = 48
          Width = 23
          Height = 13
          Caption = #1060#1086#1085
        end
        object Label3: TLabel
          Left = 16
          Top = 24
          Width = 86
          Height = 13
          Caption = #1069#1082#1089#1087#1086#1088#1090#1080#1088#1086#1074#1072#1090#1100':'
          WordWrap = True
        end
        object Label4: TLabel
          Left = 160
          Top = 24
          Width = 28
          Height = 13
          Caption = #1062#1074#1077#1090':'
        end
        object Label5: TLabel
          Left = 160
          Top = 80
          Width = 32
          Height = 13
          Caption = #1051#1080#1085#1080#1080
        end
        object Label10: TLabel
          Left = 160
          Top = 113
          Width = 42
          Height = 13
          Caption = #1060#1086#1088#1084#1072#1090
        end
        object BgColor: TComboBox
          Left = 232
          Top = 40
          Width = 81
          Height = 21
          TabOrder = 0
          Text = #1063#1077#1088#1085#1099#1077
          Items.Strings = (
            #1041#1077#1083#1099#1081
            #1057#1077#1088#1099#1081
            #1063#1077#1088#1085#1099#1081)
        end
        object CheckSurface: TCheckBox
          Left = 16
          Top = 48
          Width = 97
          Height = 17
          Caption = #1056#1077#1083#1100#1077#1092
          Checked = True
          State = cbChecked
          TabOrder = 1
        end
        object CheckDuplicate: TCheckBox
          Left = 16
          Top = 80
          Width = 145
          Height = 17
          Caption = #1044#1091#1073#1083#1080#1088#1091#1102#1097#1080#1077' '#1089#1098#1077#1084#1082#1080
          Checked = True
          State = cbChecked
          TabOrder = 2
        end
        object vrml_lines: TComboBox
          Left = 232
          Top = 72
          Width = 81
          Height = 21
          TabOrder = 3
          Text = #1062#1074#1077#1090#1085#1099#1077
          Items.Strings = (
            #1062#1074#1077#1090#1085#1099#1077
            #1063#1077#1088#1085#1099#1077)
        end
        object CheckBox1: TCheckBox
          Left = 16
          Top = 112
          Width = 145
          Height = 17
          Caption = #1054#1073#1098#1077#1084
          Checked = True
          State = cbChecked
          TabOrder = 4
        end
        object ComboBox3: TComboBox
          Left = 232
          Top = 110
          Width = 80
          Height = 21
          ItemIndex = 0
          TabOrder = 5
          Text = 'vrml'
          Items.Strings = (
            'vrml'
            'x3d')
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'txt'
      ImageIndex = 1
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object OptionsTxt: TGroupBox
        Left = 16
        Top = 16
        Width = 329
        Height = 209
        Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1080
        TabOrder = 0
        object Txt_Statistics: TCheckBox
          Left = 16
          Top = 32
          Width = 153
          Height = 17
          Caption = #1057#1090#1072#1090#1080#1089#1090#1080#1082#1072' '#1087#1086' '#1087#1077#1097#1077#1088#1077
          Checked = True
          State = cbChecked
          TabOrder = 0
        end
        object Txt_Surveys: TCheckBox
          Left = 16
          Top = 56
          Width = 113
          Height = 17
          Caption = #1044#1072#1085#1085#1099#1077' '#1089#1098#1077#1084#1086#1082
          Checked = True
          State = cbChecked
          TabOrder = 1
        end
        object Txt_Loops: TCheckBox
          Left = 16
          Top = 80
          Width = 169
          Height = 17
          Caption = #1044#1072#1085#1085#1099#1077' '#1087#1086' '#1082#1086#1083#1100#1094#1072#1084
          Checked = True
          State = cbChecked
          TabOrder = 2
        end
        object Txt_Stations: TCheckBox
          Left = 16
          Top = 104
          Width = 97
          Height = 17
          Caption = #1055#1080#1082#1077#1090#1099
          Checked = True
          State = cbChecked
          TabOrder = 3
          OnClick = Txt_StationsClick
        end
        object Txt_Surface: TCheckBox
          Left = 32
          Top = 128
          Width = 97
          Height = 17
          Caption = 'GPS '#1084#1077#1090#1082#1080
          TabOrder = 4
        end
        object Txt_Duplicate: TCheckBox
          Left = 32
          Top = 152
          Width = 193
          Height = 17
          Caption = #1044#1091#1073#1083#1080#1088#1091#1102#1097#1080#1077' '#1089#1098#1077#1084#1082#1080
          TabOrder = 5
        end
      end
    end
  end
  object SaveDialog1: TSaveDialog
    Filter = 'All|*.*'
    Title = 'Save track'
    Left = 104
    Top = 296
  end
end
