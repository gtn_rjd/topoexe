﻿//---------------------------------------------------------------------------

#ifndef Form3dExpH
#define Form3dExpH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include <Vcl.Dialogs.hpp>
//---------------------------------------------------------------------------
class TExp3dForm : public TForm
{
__published:	// IDE-managed Components
        TButton *Cancel;
        TButton *OK;
	TPageControl *PageCtr;
	TTabSheet *TabSheet1;
	TLabel *Label1;
	TGroupBox *Options;
	TLabel *Label2;
	TLabel *Label3;
	TLabel *Label4;
	TLabel *Label5;
	TLabel *Label10;
	TComboBox *BgColor;
	TCheckBox *CheckSurface;
	TCheckBox *CheckDuplicate;
	TComboBox *vrml_lines;
	TCheckBox *CheckBox1;
	TComboBox *ComboBox3;
	TTabSheet *TabSheet2;
	TGroupBox *OptionsTxt;
	TCheckBox *Txt_Statistics;
	TCheckBox *Txt_Surveys;
	TCheckBox *Txt_Loops;
	TCheckBox *Txt_Stations;
	TCheckBox *Txt_Surface;
	TCheckBox *Txt_Duplicate;
	TSaveDialog *SaveDialog1;
		void __fastcall CancelClick(TObject *Sender);
		void __fastcall OKClick(TObject *Sender);
        void __fastcall Txt_StationsClick(TObject *Sender);
private:	// User declarations
//        void wrNewPath(int Ncol, bool colorFlag);
//        void wrEndPath(int a);
//        void wrVPoint(void);
public:		// User declarations
        __fastcall TExp3dForm(TComponent* Owner);
        void MinMax3D();
		void WriteVrml();
		void Writex3d();
		void WriteTxt();
        double xmin, ymin, zmin, xmax, ymax, zmax;
        TStringList* wrList;      // текст для сохранения в vrml
        TStringList* txtList;     // текст для сохранения в txt
		TStringList* x3dList;
		};

//---------------------------------------------------------------------------
extern PACKAGE TExp3dForm *Exp3dForm;
//---------------------------------------------------------------------------
#endif

