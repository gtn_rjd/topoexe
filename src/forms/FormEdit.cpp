﻿//---------------------------------------------------------------------------
#include <vcl.h>
#include <Printers.hpp>
#include <SysUtils.hpp>
#include <dir.h>
#include <algorithm>
#pragma hdrstop

#include "FormEdit.h"
#include "FormHelp.h"
#include "TOpO.h"
#include "UnitInsertNewPiket.h"
#include "FormInsSurv.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"

TEditForm *EditForm;
UnicodeString FFileName;
boolean edit_flag;

#define RETURN_LENGTH 1

void LoadFromFileUTF8Test(TStrings * list, UnicodeString fileName);

//---------------------------------------------------------------------------
char TEditForm::Confirm(void)
{
 int button = 0;
 if (EditForm->RichEdit1->Modified == true)
 {
  EditForm->WindowState=wsNormal; //восстановить (если свернуто)
  EditForm->Show();               //и показать
  button = Application->MessageBox(L"File has been modified. Save?",
                                   L"TOpO Edit: Confirm", MB_YESNOCANCEL);
  if (button == IDNO) {RichEdit1->Modified = false; return 0;}
  if (button == IDYES) {
   if(FFileName!=L"new file") EditForm->RichEdit1->Lines->SaveToFile(FFileName);
    else EditForm->Saveas1Click(EditForm); RichEdit1->Modified = false; return 1;}
  if (button == IDCANCEL) return 2;
 }
 return 3; //если нет изменений, ничего не происходит
}
//---------------------------------------------------------------------------

void TEditForm::SetFileName(const UnicodeString FileName)
{
 Caption = L"TOpO Edit - " + ExtractFileName(FileName);
 StatusBar1->Panels->Items[1]->Text = FileName;
 FFileName=FileName;
 UndoButton->Enabled=false;
 EditForm->RichEdit1->Modified = false;
 updateCanOpenMainFile();
}
//---------------------------------------------------------------------------
__fastcall TEditForm::TEditForm(TComponent* Owner)
        : TForm(Owner)
{
 RichEdit1->PlainText = true;
 RichEdit1->Modified = false;
}

//**************************** Menu click ************************************

//------------------------------- Help ---------------------------------------
void __fastcall TEditForm::C1Click(TObject *Sender)
 { HelpForm->SetHelpNum(1);  HelpForm->Show(); }
void __fastcall TEditForm::Data1Click(TObject *Sender)
 { HelpForm->SetHelpNum(2);  HelpForm->Show(); }
void __fastcall TEditForm::Commands1Click(TObject *Sender)
 { HelpForm->SetHelpNum(3);  HelpForm->Show(); }
void __fastcall TEditForm::Corrections1Click(TObject *Sender)
 { HelpForm->SetHelpNum(4);  HelpForm->Show(); }
void __fastcall TEditForm::LoopsClick(TObject *Sender)
 { HelpForm->SetHelpNum(7);  HelpForm->Show(); }
void __fastcall TEditForm::ProfileClick(TObject *Sender)
 { HelpForm->SetHelpNum(8);  HelpForm->Show(); }

//------------------------------- Edit ---------------------------------------
void __fastcall TEditForm::Cut1Click(TObject *Sender)
 { RichEdit1->CutToClipboard(); }
void __fastcall TEditForm::Copy1Click(TObject *Sender)
 { RichEdit1->CopyToClipboard(); }
void __fastcall TEditForm::Paste1Click(TObject *Sender)
 { RichEdit1->PasteFromClipboard(); }
void __fastcall TEditForm::SelectAll1Click(TObject *Sender)
 { RichEdit1->SelectAll(); }

//------------------------------- File (New)---------------------------------
void __fastcall TEditForm::New1Click(TObject *Sender)
{
 if(Confirm() == 2) return;
 EditForm->RichEdit1->Text=L"";
 SetFileName(L"new file");
 RichEdit1->Modified = false;
}
//------------------------------- File (Open) -------------------------------
void __fastcall TEditForm::Open1Click(TObject *Sender)
{
 if(Confirm() == 2) return;
   if(EditForm->OpenDialog1->Execute()){
	  wchar_t open_dir[MAXPATH];
	  _wgetcwd(open_dir,MAXPATH);
	  OpenDialog1->InitialDir=open_dir;
	  LoadFromFileUTF8Test(EditForm->RichEdit1->Lines, OpenDialog1->FileName);
	  SetFileName(OpenDialog1->FileName);
	  RichEdit1->Modified = false;

	  FoundComentars( );

	  edit_flag=0;
	}
}
//------------------------------- File (Save as) ----------------------------
void __fastcall TEditForm::Saveas1Click(TObject *Sender)
{
  if(EditForm->SaveDialog1->Execute())
  {
      RichEdit1->Lines->SaveToFile(SaveDialog1->FileName);
      SetFileName(SaveDialog1->FileName);
      RichEdit1->Modified = false;
      if(MainForm1->State==STATE_EDIT_NEW)
      {
       MainForm1->SetState(STATE_OPENED);
       MainForm1->CurrFileName=SaveDialog1->FileName;
      }
  }
}
//------------------------------- File (Save) -------------------------------
void __fastcall TEditForm::Save1Click(TObject *Sender)
{
  if(FFileName!=L"new file") {                                  
   RichEdit1->Lines->SaveToFile(FFileName);
   RichEdit1->Modified = false; }
  else EditForm->Saveas1Click(EditForm);
}
//------------------------------- File (Print) -------------------------------
void __fastcall TEditForm::Print1Click(TObject *Sender)
{
  if(PrintDialog1->Execute())
     EditForm->RichEdit1->Print(L"Form1->Caption");
}
//------------------------------- File (Exit) --------------------------------
void __fastcall TEditForm::Exit1Click(TObject *Sender)
{
  EditForm->Close();
}
//---------------------------------- Close -----------------------------------

void __fastcall TEditForm::FormCloseQuery(TObject *Sender, bool &CanClose)
{
  FindDialog1->CloseDialog();
  if(Confirm() == 2) CanClose = false;
}
//----------------------------------------------------------------------------
//******************************* Decode *************************************
void decode( bool dir)// false for dos->win, true for win->dos
{
 unsigned wchar_t No = 0;
 unsigned wchar_t Nop = 0;
 UnicodeString Buff;
 Buff = EditForm->RichEdit1->Text;
 if(dir == false)
   for(int s=1; s < EditForm->RichEdit1->Text.Length(); ++s){
    No = Buff[s];
    if(No > 127) {    //866 -> 1251
     if(No<176)           Nop=64;
     if(No>223 && No<240) Nop=16;
     if(No>175 && No<224) Nop=-48;
     if(No>239)           Nop=-64;
     No=(unsigned wchar_t)(No+Nop);
     Buff.Delete(s,1);
     Buff.Insert(CHAR(No),s);
  //    EditForm->Edit2->Text= Buff[16];
     }
   }
   else
   for(int s=1; s < EditForm->RichEdit1->Text.Length(); ++s){
    No = Buff[s];
    if(No > 127) {    //1251 -> 866
     if(No>191 && No<240)       Nop=64;
     if(No>223+16) Nop=16;
     if(No>175-48 && No<224-48) Nop=-48;
     if(No>239-64 && No<192)    Nop=-64;
     No=(unsigned wchar_t)(No-Nop);
     Buff.Delete(s,1);
     Buff.Insert(CHAR(No),s);
     }
   }
  EditForm->RichEdit1->Text = Buff;
  EditForm->UndoButton->Enabled=false;
}
//----------------------------------------------------------------------------
void __fastcall TEditForm::Decode1Click(TObject *Sender) { decode(false); }
void __fastcall TEditForm::Decode2Click(TObject *Sender) { decode(true);  }

//---------------------------- Line & Position -------------------------------
void __fastcall TEditForm::RichEdit1SelectionChange(TObject *Sender)
{
//RichEdit1->SelAttributes->Color = clBlue;

 UnicodeString Position = L"line ";  Position += RichEdit1->CaretPos.y;
 Position += L" : ";  Position += RichEdit1->CaretPos.x;
 StatusBar1->Panels->Items[0]->Text = Position;
}

//***************************** Undo/Redo ************************************
void __fastcall TEditForm::UndoButtonClick(TObject *Sender)
{
 RichEdit1->Undo();
 UndoButton->Enabled=false;
 RedoButton->Enabled=true;
}
//---------------------------------------------------------------------------
void __fastcall TEditForm::RichEdit1Change(TObject *Sender)
{
 UndoButton->Enabled=true;
 RedoButton->Enabled=false;
}
//---------------------------------------------------------------------------
void __fastcall TEditForm::RedoButtonClick(TObject *Sender)
{
 RichEdit1->Undo();
 UndoButton->Enabled=true;
 RedoButton->Enabled=false;
}

//******************************* Find ***************************************
void __fastcall TEditForm::ToolButton10Click(TObject *Sender)
{
  FindDialog1->Position = Point(RichEdit1->Left /*- FindDialog1->Width*/, RichEdit1->Top);

  FindDialog1->Options << frHideUpDown;
  FindDialog1->Execute();
}
//---------------------------------------------------------------------------
void __fastcall TEditForm::FindDialog1Find(TObject *Sender)
{
  //bool FlagDown=FindDialog1->Options.Contains(frDown);

  int FoundAt = 0;
  int StartPos = 0;
  int ToEnd = 0;
  // begin the search after the current selection
  // if there is one 
  // otherwise, begin at the start of the text
  if (RichEdit1->SelLength)    //Read SelLength to determine the length, in bytes, of the selected text.
  {//т.е. мы уже заходим сюда повторно

     StartPos = RichEdit1->SelStart + RichEdit1->SelLength;
  }
  else
  { StartPos = 0;

  }
  // ToEnd is the length from StartPos to the end of the text in the rich edit control
    ToEnd = RichEdit1->Text.Length() - StartPos;
    FoundAt = RichEdit1->FindText(FindDialog1->FindText, StartPos, ToEnd, TSearchTypes()<< stMatchCase);

  if (FoundAt != -1)
  {
    RichEdit1->SetFocus();
    RichEdit1->SelStart = FoundAt;
    RichEdit1->SelLength = FindDialog1->FindText.Length();
  }
  else ShowMessage(L"Not found...");
}
//---------------------------- Rebuild & View --------------------------------

void __fastcall TEditForm::ToolButton13Click(TObject *Sender)
{
 EditForm->Save1Click(Sender);
 MainForm1->Show();
 MainForm1->BuildClick(Sender);
 EditForm->Show();
 if(MainForm1->State!=0)
 MainForm1->Show();
}
//----------------------------------------------------------------------------

void __fastcall TEditForm::ToolButton14Click(TObject *Sender)
{
 MainForm1->Show();
}
//---------------------------------------------------------------------------
 
void __fastcall TEditForm::PopapMenu_InsertLineClick(TObject *Sender)
{
 FormInsertNewPiket->ShowModal();
 //int X=RichEdit1->CaretPos.x, Y=RichEdit1->CaretPos.y;
}
//---------------------------------------------------------------------------

void __fastcall TEditForm::PopapMenu_InsertComand(UnicodeString Str)
{
 UnicodeString CurrStr=RichEdit1->Lines->Strings[RichEdit1->CaretPos.y];
            CurrStr.Insert(Str,RichEdit1->CaretPos.x+1);
 RichEdit1->Lines->Strings[RichEdit1->CaretPos.y]=CurrStr;
}

//---------------------------------------------------------------------------
void __fastcall TEditForm::InsertComand(UnicodeString Str, int CaretPos_x, int CaretPos_y)
{
 UnicodeString CurrStr=RichEdit1->Lines->Strings[CaretPos_y];
            CurrStr.Insert(Str,CaretPos_x+1);
 RichEdit1->Lines->Strings[CaretPos_y]=CurrStr;
}

//---------------------------- Insert #Color[]------------------------------

void __fastcall TEditForm::PopapMenu_InsertColor_BlackClick(TObject *Sender)
 {PopapMenu_InsertComand(L" #color[0] ");}
void __fastcall TEditForm::PopapMenu_InsertColor_BlueClick(TObject *Sender)
 {PopapMenu_InsertComand(L" #color[1] ");}
void __fastcall TEditForm::PopapMenu_InsertColor_GreenClick(TObject *Sender)
 {PopapMenu_InsertComand(L" #color[2] ");}
void __fastcall TEditForm::PopapMenu_InsertColor_CyanClick(TObject *Sender)
 {PopapMenu_InsertComand(L" #color[3] ");}
void __fastcall TEditForm::PopapMenu_InsertColor_RedClick(TObject *Sender)
 {PopapMenu_InsertComand(L" #color[4] ");}
void __fastcall TEditForm::PopapMenu_InsertColor_MagentaClick(TObject *Sender)
 {PopapMenu_InsertComand(L" #color[5] ");}
void __fastcall TEditForm::PopapMenu_InsertColor_BrownClick(TObject *Sender)
 {PopapMenu_InsertComand(L" #color[6] ");}
void __fastcall TEditForm::PopapMenu_InsertColor_LightGrayClick(TObject *Sender)
 {PopapMenu_InsertComand(L" #color[7] ");}
void __fastcall TEditForm::PopapMenu_InsertColor_DarkGrayClick(TObject *Sender)
 {PopapMenu_InsertComand(L" #color[8] ");}
void __fastcall TEditForm::PopapMenu_InsertColor_LightBlueClick(TObject *Sender)
 {PopapMenu_InsertComand(L" #color[9] ");}
void __fastcall TEditForm::PopapMenu_InsertColor_LightGreenClick(TObject *Sender)
 {PopapMenu_InsertComand(L" #color[10] ");}
void __fastcall TEditForm::PopapMenu_InsertColor_LightCyanClick(TObject *Sender)
 {PopapMenu_InsertComand(L" #color[11] ");}
void __fastcall TEditForm::PopapMenu_InsertColor_LightRedClick(TObject *Sender)
 {PopapMenu_InsertComand(L" #color[12] ");}
void __fastcall TEditForm::PopapMenu_InsertColor_LightMagentaClick(TObject *Sender)
 {PopapMenu_InsertComand(L" #color[13] ");}
void __fastcall TEditForm::PopapMenu_InsertColor_YellowClick(TObject *Sender)
 { PopapMenu_InsertComand(L" #color[14] ");}
void __fastcall TEditForm::PopapMenu_InsertColor_WhiteClick(TObject *Sender)
 { PopapMenu_InsertComand(L" #color[15] ");}
void __fastcall TEditForm::PopapMenu_InsertColor_OrangeClick(TObject *Sender)
 { PopapMenu_InsertComand(L" #color[16] ");}
//---------------------------------------------------------------------------
/* #surface        : начало поверхностной съемки
 #end_surface    : конец         -//-
 #magnet[x]      : устанавливает магнитное склонение x градусов
 #end_magnet     : устанавливает магнитное склонение 0 градусов
 #survey         : имена топосъемщиков, дата съемки            
 #truth[n]       : точность съемки (см. раздел "Loops")      
 #exit           : не обрабатывать все последующие пикеты до конца файла
 #RR,#R0,#R180,#PR[n] : см. раздел Proj. Profile 
      в том числе команды, задающие формат ввода данных          
 #label          : ввод данных без номеров пикетов (по умолчанию)
 #number         : ввод данных с номерами пикетов
 #angle          : режим с вводом угла наклона (по умолчанию)
 #fall           : режим с вводом перепада высот
 #data_order L Az An : устанавливает порядок ввода (аргументы -
                   L, Az, An или D, подробнее см. раздел Data

    Команды, относящиеся к пещере в целом

 #include filename : включить файл filename в список транслируемых
 #fix ^label     : фиксирует нулевые координаты точки с меткой ^label
 #cave cavename  : cavename - название пещеры
 #region region  : регион
 #sur_color[n]   : цвет линий поверхностной съемки [1]

 [1] Длина поверхностной съемки не входит в высчитываемые характеристики
     пещеры; по умолчанию цвет линий поверхностной съемки - DarkGray.
 [2] Эти команды действуют начиная со строки, в которой стоят.
     Автоматический переход в режим по умолчанию - в конце каждого
     файла данных, за исключением магнитного склонения, которое
     устанавливается в ноль только командой #end_magnet, и команд
     построения разрезаева-развертки./**/

//************************** Insert Commands *********************************
//------------------------ General ommands ----------------------------------
void __fastcall TEditForm::N_General_cavenameClick(TObject *Sender)
{PopapMenu_InsertComand(L" #cave ... ");}   //cavename - название пещеры
void __fastcall TEditForm::N_General_RegionClick(TObject *Sender)
{PopapMenu_InsertComand(L" #region ... ");}   //регион
void __fastcall TEditForm::declination1Click(TObject *Sender)
{PopapMenu_InsertComand(L" #declination[...] ");}   //магн. склонение
//---------------------------------------------------------------------------
void __fastcall TEditForm::N_General_SurvTeamClick(TObject *Sender)
{InsSurvForm->ShowModal();}
void __fastcall TEditForm::N_general_EndSurveyClick(TObject *Sender)
{PopapMenu_InsertComand(L"#end_survey ");}
//---------------------------------------------------------------------------
void __fastcall TEditForm::N_General_includeClick(TObject *Sender)
{PopapMenu_InsertComand(L"#include ");}  //#include
void __fastcall TEditForm::N_General_FixClick(TObject *Sender)
{PopapMenu_InsertComand(L"#fix ");}  //#fix
//---------------------------------------------------------------------------
void __fastcall TEditForm::prefix1Click(TObject *Sender)
{PopapMenu_InsertComand(L"#prefix ");}  //#prefix
void __fastcall TEditForm::endprefix1Click(TObject *Sender)
{PopapMenu_InsertComand(L"#end_prefix ");}  //#end_#prefix

//------------------------- Format Commands ---------------------------------
void __fastcall TEditForm::PopapMenu_InsertFormat_StandartClick(TObject *Sender)
{PopapMenu_InsertComand(L" #angle ");}
void __fastcall TEditForm::PopapMenu_InsertFormat_FallClick(TObject *Sender)
{PopapMenu_InsertComand(L" #fall ");}
void __fastcall TEditForm::PopapMenu_InsertFormat_FromToClick(TObject *Sender)
{PopapMenu_InsertComand(L"#from_to ");}  //#from_to сплошная нумерация
void __fastcall TEditForm::PopapMenu_InsertFormat_LabelClick(TObject *Sender)
{PopapMenu_InsertComand(L"#label ");}  // #label   метки
//---------------------------------------------------------------------------

//------------------------- Extended Profile Commands ------------------------
void __fastcall TEditForm::PopapMenu_InsertRazvertka_0Click(TObject *Sender)
{PopapMenu_InsertComand(L" #R0 ");}
void __fastcall TEditForm::PopapMenu_InsertRazvertka_180Click(TObject *Sender)
{PopapMenu_InsertComand(L" #R180 ");}
void __fastcall TEditForm::PopapMenu_InsertRazvertka_RRClick(TObject *Sender)
{PopapMenu_InsertComand(L" #RR ");}  //#RR изменить на противоположное

//------------------------ Surface & Duplicate Commands ---------------------
void __fastcall TEditForm::N_surf1Click(TObject *Sender)
{PopapMenu_InsertComand(L" #surface ");}
void __fastcall TEditForm::N_surf2Click(TObject *Sender)
{PopapMenu_InsertComand(L" #end_surface ");}
void __fastcall TEditForm::N_surf3Click(TObject *Sender)
{PopapMenu_InsertComand(L" #sur_color[...] ");}
//---------------------------------------------------------------------------
void __fastcall TEditForm::N_dup1Click(TObject *Sender)
{PopapMenu_InsertComand(L" #duplicate ");}
void __fastcall TEditForm::N_dup2Click(TObject *Sender)
{PopapMenu_InsertComand(L" #end_duplicate ");}
void __fastcall TEditForm::N_dup3Click(TObject *Sender)
{PopapMenu_InsertComand(L" #dup_color[...] ");}
//---------------------------------------------------------------------------

//--------------------------- Calibration Commands --------------------------
void __fastcall TEditForm::Az1_begClick(TObject *Sender)
{PopapMenu_InsertComand(L" #corr_A_g ");}  //начать коррекцию азимута (под горный компас)
void __fastcall TEditForm::Az1_endClick(TObject *Sender)
{PopapMenu_InsertComand(L" #end_corr_A_g ");}  //End коррекцию азимута (под горный компас)
//---------------------------------------------------------------------------
void __fastcall TEditForm::Az2_begClick(TObject *Sender)
{PopapMenu_InsertComand(L" #corr_A_180 ");}  // начать коррекцию азимута (на обратный азимут)
void __fastcall TEditForm::Az2_endClick(TObject *Sender)
{PopapMenu_InsertComand(L" #end_corr_A_180 ");}  // End коррекцию азимута (на обратный азимут)
//---------------------------------------------------------------------------
void __fastcall TEditForm::Azn_begClick(TObject *Sender)
{PopapMenu_InsertComand(L" #corr_A[...] ");} // начать коррекцию азимута на ... градусов
void __fastcall TEditForm::Azn_endClick(TObject *Sender)
{PopapMenu_InsertComand(L" #end_corr_A ");}  // End коррекцию азимута на ... градусов
//---------------------------------------------------------------------------
void __fastcall TEditForm::Lx1_begClick(TObject *Sender)
{PopapMenu_InsertComand(L" #corr_L_beg[...] ");} // сдвинуть начало отсчета на ... единиц длины
void __fastcall TEditForm::Lx1_endClick(TObject *Sender)
{PopapMenu_InsertComand(L" #end_corr_L_beg ");}  // End сдвинуть начало отсчета на ... единиц длины
//---------------------------------------------------------------------------
void __fastcall TEditForm::Lx2_begClick(TObject *Sender)
{PopapMenu_InsertComand(L" #corr_L[...] ");}  // начать коррекцию единицы длины в ... раз
void __fastcall TEditForm::Lx2_endClick(TObject *Sender)
{PopapMenu_InsertComand(L" #end_corr_L ");}   // End коррекцию единицы длины в ... раз
//---------------------------------------------------------------------------
void __fastcall TEditForm::Angle1_begClick(TObject *Sender)
{PopapMenu_InsertComand(L" #corr_clino[...] ");} // начать коррекцию угла наклона на ... градусов
void __fastcall TEditForm::Angle1_endClick(TObject *Sender)
{PopapMenu_InsertComand(L" #end_corr_clino ");}  // End  коррекцию угла наклона на ... градусов
//---------------------------------------------------------------------------
void __fastcall TEditForm::Az_endClick(TObject *Sender)
{PopapMenu_InsertComand(L" #end_corr_A ");}     // кончить коррекцию азимута
void __fastcall TEditForm::Az_L_Angle_endClick(TObject *Sender)
{PopapMenu_InsertComand(L" #end_corr ");}     // кончить коррекцию азимута, длины и угла

//-------------------------- Data Order --------------------------------------
void __fastcall TEditForm::DataOrder_L_Az_AnClick(TObject *Sender)
{PopapMenu_InsertComand(L" #data_order L Az An ");}   //порядок забивания данных
void __fastcall TEditForm::DataOrder_Az_L_AnClick(TObject *Sender)
{PopapMenu_InsertComand(L" #data_order Az L An ");}   //порядок забивания данных
void __fastcall TEditForm::DataOrder_L_An_AzClick(TObject *Sender)
{PopapMenu_InsertComand(L" #data_order L An Az ");}   //порядок забивания данных
void __fastcall TEditForm::DataOrder_An_L_AzClick(TObject *Sender)
{PopapMenu_InsertComand(L" #data_order An L Az ");}   //порядок забивания данных
void __fastcall TEditForm::DataOrder_An_Az_LClick(TObject *Sender)
{PopapMenu_InsertComand(L" #data_order An Az L ");}   //порядок забивания данных
void __fastcall TEditForm::DataOrder_Az_An_LClick(TObject *Sender)
{PopapMenu_InsertComand(L" #data_order Az An L ");}   //порядок забивания данных
void __fastcall TEditForm::Data_order_LAzAnlrudClick(TObject *Sender)
{PopapMenu_InsertComand(L" #data_order L Az An l r u d");}
//---------------------------------------------------------------------------

//-------------------------- Station features Commands ----------------------
void __fastcall TEditForm::N2equate1Click(TObject *Sender)
{PopapMenu_InsertComand(L"#equate ");}  //#equate
//---------------------------------------------------------------------------
void __fastcall TEditForm::NPiketPropert_entClick(TObject *Sender)
{PopapMenu_InsertComand(L" #ent ");}  //вход (зеленый кружок)
void __fastcall TEditForm::NPiketPropert_sifClick(TObject *Sender)
{PopapMenu_InsertComand(L" #sif ");}   //сифон (голубой кружок)
void __fastcall TEditForm::NPiketPropert_redClick(TObject *Sender)
{PopapMenu_InsertComand(L" #red ");}   //красный кружок
void __fastcall TEditForm::NPiketPropert_comClick(TObject *Sender)
{PopapMenu_InsertComand(L" #com ");}   //комментарий (до конца строки)
void __fastcall TEditForm::NPiketPropert_endlabelClick(TObject *Sender)
{ PopapMenu_InsertComand(L" #end ");}   //конец ветки (в режиме label)

//---------------------------------------------------------------------------

int special_min(int a1, int a2)
{//процедура специального минимума, чтобы найти наименьшее ненулевое число из двух....
  if(a1+a2==0) return 0;//т.е. оба нули
  if(a1*a2>0)//т.е. оба не нули
  {
    if(a1<a2) return a1;  else return a2;
  }
  else //т.е. один из них-- ноль, другой--  нет
    if(a1!=0) return a1;  else return a2;
}
//---------------------------------------------------------------------------
bool __fastcall TEditForm::FoundNearestString(UnicodeString Str, int & iy, int& iPos)
{//iPos-- показывает позицию начала вхождения символа (начиная с нуля); iy-- номер рядка

  iPos=-1;  iy=-1;

   int CurrPos = 0;


   UnicodeString CurrStr, NewStr;
   int i = 0;
   for(i=RichEdit1->CaretPos.y; i>=0; i--)
   {
      iPos = -1;

      if(i==RichEdit1->CaretPos.y)
            CurrStr=RichEdit1->Lines->Strings[i].SubString(1, RichEdit1->CaretPos.x);
      else  CurrStr=RichEdit1->Lines->Strings[i];


      int FirstPos=CurrStr.Pos(Str),PosComent;
      if(FirstPos>0)
      { //т.е. нашли входжение нужной строки
        //в текущей строке может быть несколько таких входжений. Мы нашли первое, а надо последнее

          //Найдем в данной строке позицию символа-комментария:
          PosComent=special_min(CurrStr.Pos(L";"),CurrStr.Pos(L"%"));
          if(PosComent>0)//тобто комментарий существует
               //тогда ищем ПОСЛЕДНЕЕ вхождение нашей строки в промежутке до комментария:
               NewStr=CurrStr.SubString(1, PosComent-1);
          else NewStr=CurrStr; //т.е. комментария в этой строке нету. Тогда ничего не обрезаем справа.

          //в текущей строке NewStr может быть несколько входжений Str. Мы нашли первое, а надо последнее


          while((CurrPos=NewStr.Pos(Str))>0)
          {
            //запишем позицию вхождения:
              iPos+=CurrPos;
            //обрезаем:
            NewStr=NewStr.SubString(CurrPos+1, NewStr.Length()-CurrPos);
          }

      }

      if(iPos>=0) { iy=i; return true;}
   }
 //если попали сюда, то значит перебрали все строчки аж до начала и нужного вхождения не нашли...


return true;
}

//---------------------------------------------------------------------------
bool FindStringLocation
            (TRichEdit *RichEdit1,
             UnicodeString StrFind,
             int CurrRaw,int CurrSymbolPos,
             int &ResRaw,int &ResSymbolPos, int &CountSymbolsBetween)
{//Шукаємо у RichEdit1 перше незакоментоване входження стрічки StrFind, починаючи з CurrRaw-рядка і з CurrSymbolPos-символа у цьому рядку
 //Якщо вдалося знайти таке входження-- то повертаємо true, інакше- false.

 int i = 0;
 int Begin = 0;
 int PosComent = 0;
 UnicodeString CurrStr;

 CountSymbolsBetween=0;
  for(i=CurrRaw; i<RichEdit1->Lines->Count; i++)
  {

     if(i==CurrRaw)  CurrStr=RichEdit1->Lines->Strings[i].SubString(CurrSymbolPos+1, RichEdit1->Lines->Strings[i].Length()-CurrSymbolPos);
     else            CurrStr=RichEdit1->Lines->Strings[i];




      Begin=CurrStr.Pos(StrFind);
      if(Begin>0)//т.е. нашли в данной строке вхождение StrFind
      { //теперь надо проверить, а вдруг эта часть строки заккоментирована

        PosComent=special_min(CurrStr.Pos(L";"),CurrStr.Pos(L"%"));
        if( PosComent>Begin  || PosComent==0   )//т.е. наша строка StrFind-- не закомментирована!
        {
          ResRaw = i;
          if(i==CurrRaw) ResSymbolPos = CurrSymbolPos + Begin-1; else ResSymbolPos = Begin-1;
          CountSymbolsBetween += Begin-1;
          return true;
        }
      }
      CountSymbolsBetween+=CurrStr.Length()+RETURN_LENGTH;



  }


return false;
}

//------------------------------------------------------------------------
void __fastcall TEditForm::HideComentars()
{
  bool FlagModified= RichEdit1->Modified;
  int i,CurrSymbolsCount=0;
  for(i=0; i<RichEdit1->Lines->Count; i++)
  {
      CurrSymbolsCount+=RichEdit1->Lines->Strings[i].Length()+RETURN_LENGTH;
  }

  RichEdit1->SelStart = 0;
  RichEdit1->SelLength = CurrSymbolsCount;
  RichEdit1->SelAttributes->Color = clBlack;

  RichEdit1->SelStart = 0;
  RichEdit1->SelLength = 0;
  RichEdit1->Modified = FlagModified;
}
//---------------------------------------------------------------------------
void __fastcall TEditForm::FoundComentars()
{//функция определения Комментариев

   bool FlagModified= RichEdit1->Modified;

   UnicodeString CurrStr;
   int PosComent = 0;
   int i = 0;
   int i_curr = 0;
   int CurrSymbolsCount=0;
   bool flag=true;


   for(i=0; i<RichEdit1->Lines->Count; i++)
   {
      CurrStr=RichEdit1->Lines->Strings[i];

      PosComent=special_min(CurrStr.Pos(L";"),CurrStr.Pos(L"%"));
      if(PosComent>0)//тобто комментарий существует
      { //т.е. нашли ПЕРВОЕ входжение комментария
        RichEdit1->SelStart = CurrSymbolsCount + PosComent-1;
        RichEdit1->SelLength = CurrStr.Length()-(PosComent-1);
        RichEdit1->SelAttributes->Color = clFuchsia;
        //RichEdit1->SelAttributes->Style<< fsItalic;

      }

      CurrSymbolsCount+=CurrStr.Length()+RETURN_LENGTH;
   }



   //2.тепрь пройдемся по комментариям в виде скобок
   CurrSymbolsCount = 0;
   bool Flag;  int CurrRaw,CurrSymbolPos,ResRaw,ResSymbolPos, CountSymbolsBetween, Count=0;

   CurrRaw=0; CurrSymbolPos=0;
   int tempLen=RichEdit1->Lines->Strings[RichEdit1->Lines->Count-1].Length();

   while(1)
   {

       Flag=FindStringLocation(RichEdit1,L"{",CurrRaw,CurrSymbolPos,ResRaw,ResSymbolPos, CountSymbolsBetween);//починаючи з якого рядка, з якого символа у рядку


       if(Flag)
       { CurrSymbolsCount+=CountSymbolsBetween;
         CurrRaw=ResRaw;
         CurrSymbolPos=ResSymbolPos;

         /*if(CurrRaw==836)
          int fff=4;/**/

         Flag=FindStringLocation(RichEdit1,L"}",CurrRaw,CurrSymbolPos,ResRaw,ResSymbolPos, CountSymbolsBetween);//починаючи з якого рядка, з якого символа у рядку
         if(Flag)
         {
             RichEdit1->SelStart = CurrSymbolsCount;
             RichEdit1->SelLength = CountSymbolsBetween+1;
             RichEdit1->SelAttributes->Color = clBlue;

             //Count++; if(Count>10) return;
         }
       }
       else break;

       //return;
       
       CurrRaw=ResRaw;
       CurrSymbolPos=ResSymbolPos;
       CurrSymbolsCount+=CountSymbolsBetween;
   }

  RichEdit1->SelStart = 0;
  RichEdit1->SelLength = 0;


  RichEdit1->Modified = FlagModified;
}

//---------------------------------------------------------------------------
void __fastcall TEditForm::DefineCurrColor(UnicodeString FoundStr)
{//функция определения текущего цвета (по положению текущего курсора)

     int iPos = 0;
	 int iy = 0;

     FoundNearestString(FoundStr, iy, iPos);

     if(iy<0)//это означает, что вхождения строчки не нашлось. Значит стоит Зеленый цвет (по умолчанию)
         Memo1->Lines->Add(L"Цвет: Зеленый ( #color[2] по умолчанию)");
     else
     {
       int Pos_scobka = 0;
       UnicodeString CurrStr=RichEdit1->Lines->Strings[iy].SubString(iPos+FoundStr.Length()+RETURN_LENGTH, 5);

                  if((Pos_scobka=CurrStr.Pos(L"]"))==0)  ShowMessage(L"Ошибка в операторе цвета!");
                  else
                  {
                       CurrStr=CurrStr.SubString(1, Pos_scobka-1);  CurrStr=CurrStr.Trim();
                       UnicodeString StrColor;
                       try
                       {
                         int iColor=StrToInt(CurrStr);
                         switch(iColor)
                         { case 0: StrColor=L"Черный (#color[0])"; break;
                           case 1: StrColor=L"Blue (#color[1])"; break;
                           case 2: StrColor=L"Green (#color[2])"; break;
                           case 3: StrColor=L"Cyan (#color[3])"; break;
                           case 4: StrColor=L"Red (#color[4])"; break;
                           case 5: StrColor=L"Magenta (#color[5])"; break;
                           case 6: StrColor=L"Brown (#color[6])"; break;
                           case 7: StrColor=L"LightGray (#color[7])"; break;
                           case 8: StrColor=L"DarkGray (#color[8])"; break;
                           case 9: StrColor=L"LightBlue (#color[9])"; break;
                           case 10: StrColor=L"LightGreen (#color[10])"; break;
                           case 11: StrColor=L"LightCyan (#color[11])"; break;
                           case 12: StrColor=L"LightRed (#color[12])"; break;
                           case 13: StrColor=L"LightMagenta (#color[13])"; break;
                           case 14: StrColor=L"Yellow (#color[14])"; break;
                           case 15: StrColor=L"White (#color[15])"; break;
                           case 16: StrColor=L"Orange (#color[16])"; break;
                           default: StrColor=L"Неизвестн.";
                         }
                       }
                       catch(...) {StrColor=L"Неизвестн.";};

                   StrColor= L"Цвет: "+ StrColor;
                   Memo1->Lines->Add(StrColor);
                  }
     }

}
//---------------------------------------------------------------------------
void __fastcall TEditForm::DefineCurrMode( UnicodeString Str1,//первый из 2 ключевых режимов (оно же и по умолчанию)
                                           UnicodeString Str2 ,//второй из 2 ключевых режимов
                                           UnicodeString Str3,//Слово: название режима
                                           UnicodeString Str4,//надпись для первого режима
                                           UnicodeString Str5//надпись для второго режима
                                          )
{//функция определения (по положению текущего курсора) текущего Mode (оно определяется строками Str1 и Str2, т.е. режим может иметь только два значения!)

     int iPos1 = 0;
	 int iy1 = 0;
	 int iPos2 = 0;
	 int iy2 = 0;
     FoundNearestString(Str1, iy1, iPos1);
     FoundNearestString(Str2, iy2, iPos2);

     int CurrMode = 0;
     UnicodeString Str_help;
     if(iy1>=0 && iy2>=0) //т.е. в тексте существуют оба оператора
     { //тогда надо найти ближайший

       if(iy1>iy2) CurrMode=1;
       else  if(iy2>iy1) CurrMode=2;
             else //если одинаковы (в одной строке), то надо искать тот оператор, что правее
             {
               if(iPos1>iPos2) CurrMode=1; else CurrMode=2;
             }
     }
     else //т.е. один из операторов (или оба) в тексте отсутствует
     {
       if(iy2>=0) CurrMode=2;
       else
       {  CurrMode=1;
          if(iy1>=0) Str_help=L""; else Str_help=L" (по умолчанию)";
       }
     }


         UnicodeString Str;
         if(CurrMode==1)
              Str = Str3 + Str4 + L" (L"+ Str1+") "+  Str_help;
         else Str = Str3 + Str5 +" (L"+ Str2+") ";

    Memo1->Lines->Add(Str);
}


//---------------------------------------------------------------------------
void __fastcall TEditForm::DefineDataOrder()
{//функция определения (по положению текущего курсора) текущего DataOrder


     int iPos1 = 0;
	 int iy1 = 0;
     FoundNearestString(L"#data_order", iy1, iPos1);

     int CurrMode=0;//трехзначное число из цифер: 1,2 и 3 (1=L, 2=Az, 3=An)


     UnicodeString Str_help;
     if(iy1<0 ) //т.е. в тексте оператор "#data_order"  отсутствует
     {  Memo1->Lines->Add(L"Порядок данных: L Az An  (по умолчанию)"); return;}

     //т.е. оператор "#data_order" в тексте ЕСТЬ. Тогда разберем последовательность следования данных:
     UnicodeString CurrStr=RichEdit1->Lines->Strings[iy1].SubString(iPos1+12,1000);

                CurrStr =  CurrStr.Trim();
     //т.е. в  CurrStr находится текущая строка (начиная уже с данных)


    int Pos_L = 0;
	int Pos_An = 0;
	int Pos_Az = 0;
    Pos_L=CurrStr.Pos(L"L");
    Pos_An=CurrStr.Pos(L"An");
    Pos_Az=CurrStr.Pos(L"Az");

    if( Pos_L==0 || Pos_An==0 || Pos_Az==0)  { Memo1->Lines->Add(L"Порядок данных: ошибка в параметрах"); return;}
    //если попали сюда, то значит все ключевые слова присутствуют

   if(Pos_L<Pos_An && Pos_L<Pos_Az)  CurrMode+=100;
   else if(Pos_L>Pos_An && Pos_L>Pos_Az)  CurrMode+=1;
        else   CurrMode+=10;

   if(Pos_An<Pos_L && Pos_An<Pos_Az)  CurrMode+=100;
   else if(Pos_L>Pos_An && Pos_L>Pos_Az)  CurrMode+=1;
        else   CurrMode+=10;

}
//---------------------------------------------------------------------------
void __fastcall TEditForm::DefineCurrMode_bez_umolchania( UnicodeString Str1,//первый из 2 ключевых режимов 
                                           UnicodeString Str2 ,//отключение режима
                                           UnicodeString Str3,//Слово: название режима
                                           UnicodeString Str4,//надпись для первого режима
                                           UnicodeString Str5//надпись для второго режима
                                          )
{//функция определения (по положению текущего курсора) текущего Mode (оно определяется строками Str1 и Str2, т.е. режим может иметь только два значения!)
 //Аналогична "DefineCurrMode", но без (L"по умолчанию")-- это свойственно например оператору #surface, для которого нету "по умолчанию". Отсутствие оператора означает, что режим отключен.

     int iPos1 = 0;
	 int iy1 = 0;
	 int iPos2 = 0;
	 int iy2 = 0;
     FoundNearestString(Str1, iy1, iPos1);
     FoundNearestString(Str2, iy2, iPos2);

     int CurrMode = 0;
     UnicodeString Str_help=L"";
     if(iy1>=0 && iy2>=0) //т.е. в тексте существуют оба оператора
     { //тогда надо найти ближайший

       if(iy1>iy2) CurrMode=1;
       else  if(iy2>iy1) CurrMode=2;
             else //если одинаковы (в одной строке), то надо искать тот оператор, что правее
             {
               if(iPos1>iPos2) CurrMode=1; else CurrMode=2;
             }
     }
     else //т.е. один из операторов (или оба) в тексте отсутствует
     {
       if(iy2>=0) {CurrMode=2;  Str_help=L" (ошибка! нету начинающего оператора)";}//т.е. второй оператор существует, а первый--  нет.... Тут можно и ошибку выдать...
       else
       {  //т.е. второй оператор НЕ существует
          Str_help=L"";
          if(iy1>=0) CurrMode=1;//т.е. Первый оператор существует
          else      CurrMode=0;//т.е. НИ первый НИ второй оператор НЕ существуют
       }
     }


         UnicodeString Str;
         if(CurrMode==1 ) //т.е. Прервый оператор существует
              Str = Str3 + Str4 +  Str_help +" (L"+ Str1 +")";
         else if(CurrMode==2 ) //т.е. второй оператор существует
              Str = Str3 + Str5 + Str_help +" (L"+ Str2 +")";
              else  //т.е. НИ первый НИ второй оператор НЕ существуют
               Str = Str3 + Str5 + L" (по умолчанию)";

    Memo1->Lines->Add(Str);
}

//---------------------------------------------------------------------------
void __fastcall TEditForm::DefineCurrMode_bez_umolchania2( UnicodeString Str1,//первый из 2 ключевых режимов (оно же и по умолчанию)
                                           UnicodeString Str2_1 ,//отключение режима1
                                           UnicodeString Str2_2 ,//отключение режима2 (Oобщее отключение всех режимов)
                                           UnicodeString Str3,//Слово: название режима
                                           UnicodeString Str4,//надпись для первого режима
                                           UnicodeString Str5//надпись для второго режима
                                          )
{//функция определения (по положению текущего курсора) текущего Mode (оно определяется строками Str1 и Str2, т.е. режим может иметь только два значения!)
 //Аналогична "DefineCurrMode", но без (L"по умолчанию")-- это свойственно например оператору #surface, для которого нету "по умолчанию". Отсутствие оператора означает, что режим отключен.

 //Введено 2 типа "отключения режима" -- это свойственно например операторам #correction, для которых есть 2 способа отключения режима (конкретно для этого режима, и общий)



     int iPos1 = 0;
	 int iy1 = 0;
     FoundNearestString(Str1, iy1, iPos1);

     int iPos2_1 = 0;
	 int iy2_1 = 0;
	 int iPos2_2 =0;
	 int iy2_2 = 0;
     FoundNearestString(Str2_1, iy2_1, iPos2_1);
     FoundNearestString(Str2_2, iy2_2, iPos2_2);
       //(ищем самое правое из отключений):



     //У коррекции есть вредная фича: #end_corr, которая завершает все коррекции, совпадает с куском комманды #end_corr_A_g.
     // Поетому надо сделать еще проверку и на этот скорбный факт
     if(Str2_2==L"#end_corr" && iy2_2>=0)
     { //тогда проверим еще, чтобы это было именно слово  "#end_corr", а не кусок слова  "#end_corr_..."
          UnicodeString CurrStr=RichEdit1->Lines->Strings[iy2_2].SubString(iPos2_2,15);
          if(CurrStr.Pos(L"#end_corr_"))
          {  iy2_2=-1;
             iPos2_2==-1;
          }
     }
     

     int Mode_end = 0;//какой из режимов приводит к окончанию
     int iPos2 = 0;
	 int iy2 = 0;
        iy2=std::max(iy2_1,iy2_2);
         if(iy2>=0)//т.е. существует такое
         { if(iy2==iy2_1)
           { iPos2=iPos2_1;
             Mode_end=1;
           }
           else
           { iPos2=iPos2_2;
             Mode_end=2;
           }
         }
         else { iPos2=-1; Mode_end==-1;}






     int CurrMode = 0;
     UnicodeString Str_help=L"";
     if(iy1>=0 && iy2>=0) //т.е. в тексте существуют оба оператора
     { //тогда надо найти ближайший

       if(iy1>iy2) CurrMode=1;
       else  if(iy2>iy1) CurrMode=2;
             else //если одинаковы (в одной строке), то надо искать тот оператор, что правее
             {
               if(iPos1>iPos2) CurrMode=1; else CurrMode=2;
             }
     }
     else //т.е. один из операторов (или оба) в тексте отсутствует
     {
       if(iy2>=0 && Mode_end==1) {CurrMode=2;  Str_help=L" (ошибка! нету начинающего оператора)";}//т.е. второй оператор существует, а первый--  нет.... Тут можно и ошибку выдать...
       else
       {  //т.е. второй оператор НЕ существует
          Str_help=L"";
          if(iy1>=0) CurrMode=1;//т.е. Первый оператор существует
          else      CurrMode=0;//т.е. НИ первый НИ второй оператор НЕ существуют
       }
     }


         UnicodeString Str;
         if(CurrMode==1 ) //т.е. Прервый оператор существует
              Str = Str3 + Str4 +  Str_help +" (L"+ Str1 +")";
         else if(CurrMode==2 ) //т.е. второй оператор существует
              {
                Str = Str3 + Str5 + Str_help;
                if(Mode_end==1) Str+=L" (L"+ Str2_1 +")"; else Str+=L" (L"+ Str2_2 +")";
              }
              else  //т.е. НИ первый НИ второй оператор НЕ существуют
              Str = Str3 + Str5 + L" (по умолчанию)";

               
    Memo1->Lines->Add(Str);
}

//---------------------------------------------------------------------------
void __fastcall TEditForm::DefineCurrMode_bez_umolchania_n
                                         ( UnicodeString Str1,//первый из 2 ключевых режимов (оно же и по умолчанию)
                                           UnicodeString Str2_1 ,//отключение режима1
                                           UnicodeString Str2_2 ,//отключение режима2 (Oобщее отключение всех режимов)
                                           UnicodeString Str3,UnicodeString Str3_help,//Слово-- название режима и вспомогательное слово (определяет, какие единицы, метры, градусы)
                                           UnicodeString Str4,//надпись для первого режима
                                           UnicodeString Str5//надпись для второго режима
                                          )
{//для режимов типа  #corr_A[n], #corr_L_beg[x], #corr_clino[x], где необходимо определить еще и на какую величину корректировать...


//функция определения (по положению текущего курсора) текущего Mode (оно определяется строками Str1 и Str2, т.е. режим может иметь только два значения!)
 //Аналогична "DefineCurrMode", но без (L"по умолчанию")-- это свойственно например оператору #surface, для которого нету "по умолчанию". Отсутствие оператора означает, что режим отключен.
 //Введено 2 типа "отключения режима" -- это свойственно например операторам #correction, для которых есть 2 способа отключения режима (конкретно для этого режима, и общий)

     int iPos1 = 0;
	 int iy1 = 0;
     FoundNearestString(Str1, iy1, iPos1);

     int iPos2_1 = 0;
	 int iy2_1 = 0;
	 int iPos2_2 = 0;
	 int iy2_2 = 0;
     FoundNearestString(Str2_1, iy2_1, iPos2_1);
     FoundNearestString(Str2_2, iy2_2, iPos2_2);
       //(ищем самое правое из отключений):



     //У коррекции есть вредная фича: #end_corr, которая завершает все коррекции, совпадает с куском комманды #end_corr_A_g.
     // Поетому надо сделать еще проверку и на этот скорбный факт
     if(Str2_2==L"#end_corr" && iy2_2>=0)
     { //тогда проверим еще, чтобы это было именно слово  "#end_corr", а не кусок слова  "#end_corr_..."
         UnicodeString CurrStr=RichEdit1->Lines->Strings[iy2_2].SubString(iPos2_2,15);
          if(CurrStr.Pos(L"#end_corr_"))
          {  iy2_2=-1;
             iPos2_2==-1;
          }
     }

     //еще один скорбный факт: режим #end_corr_L     : / кончить коррекцию единицы длины в x раз
     //окончание этого режима есть подмножеством режима #end_corr_L_beg, поетому надо провести дополнительную проверку:
     if(Str2_1==L"#end_corr_L" && iy2_1>=0)
     { //тогда проверим еще, чтобы это было именно слово  "#end_corr_L", а не кусок слова  "#end_corr_L_..."
         UnicodeString CurrStr=RichEdit1->Lines->Strings[iy2_1].SubString(iPos2_2,17);
          if(CurrStr.Pos(L"#end_corr_L_"))
          {  iy2_1=-1;
             iPos2_1==-1;
          }
     }


 
     int Mode_end = 0;//какой из режимов приводит к окончанию
     int iPos2 = 0; 
	 int iy2 = 0;
        iy2=std::max(iy2_1,iy2_2);
         if(iy2>=0)//т.е. существует такое
         { if(iy2==iy2_1)
           { iPos2=iPos2_1;
             Mode_end=1;
           }
           else
           { iPos2=iPos2_2;
             Mode_end=2;
           }
         }
         else { iPos2=-1; Mode_end==-1;}






     int CurrMode = 0;
     UnicodeString Str_help=L"";
     if(iy1>=0 && iy2>=0) //т.е. в тексте существуют оба оператора
     { //тогда надо найти ближайший

       if(iy1>iy2) CurrMode=1;
       else  if(iy2>iy1) CurrMode=2;
             else //если одинаковы (в одной строке), то надо искать тот оператор, что правее
             {
               if(iPos1>iPos2) CurrMode=1; else CurrMode=2;
             }
     }
     else //т.е. один из операторов (или оба) в тексте отсутствует
     {
       if(iy2>=0 && Mode_end==1) {CurrMode=2;  Str_help=L"";}//" (ошибка! нету начинающего оператора)";}//т.е. второй оператор существует, а первый--  нет.... Тут можно и ошибку выдать...
       else
       {  //т.е. второй оператор НЕ существует
          Str_help=L"";
          if(iy1>=0) CurrMode=1;//т.е. Первый оператор существует
          else      CurrMode=0;//т.е. НИ первый НИ второй оператор НЕ существуют
       }
     }


     UnicodeString Str_x;//стринг, который представляет число корректировки

         UnicodeString Str;
         if(CurrMode==1 ) //т.е. Первый оператор существует
         {//включен режим. Тогда определим, на сколько же именно единиц коррекция:

            UnicodeString CurrStr=RichEdit1->Lines->Strings[iy1].SubString(iPos1+Str1.Length()+1,150);
             int Pos1 = 0;
             if(Pos1==CurrStr.Pos(L"]"))
             {
                UnicodeString CurrStr1=CurrStr.SubString(1,Pos1-1);
                Str_x=CurrStr1.Trim();
             }
             else Str_x=L"Неизв. параметр";

            Str = Str3 + Str4  +" ( на "+ Str_x +" "+ Str3_help +")" +  Str_help +" (L"+ Str1 +")";
         }
         else if(CurrMode==2 ) //т.е. второй оператор существует
              {
                Str = Str3 + Str5 + Str_help;
                if(Mode_end==1) Str+=L" (L"+ Str2_1 +")"; else Str+=L" (L"+ Str2_2 +")";
              }
              else  //т.е. НИ первый НИ второй оператор НЕ существуют
              Str = Str3 + Str5 + L" (по умолчанию)";

               
    Memo1->Lines->Add(Str);
}
//---------------------------------------------------------------------------
void __fastcall TEditForm::RecalculateModes()
{//для текущего курсора в текстевычисляем его режимы (цвет, тип съемки, развертки и  т.д.)

  return; 
     Memo1->Clear();

     //DrawingTextByType();

     //цвет:
     DefineCurrColor(L"#color");

     //Формат измерений:
     DefineCurrMode( "#angle",//первый из 2 ключевых режимов (оно же и по умолчанию)
                     "#fall",//второй из 2 ключевых режимов
                     "Формат измерений: ",//Слово: название режима
                     "Стандартн.",//надпись для первого режима
                     "Превышение"//надпись для второго режима
                );

     //Направление разр-разв:
     DefineCurrMode( "#R0",//первый из 2 ключевых режимов (оно же и по умолчанию)
                     "#R180",//второй из 2 ключевых режимов
                     "Направление разр-разв: ",//Слово: название режима
                     "0.",//надпись для первого режима
                     "180"//надпись для второго режима
                  );

     //Съемка (surface || end_surface):
     DefineCurrMode_bez_umolchania
                (    "#surface",//первый из 2 ключевых режимов
                     "#end_surface",//отключение режима
                     "Съемка : ",//Слово: название режима
                     "на поверхности",//надпись для первого режима
                     "в пещере"//надпись для второго режима
                );

     //Correction Az1  (под горный компас):
     DefineCurrMode_bez_umolchania2
                (    "#corr_A_g",//первый из 2 ключевых режимов
                     "#end_corr_A_g",//отключение режима1
                     "#end_corr",   //отключение режима2 (Oобщее отключение всех режимов)
                     "Коррекция Az (под горный компас): ",//Слово: название режима
                     "есть",//надпись для первого режима
                     "нету"//надпись для второго режима
                );

     //Correction Az2  (обратный Азимут):
     DefineCurrMode_bez_umolchania2
                (    "#corr_A_180",//первый из 2 ключевых режимов
                     "#end_corr_A_180",//отключение режима1
                     "#end_corr",   //отключение режима2 (Oобщее отключение всех режимов)
                     "Коррекция Az (на обратный): ",//Слово: название режима
                     "есть",//надпись для первого режима
                     "нету"//надпись для второго режима
                );

     //Correction Az3  (коррекция на ... градусов):
     DefineCurrMode_bez_umolchania_n
                (    "#corr_A[",//первый из 2 ключевых режимов
                     "#end_corr_A",//отключение режима1
                     "#end_corr",   //отключение режима2 (Oобщее отключение всех режимов)
                     "Коррекция Az (на х градусов): ", L"градусов",//Слово-- название режима и вспомогательное слово (определяет, какие единицы, метры, градусы)
                     "есть",//надпись для первого режима
                     "нету"//надпись для второго режима
                ); /**/

     //Correction L  (коррекция на ... метров):
     DefineCurrMode_bez_umolchania_n
                (    "#corr_L_beg[",//первый из 2 ключевых режимов
                     "#end_corr_L_beg",//отключение режима1
                     "#end_corr",   //отключение режима2 (Oобщее отключение всех режимов)
                     "Коррекция длины (на х метров): ", L"метров",//Слово-- название режима и вспомогательное слово (определяет, какие единицы, метры, градусы)
                     "есть",//надпись для первого режима
                     "нету"//надпись для второго режима
                ); /**/

     //Correction L  (коррекция в ... раз):
     DefineCurrMode_bez_umolchania_n
                (    "#corr_L[",//первый из 2 ключевых режимов
                     "#end_corr_L",//отключение режима1
                     "#end_corr",   //отключение режима2 (Oобщее отключение всех режимов)
                     "Коррекция длины (в х раз): ", L"раз",//Слово-- название режима и вспомогательное слово (определяет, какие единицы, метры, градусы)
                     "есть",//надпись для первого режима
                     "нету"//надпись для второго режима
                ); /**/

     //Correction alfa  (коррекция угла наклона на х градусов ):
     DefineCurrMode_bez_umolchania_n
                (    "#corr_clino[",//первый из 2 ключевых режимов
                     "#end_corr_clino",//отключение режима1
                     "#end_corr",   //отключение режима2 (Oобщее отключение всех режимов)
                     "Коррекция угла наклона (на х градусов): ", L"градусов",//Слово-- название режима и вспомогательное слово (определяет, какие единицы, метры, градусы)
                     "есть",//надпись для первого режима
                     "нету"//надпись для второго режима
                ); /**/


     DefineDataOrder();//функция определения (по положению текущего курсора) текущего DataOrder

}

//---------------------------------------------------------------------------
void __fastcall TEditForm::RichEdit1MouseUp(TObject *Sender,
      TMouseButton Button, TShiftState Shift, int X, int Y)
{
  if(Button==mbLeft)
  {
    RecalculateModes();

  }//end of if(Button==mbLeft).
}
//---------------------------------------------------------------------------
void __fastcall TEditForm::DrawingTextByType()
{ //раскрашиваем текст по типам (коменты серым, и т.д.)

/*  //1.Раскрашиваем коменты серым:
   int i,CommentPos;   UnicodeString CurrStr, CurrStr_;
   int CountLen=0;

   for(i=0; i<RichEdit1->Lines->Count; i++)
   {
      CurrStr=RichEdit1->Lines->Strings[i];

      CommentPos=CurrStr.Pos(L";");
      if(CommentPos>0)
      { //т.е. нашли входжение первого комментария в данной строке
        //тогда все что после ";"-- закрашиваем цветом комментария

            RichEdit1->SelAttributes->Color = clRed;

            RichEdit1->SelStart=CountLen + CommentPos;
            RichEdit1->SelLength = 2;

            //RichEdit1->SelAttributes->Color = 0;

            //RichEdit1->SelText = L"Test";

             //RichEdit1->SelStart= 0; RichEdit1->SelLength = 0;
            //RichEdit1->HideSelection = true;
      }

      CountLen+=CurrStr.Length();

   }    /**/

}

//---------------------------------------------------------------------------
void __fastcall TEditForm::RichEdit1KeyUp(TObject *Sender, WORD &Key,
      TShiftState Shift)
{
    RecalculateModes();

     //FoundComentars( );
}
//---------------------------------------------------------------------------
void __fastcall TEditForm::FormResize(TObject *Sender)
{
  Panel3->Width=EditForm->Width/2-6;
  Panel4->Width=EditForm->Width/2-6;
}
//---------------------------------------------------------------------------

//------------------------ Insert Tabs --------------------------------------
void __fastcall TEditForm::RichEdit1KeyDown(TObject *Sender, WORD &Key,
      TShiftState Shift)
{
 if(Key == VK_F1) //можно на ctrl - стрелка вниз ?
  {
   if( (RichEdit1->CaretPos.y>=RichEdit1->Lines->Count-1)<=0 )
    {
     RichEdit1->Lines->Insert(RichEdit1->CaretPos.y+1, L"\t\t\t\t\t");
     RichEdit1->CaretPos=Point(0,RichEdit1->CaretPos.y-1);
    }
   else RichEdit1->Lines->Add(L"\t\t\t\t\t");
  }
}
//---------------------------------------------------------------------------
void __fastcall TEditForm::ToolButton15Click(TObject *Sender)
{
   if( (RichEdit1->CaretPos.y>=RichEdit1->Lines->Count-1)<=0 )
    {
     RichEdit1->Lines->Insert(RichEdit1->CaretPos.y+1, L"\t\t\t\t\t");
     RichEdit1->CaretPos=Point(0,RichEdit1->CaretPos.y-1);
    }
   else RichEdit1->Lines->Add(L"\t\t\t\t\t");
}

//------------------------- Highlight comments ------------------------------
void __fastcall TEditForm::FormShow(TObject *Sender)
 {if(ToolButton16->Down) FoundComentars();}
//---------------------------------------------------------------------------
void __fastcall TEditForm::ToolButton16Click(TObject *Sender)
{
 if(ToolButton16->Down) FoundComentars();
 else HideComentars();
}

//------------------------- Form Close ---------------------------------------
void __fastcall TEditForm::FormClose(TObject *Sender, TCloseAction &Action)
{
    RichEdit1->SelStart =0;
    RichEdit1->SelLength = 0;
    RichEdit1->SelAttributes->Color = clBlack;
    //RichEdit1->SelAttributes=RichEdit1->SelAttributes>>[];
}
//---------------------------------------------------------------------------



void __fastcall TEditForm::H_begClick(TObject *Sender)
{PopapMenu_InsertComand(L" #corr_H[...] ");}  // начать коррекцию высоты на x
//---------------------------------------------------------------------------

void __fastcall TEditForm::H_endClick(TObject *Sender)
{PopapMenu_InsertComand(L" #end_corr_H[...] ");}  // закончить коррекцию высоты на x
//---------------------------------------------------------------------------
void TEditForm::updateCanOpenMainFile() {
	OpenMain1->Enabled = (MainForm1->CurrFileName.Length() > 0 && FFileName != MainForm1->CurrFileName);
}
//---------------------------------------------------------------------------

void __fastcall TEditForm::OpenMain1Click(TObject *Sender)
{
	if (MainForm1->CurrFileName.Length() > 0) {
		if(Confirm() == 2) return;
		edit_flag=0;
		LoadFromFileUTF8Test(RichEdit1->Lines, MainForm1->CurrFileName);
		SetFileName(MainForm1->CurrFileName);
		RichEdit1->Modified = false;

		FoundComentars();
	}
}
//---------------------------------------------------------------------------

