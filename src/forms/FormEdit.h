﻿//---------------------------------------------------------------------------
#ifndef FormEditH
#define FormEditH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <ComCtrls.hpp>
#include <ToolWin.hpp>
#include <ImgList.hpp>
#include <Menus.hpp>
#include <Dialogs.hpp>
#include <System.ImageList.hpp>
//---------------------------------------------------------------------------
//#define max(a, b)  (((a) > (b)) ? (a) : (b))

class TEditForm : public TForm
{
__published:	// IDE-managed Components
        TControlBar *ControlBar1;
        TToolBar *ToolBar1;
        TToolButton *ToolButton1;
        TToolButton *ToolButton2;
        TToolButton *ToolButton3;
        TImageList *Standard;
        TToolBar *ToolBar2;
        TToolButton *ToolButton4;
        TToolButton *ToolButton5;
        TToolButton *ToolButton6;
        TToolButton *ToolButton7;
        TToolButton *ToolButton9;
        TMainMenu *MainMenu1;
        TMenuItem *File1;
        TMenuItem *Help2;
        TMenuItem *New1;
        TMenuItem *Open1;
        TMenuItem *Save1;
        TMenuItem *Edit1;
        TMenuItem *Cut1;
        TMenuItem *Copy1;
        TMenuItem *Paste1;
        TToolButton *ToolButton8;
        TImageList *ImageList1;
        TMenuItem *Saveas1;
        TMenuItem *N2;
        TMenuItem *Decode1;
        TMenuItem *Exit1;
        TMenuItem *N3;
        TMenuItem *C1;
        TMenuItem *Data1;
        TMenuItem *Commands1;
        TMenuItem *SelectAll1;
        TMenuItem *Print1;
        TMenuItem *N1;
        TOpenDialog *OpenDialog1;
        TPrintDialog *PrintDialog1;
        TSaveDialog *SaveDialog1;
        TEdit *Edit2;
        TMenuItem *from1;
        TMenuItem *fromWin1251toDOS8661;
        TStatusBar *StatusBar1;
        TToolButton *UndoButton;
        TToolButton *RedoButton;
        TToolButton *ToolButton11;
        TFindDialog *FindDialog1;
        TToolButton *ToolButton10;
        TToolButton *ToolButton12;
        TMenuItem *Corrections1;
        TToolButton *ToolButton13;
        TToolButton *ToolButton14;
        TMenuItem *Loops;
        TMenuItem *Profile;
        TPanel *Panel1;
        TPanel *Panel2;
        TPopupMenu *PopupMenu1;
        TMenuItem *PopapMenu_InsertLine;
        TMenuItem *PopapMenu_InsertColor;
        TMenuItem *PopapMenu_InsertColor_Yellow;
        TMenuItem *PopapMenu_InsertColor_Blue;
        TMenuItem *PopapMenu_InsertFormat;
        TMenuItem *PopapMenu_InsertFormat_Standart;
        TMenuItem *PopapMenu_InsertFormat_Fall;
        TMenuItem *PopapMenu_InsertRazvertka;
        TMenuItem *PopapMenu_InsertRazvertka_0;
        TMenuItem *PopapMenu_InsertRazvertka_180;
        TMenuItem *PopapMenu_InsertColor_Black;
        TMenuItem *PopapMenu_InsertColor_Green;
        TMenuItem *PopapMenu_InsertColor_Cyan;
        TMenuItem *PopapMenu_InsertColor_Red;
        TMenuItem *PopapMenu_InsertColor_Magenta;
        TMenuItem *PopapMenu_InsertColor_Brown;
        TMenuItem *PopapMenu_InsertColor_LightGray;
        TMenuItem *PopapMenu_InsertColor_DarkGray;
        TMenuItem *PopapMenu_InsertColor_LightBlue;
        TMenuItem *PopapMenu_InsertColor_LightGreen;
        TMenuItem *PopapMenu_InsertColor_LightCyan;
        TMenuItem *PopapMenu_InsertColor_LightRed;
        TMenuItem *PopapMenu_InsertColor_LightMagenta;
        TMenuItem *PopapMenu_InsertColor_White;
        TMenuItem *PopapMenu_InsertSurface;
        TMenuItem *PopapMenu_InsertSurfaceBegin;
        TMenuItem *PopapMenu_InsertSurfaceEnd;
        TMenuItem *Correction1;
        TMenuItem *Az1;
        TMenuItem *Az2;
        TMenuItem *Azn;
        TMenuItem *Lx1;
        TMenuItem *Lx2;
        TMenuItem *Az1_beg;
        TMenuItem *Az1_end;
        TMenuItem *Az2_beg;
        TMenuItem *Az2_end;
        TMenuItem *Azn_beg;
        TMenuItem *Azn_end;
        TMenuItem *Lx1_beg;
        TMenuItem *Lx1_end;
        TMenuItem *Lx2_beg;
        TMenuItem *Lx2_end;
        TMenuItem *Angle1;
        TMenuItem *Angle1_beg;
        TMenuItem *Angle1_end;
        TMenuItem *Az_L_Angle_end;
        TMenuItem *N_General;
        TMenuItem *N_General_cavename;
        TMenuItem *N_General_Region;
        TMenuItem *N_General_SurvTeam;
        TPanel *Panel3;
        TPanel *Panel4;
        TRichEdit *Memo3;
        TMemo *Memo1;
        TMenuItem *NDataOrder;
        TMenuItem *DataOrder_L_Az_An;
        TMenuItem *DataOrder_Az_L_An;
        TMenuItem *DataOrder_L_An_Az;
        TMenuItem *DataOrder_An_L_Az;
        TMenuItem *DataOrder_An_Az_L;
        TMenuItem *DataOrder_Az_An_L;
        TMenuItem *PopapMenu_InsertColor_Orange;
        TToolButton *ToolButton15;
        TRichEdit *RichEdit1;
        TMenuItem *NPiketPropert;
        TMenuItem *NPiketPropert_ent;
        TMenuItem *NPiketPropert_sif;
        TMenuItem *NPiketPropert_red;
        TMenuItem *NPiketPropert_com;
        TMenuItem *NPiketPropert_endlabel;
        TMenuItem *PopapMenu_InsertFormat_FromTo;
        TMenuItem *PopapMenu_InsertFormat_Label;
        TMenuItem *PopapMenu_InsertRazvertka_RR;
        TMenuItem *N_General_include;
        TMenuItem *N_General_Fix;
        TMenuItem *N2equate1;
        TMenuItem *prefix1;
        TMenuItem *endprefix1;
        TMenuItem *Az_end;
        TToolButton *ToolButton16;
        TMenuItem *N_surf1;
        TMenuItem *N_surf2;
        TMenuItem *N_surf3;
        TMenuItem *N_dup1;
        TMenuItem *N_dup2;
        TMenuItem *N_dup3;
        TMenuItem *N_general_EndSurvey;
        TMenuItem *declination1;
        TMenuItem *Data_order_LAzAnlrud;
        TMenuItem *x1;
        TMenuItem *H_beg;
        TMenuItem *H_end;
	TMenuItem *OpenMain1;
        void __fastcall C1Click(TObject *Sender);
        void __fastcall Data1Click(TObject *Sender);
        void __fastcall Commands1Click(TObject *Sender);
        void __fastcall New1Click(TObject *Sender);
        void __fastcall Cut1Click(TObject *Sender);
        void __fastcall Copy1Click(TObject *Sender);
        void __fastcall Paste1Click(TObject *Sender);
        void __fastcall SelectAll1Click(TObject *Sender);
        void __fastcall Open1Click(TObject *Sender);
        void __fastcall Print1Click(TObject *Sender);
        void __fastcall Saveas1Click(TObject *Sender);
        void __fastcall Save1Click(TObject *Sender);
        void __fastcall Exit1Click(TObject *Sender);
        void __fastcall FormCloseQuery(TObject *Sender, bool &CanClose);
        void __fastcall Decode1Click(TObject *Sender);
        void __fastcall Decode2Click(TObject *Sender);
        void __fastcall RichEdit1SelectionChange(TObject *Sender);
        void __fastcall UndoButtonClick(TObject *Sender);
        void __fastcall RichEdit1Change(TObject *Sender);
        void __fastcall RedoButtonClick(TObject *Sender);
        void __fastcall ToolButton10Click(TObject *Sender);
        void __fastcall FindDialog1Find(TObject *Sender);
        void __fastcall Corrections1Click(TObject *Sender);
        void __fastcall ToolButton13Click(TObject *Sender);
        void __fastcall ToolButton14Click(TObject *Sender);
        void __fastcall LoopsClick(TObject *Sender);
        void __fastcall ProfileClick(TObject *Sender);
        void __fastcall PopapMenu_InsertLineClick(TObject *Sender);
        void __fastcall PopapMenu_InsertColor_YellowClick(TObject *Sender);
        void __fastcall PopapMenu_InsertColor_BlackClick(TObject *Sender);
        void __fastcall PopapMenu_InsertColor_BlueClick(TObject *Sender);
        void __fastcall PopapMenu_InsertColor_GreenClick(TObject *Sender);
        void __fastcall PopapMenu_InsertColor_CyanClick(TObject *Sender);
        void __fastcall PopapMenu_InsertColor_RedClick(TObject *Sender);
        void __fastcall PopapMenu_InsertColor_MagentaClick(
          TObject *Sender);
        void __fastcall PopapMenu_InsertColor_BrownClick(TObject *Sender);
        void __fastcall PopapMenu_InsertColor_LightGrayClick(
          TObject *Sender);
        void __fastcall PopapMenu_InsertColor_DarkGrayClick(
          TObject *Sender);
        void __fastcall PopapMenu_InsertColor_LightBlueClick(
          TObject *Sender);
        void __fastcall PopapMenu_InsertColor_LightGreenClick(
          TObject *Sender);
        void __fastcall PopapMenu_InsertColor_LightCyanClick(
          TObject *Sender);
        void __fastcall PopapMenu_InsertColor_LightRedClick(
          TObject *Sender);
        void __fastcall PopapMenu_InsertColor_LightMagentaClick(
          TObject *Sender);
        void __fastcall PopapMenu_InsertColor_WhiteClick(TObject *Sender);
        void __fastcall PopapMenu_InsertFormat_StandartClick(
          TObject *Sender);
        void __fastcall PopapMenu_InsertFormat_FallClick(TObject *Sender);
        void __fastcall RichEdit1MouseUp(TObject *Sender,
          TMouseButton Button, TShiftState Shift, int X, int Y);
        void __fastcall RichEdit1KeyUp(TObject *Sender, WORD &Key,
          TShiftState Shift);
        void __fastcall PopapMenu_InsertRazvertka_0Click(TObject *Sender);
        void __fastcall PopapMenu_InsertRazvertka_180Click(
          TObject *Sender);
        void __fastcall RichEdit1KeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);
        void __fastcall Az1_begClick(TObject *Sender);
        void __fastcall Az1_endClick(TObject *Sender);
        void __fastcall Az2_begClick(TObject *Sender);
        void __fastcall Az2_endClick(TObject *Sender);
        void __fastcall Azn_begClick(TObject *Sender);
        void __fastcall Azn_endClick(TObject *Sender);
        void __fastcall Lx1_begClick(TObject *Sender);
        void __fastcall Lx1_endClick(TObject *Sender);
        void __fastcall Lx2_begClick(TObject *Sender);
        void __fastcall Lx2_endClick(TObject *Sender);
        void __fastcall Angle1_begClick(TObject *Sender);
        void __fastcall Angle1_endClick(TObject *Sender);
        void __fastcall Az_L_Angle_endClick(TObject *Sender);
        void __fastcall N_General_cavenameClick(TObject *Sender);
        void __fastcall N_General_RegionClick(TObject *Sender);
        void __fastcall N_General_SurvTeamClick(TObject *Sender);
        void __fastcall FormResize(TObject *Sender);
        void __fastcall DataOrder_L_Az_AnClick(TObject *Sender);
        void __fastcall DataOrder_Az_L_AnClick(TObject *Sender);
        void __fastcall DataOrder_L_An_AzClick(TObject *Sender);
        void __fastcall DataOrder_An_L_AzClick(TObject *Sender);
        void __fastcall DataOrder_An_Az_LClick(TObject *Sender);
        void __fastcall DataOrder_Az_An_LClick(TObject *Sender);
        void __fastcall PopapMenu_InsertColor_OrangeClick(TObject *Sender);
        void __fastcall ToolButton15Click(TObject *Sender);
        void __fastcall FormShow(TObject *Sender);
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
        void __fastcall NPiketPropert_entClick(TObject *Sender);
        void __fastcall NPiketPropert_sifClick(TObject *Sender);
        void __fastcall NPiketPropert_redClick(TObject *Sender);
        void __fastcall NPiketPropert_comClick(TObject *Sender);
        void __fastcall NPiketPropert_endlabelClick(TObject *Sender);
        void __fastcall PopapMenu_InsertFormat_FromToClick(
          TObject *Sender);
        void __fastcall PopapMenu_InsertFormat_LabelClick(TObject *Sender);
        void __fastcall PopapMenu_InsertRazvertka_RRClick(TObject *Sender);
        void __fastcall N_General_includeClick(TObject *Sender);
        void __fastcall N_General_FixClick(TObject *Sender);
        void __fastcall N2equate1Click(TObject *Sender);
        void __fastcall prefix1Click(TObject *Sender);
        void __fastcall endprefix1Click(TObject *Sender);
        void __fastcall ToolButton16Click(TObject *Sender);
        void __fastcall N_surf1Click(TObject *Sender);
        void __fastcall N_surf2Click(TObject *Sender);
        void __fastcall N_surf3Click(TObject *Sender);
        void __fastcall N_dup1Click(TObject *Sender);
        void __fastcall N_dup2Click(TObject *Sender);
        void __fastcall N_dup3Click(TObject *Sender);
        void __fastcall Az_endClick(TObject *Sender);
        void __fastcall N_general_EndSurveyClick(TObject *Sender);
        void __fastcall declination1Click(TObject *Sender);
        void __fastcall Data_order_LAzAnlrudClick(TObject *Sender);
        void __fastcall H_begClick(TObject *Sender);
        void __fastcall H_endClick(TObject *Sender);
	void __fastcall OpenMain1Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall TEditForm(TComponent* Owner);
        void SetFileName(const UnicodeString);
        char Confirm(void);
        UnicodeString FFileName;
        boolean edit_flag;

        //int CurrX,CurrY;//текущие координаты нажатой мышки в ReachEdit
        void __fastcall PopapMenu_InsertComand(UnicodeString Str);
        void __fastcall InsertComand(UnicodeString Str, int CaretPos_x, int CaretPos_y);


        bool __fastcall FoundNearestString(UnicodeString Str, int & iy, int& iPos);


        void __fastcall DefineCurrColor(UnicodeString FoundStr);//функция определения текущего цвета (по положению текущего курсора)

        void __fastcall DefineCurrMode( UnicodeString Str1,//первый из 2 ключевых режимов (оно же и по умолчанию)
                                        UnicodeString Str2 ,//второй из 2 ключевых режимов
                                        UnicodeString Str3,//Слово: название режима
                                        UnicodeString Str4,//надпись для первого режима
                                        UnicodeString Str5//надпись для второго режима
                                       );//функция определения (по положению текущего курсора) текущего Mode (оно определяется строками Str1 и Str2, т.е. режим может иметь только два значения!)

        void __fastcall DefineCurrMode_bez_umolchania( UnicodeString Str1,//первый из 2 ключевых режимов (оно же и по умолчанию)
                                                   UnicodeString Str2 ,//отключение режима
                                                   UnicodeString Str3,//Слово: название режима
                                                   UnicodeString Str4,//надпись для первого режима
                                                   UnicodeString Str5//надпись для второго режима
                                                  );//функция определения (по положению текущего курсора) текущего Mode (оно определяется строками Str1 и Str2, т.е. режим может иметь только два значения!)
                                                   //Аналогична "DefineCurrMode", но без (L"по умолчанию")-- это свойственно например оператору #surface, для которого нету "по умолчанию". Отсутствие оператора означает, что режим отключен.

        void __fastcall DefineCurrMode_bez_umolchania2( UnicodeString Str1,//первый из 2 ключевых режимов (оно же и по умолчанию)
                                                         UnicodeString Str2_1 ,//отключение режима1
                                                         UnicodeString Str2_2 ,//отключение режима2 (Oобщее отключение всех режимов)
                                                         UnicodeString Str3,//Слово: название режима
                                                         UnicodeString Str4,//надпись для первого режима
                                                         UnicodeString Str5//надпись для второго режима
                                                        ); //функция определения (по положению текущего курсора) текущего Mode (оно определяется строками Str1 и Str2, т.е. режим может иметь только два значения!)
                                                           //Аналогична "DefineCurrMode", но без (L"по умолчанию")-- это свойственно например оператору #surface, для которого нету "по умолчанию". Отсутствие оператора означает, что режим отключен.
                                                           //Введено 2 типа "отключения режима" -- это свойственно например операторам #correction, для которых есть 2 способа отключения режима (конкретно для этого режима, и общий)

        void __fastcall DefineCurrMode_bez_umolchania_n
                                                 ( UnicodeString Str1,//первый из 2 ключевых режимов (оно же и по умолчанию)
                                                   UnicodeString Str2_1 ,//отключение режима1
                                                   UnicodeString Str2_2 ,//отключение режима2 (Oобщее отключение всех режимов)
                                                   UnicodeString Str3,UnicodeString Str3_help,//Слово-- название режима и вспомогательное слово (определяет, какие единицы, метры, градусы)
                                                   UnicodeString Str4,//надпись для первого режима
                                                   UnicodeString Str5//надпись для второго режима
                                                  );  //для режимов типа  #corr_A[n], #corr_L_beg[x], #corr_clino[x], где необходимо определить еще и на какую величину корректировать...

        void __fastcall DefineDataOrder();//функция определения (по положению текущего курсора) текущего DataOrder


        void __fastcall RecalculateModes();//для текущего курсора в текстевычисляем его режимы (цвет, тип съемки, развертки и  т.д.)

        void __fastcall DrawingTextByType();//раскрашиваем текст по типам  (коменты серым, и т.д.)

        void __fastcall FoundComentars( );//функция определения Комментариев
        void __fastcall HideComentars();

		void updateCanOpenMainFile();

};
//---------------------------------------------------------------------------
extern PACKAGE TEditForm *EditForm;
//---------------------------------------------------------------------------
#endif
