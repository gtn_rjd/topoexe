﻿//---------------------------------------------------------------------------
#ifndef FormHelpH
#define FormHelpH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
//---------------------------------------------------------------------------
class THelpForm : public TForm
{
__published:    // IDE-managed Components
        TTabSheet *TabSheet1;
        TTabSheet *TabSheet2;
        TTabSheet *TabSheet3;
        TMemo *Memo2;
        TMemo *Memo3;
        TMemo *Memo4;
        TTabSheet *TabSheet4;
        TMemo *Memo5;
        TPageControl *PageControl1;
        TTabSheet *TabSheet5;
        TMemo *Memo6;
        TTabSheet *TabSheet6;
        TMemo *Memo7;
        TTabSheet *TabSheet7;
        TMemo *Memo8;
        TTabSheet *TabSheet8;
        TMemo *Memo9;
        TTabSheet *TabSheet9;
        TMemo *Memo1;
        TTabSheet *TabSheet10;
        TMemo *Memo10;
private:        // User declarations
public:         // User declarations
        __fastcall THelpForm(TComponent* Owner);
        //char HelpNum;
        void SetHelpNum(char);
};
//---------------------------------------------------------------------------
extern PACKAGE THelpForm *HelpForm;
//---------------------------------------------------------------------------
#endif
  