﻿//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "FormInsSurv.h"
#include "FormEdit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TInsSurvForm *InsSurvForm;
//---------------------------------------------------------------------------
__fastcall TInsSurvForm::TInsSurvForm(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TInsSurvForm::Button1Click(TObject *Sender)
{
 InsSurvForm->Close();
}
//---------------------------------------------------------------------------

void __fastcall TInsSurvForm::Button2Click(TObject *Sender)
{
 UnicodeString srv=L"#survey ";
 UnicodeString srvTitle=L"#survey_title ";
 UnicodeString srvDate=L"#survey_date ";
 if(Edit1->Text.Trim()!=L"")
  {
   if(Edit1->Text[1]!=L'^') Edit1->Text = Edit1->Text.Insert(L"^",1);
   srv+= Edit1->Text;
  }
 if(Edit2->Text.Trim()!=L""){srv = srv + L" " + Edit2->Text.Trim();}

 EditForm->RichEdit1->Lines->Insert(EditForm->RichEdit1->CaretPos.y+1, L" ");
 EditForm->RichEdit1->CaretPos=Point(0,EditForm->RichEdit1->CaretPos.y-1);
 EditForm->RichEdit1->Lines->Insert(EditForm->RichEdit1->CaretPos.y+1, srv);
 EditForm->RichEdit1->CaretPos=Point(0,EditForm->RichEdit1->CaretPos.y-1);

 if(Edit3->Text.Trim()!=L""){
   srvTitle += Edit3->Text.Trim();
   EditForm->RichEdit1->Lines->Insert(EditForm->RichEdit1->CaretPos.y+1, srvTitle);
   EditForm->RichEdit1->CaretPos=Point(0,EditForm->RichEdit1->CaretPos.y-1);
   }
 if(Edit4->Text.Trim()!=L""){
   srvDate += Edit4->Text.Trim();
   EditForm->RichEdit1->Lines->Insert(EditForm->RichEdit1->CaretPos.y+1, srvDate);
   EditForm->RichEdit1->CaretPos=Point(0,EditForm->RichEdit1->CaretPos.y-1);
   }
 InsSurvForm->Close();  
}
//---------------------------------------------------------------------------

