﻿//---------------------------------------------------------------------------

#ifndef FormInsSurvH
#define FormInsSurvH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
//---------------------------------------------------------------------------
class TInsSurvForm : public TForm
{
__published:	// IDE-managed Components
        TEdit *Edit1;
        TEdit *Edit2;
        TEdit *Edit3;
        TLabel *Label1;
        TLabel *Label2;
        TLabel *Label3;
        TEdit *Edit4;
        TLabel *Label4;
        TButton *Button1;
        TButton *Button2;
        void __fastcall Button1Click(TObject *Sender);
        void __fastcall Button2Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall TInsSurvForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TInsSurvForm *InsSurvForm;
//---------------------------------------------------------------------------
#endif
