﻿// ---------------------------------------------------------------------------
#include <vcl.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <printers.hpp>
#include <string>
#include <vector>
#pragma hdrstop

#include "TOpO.h"
#include "FormPrn.h"
#include "FormHelp.h"
#include "formtrans.h"
#include "UnitPrintScalePosition.h"
#include "OgreCMCaveModel.h"
#include "CMAssertions.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "CSPIN"
#pragma resource "*.dfm"
TPrnForm *PrnForm;
// ---------------------------------------------------------------------------

TColor SetTColor(int oldColor);

enum ProjectionMode {
    PROJMODE_PLAN = 0, PROJMODE_VERT = 1, PROJMODE_EXTENDED_PROFILE = 2, PROJMODE_OTHER = 3
};

inline V2 besier3(float t, V2 a, V2 ac, V2 bc, V2 b) {
    return a * (1.0f - t) * (1.0f - t) * (1.0f - t) + 3.0f * ac * t * (1.0f - t) * (1.0f - t) + 3.0f * bc * t * t * (1.0f - t) + b * t * t * t;
}

// -----------------------------------------------------------------------------
__fastcall TPrnForm::TPrnForm(TComponent* Owner) : TForm(Owner) {
    sm2i = 2.54; // sm/inch
    PRes = 600; // printer resolution

    Razmer = 1; // vhod_ data array size
    mashtab = 10; // print scale: m/sm
    NList_x = 0;
    NList_y = 0;
    B_Grid_x = 0;
    B_Grid_y = 0;

    X_cm = 20; // A4, sm
    Y_cm = 27;

    Phi = 0; // spherical angles
    Theta = 0;
    mode = 0; // 1 - развертка, 0 - план/разрез

    ShiftX = ShiftY = 0.; // смещение нитки относительно края листа (в сантиметрах)
    ShiftX_ = ShiftX * mashtab * 100.; // смещение нитки относительно края листа (в сантиметрах пещерных)
    ShiftY_ = ShiftY * mashtab * 100.; // смещение нитки относительно края листа (в сантиметрах пещерных)

    cmCave = NULL;
}

// -----------------------------------------------------------------------------
void TPrnForm::SetParam(unsigned int size, float th, float ph, char State) {
    Razmer = size;
    Theta = th;
    Phi = ph;
    mode = State - 5; // 0 для проекций, 1 для разреза-разв.
    Bitmap1 = new Graphics::TBitmap();
    Bitmap1->Width = PaintBox1->Width; // assign the initial width...
    Bitmap1->Height = PaintBox1->Height; // ...and the initial height
    if (mode == 0) {
        ThetaE->Text = (Theta * 180. / 3.14159265358979);
        PhiEdit->Text = (Phi * 180. / 3.14159265358979);
    }
    LastTheta = Theta * 180. / 3.14159265358979;
    LastPhi = Phi * 180. / 3.14159265358979; // последние значения углов, которые вводились!

    proj_x = new long[Razmer + 1];
    proj_y = new long[Razmer + 1];

    if (MainForm1->WRazmer) {
        Wpro = new WPRO[MainForm1->WRazmer];
    }; // проекции ежика

    if (TransForm->DataLength.lrud) // проекции lrud
    {
        Pro_x = new PRO[Razmer + 1];
        Pro_y = new PRO[Razmer + 1];
    }

    Fitting(); // draw Bitmap

    RadioGroupProjectionMode->ItemIndex = PROJMODE_OTHER; // Другая проекция
}

// ---------------------------- RePaint window ---------------------------------
void __fastcall TPrnForm::PaintBox1Paint(TObject *Sender) {
    PaintBox1->Canvas->Draw(0, 0, Bitmap1);
    PaintBox1->Canvas->Pen->Color = clGreen;
    PaintBox1->Canvas->Brush->Style = bsBDiagonal;
    PaintBox1->Canvas->Brush->Color = clGreen;

    for (int N_x = 0; N_x < raz_x; ++N_x)
        for (int N_y = 0; N_y < raz_y; ++N_y)
            if (p_mass[N_x][N_y] == 1)
                PaintBox1->Canvas->Rectangle(X_cm*mashtab*100*Fitfac*N_x + 1, Y_cm*mashtab*100*Fitfac*N_y + 1, X_cm*mashtab*100*Fitfac*(N_x + 1) - 1,
                Y_cm*mashtab*100*Fitfac*(N_y + 1) - 1);
    PaintBox1->Canvas->Pen->Color = clBlack;
    PageLines();
}

// ================================functions====================================

void TPrnForm::projection(float phi, float theta) {
    float C = cos(phi);
    float S = sin(phi);
    float Ct = cos(theta);
    float St = sin(theta);
    if (mode == 0)
        for (unsigned int s = 0; s < Razmer; ++s) // проекции точек нитки
        {
            proj_x[s] = P[s].pos.y * C - P[s].pos.x * S;
            proj_y[s] = (P[s].pos.y * S + P[s].pos.x * C) * St + P[s].pos.z * Ct;
        }
    extendedProfileSwap = false;
    if (mode == 1)
        for (unsigned int s = 0; s < Razmer; ++s) {
            proj_x[s] = P[s].XY;
            proj_y[s] = P[s].pos.z;
        }
    if (MainForm1->WRazmer) // walls - ежик и lrud
    {
        if (mode == 0)
            for (unsigned int s = 0; s < MainForm1->WRazmer; ++s) {
                Wpro[s].X = W[s].pos.y * C - W[s].pos.x * S;
                Wpro[s].Y = (W[s].pos.x * C + W[s].pos.y * S) * St + W[s].pos.z * Ct;
            }
        if (mode == 1)
            for (unsigned int s = 0; s < MainForm1->WRazmer; ++s) {
                Wpro[s].X = (int)(P[W[s].pik].XY);
                Wpro[s].Y = (int)(W[s].pos.z);
            }
    }

    if (TransForm->DataLength.lrud) // если есть lrud
    {
        if (mode == 0)
            for (unsigned int s = 0; s < Razmer; ++s) {
                if (P[s].left >= 0) {
                    Pro_x[s].left = (P[s].pos.y - P[s].left * P[s].sin_c) * C - (P[s].pos.x - P[s].left * P[s].cos_c) * S;
                    Pro_y[s].left = ((P[s].pos.y - P[s].left * P[s].sin_c) * S + (P[s].pos.x - P[s].left * P[s].cos_c) * C) * St + P[s].pos.z * Ct;
                }
                if (P[s].right >= 0) {
                    Pro_x[s].right = (P[s].pos.y + P[s].right * P[s].sin_c) * C - (P[s].pos.x + P[s].right * P[s].cos_c) * S;
                    Pro_y[s].right = ((P[s].pos.y + P[s].right * P[s].sin_c) * S + (P[s].pos.x + P[s].right * P[s].cos_c) * C) * St + P[s].pos.z * Ct;
                }
                if (P[s].up >= 0) {
                    Pro_x[s].up = (P[s].pos.y) * C - (P[s].pos.x) * S;
                    Pro_y[s].up = ((P[s].pos.y) * S + (P[s].pos.x) * C) * St + (P[s].pos.z + P[s].up) * Ct;
                }
                if (P[s].down >= 0) {
                    Pro_x[s].down = (P[s].pos.y) * C - (P[s].pos.x) * S;
                    Pro_y[s].down = ((P[s].pos.y) * S + (P[s].pos.x) * C) * St + (P[s].pos.z - P[s].down) * Ct;
                }
            }
        if (mode == 1)
            for (unsigned int s = 0; s < Razmer; ++s) {
                if (P[s].up >= 0) {
                    Pro_x[s].up = P[s].XY;
                    Pro_y[s].up = P[s].pos.z + P[s].up;
                }
                if (P[s].down >= 0) {
                    Pro_x[s].down = P[s].XY;
                    Pro_y[s].down = P[s].pos.z - P[s].down;
                }
            }
    }
}

// -----------------------------------------------------------------------------
void TPrnForm::MinMax(void) {
    // Edit1->Text=Razmer;
    xmin = xmax = proj_x[0];
    ymin = ymax = proj_y[0];
    for (unsigned int s = 1; s < Razmer; ++s) {
        if (proj_x[s] < xmin)
            xmin = proj_x[s];
        if (proj_x[s] > xmax)
            xmax = proj_x[s];
        if (proj_y[s] < ymin)
            ymin = proj_y[s];
        if (proj_y[s] > ymax)
            ymax = proj_y[s];
    }
}

// ********************** selected pages 2D array ******************************
// -------------------------- initialization -----------------------------------
void TPrnForm::ini_p_mass() {
    // raz_x = (xmax-xmin)/(mashtab*X_cm*100)+1;
    // raz_y = (ymax-ymin)/(mashtab*Y_cm*100)+1;

    // #2008_01_31 Я попробую растянуть так, чтобы весь лист А4 (нижний и правый)--  помещались на рисунок
    raz_x = int((xmax - xmin + ShiftX_) / (mashtab * X_cm * 100) + 0.99999);
    raz_y = int((ymax - ymin + ShiftY_) / (mashtab * Y_cm * 100) + 0.99999);

    p_mass = new wchar_t* [raz_x];
    int i = 0;
	int j = 0;
    // if(p_mass == NULL)
    // Application->MessageBox(L"Unsufficient memory!\n ",L"TOpO Print: Error", MB_OK);
    for (i = 0; i < raz_x; i++)
        p_mass[i] = new wchar_t[raz_y];
    for (i = 0; i < raz_x; ++i)
        for (j = 0; j < raz_y; ++j)
            p_mass[i][j] = 0;

    // #2008_01_31 - Это я добавил, чтобы оно пересчитывала множитель Fitfac:
    Fitfac = PaintBox1->Width / (raz_x * X_cm * mashtab * 100);
    if (Fitfac * (raz_y * Y_cm * mashtab * 100) > PaintBox1->Height)
        Fitfac = PaintBox1->Height / (raz_y * Y_cm * mashtab * 100);
}

// -------------------------------- delete -------------------------------------
void TPrnForm::del_p_mass() {
    for (int i = 0; i < raz_x; i++)
        delete[]p_mass[i];
    delete[]p_mass;
}

// ****************************** Bitmap ***************************************
// ---------------------------- (Re)draw Bitmap --------------------------------
void TPrnForm::ReDrawBmp() {
    Bitmap1->Canvas->Brush->Color = clWhite;
    Bitmap1->Canvas->Rectangle(0, 0, Bitmap1->Width, Bitmap1->Height);
    Bitmap1->Canvas->Pen->Color = clBlack;

    for (unsigned int s = 1; s < Razmer; ++s) {
        if ((!(P[s].priz & PRIZ_BRANCH)) && !((P[s].priz & PRIZ_SURFACE) && !(CheckBox4->State == cbChecked)) && !
            ((P[s].priz & PRIZ_DUPLICATE) && !(CheckBox9->State == cbChecked))) {
            Bitmap1->Canvas->MoveTo((proj_x[s - 1] - xmin + ShiftX_)*Fitfac, (ymax - proj_y[s - 1] + ShiftY_)*Fitfac);
            Bitmap1->Canvas->LineTo((proj_x[s] - xmin + ShiftX_)*Fitfac, (ymax - proj_y[s] + ShiftY_)*Fitfac);
        }
    }

    if (CheckBox11->State == cbChecked) {
        auto func = [this](V2 pos) {
            return V2((pos.x - xmin + ShiftX_)*Fitfac, (ymax - (pos.y) + ShiftY_)*Fitfac);
        };

        Bitmap1->Canvas->Pen->Color = clLtGray;
        drawOutline(Bitmap1->Canvas, 3, func);
        Bitmap1->Canvas->Pen->Color = clBlack;
    }
}

// ---------------------------- Bitmap Fitting ---------------------------------
void TPrnForm::Fitting() {
    projection(Phi, Theta);
    MinMax();
    ini_p_mass();

    /* Это Лешин код:
     Fitfac = PaintBox1->Width/(xmax-xmin);
     if(Fitfac*(ymax-ymin) > PaintBox1->Height)
     Fitfac = PaintBox1->Height/(ymax-ymin);/* */

    // #2008_01_31 Я попробую растянуть так, чтобы весь лист А4 (нижний и правый)--  помещались на рисунок
    Fitfac = PaintBox1->Width / (raz_x * X_cm * mashtab * 100);
    if (Fitfac * (raz_y * Y_cm * mashtab * 100) > PaintBox1->Height)
        Fitfac = PaintBox1->Height / (raz_y * Y_cm * mashtab * 100);

    ReDrawBmp();
    PaintBox1->Canvas->Draw(0, 0, Bitmap1);
}

// ------------------------Bitmap - page lines----------------------------------
void TPrnForm::PageLines() {
    PaintBox1->Canvas->Pen->Color = clRed;
    for (int i = 0; X_cm*i*mashtab * 100 * Fitfac < PaintBox1->Width; ++i) {
        PaintBox1->Canvas->MoveTo(X_cm*i*mashtab*100*Fitfac, 0);
        PaintBox1->Canvas->LineTo(X_cm*i*mashtab*100*Fitfac, PaintBox1->Height);
    }
    for (int i = 0; Y_cm*i*mashtab * 100 * Fitfac < PaintBox1->Height; ++i) {
        PaintBox1->Canvas->MoveTo(0, Y_cm*i*mashtab*100*Fitfac);
        PaintBox1->Canvas->LineTo(PaintBox1->Width, Y_cm*i*mashtab*100*Fitfac);
    }

    PaintBox1->Canvas->Pen->Color = clBlack;
}

// =========================== Canvas for printing =============================
// --------------------------------- Ramka -------------------------------------
void TPrnForm::Ramka(TCanvas *Canvas) {
    Canvas->Pen->Style = psSolid;
    // Canvas->Pen->Color = clGray;
    Canvas->Brush->Style = bsClear;
    Canvas->Rectangle(0, 0, X_cm*PRes / sm2i, Y_cm*PRes / sm2i);
}

// --------------------------------- Setka -------------------------------------
void __fastcall TPrnForm::MyGrid(TCanvas *Canvas, int Num_x, int Num_y) {

    float m;
    try {
        m = StrToFloat(EditGridScale->Text);
    }
    catch (...) {
        m = 1;
        EditGridScale->Text = m;
    }
    // -----------Остаток-для-сетки -----------------------//
    float GOst_x = 0, GOst_y = 0;

    int n = 0;
    for (n = 0; m*n < X_cm * Num_x; ++n)
        GOst_x = m * (n + 1) - X_cm * Num_x;
    for (n = 0; m*n < Y_cm * Num_y; ++n)
        GOst_y = m * (n + 1) - Y_cm * Num_y;

    // -------------------------------------------------//
    float Step = PRes / sm2i;
    Canvas->Pen->Style = psDot;
    // Edit2->Text = GOst_y;

    for (int i = 0; i < (X_cm + 1) / m; ++i) {
        Canvas->MoveTo((i*m + GOst_x)*Step, 0);
        Canvas->LineTo((i*m + GOst_x)*Step, (PRes / sm2i)*Y_cm);
    }

    for (int i = 0; i < (Y_cm + 1) / m; ++i) {
        Canvas->MoveTo(0, (i*m + GOst_y)*Step);
        Canvas->LineTo((PRes / sm2i)*X_cm, (i*m + GOst_y)*Step);
    }

	Canvas->Pen->Style = psSolid;
}

// -------------------------------- Walls --------------------------------------
void TPrnForm::Walls(TCanvas *Canvas, float PFf, int N_x, int N_y, int R) {
	// R - размер (в pix) кружков, 3*R или 2*R+1 - линий (рисок)
    unsigned int s = 0;
    double P_X, P_Y, Pk_X, Pk_Y; // текущие проекции
	float C = cos(Phi);
    float S = sin(Phi);
    if (mode == 0) {
        for (s = 1; s < MainForm1->WRazmer; ++s) // риски
        {
            if (!(P[W[s].pik].priz & PRIZ_DUPLICATE) || (CheckBox9->State == cbChecked)) // показ. duplicate только при нажатой кнопке
            {
                float vx = 0;
                float vy = 0;
                float v2 = 0;
                float ax = 0;
                float ay = 0; // переменные для рисок                    
                Pk_X = (proj_x[W[s].pik] - xmin + ShiftX_) * PFf - N_x * X_cm * PRes / sm2i;
                Pk_Y = (ymax - proj_y[W[s].pik] + ShiftY_) * PFf - N_y * Y_cm * PRes / sm2i;
                P_X = (Wpro[s].X - xmin + ShiftX_) * PFf - N_x * X_cm * PRes / sm2i;
                P_Y = (ymax - Wpro[s].Y + ShiftY_) * PFf - N_y * Y_cm * PRes / sm2i;
                vx = Wpro[s].X - proj_x[W[s].pik]; // проекция вектора от станции к стене в см
                vy = Wpro[s].Y - proj_y[W[s].pik];
                v2 = vx * vx + vy * vy; // длина в см
                if (v2 > 90) // если длина (расст. в данной проекции до стены) > 9 см - рисуем риску
                {
                    // Canvas->Ellipse(P_X-2*R,P_Y+2*R, P_X+2*R,P_Y-2*R);  // для отладки
                    ay = R * vx / sqrt(v2);
                    ax = R * vy / sqrt(v2); // вектор длины R
                    Canvas->MoveTo(Pk_X, Pk_Y);
//                    Canvas->MoveTo(P_X - 3*ay, P_Y + 3*ax);
                    Canvas->LineTo(P_X, P_Y);
                    Canvas->MoveTo(P_X + 2*ax, P_Y + 2*ay);
                    Canvas->LineTo(P_X - 2*ax, P_Y - 2*ay);


                }
            }
        }
    }
    if (mode == 1) // разрез-разв.
    {
		for (s = 1; s < MainForm1->WRazmer; ++s) // риски
        {
            if (!(P[W[s].pik].priz & PRIZ_DUPLICATE) || (CheckBox9->State == cbChecked)) // показ. duplicate только при нажатой кнопке
            {
                Pk_X = (proj_x[W[s].pik] - xmin + ShiftX_) * PFf - N_x * X_cm * PRes / sm2i;
                Pk_Y = (ymax - proj_y[W[s].pik] + ShiftY_) * PFf - N_y * Y_cm * PRes / sm2i;
                P_X = (Wpro[s].X - xmin + ShiftX_) * PFf - N_x * X_cm * PRes / sm2i;
                P_Y = (ymax - Wpro[s].Y + ShiftY_) * PFf - N_y * Y_cm * PRes / sm2i;
                int ay = Wpro[s].Y - proj_y[W[s].pik];
                Canvas->MoveTo(Pk_X, Pk_Y);
//                if (abs(ay) > 10) 
                {
//                    if (ay > 0)
//                        Canvas->MoveTo(P_X, P_Y + (2*R + 1));
//                    else
//                        Canvas->MoveTo(P_X, P_Y - (2*R + 1));
                    Canvas->LineTo(P_X, P_Y);
                    Canvas->MoveTo(P_X - (2*R - 1), P_Y);
                    Canvas->LineTo(P_X + 2*R, P_Y);
                }
            }
        }
    }
}

// -------------------------------- PrePrint -----------------------------------
bool TPrnForm::PrePrint() {
    char printFlag = 0;
    int N_x = 0;
	int N_y = 0;
    for (N_x = 0; N_x < raz_x; ++N_x)
        for (N_y = 0; N_y < raz_y; ++N_y)
            if (p_mass[N_x][N_y] == 1) {
                printFlag = 1;
                break;
            }
    if (printFlag != 1) {
        Application->MessageBox(L"There are not marked pages.\n Nothing to print!", L"TOpO Print: Message", MB_OK);
        return false;
    }
    else
        return true;
    // Edit1->Text = UnicodeString(Printer()->PageWidth) + L" pixels.";
    // Edit2->Text = UnicodeString(PRes);
}

// ---------------------------------------------------------------------------
void TPrnForm::WriteToFile() {
    int PRes_ = 0;
    try {
        PRes_ = StrToInt(EditBitmapResolution->Text); // read printer resolution
    }
    catch (...) {
        Application->MessageBox(L"Incorrect Bitmap resolution.", L"TOpO Print: Error", MB_OK);
        return;
    }
    if (PRes_ < 10) {
        Application->MessageBox(L"Incorrect Bitmap resolution.", L"TOpO Print: Error", MB_OK);
        return;
    }
    PRes = (unsigned int)PRes_;

    int button = Application->MessageBox(L"Marked pages will be saved as \nmonohrome bitmap files pageYX.bmp", L"TOpO Print: Message", MB_OKCANCEL);
    if (button == IDCANCEL)
        return;
    float PFf = PRes / (sm2i * mashtab * 100);
    Graphics::TBitmap *SaveBitmap;
    UnicodeString SFileName;

    int N_x = 0;
	int N_y = 0;
    unsigned int s = 0;
    for (N_x = 0; N_x < raz_x; ++N_x)
        for (N_y = 0; N_y < raz_y; ++N_y)
            if (p_mass[N_x][N_y] == 1) {
                SaveBitmap = new Graphics::TBitmap();
                SaveBitmap->Monochrome = CheckBox13->State != cbChecked;
                // SaveBitmap->PixelFormat = pf1bit;   //24 -> 8 bit/pixel
                SaveBitmap->Width = (int)X_cm * PRes / sm2i;
                SaveBitmap->Height = (int)Y_cm * PRes / sm2i;
                SaveBitmap->Canvas->Brush->Color = clWhite;
                SaveBitmap->Canvas->Pen->Color = clWhite;
                SaveBitmap->Canvas->Rectangle(0, 0, SaveBitmap->Width, SaveBitmap->Height);
                SaveBitmap->Canvas->Pen->Color = clBlack;

                SFileName = ExtractFilePath(MainForm1->CurrFileName) + L"page" + CurrToStr(N_y) + CurrToStr(N_x) + L".bmp";

                if (CheckBox8->State == cbChecked) // все метки
                {
                    SaveBitmap->Canvas->Font->Size = 8;
                    for (s = 0; s < Razmer; ++s) {
                        // ShiftX_, ShiftY_;//смещение нитки относительно края листа (в сантиметрах пещерных)
                        if (P[s].met > 0)
                            SaveBitmap->Canvas->TextOut((proj_x[s] - xmin + ShiftX_)*PFf - N_x*X_cm*PRes / sm2i + 2, (ymax - proj_y[s] + ShiftY_)*PFf - N_y*Y_cm*PRes / sm2i + 2,
                            StationList->Strings[P[s].met]);
                    }
                    SaveBitmap->Canvas->Font->Size = 10;
                }

                if (CheckBox5->State == cbChecked) // метки т. биф.
                {
                    SaveBitmap->Canvas->Font->Size = 8;
                    for (s = 0; s < Razmer; ++s) {
                        if ((P[s].priz & PRIZ_BRANCH) && (V[P[s].vet].B) != 0)
                            SaveBitmap->Canvas->TextOut((proj_x[s] - xmin + ShiftX_)*PFf - N_x*X_cm*PRes / sm2i + 2, (ymax - proj_y[s] + ShiftY_)*PFf - N_y*Y_cm*PRes / sm2i + 2,
                            StationList->Strings[V[P[s].vet].B]);
                        if ((P[s + 1].priz & PRIZ_BRANCH) && (V[P[s].vet].E) != 0)
                            SaveBitmap->Canvas->TextOut((proj_x[s] - xmin + ShiftX_)*PFf - N_x*X_cm*PRes / sm2i + 2, (ymax - proj_y[s] + ShiftY_)*PFf - N_y*Y_cm*PRes / sm2i + 2,
                            // MainForm1->Name(Labels,V[P[s].vet].E).c_str());
                            StationList->Strings[V[P[s].vet].E]);
                    }
                    SaveBitmap->Canvas->Font->Size = 10;
                }

                if (CheckBox7->State == cbChecked) // комментарий
                {
                    SaveBitmap->Canvas->Font->Size = 8;
                    for (s = 0; s < Razmer; ++s) {
                        if (P[s].com)
                            SaveBitmap->Canvas->TextOut((proj_x[s] - xmin + ShiftX_)*PFf - N_x*X_cm*PRes / sm2i + 2, (ymax - proj_y[s] + ShiftY_)*PFf - N_y*Y_cm*PRes / sm2i + 2,
                            MainForm1->Name(Comments, P[s].com).c_str());
                    }
                    if ((CheckBox7->State == cbChecked) && (MainForm1->WRazmer)) // walls - комментарий
                        for (s = 0; s < MainForm1->WRazmer; ++s) {
                            if (W[s].com)
                                SaveBitmap->Canvas->TextOut((Wpro[s].X - xmin + ShiftX_)*PFf - N_x*X_cm*PRes / sm2i + 2,
                                (ymax - Wpro[s].Y + ShiftY_)*PFf - N_y*Y_cm*PRes / sm2i + 2, MainForm1->Name(WComments, W[s].com).c_str());
                        }
                    SaveBitmap->Canvas->Font->Size = 10;
                }

                if (CheckBox2->State == cbChecked) // рамка
                        Ramka(SaveBitmap->Canvas);
                if (CheckBox1->State == cbChecked) // сетка
                        MyGrid(SaveBitmap->Canvas, N_x, N_y);
                if (CheckBox3->State == cbChecked) // кружки пикетов (станций)
                    for (s = 0; s < Razmer; ++s)
                        if ((!(P[s].priz & PRIZ_BRANCH)) && !((P[s].priz & PRIZ_SURFACE) && !(CheckBox4->State == cbChecked)) && !
                            ((P[s].priz & PRIZ_DUPLICATE) && !(CheckBox9->State == cbChecked))) {
                            SaveBitmap->Canvas->Ellipse((proj_x[s] - xmin + ShiftX_)*PFf - N_x*X_cm*PRes / sm2i - 2, (ymax - proj_y[s] + ShiftY_)*PFf - N_y*Y_cm*PRes / sm2i + 2,
                                (proj_x[s] - xmin + ShiftX_)*PFf - N_x*X_cm*PRes / sm2i + 2, (ymax - proj_y[s] + ShiftY_)*PFf - N_y*Y_cm*PRes / sm2i - 2);
                        }
                if (CheckBox6->State == cbChecked) // линии между пикетами
                    for (s = 1; s < Razmer; ++s)
                        if ((!(P[s].priz & PRIZ_BRANCH)) && !((P[s].priz & PRIZ_SURFACE) && !(CheckBox4->State == cbChecked)) && !
							((P[s].priz & PRIZ_DUPLICATE) && !(CheckBox9->State == cbChecked))) {
							SaveBitmap->Canvas->Pen->Color = SetTColor(P[s].col);
                            SaveBitmap->Canvas->MoveTo((proj_x[s - 1] - xmin + ShiftX_)*PFf - N_x*X_cm*PRes / sm2i, (ymax - proj_y[s - 1] + ShiftY_)*PFf - N_y*Y_cm*PRes / sm2i);
                            SaveBitmap->Canvas->LineTo((proj_x[s] - xmin + ShiftX_)*PFf - N_x*X_cm*PRes / sm2i, (ymax - proj_y[s] + ShiftY_)*PFf - N_y*Y_cm*PRes / sm2i);
                        }

				auto transformFunc = [&](V2 from) {
					return V2((from.x - xmin + ShiftX_)*PFf - N_x*X_cm*PRes / sm2i, (ymax - (from.y) + ShiftY_)*PFf - N_y*Y_cm*PRes / sm2i);
				};

                if (CheckBox11->State == cbChecked) {
					drawOutline(SaveBitmap->Canvas, 10, transformFunc);
				}

				if ((CheckBox10->State == cbChecked || CheckBox12->State == cbChecked) && ((TransForm->DataLength.lrud) || (MainForm1->WRazmer))) {// отметки (риски) стен
//					if (PRes_ <= 300)
//						Walls(SaveBitmap->Canvas, PFf, N_x, N_y, 2);
//					else
//						Walls(SaveBitmap->Canvas, PFf, N_x, N_y, 4);
					auto printFunc = [SaveBitmap, this](V2 f, V2 t, CM::Color) {
						SaveBitmap->Canvas->MoveTo(f.x, f.y);
						SaveBitmap->Canvas->LineTo(t.x, t.y);
                    };
					drawSplay(CheckBox10->State != cbChecked, printFunc, transformFunc);
				}
                SaveBitmap->SaveToFile(SFileName);
                delete SaveBitmap;
            }
}

// ---------------------------------------------------------------------------
void TPrnForm::SendToPrinter() {
    int PRes_ = 0;
    try {
        PRes_ = StrToInt(EditPrinterResolution->Text); // read printer resolution
    }
    catch (...) {
        Application->MessageBox(L"Incorrect Printer resolution.", L"TOpO Print: Error", MB_OK);
        return;
    }
    if (PRes_ < 50) {
        Application->MessageBox(L"Incorrect Printer resolution.", L"TOpO Print: Error", MB_OK);
        return;
    }
    PRes = (unsigned int)PRes_;

    float PFf = PRes / (sm2i * mashtab * 100);
    TPrinter *APrinter = Printer();
    APrinter->Canvas->Pen->Color = clBlue;
    // PRes=APrinter->PrinterSettings->PixelsPerX;
    int N_x =0;
	int N_y = 0;
    unsigned int s = 0;
    for (N_x = 0; N_x < raz_x; ++N_x)
        for (N_y = 0; N_y < raz_y; ++N_y)
            if (p_mass[N_x][N_y] == 1) {
                APrinter->BeginDoc();

                if (CheckBox3->State == cbChecked) // stations
                {
                    for (s = 0; s < Razmer; ++s)
                        if ((!(P[s].priz & PRIZ_BRANCH)) && !((P[s].priz & PRIZ_SURFACE) && !(CheckBox4->State == cbChecked)) && !
                            ((P[s].priz & PRIZ_DUPLICATE) && !(CheckBox9->State == cbChecked))) {
                            APrinter->Canvas->Ellipse((proj_x[s] - xmin + ShiftX_)*PFf - N_x*X_cm*PRes / sm2i - 5, (ymax - proj_y[s] + ShiftY_)*PFf - N_y*Y_cm*PRes / sm2i + 5,
                                (proj_x[s] - xmin + ShiftX_)*PFf - N_x*X_cm*PRes / sm2i + 5, (ymax - proj_y[s] + ShiftY_)*PFf - N_y*Y_cm*PRes / sm2i - 5);
                        }
                }
                if (CheckBox6->State == cbChecked) // lines
                {
                    for (s = 1; s < Razmer; ++s)
                        if ((!(P[s].priz & PRIZ_BRANCH)) && !((P[s].priz & PRIZ_SURFACE) && !(CheckBox4->State == cbChecked)) && !
                            ((P[s].priz & PRIZ_DUPLICATE) && !(CheckBox9->State == cbChecked))) {
                            APrinter->Canvas->MoveTo((proj_x[s - 1] - xmin + ShiftX_)*PFf - N_x*X_cm*PRes / sm2i, (ymax - proj_y[s - 1] + ShiftY_)*PFf - N_y*Y_cm*PRes / sm2i);
                            APrinter->Canvas->LineTo((proj_x[s] - xmin + ShiftX_)*PFf - N_x*X_cm*PRes / sm2i, (ymax - proj_y[s] + ShiftY_)*PFf - N_y*Y_cm*PRes / sm2i);
                        }
                }
				auto transformFunc = [&](V2 from) {
					return V2((from.x - xmin + ShiftX_)*PFf - N_x*X_cm*PRes / sm2i, (ymax - (from.y) + ShiftY_)*PFf - N_y*Y_cm*PRes / sm2i);
				};

				if (CheckBox11->State == cbChecked) {
					drawOutline(APrinter->Canvas, 10, transformFunc);
                }

                if (CheckBox8->State == cbChecked) // все метки
                {
                    APrinter->Canvas->Font->Size = 8;
                    for (s = 0; s < Razmer; ++s) {
                        if (P[s].met > 0)
                            APrinter->Canvas->TextOut((proj_x[s] - xmin + ShiftX_)*PFf - N_x*X_cm*PRes / sm2i + 2, (ymax - proj_y[s] + ShiftY_)*PFf - N_y*Y_cm*PRes / sm2i + 2,
                            StationList->Strings[P[s].met]);
                    }
                    APrinter->Canvas->Font->Size = 10;
                }

                if (CheckBox5->State == cbChecked) // метки т.биф.
                {
                    APrinter->Canvas->Font->Size = 8;
                    for (s = 0; s < Razmer; ++s) {
                        if ((P[s].priz & PRIZ_BRANCH) && (V[P[s].vet].B) != 0) {
                            APrinter->Canvas->TextOut((proj_x[s] - xmin + ShiftX_)*PFf - N_x*X_cm*PRes / sm2i + 2, (ymax - proj_y[s] + ShiftY_)*PFf - N_y*Y_cm*PRes / sm2i + 2,
                                StationList->Strings[V[P[s].vet].B]);
                        }
                        // MainForm1->Name(Labels,V[P[s].vet].B).c_str());                   }
                        if ((P[s + 1].priz & PRIZ_BRANCH) && (V[P[s].vet].E) != 0) {
                            APrinter->Canvas->TextOut((proj_x[s] - xmin + ShiftX_)*PFf - N_x*X_cm*PRes / sm2i + 2, (ymax - proj_y[s] + ShiftY_)*PFf - N_y*Y_cm*PRes / sm2i + 2,
                                StationList->Strings[V[P[s].vet].E]);
                        }
                        // MainForm1->Name(Labels,V[P[s].vet].E).c_str());                   }
                    }
                    APrinter->Canvas->Font->Size = 10;
                }
                if (CheckBox7->State == cbChecked) // comment
                {
                    APrinter->Canvas->Font->Size = 8;
                    for (s = 0; s < Razmer; ++s) {
                        if (P[s].com)
                            APrinter->Canvas->TextOut((proj_x[s] - xmin + ShiftX_)*PFf - N_x*X_cm*PRes / sm2i + 2, (ymax - proj_y[s] + ShiftY_)*PFf - N_y*Y_cm*PRes / sm2i + 2,
                            MainForm1->Name(Comments, P[s].com).c_str());
                    }
                    APrinter->Canvas->Font->Size = 10;
                }
                if (CheckBox2->State == cbChecked)
                    Ramka(APrinter->Canvas);
                if (CheckBox1->State == cbChecked)
                    MyGrid(APrinter->Canvas, N_x, N_y);
				if ((CheckBox10->State == cbChecked || CheckBox12->State == cbChecked) && ((TransForm->DataLength.lrud) || (MainForm1->WRazmer))) {
					//Walls(APrinter->Canvas, PFf, N_x, N_y, 5); // отметки (риски) стен

					auto printFunc = [APrinter, this](V2 f, V2 t, CM::Color) {
						APrinter->Canvas->MoveTo(f.x, f.y);
						APrinter->Canvas->LineTo(t.x, t.y);
					};
					drawSplay(CheckBox10->State != cbChecked, printFunc, transformFunc);
                }
                APrinter->EndDoc();
            }
    APrinter->Canvas->Pen->Color = clBlack;
}

// -------------------------------PrintButton-----------------------------------
void __fastcall TPrnForm::PrintButtonClick(TObject *Sender) {
    if (PrePrint() && PrintDialog1->Execute())
        SendToPrinter();
}

// ---------------------------------------------------------------------------
void __fastcall TPrnForm::SaveButtonClick(TObject *Sender) {
    if (PrePrint())
        WriteToFile();
}
// ---------------------------------------------------------------------------

// --------------------------mark-pages-for-printing----------------------------
void __fastcall TPrnForm::PaintBox1MouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y) {
    if (Shift.Contains(ssLeft)) {
        NList_x = X / (X_cm * mashtab * 100 * Fitfac);
        NList_y = Y / (Y_cm * mashtab * 100 * Fitfac);
        if ((NList_x < raz_x) && (NList_y < raz_y)) {
            PaintBox1->Canvas->Pen->Color = clGreen;
            PaintBox1->Canvas->Brush->Style = bsBDiagonal;
            PaintBox1->Canvas->Brush->Color = clGreen;
            PaintBox1->Canvas->Rectangle(X_cm*mashtab*100*Fitfac*NList_x + 1, Y_cm*mashtab*100*Fitfac*NList_y + 1, X_cm*mashtab*100*Fitfac*(NList_x + 1) - 1,
                Y_cm*mashtab*100*Fitfac*(NList_y + 1) - 1);

            if (p_mass[NList_x][NList_y] == 1)
                p_mass[NList_x][NList_y] = 0;
            else
                p_mass[NList_x][NList_y] = 1;

            /* ---------Выщелкивание------------- */
            if (p_mass[NList_x][NList_y] == 0) {
                HRGN MyRgn;
                MyRgn = ::CreateRectRgn(PaintBox1->Left + X_cm * mashtab * 100 * Fitfac * NList_x + 1, PaintBox1->Top + Y_cm * mashtab * 100 * Fitfac * NList_y + 1,
                    PaintBox1->Left + X_cm * mashtab * 100 * Fitfac * (NList_x + 1) - 1, PaintBox1->Top + Y_cm * mashtab * 100 * Fitfac * (NList_y + 1) - 1);
                ::SelectClipRgn(PaintBox1->Canvas->Handle, MyRgn);
                PaintBox1->Canvas->Draw(0, 0, Bitmap1);
                ::SelectClipRgn(PaintBox1->Canvas->Handle, NULL);
                ::DeleteObject(MyRgn);
            }

            PaintBox1->Canvas->Pen->Color = clBlack;
        }
    }
}
// ------------------------------------------------------------------------------

// *************************** Set Parameters ***********************************
// -------------------------------new-scale-------------------------------------
void __fastcall TPrnForm::Edit4KeyDown(TObject *Sender, WORD &Key, TShiftState Shift) {
    /* if (Key == VK_RETURN) {
     UnicodeString S = Edit4->Text;
     if ( (S.ToDouble()) < 0.5 )
     {Application->MessageBox(L"Incorrect scale.",
     "TOpO Print: Error", MB_OK); return;}
     mashtab = (float)(S.ToDouble());
     del_p_mass();
     ini_p_mass();
     //Edit1->Text = raz_x;
     //Edit2->Text = NList_x;

     PaintBox1->Canvas->Draw(0,0,Bitmap1);
     PageLines(); }
    /* */
}

// ------------------------------set-portret-------------------------------------
void __fastcall TPrnForm::RadioButton1Click(TObject *Sender) {
    X_cm = 20;
    Y_cm = 27;
    del_p_mass();
    ini_p_mass();
    Printer()->Orientation = poPortrait;
    PaintBox1->Canvas->Draw(0, 0, Bitmap1);
    PageLines();
}

// ------------------------------set-landscape-----------------------------------
void __fastcall TPrnForm::RadioButton2Click(TObject *Sender) {
    X_cm = 27;
    Y_cm = 20;
    del_p_mass();
    ini_p_mass();
    Printer()->Orientation = poLandscape;
    PaintBox1->Canvas->Draw(0, 0, Bitmap1);
    PageLines();
}

// ---------------------------read-phi-&-theta----------------------------------
void __fastcall TPrnForm::ThetaEKeyDown(TObject *Sender, WORD &Key, TShiftState Shift) {
    if (Key == VK_RETURN) {
        try {
            Theta = StrToFloat(ThetaE->Text) * 3.14159265358979 / 180.;
            Phi = StrToFloat(PhiEdit->Text) * 3.14159265358979 / 180.;

            LastTheta = StrToFloat(ThetaE->Text);
            LastPhi = StrToFloat(PhiEdit->Text); // последние значения углов, которые вводились!

            del_p_mass();
            Fitting();
            PageLines();
        }
        catch (...) {
            ShowMessage(L"Правильно введите параметры углов!");
        }
    }
}

// ---------------------------------------------------------------------------
void __fastcall TPrnForm::PhiEditKeyDown(TObject *Sender, WORD &Key, TShiftState Shift) {
    if (Key == VK_RETURN) {
        try {
            Theta = StrToFloat(ThetaE->Text) * 3.14159265358979 / 180.;
            Phi = StrToFloat(PhiEdit->Text) * 3.14159265358979 / 180.;

            LastTheta = StrToFloat(ThetaE->Text);
            LastPhi = StrToFloat(PhiEdit->Text); // последние значения углов, которые вводились!

            del_p_mass();
            Fitting();
            PageLines();
        }
        catch (...) {
            ShowMessage(L"Правильно введите параметры углов!");
        }
    }
}

// ---------------------------------------------------------------------------
void __fastcall TPrnForm::CheckBox6Click(TObject *Sender) {
//    if (CheckBox6->State == cbUnchecked)
//        CheckBox3->State = cbChecked;
}
// ---------------------------------------------------------------------------

void __fastcall TPrnForm::CheckBox3Click(TObject *Sender) {
//    if (CheckBox3->State == cbUnchecked)
//        CheckBox6->State = cbChecked;
}

// ------------------------------- Help ----------------------------------------
void __fastcall TPrnForm::SpeedButton1Click(TObject *Sender) {
    HelpForm->SetHelpNum(6);
    HelpForm->Show();
}

// -----------------------------------------------------------------------------
void __fastcall TPrnForm::CheckBox8Click(TObject *Sender) // перещелкивание
{
    if (CheckBox8->Checked == true)
        CheckBox5->Checked = false;
}

void __fastcall TPrnForm::CheckBox5Click(TObject *Sender) {
    if (CheckBox5->Checked == true)
        CheckBox8->Checked = false;
}
// ---------------------------------------------------------------------------

// -----------------------------Cancel-Button-----------------------------------//--------------------------delete-for exit------------------------------------
void __fastcall TPrnForm::FormCloseQuery(TObject *Sender, bool &CanClose) {
    delete[]proj_x;
    delete[]proj_y;
    delete Bitmap1;
    del_p_mass();
    if (Pro_x != NULL) {
        delete[]Pro_x;
        delete[]Pro_y;
    } // обрисовка
    if (Wpro != NULL) {
        delete[]Wpro;
	}
	if (cmCave) {
		delete cmCave;
        cmCave = nullptr;
	}
}

// ---------------------------- Surface - Redraw -------------------------------
void __fastcall TPrnForm::CheckBox4Click(TObject *Sender) {
    ReDrawBmp();
    PaintBox1Paint(Sender);
}

// ------------------------- Duplicate - Redraw --------------------------------
void __fastcall TPrnForm::CheckBox9Click(TObject *Sender) {
    ReDrawBmp();
    PaintBox1Paint(Sender);
}

// -----
void __fastcall TPrnForm::BitBtnRazvMirrowClick(TObject *Sender) {
    extendedProfileSwap = !extendedProfileSwap;
    for (unsigned int s = 0; s < Razmer; ++s)
        proj_x[s] = -proj_x[s];
    MinMax();
    del_p_mass();
    ini_p_mass();

    /* Fitfac=PaintBox1->Width/(xmax-xmin);
     if(Fitfac*(ymax-ymin) > PaintBox1->Height)
     Fitfac=PaintBox1->Height/(ymax-ymin);
     ReDrawBmp();
     PaintBox1->Canvas->Draw(0,0,Bitmap1);
     PageLines();/* */

    // Пересчитывем все с новым масштабом:
	// стены при изменениии направления развертки смещаются по x!!!!!!!! xmin?
	if (Pro_x) {
		for (unsigned int s = 0; s < Razmer; ++s) {
			Pro_x[s].up = -Pro_x[s].up;
			Pro_x[s].down = -Pro_x[s].down;
		}
	}
	// !!!!!не забыть сделать то же для Wpro:
    // for(unsigned int s=0; s < MainForm1->WRazmer; ++s) Wpro[s].X=-Wpro[s].X;

    PaintBox1->Canvas->Draw(0, 0, Bitmap1);
    PageLines();
    GenerateShift();
}

// ---------------------------------------------------------------------------
// в 1 см mashtab м   в 72/2.54 pt mashtab м
// proj*(72/2.54)/(mashtab) в pt
// mashtab=10;         // print scale: m/sm
// sm2i = 2.54; // sm/inch    1 мм = 72/25.4 пунктов 1 см = 72/2.54 пунктов
// ************************* Export to PS ************************************
void TPrnForm::psLine(int s) {
    wchar_t buffer[50];
    UnicodeString psStr;
    float x = (proj_x[s] - xmin) * psFac;
    float y = (proj_y[s] - ymin) * psFac;
    swprintf(buffer, L"%.4f  %.4f  ", x, y);
    if (P[s].priz & PRIZ_BRANCH)
        psStr = L"moveto";
    else
        psStr = L"lineto";
    psStr = UnicodeString(buffer) + L" " + psStr;
    psList->Add(psStr);
}

// ---------------------------------------------------------------------------
void TPrnForm::psWallLine(int s) // стены
{
    float C = cos(Phi);
    float S = sin(Phi);
    wchar_t buffer[50];
    UnicodeString psStr, ps1Str;
    float x, x1, x2, y, y1, y2, xp, yp;
    int R = 2; // длина засечки
    if (mode == 0) { {
            if (!(P[W[s].pik].priz & PRIZ_DUPLICATE) || (CheckBox9->State == cbChecked)) // показ. duplicate только при нажатой кнопке
            {
                float vx = 0;
                float vy = 0;
                float v2 = 0;
                float ax = 0;
                float ay = 0; // переменные для рисок
                x = (Wpro[s].X - xmin) * psFac; // черта перп. стене
                y = (Wpro[s].Y - ymin) * psFac;
                xp = (proj_x[W[s].pik] - xmin) * psFac; 
                yp = (proj_y[W[s].pik] - ymin) * psFac;
                swprintf(buffer, L"%.4f  %.4f  ", x, y);
                psStr = L"moveto ";
                psStr = UnicodeString(buffer) + L" " + psStr;
                vx = Wpro[s].X - proj_x[W[s].pik]; // проекция вектора от станции к стене в см
                vy = Wpro[s].Y - proj_y[W[s].pik];
                v2 = vx * vx + vy * vy; // длина в см
                ay = R * vx / sqrt(v2);
                ax = R * vy / sqrt(v2); // вектор длины R
                x1 = x - ay;
                y1 = y - ax;
//                swprintf(buffer, L"%.4f  %.4f  ", x1, y1);
                swprintf(buffer, L"%.4f  %.4f  ", xp, yp);   
                psStr = psStr + UnicodeString(buffer) + L" " + L"lineto";   
                psList->Add(psStr);
                if (v2 > 90) // если длина (расст. в данной проекции до стены) > 9.5 см - рисуем риску
                {
                    x1 = x - ax; // черта вдоль стены
                    y1 = y + ay;
                    swprintf(buffer, L"%.4f  %.4f  ", x1, y1);
                    psStr = L"moveto ";
                    psStr = UnicodeString(buffer) + L" " + psStr;
                    x2 = x + ax;
                    y2 = y - ay;
                    swprintf(buffer, L"%.4f  %.4f  ", x2, y2);
                    psStr = psStr + UnicodeString(buffer) + L" " + L"lineto";
                    psList->Add(psStr);
                }
            }
        }
    }
    if (mode == 1) // разрез-развертка  риски
    { {
            if (!(P[W[s].pik].priz & PRIZ_DUPLICATE) || (CheckBox9->State == cbChecked)) // показ. duplicate только при нажатой кнопке
            {
                float vx = 0;
                float vy = 0;
                float v2 = 0;
                float ax = 0;
                float ay = 0; // переменные для рисок
                x = (Wpro[s].X - xmin) * psFac; // черта перп. стене
                y = (Wpro[s].Y - ymin) * psFac;        
                xp = (proj_x[W[s].pik] - xmin) * psFac; 
                yp = (proj_y[W[s].pik] - ymin) * psFac;      
                vx = Wpro[s].X - proj_x[W[s].pik]; // проекция вектора от станции к стене в см
                vy = Wpro[s].Y - proj_y[W[s].pik];
                swprintf(buffer, L"%.4f  %.4f  ", x, y);
                psStr = L"moveto ";
                psStr = UnicodeString(buffer) + L" " + psStr;
                int az = Wpro[s].Y - proj_y[W[s].pik];
//                if (ay > 0)
//                    y1 = y + 2 * R;
//                else
//                    y1 = y - 2 * R;
//                swprintf(buffer, L"%.4f  %.4f  ", x, y1);
                swprintf(buffer, L"%.4f  %.4f  ", xp, yp); 
                psStr = psStr + UnicodeString(buffer) + L" " + L"lineto";
                psList->Add(psStr);
                if (abs(az) > 10) {
                    x1 = x - 2 * R; // черта вдоль стены
                    swprintf(buffer, L"%.4f  %.4f  ", x1, y1);
                    psStr = L"moveto ";
                    psStr = UnicodeString(buffer) + L" " + psStr;
                    x2 = x + 2 * R;
                    swprintf(buffer, L"%.4f  %.4f  ", x2, y1);
                    psStr = psStr + UnicodeString(buffer) + L" " + L"lineto";
                    psList->Add(psStr);
                }
            }
        }
    }
}

// ---------------------------------------------------------------------------
void TPrnForm::psHorizGrid(float n_step) {
    wchar_t buffer[50];
    UnicodeString psStr;
    float x = (xmax - xmin) * psFac;
    float y = (ymax - ymin - n_step) * psFac;
    swprintf(buffer, L"%.4f ", y);
    psStr = L"0 " + UnicodeString(buffer) + L"moveto";
    psList->Add(psStr);
    swprintf(buffer, L"%.4f  %.4f  ", x, y);
    psStr = UnicodeString(buffer) + L"lineto";
    psList->Add(psStr);
}

// ---------------------------------------------------------------------------
void TPrnForm::psVertGrid(float n_step) {
    wchar_t buffer[50];
    UnicodeString psStr;
    float x = (n_step) * psFac;
    float y = (ymax - ymin) * psFac;
    swprintf(buffer, L"%.4f ", x);
    psStr = UnicodeString(buffer) + L"0 " + L"moveto";
    psList->Add(psStr);
    swprintf(buffer, L"%.4f  %.4f  ", x, y);
    psStr = UnicodeString(buffer) + L"lineto";
    psList->Add(psStr);
}

// ---------------------------------------------------------------------------
void TPrnForm::psCircle(int s, float r) {
    wchar_t buffer[50];
    UnicodeString psStr;
    float x = (proj_x[s] - xmin) * psFac + r;
    float y = (proj_y[s] - ymin) * psFac;
    swprintf(buffer, L"%.4f  %.4f  ", x, y);
    psStr = UnicodeString(buffer) + L" moveto ";
    x = (proj_x[s] - xmin) * psFac;
    swprintf(buffer, L"%.4f  %.4f  ", x, y);
    psStr += UnicodeString(buffer);
    swprintf(buffer, L"%.2f  ", r);
    psStr += UnicodeString(buffer) + L" 0 360 arc closepath";
    psList->Add(psStr);
}

// ---------------------------------------------------------------------------
void TPrnForm::psName(int s, float r) {
    wchar_t buffer[50];
    UnicodeString psStr;
    float x = (proj_x[s] - xmin) * psFac + r + 1;
    float y = (proj_y[s] - ymin) * psFac;
    swprintf(buffer, L"%.4f  %.4f  ", x, y);
    psStr = UnicodeString(buffer) + L" moveto (L" + StationList->Strings[P[s].met] + L") show";
    psList->Add(psStr);
}

// ---------------------------------------------------------------------------
void TPrnForm::psComm(int s, float r) {
    wchar_t buffer[50];
    UnicodeString psStr;
    float x = (proj_x[s] - xmin) * psFac + r + 1;
    float y = (proj_y[s] - ymin) * psFac - psFontSize;
    swprintf(buffer, L"%.4f  %.4f  ", x, y);
    psStr = UnicodeString(buffer) + L" moveto (L" + MainForm1->Name(Comments, P[s].com) + L") show";
    psList->Add(psStr);
}

// --------------------------- Color conversion ------------------------------
UnicodeString Get_psColor(int oldColor) {
    switch (oldColor) {
    case 0:
        return L"0 0 0"; // clBlack;
    case 1:
        return L"0 0 1"; // clBlue;
    case 2:
        return L"0 0.66 0"; // clGreen;
    case 3:
        return L"0 1 1"; // clAqua;
    case 4:
        return L"0.9 0.17 0.17"; // clRed;
    case 5:
        return L"0.83 0 0.83"; // clPurple;  Magenta
    case 6:
        return L"0.94 0.5 0.5"; // (TColor)0x008080F0;  brown
    case 7:
        return L"0.9 0.9 0.9"; // clLtGray;
    case 8:
        return L"0.66 0.66 0.66"; // clDkGray;
    case 9:
        return L"0 0.66 1"; // (TColor)0x00FFAA00;  светло-синий
    case 10:
        return L"0 1 0"; // clLime;
    case 11:
        return L"0.83 0.83 0.17"; // (TColor)0x0020C0C0;  //khaki защитный
    case 12:
        return L"1 0 0"; // (TColor)0x000000FF;  ярко-красный
    case 13:
        return L"1 0 1"; // clFuchsia;
    case 14:
        return L"1 1 0"; // clYellow;
    case 15:
        return L"1 1 1"; // clWhite;
    case 16:
        return L"0.94 0.7 0.17"; // (TColor)0x002AAFF 0; //оранжевый
        // clCream  clGray  clMaroon clSkyBlue clMoneyGreen (TColor)0x00FF7070;
    default:
        return L"0 0 0";
    }
}

// ---------------------------------------------------------------------------

void TPrnForm::psNewPath(int Ncol, bool colorFlag) {
	psNewPath(Get_psColor(Ncol), colorFlag);
}

void TPrnForm::psNewPath(UnicodeString color, bool colorFlag) {
    psList->Add(L"stroke");
    psList->Add(L"");
    if ((psColor->Checked) && (colorFlag))
		psList->Add(color + L" " + L"setrgbcolor");
    psList->Add(L"newpath");
}

// ---------------------------------------------------------------------------
	void TPrnForm::psExport() {
    wchar_t buffer[50];
    bool colorFlag = 0;
    int psLineColor = 2;
    float CirRad = 1;
    float LineWidth = 0.4;
    float CirWidth = 0.2;
    float step;
    psFontSize = 8;
    int psMaxGrid = 0;

    // mashtab = (float)Edit4->Text.ToDouble();

    // psFac=254/(720.*mashtab);
    psFac = 72. / (254. * mashtab); // ?????????????????????????????????????????????

    psFontSize = Edit1->Text.ToIntDef(8); // font size
    CirRad = (float)Edit8->Text.ToDouble(); // радиус
    LineWidth = (float)Edit7->Text.ToDouble(); // толщина линий
    CirWidth = (float)Edit9->Text.ToDouble(); // толщина кружков
    step = 100 * mashtab * (float)EditGridScale->Text.ToDouble(); // шаг сетки
    psColor->Checked;
                       
    psList = new TStringList();
    psList->Duplicates = TDuplicates::dupAccept;
    psList->Add(L"%!PS-Adobe-1.0");
    swprintf(buffer, L"%.f  %.f  ", (xmax - xmin)*psFac + psFontSize*2, (ymax - ymin)*psFac + psFontSize*2);
    psList->Add(L"%%BoundingBox: 0 0 " + UnicodeString(buffer));

    swprintf(buffer, L"%.4f ", LineWidth);
    psList->Add(UnicodeString(buffer) + L"setlinewidth");
    if (CheckBox6->State == cbChecked) // линии
    {
        for (unsigned int s = 0; s < Razmer; ++s) {
            if (!((P[s].priz & PRIZ_DUPLICATE) && !(CheckBox9->State == cbChecked)) && !((P[s].priz & PRIZ_SURFACE) && !(CheckBox4->State == cbChecked))) {
                if ((P[s].priz & PRIZ_BRANCH) || (P[s].col != P[s - 1].col) || // newVetka, color
                    ((P[s].priz & PRIZ_SURFACE) != (P[s - 1].priz & PRIZ_SURFACE)) || // surf
                    ((P[s].priz & PRIZ_DUPLICATE) != (P[s - 1].priz & PRIZ_DUPLICATE))) // dupl
                {
                    if (s == 0) {
                        psLineColor = P[0].col;
                        colorFlag = 1;
                        if (P[0].priz & PRIZ_SURFACE)
                            psLineColor = MainForm1->sur_col;
                        if (P[0].priz & PRIZ_DUPLICATE)
                            psLineColor = MainForm1->dup_col;
                        psNewPath(psLineColor, colorFlag);
                        colorFlag = 0;
                    }
                    else {
                        if ((P[s].col != P[s - 1].col) || ((P[s].priz & PRIZ_SURFACE) != (P[s - 1].priz & PRIZ_SURFACE)) ||
                            ((P[s].priz & PRIZ_DUPLICATE) != (P[s - 1].priz & PRIZ_DUPLICATE))) {
                            colorFlag = 1;
                            psLineColor = P[s].col;
                            if ((P[s].priz & PRIZ_SURFACE) > (P[s - 1].priz & PRIZ_SURFACE))
                                psLineColor = MainForm1->sur_col;
                            if ((P[s].priz & PRIZ_DUPLICATE) > (P[s - 1].priz & PRIZ_DUPLICATE))
                                psLineColor = MainForm1->dup_col;
                            psNewPath(psLineColor, colorFlag);
                            colorFlag = 0;
                        }
                    }
                    if (!(P[s].priz & PRIZ_BRANCH)) {
                        swprintf(buffer, L"%.4f  %.4f  ", (proj_x[s - 1] - xmin)*psFac, (proj_y[s - 1] - ymin)*psFac);
                        psList->Add(UnicodeString(buffer) + L" moveto");
                    }
                }
                psLine(s);
            }
        }
        psList->Add(L"stroke");
    }


	auto posConvertor = [this](V2 pos) {
		return V2((pos.x - xmin)*psFac, (pos.y - ymin)*psFac);
	};

    if (CheckBox11->State == cbChecked) { // обводка
        psList->Add(L"0.5 0.5 0.5 setrgbcolor");

        updateCaveModel(Phi, Theta);
        for (const auto&bz : caveOutline) {

            V2 pos = posConvertor(bz.a);
            swprintf(buffer, L"%.4f  %.4f moveto ", pos.x, pos.y);
            psList->Add(UnicodeString(buffer));

            pos = posConvertor(bz.ac);
            swprintf(buffer, L"%.4f  %.4f   ", pos.x, pos.y);
            psList->Add(UnicodeString(buffer));

            pos = posConvertor(bz.bc);
            swprintf(buffer, L"%.4f  %.4f    ", pos.x, pos.y);
            psList->Add(UnicodeString(buffer));
            
            pos = posConvertor(bz.b);
            swprintf(buffer, L"%.4f  %.4f curveto", pos.x, pos.y);
            psList->Add(UnicodeString(buffer));
            
            psList->Add(L"stroke");
        }
    }

    if ((CheckBox10->State == cbChecked || CheckBox12->State == cbChecked) && (MainForm1->WRazmer > 0)) // стены
	{
		CM::Color curColor = CM::Color::None;
		auto drawFunc = [this, &curColor](V2 f, V2 t, CM::Color c) {
			if (curColor != c) {
				if (psList->Strings[psList->Count - 1] == L"newpath") {
					psList->Delete(psList->Count - 1); // убираем строки,
					psList->Delete(psList->Count - 1); // если не было стен данного цвета
					psList->Delete(psList->Count - 1);
					psList->Delete(psList->Count - 1);
				}
				curColor = c;

				wchar_t buffer[50];
				swprintf(buffer, L"%.4f  %.4f %.4f", c.r, c.g, c.b);
				psNewPath(buffer, 1);
			}

			wchar_t buffer[50];

			swprintf(buffer, L"%.4f  %.4f moveto ", f.x, f.y);
			psList->Add(UnicodeString(buffer));

			swprintf(buffer, L"%.4f  %.4f lineto ", t.x, t.y);
			psList->Add(UnicodeString(buffer));
			//psList->Add(L"stroke");


		};
		drawSplay(CheckBox10->State != cbChecked, drawFunc, posConvertor);

//        for (unsigned int s = 0; s < MainForm1->WRazmer; ++s)
//            // for(unsigned int s=0; s < Razmer; ++s)
//        {
//			if (s == 0) // цвет
//            {
//                psLineColor = W[s].col;
//                colorFlag = 1;
//                if (P[W[s].pik].priz & PRIZ_SURFACE)
//                    psLineColor = MainForm1->sur_col;
//                if (P[W[s].pik].priz & PRIZ_DUPLICATE)
//                    psLineColor = MainForm1->dup_col;
//                if ((psColor->Checked) && (colorFlag))
//                    psList->Add(Get_psColor(psLineColor) + L" " + L"setrgbcolor");
//                psList->Add(L"newpath");
//                colorFlag = 0;
//            }
//            else {
//                if ((W[s].col != W[s - 1].col) || ((P[W[s].pik].priz & PRIZ_SURFACE) != (P[W[s - 1].pik].priz & PRIZ_SURFACE)) ||
//                    ((P[W[s].pik].priz & PRIZ_DUPLICATE) != (P[W[s - 1].pik].priz & PRIZ_DUPLICATE))) {
//                    if (psList->Strings[psList->Count - 1] == L"newpath") {
//                        psList->Delete(psList->Count - 1); // убираем строки,
//                        psList->Delete(psList->Count - 1); // если не было стен данного цвета
//                        psList->Delete(psList->Count - 1);
//                        psList->Delete(psList->Count - 1);
//                    }
//
//                    colorFlag = 1;
//                    psLineColor = W[s].col;
//                    if ((P[W[s].pik].priz & PRIZ_SURFACE) > (P[W[s - 1].pik].priz & PRIZ_SURFACE))
//                        psLineColor = MainForm1->sur_col;
//                    if ((P[W[s].pik].priz & PRIZ_DUPLICATE) > (P[W[s - 1].pik].priz & PRIZ_DUPLICATE))
//                        psLineColor = MainForm1->dup_col;
//                    psNewPath(psLineColor, colorFlag);
//                    colorFlag = 0;
//                }
//            }
//			psWallLine(s);
//        }
        if (psList->Strings[psList->Count - 1] == L"newpath") {
            psList->Delete(psList->Count - 1);
            psList->Delete(psList->Count - 1);
            psList->Delete(psList->Count - 1);
        }
        else
            psList->Add(L"stroke");
        psList->Add(L"");
	}

    if (CheckBox3->State == cbChecked) // кружки
    {
        psList->Add(L" ");
        swprintf(buffer, L"%.4f ", CirWidth);
        psList->Add(UnicodeString(buffer) + L"setlinewidth");
        psList->Add(L"0 0 0 setrgbcolor");
        psList->Add(L"newpath");
        for (unsigned int s = 0; s < Razmer; ++s)
            if (!((P[s].priz & PRIZ_SURFACE) && !(CheckBox4->State == cbChecked)) && !((P[s].priz & PRIZ_DUPLICATE) && !(CheckBox9->State == cbChecked))) {
                psCircle(s, CirRad);
            }
        psList->Add(L"stroke");
        psList->Add(L" ");

        psList->Add(L"0.9 0.17 0.17 setrgbcolor"); // красныые кружки (#red)
        psList->Add(L"newpath");
        for (unsigned int s = 0; s < Razmer; ++s)
            if (!((P[s].priz & PRIZ_SURFACE) && !(CheckBox4->State == cbChecked)) && !((P[s].priz & PRIZ_DUPLICATE) && !(CheckBox9->State == cbChecked))) {
                if (P[s].priz & PRIZ_BEGIN)
                    psCircle(s, CirRad + 1);
            }
        psList->Add(L"stroke");
        psList->Add(L" ");
    }


    if (CheckBox8->State == cbChecked) // все метки
    {
        psList->Add(L"0 setgray");
        psList->Add(L"/Helvetica findfont");
        psList->Add(UnicodeString(psFontSize) + L" scalefont");
        psList->Add(L"setfont");
        for (unsigned int s = 0; s < Razmer; ++s) {
            if (P[s].met > 0)
                psName(s, CirRad);
        }
        psList->Add(L"");
    }
    if (CheckBox5->State == cbChecked) // метки т. биф.
    {
        psList->Add(L"0 setgray");
        psList->Add(L"/Helvetica findfont");
        psList->Add(UnicodeString(psFontSize) + L" scalefont");
        psList->Add(L"setfont");
        for (unsigned int s = 0; s < Razmer; ++s) {
            if ((P[s].priz & PRIZ_BRANCH) && (P[s].met > 0)) {
                psName(s, CirRad);
                if ((s != 0) && (P[s - 1].met > 0))
                    psName(s - 1, CirRad);
            }
        }
        psList->Add(L"");
    }
    if (CheckBox7->State == cbChecked) // комментарий
    {
        psList->Add(L"0 setgray");
        psList->Add(L"/Helvetica findfont");
        psList->Add(UnicodeString(psFontSize) + L" scalefont");
        psList->Add(L"setfont");
        for (unsigned int s = 0; s < Razmer; ++s) {
            if (P[s].com)
                psComm(s, CirRad);
        }
        psList->Add(L"");
    }
    if (CheckBox1->State == cbChecked) // сетка
    {
        psList->Add(L"0.1 setlinewidth");
        psList->Add(L"newpath");
        psList->Add(L"0 setgray");
        psMaxGrid = (ymax - ymin) / step + 1;
        for (int s = 0; s < psMaxGrid; ++s)
            psHorizGrid(step*s);
        psList->Add(L"stroke");
        psList->Add(L"");
        psList->Add(L"newpath");
        psList->Add(L"0 setgray");
        psMaxGrid = (xmax - xmin) / step + 1;
        for (int s = 0; s < psMaxGrid; ++s)
            psVertGrid(step*s);
        psList->Add(L"stroke");
        psList->Add(L"");
    }
    psList->Add(L"showpage");
    psList->Add(L"%%EOF");

    for (int s = 2; s < psList->Count; s++) // убираем повторы
    {
        if (psList->Strings[s] == psList->Strings[s - 1]) {
            psList->Delete(s);
            s = s - 1;
        }
    }
    // UnicodeString psFile = ExtractFileName(MainForm1->CurrFileName);
    // psFile.Delete( psFile.LastDelimiter(L"."), psFile.Length()+1-psFile.LastDelimiter(L"."));
    // UnicodeString message = L"Picture will be save as " + psFile+ L".ps";
    // int button = Application->MessageBox(message.c_str(), L"TOpO Print: Message", MB_OKCANCEL);
    // if (button != IDCANCEL) psList->SaveToFile(ExtractFilePath(MainForm1->CurrFileName) + psFile+".ps");

    psList->SaveToFile(SaveDialog1->FileName);
    if (psList != NULL)
        delete psList;
}

// ---------------------------------------------------------------------------
void __fastcall TPrnForm::ButtonPSExpClick(TObject *Sender) {
    SaveDialog1->InitialDir = ExtractFilePath(MainForm1->CurrFileName);
    UnicodeString psFile = ExtractFileName(MainForm1->CurrFileName);
    psFile.Delete(psFile.LastDelimiter(L"."), psFile.Length() + 1 - psFile.LastDelimiter(L"."));
    SaveDialog1->FileName = psFile;

    if (SaveDialog1->Execute()) {
		psExport();
    }
}
// ---------------------------------------------------------------------------

void __fastcall TPrnForm::ButtonScaleClick(TObject *Sender) {
    TFormPrintScalePosition* FormPrintScalePosition = new TFormPrintScalePosition(this);
    FormPrintScalePosition->ShowModal();
    delete FormPrintScalePosition;

    // Пересчитывем все с новым масштабом:

    wchar_t buff[256];
    swprintf(buff, L"1 см = %g m;  ( M 1: %g )", mashtab, mashtab*100.);
    LabelPrintScale->Caption = buff;

    // mashtab = (float)(S.ToDouble());
    del_p_mass();
    ini_p_mass();

    PaintBox1->Canvas->Draw(0, 0, Bitmap1);
    PageLines();
    GenerateShift();
}
// ---------------------------------------------------------------------------

void __fastcall TPrnForm::FormActivate(TObject *Sender) {
    wchar_t buff[256];
    swprintf(buff, L"1 см = %g m;  ( M 1: %g )", mashtab, mashtab*100.);
    LabelPrintScale->Caption = buff;

    Edit_ShiftX->Text = FloatToStr(ShiftX);
    Edit_ShiftY->Text = FloatToStr(ShiftY);
}
// ---------------------------------------------------------------------------

void __fastcall TPrnForm::GenerateShift() {
    del_p_mass();
    ini_p_mass();

    PaintBox1->Canvas->Draw(0, 0, Bitmap1);
    PageLines();
    ReDrawBmp();
    PaintBox1->Refresh();
}

// ---------------------------------------------------------------------------
void __fastcall TPrnForm::ButtonShiftLeftClick(TObject *Sender) {
    if (ShiftX <= 0.001)
        return;
    ShiftX -= 1.; // смещение нитки относительно края листа (в сантиметрах)
    ShiftX_ = ShiftX * mashtab * 100.; // смещение нитки относительно края листа (в сантиметрах пещерных)
    Edit_ShiftX->Text = FloatToStr(ShiftX);
    GenerateShift();
}

// ---------------------------------------------------------------------------
void __fastcall TPrnForm::ButtonShiftRightClick(TObject *Sender) {
    ShiftX += 1.; // смещение нитки относительно края листа (в сантиметрах)
    ShiftX_ = ShiftX * mashtab * 100.; // смещение нитки относительно края листа (в сантиметрах пещерных)
    Edit_ShiftX->Text = FloatToStr(ShiftX);
    GenerateShift();
}
// ---------------------------------------------------------------------------

void __fastcall TPrnForm::ButtonShiftUpClick(TObject *Sender) {
    if (ShiftY <= 0.001)
        return;
    ShiftY -= 1.; // смещение нитки относительно края листа (в сантиметрах)
    ShiftY_ = ShiftY * mashtab * 100.; // смещение нитки относительно края листа (в сантиметрах пещерных)
    Edit_ShiftY->Text = FloatToStr(ShiftY);
    GenerateShift();
}
// ---------------------------------------------------------------------------

void __fastcall TPrnForm::ButtonShiftDownClick(TObject *Sender) {
    ShiftY += 1.; // смещение нитки относительно края листа (в сантиметрах)
    ShiftY_ = ShiftY * mashtab * 100.; // смещение нитки относительно края листа (в сантиметрах пещерных)
    Edit_ShiftY->Text = FloatToStr(ShiftY);
    GenerateShift();
}
// ---------------------------------------------------------------------------

void __fastcall TPrnForm::BitBtnCloseClick(TObject *Sender) {
    Close();
}
// ---------------------------------------------------------------------------

void __fastcall TPrnForm::Edit_ShiftXKeyDown(TObject *Sender, WORD &Key, TShiftState Shift) {
    if (Key == VK_RETURN) {
        try {
            ShiftX = StrToFloat(Edit_ShiftX->Text); // смещение нитки относительно края листа (в сантиметрах)
            ShiftX_ = ShiftX * mashtab * 100.; // смещение нитки относительно края листа (в сантиметрах пещерных)

            GenerateShift();
        }
        catch (...) {
        }
    }
}
// ---------------------------------------------------------------------------

void __fastcall TPrnForm::Edit_ShiftYKeyDown(TObject *Sender, WORD &Key, TShiftState Shift) {
    if (Key == VK_RETURN) {
        try {
            ShiftY = StrToFloat(Edit_ShiftY->Text); // смещение нитки относительно края листа (в сантиметрах)
            ShiftY_ = ShiftY * mashtab * 100.; // смещение нитки относительно края листа (в сантиметрах пещерных)

            GenerateShift();
        }
        catch (...) {
        }
    }
}
// ---------------------------------------------------------------------------

void __fastcall TPrnForm::RadioGroupProjectionModeClick(TObject *Sender) {

    switch (RadioGroupProjectionMode->ItemIndex) {
    case PROJMODE_PLAN: // план
        mode = 0;
        BitBtnRazvMirrow->Visible = false;
        PhiEdit->Text = FloatToStr(0);
        ThetaE->Text = FloatToStr(90);

        Phi = 0.;
        Theta = 90. * 3.14159265358979 / 180.;

        PhiEdit->Enabled = false;
        ThetaE->Enabled = false;
        break;
    case PROJMODE_VERT: // вертик проекция
        mode = 0;
        PhiEdit->Text = FloatToStr(0);
        ThetaE->Text = FloatToStr(0);

        Phi = 0.;
        Theta = 0.;

        PhiEdit->Enabled = false;
        ThetaE->Enabled = false;
        BitBtnRazvMirrow->Visible = false;
        break;
    case PROJMODE_EXTENDED_PROFILE: // развертка
        mode = 1;
        PhiEdit->Text = L"--";
        ThetaE->Text = L"--";
        PhiEdit->Enabled = false;
        ThetaE->Enabled = false;
        BitBtnRazvMirrow->Visible = true;
        break;
    case PROJMODE_OTHER: // другая проекция
        mode = 0;
        BitBtnRazvMirrow->Visible = false;
        PhiEdit->Text = FloatToStr(LastPhi);
        ThetaE->Text = FloatToStr(LastTheta);

        Phi = LastPhi * 3.14159265358979 / 180.;
        Theta = LastTheta * 3.14159265358979 / 180.;

        PhiEdit->Enabled = true;
        ThetaE->Enabled = true;
        break;
    default: ;
    }

    del_p_mass();
    Fitting();
    // Пересчитывем все с новым масштабом:
    PaintBox1->Canvas->Draw(0, 0, Bitmap1);
    PageLines();
    GenerateShift();
}

// ---------------------------------------------------------------------------
std::function < V2(V3) > TPrnForm::getProjectFunctor(bool extendedElevationMode, bool extendedProfileSwap, float phi, float theta, float* rotYout, float* rotZout) {
	float rotY = 0;
	float rotZ = 0;
	if (!extendedElevationMode) {
		rotY = theta;
		rotZ = phi;
	}
	else {
        rotY = 0;
		if (extendedProfileSwap)
			rotZ = M_PI / 2;
		else
			rotZ = -M_PI / 2;
	}

	if (rotYout) *rotYout = rotY;
	if (rotZout) *rotZout = rotZ;

	float C = cos(rotZ);
	float S = sin(rotZ);
	float Ct = cos(rotY);
	float St = sin(rotY);
	auto projectVector3 = [=](const V3 & v3) {
		return V2(v3.y*C - v3.x*S, (v3.y*S + v3.x*C)*St - v3.z*Ct);
	};

	return projectVector3;
}
// ---------------------------------------------------------------------------
void TPrnForm::updateCaveModel(float phi, float theta) {
    bool extendedElevationMode = (RadioGroupProjectionMode->ItemIndex == PROJMODE_EXTENDED_PROFILE);
    V3 lookDirrection(1, 0, 0);

	float rotY;
	float rotZ;
	auto projectVector3 = getProjectFunctor(extendedElevationMode, extendedProfileSwap, phi, theta, &rotY, &rotZ);


	Ogre::Quaternion rotation = Ogre::Quaternion(Ogre::Radian(rotY), V3::UNIT_Y) * Ogre::Quaternion(Ogre::Radian(-rotZ), V3::UNIT_Z);
    lookDirrection = rotation.Inverse() * lookDirrection;
    lookDirrection.normalise();

    // cavesProxyNode->rotate(Vector3::UNIT_Y, Ogre::Radian(rotY));
    // cavesProxyNode->rotate(Vector3::UNIT_Z, Ogre::Radian(-rotZ));

//		canvas->MoveTo(f.x, f.y);
//		canvas->LineTo(t.x, t.y);
    if (cmCave && cmCave->isCnvertToExtendedElevation() != extendedElevationMode) {
        delete cmCave;
        cmCave = nullptr;
        // MainForm1->ogreRenderer->setCaveData(P, size, W, Wsize, Equates, piketNames, MainForm1->prefs, extendedElevationMode);
	}

	if (CheckBox11->State == cbChecked || CheckBox10->State == cbChecked || CheckBox12->State == cbChecked) {
        if (!cmCave || cmCave->getCaveViewPrefs().lookDirrection != lookDirrection) {
            CM::CaveViewPrefs caveViewPrefs;
			caveViewPrefs.lookDirrection = lookDirrection;
			caveViewPrefs.showSplay = true;
			caveViewPrefs.wallColoringMode = CM::WCM_SMOOTH;
            if (!cmCave) {
                std::vector<std::string> piketNames;
				cmCave = new OgreCaveDataModel(P, Razmer, W, MainForm1->WRazmer, Equates, piketNames, caveViewPrefs, extendedElevationMode);
                cmCave->setOutputType(CM::OT_SPLAY);
                cmCave->init();
            }
            else {
                cmCave->setCaveViewPrefs(caveViewPrefs);
            }

			std::vector<CM::CrossPiketLineBesier3>cmCaveOutline = cmCave->calcOutineBesier();
			caveOutline.clear();
			caveOutline.reserve(cmCaveOutline.size());

			for (const auto&bz : cmCaveOutline) {
				CM::CrossPiketLine2dBesier3 line2d;
				line2d.a = projectVector3(bz.a);
				line2d.ac = projectVector3(bz.ac);
				line2d.bc = projectVector3(bz.bc);
				line2d.b = projectVector3(bz.b);
				line2d.aid = bz.aid;
				line2d.bid = bz.bid;
				caveOutline.push_back(line2d);
			}

		}
    }
}

void TPrnForm::drawOutline(TCanvas* canvas, int curveAproxSteps, std::function < V2(V2) > posConvertor) {

	updateCaveModel(Phi, Theta);
    for (const auto&bz : caveOutline) {
		for (int i = 0; i < curveAproxSteps; i++) {
			float t0 = (float)i / curveAproxSteps;
			float t1 = (float)(i + 1) / curveAproxSteps;
			V2 f = posConvertor(besier3(t0, bz.a, bz.ac, bz.bc, bz.b));
            V2 t = posConvertor(besier3(t1, bz.a, bz.ac, bz.bc, bz.b));
			canvas->MoveTo(f.x, f.y);
			canvas->LineTo(t.x, t.y);
        }

	}
}

void TPrnForm::drawSplay(bool onlyUD, std::function < void(V2, V2, CM::Color) > printer, std::function < V2(V2) > posConvertor) {
	updateCaveModel(Phi, Theta);

    AssertReturn(cmCave, return);
	bool extendedElevationMode = (RadioGroupProjectionMode->ItemIndex == PROJMODE_EXTENDED_PROFILE);

	int angle = Edit3->Text.ToInt();

	auto projectVector3 = getProjectFunctor(extendedElevationMode, extendedProfileSwap, Phi, Theta);
	const std::vector<CM::OutputLine>& splays = cmCave->getOutputLine(CM::OT_SPLAY);
	for (const auto& splay : splays) {


		if (onlyUD) {
			V3 vec = splay.b - splay.a;
			float a = vec.angleBetween(V3(vec.x, vec.y, 0)).valueDegrees();
			if (a < angle) continue;
		}
		// && (abs(splay.a.x - splay.b.x) + abs(splay.a.y - splay.b.y)) > 10) continue;

		V2 f = posConvertor(projectVector3(splay.a));
		V2 t = posConvertor(projectVector3(splay.b));

		CM::Color color = splay.ca;
		if (CheckBox14->Checked) {
			color = CM::Color::Gray;
		}

		printer(f, t, color);

		if (CheckBox15->Checked) {
			float l = std::max<float>(2, f.distance(t) * 0.07);
			V2 perp = (t-f).perpendicular().normalisedCopy();
			V2 s0 = t - perp * l;
			V2 s1 = t + perp * l;
			printer(s0, s1, color);
		}
	}
}

void __fastcall TPrnForm::FormCreate(TObject *Sender)
{
    Edit7->Text = 0.4 ;      
    Edit9->Text = 0.2 ;  
}
//---------------------------------------------------------------------------





