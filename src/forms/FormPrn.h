﻿// ---------------------------------------------------------------------------
#ifndef FormPrnH
#define FormPrnH
// ---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Dialogs.hpp>
#include <ExtCtrls.hpp>
#include <Buttons.hpp>
#include <vector>
#include "CSPIN.h"
#include "CMTypes.h"
// ---------------------------------------------------------------------------
class OgreCaveDataModel;

namespace CM {
    struct CrossPiketLine2dBesier3;
}
namespace Ogre {
	class Vector2;
}
typedef Ogre::Vector2 V2;

class TPrnForm : public TForm {
__published: // IDE-managed Components
    TEdit *Edit2;
    TGroupBox *GroupBox1;
    TCheckBox *CheckBox1;
    TEdit *EditGridScale;
    TLabel *Label2;
    TCheckBox *CheckBox2;
    TGroupBox *GroupBox2;
    TRadioButton *RadioButton1;
    TRadioButton *RadioButton2;
    TPrintDialog *PrintDialog1;
    TCheckBox *CheckBox3;
    TSpeedButton *SpeedButton1;
    TCheckBox *CheckBox4;
    TCheckBox *CheckBox5;
    TCheckBox *CheckBox6;
    TCheckBox *CheckBox7;
    TCheckBox *CheckBox8;
    TCheckBox *CheckBox9;
    TGroupBox *PSBox;
    TButton *ButtonPSExp;
    TEdit *Edit1;
    TLabel *Label12;
    TLabel *psFontsize;
    TEdit *Edit7;
    TLabel *psLinewidth;
    TLabel *Label14;
    TCheckBox *psColor;
    TEdit *Edit8;
    TLabel *Label13;
    TLabel *psRadius;
    TLabel *Label15;
    TEdit *Edit9;
    TLabel *Label16;
    TLabel *Label17;
    TGroupBox *GroupBox6;
    TButton *ButtonShiftLeft;
    TButton *ButtonShiftUp;
    TButton *ButtonShiftDown;
    TButton *ButtonShiftRight;
    TLabel *Label3;
    TBitBtn *BitBtnClose;
    TEdit *Edit_ShiftX;
    TEdit *Edit_ShiftY;
    TLabel *Label4;
    TLabel *Label5;
    TRadioGroup *RadioGroupProjectionMode;
    TBitBtn *BitBtnRazvMirrow;
    TGroupBox *GroupBox3;
    TLabel *Label6;
    TLabel *Label7;
    TEdit *PhiEdit;
    TEdit *ThetaE;
    TCheckBox *CheckBox10;
    TSaveDialog *SaveDialog1;
        TCheckBox *CheckBox11;
	TGroupBox *GroupBox7;
	TLabel *Label8;
	TEdit *EditBitmapResolution;
	TLabel *Label9;
	TButton *SaveButton;
	TLabel *Label10;
	TEdit *EditPrinterResolution;
	TLabel *Label11;
	TButton *PrintButton;
	TGroupBox *GroupBox4;
	TPaintBox *PaintBox1;
	TButton *ButtonScale;
	TLabel *LabelPrintScale;
	TCheckBox *CheckBox12;
	TCheckBox *CheckBox13;
	TEdit *Edit3;
	TLabel *Label1;
	TCheckBox *CheckBox14;
	TCheckBox *CheckBox15;

    void __fastcall PrintButtonClick(TObject *Sender);
    void __fastcall PaintBox1Paint(TObject *Sender);

    void __fastcall Edit4KeyDown(TObject *Sender, WORD &Key, TShiftState Shift);

    void __fastcall PaintBox1MouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y);

    void __fastcall RadioButton1Click(TObject *Sender);
    void __fastcall RadioButton2Click(TObject *Sender);

    void __fastcall ThetaEKeyDown(TObject *Sender, WORD &Key, TShiftState Shift);
    void __fastcall PhiEditKeyDown(TObject *Sender, WORD &Key, TShiftState Shift);
    void __fastcall FormCloseQuery(TObject *Sender, bool &CanClose);
    void __fastcall SaveButtonClick(TObject *Sender);
    void __fastcall SpeedButton1Click(TObject *Sender);
    void __fastcall CheckBox4Click(TObject *Sender);
    void __fastcall CheckBox6Click(TObject *Sender);
    void __fastcall CheckBox3Click(TObject *Sender);
    void __fastcall BitBtnRazvMirrowClick(TObject *Sender);
    void __fastcall CheckBox8Click(TObject *Sender);
    void __fastcall CheckBox5Click(TObject *Sender);
    void __fastcall CheckBox9Click(TObject *Sender);
    void __fastcall ButtonPSExpClick(TObject *Sender);
    void __fastcall ButtonScaleClick(TObject *Sender);
    void __fastcall FormActivate(TObject *Sender);
    void __fastcall ButtonShiftLeftClick(TObject *Sender);
    void __fastcall ButtonShiftRightClick(TObject *Sender);
    void __fastcall ButtonShiftUpClick(TObject *Sender);
    void __fastcall ButtonShiftDownClick(TObject *Sender);
    void __fastcall BitBtnCloseClick(TObject *Sender);
    void __fastcall Edit_ShiftXKeyDown(TObject *Sender, WORD &Key, TShiftState Shift);
    void __fastcall Edit_ShiftYKeyDown(TObject *Sender, WORD &Key, TShiftState Shift);
    void __fastcall RadioGroupProjectionModeClick(TObject *Sender);
    void __fastcall FormCreate(TObject *Sender);

private: // User declarations
	std::function < CM::V2(CM::V3) > getProjectFunctor(bool extendedElevationMode, bool extendedProfileSwap, float phy, float theta, float* rotYout = nullptr, float* rotZout = nullptr);

public: // User declarations
    __fastcall TPrnForm(TComponent* Owner);
    void SetParam(unsigned int size, float phi, float theta, char State);

    float sm2i; // sm/inch
    unsigned int PRes = 0; // printer resolution

    unsigned int Razmer = 0; // vhod_ data array size
    long *proj_x; // projections
    long *proj_y;
    wchar_t **p_mass; // selected pages 2D array
    double xmin, ymin, xmax, ymax;
    float Fitfac; // bitmap pixels/sm of the cave
    float psFac; // масшт. коэф. для ps (pt на 1м пещеры)
    int psFontSize = 0; // FontSize в pt
    float mashtab; // print scale: m/sm
    int raz_x = 0;
	int raz_y = 0; // size of p_mass

    struct PRO {
        long left, right, up, down;
    }; // проекции lrud

    PRO *Pro_x; // указатели массивов проекций обрисовки
    PRO *Pro_y; //

    struct WPRO {
        long X, Y;
    }; // проекции стен

    WPRO *Wpro; // указатель массива проекций ежа обрисовки на плоскость экрана

    double ShiftX, ShiftY; // смещение нитки относительно края листа (в сантиметрах)
    double ShiftX_, ShiftY_; // смещение нитки относительно края листа (в сантиметрах пещерных)

    int NList_x = 0;
	int NList_y = 0;
    int B_Grid_x = 0;
    int B_Grid_y = 0;

    int X_cm = 0; // A4, sm
    int Y_cm = 0;

    float Phi; // spherical angles
    float Theta;
    float LastTheta, LastPhi; // последние значения углов, которые вводились!
    char mode = 0;
    TStringList* psList; // текст для сохранения в PS

	bool extendedProfileSwap { false };

    void projection(float phi, float theta);
    void MinMax();
    void ini_p_mass();
    void del_p_mass();
    void Fitting();
    void PageLines();
    void ReDrawBmp();
    void Ramka(TCanvas *Canvas);
    void Walls(TCanvas *Canvas, float PFf, int N_x, int N_y, int R);
    void __fastcall MyGrid(TCanvas *Canvas, int Num_x, int Num_y);
    bool PrePrint();
    void WriteToFile();
    void SendToPrinter();
    void psLine(int s);
    void psWallLine(int s);
    void psHorizGrid(float n_step);
    void psVertGrid(float n_step);
    void psCircle(int s, float r);
    void psName(int s, float r);
    void psComm(int s, float r);
    void psNewPath(int Ncol, bool colorFlag);
    void psNewPath(UnicodeString Ncol, bool colorFlag);
    void psExport();
    void __fastcall GenerateShift();
    void updateCaveModel(float phi, float theta);
	void drawOutline(TCanvas *Canvas, int curveAproxSteps, std::function < V2(V2) > posConvertor);
	void drawSplay(bool onlyUD, std::function < void(V2, V2, CM::Color) > printer, std::function < V2(V2) > posConvertor);
    // void drawLine(TCanvas *Canvas, const V2& from, const V2& to);
    Graphics::TBitmap *Bitmap1;

    OgreCaveDataModel* cmCave {nullptr};

    std::vector<CM::CrossPiketLine2dBesier3>caveOutline;
};

// ---------------------------------------------------------------------------
extern PACKAGE TPrnForm *PrnForm;
// ---------------------------------------------------------------------------
#endif
