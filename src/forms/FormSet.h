﻿//---------------------------------------------------------------------------

#ifndef FormSetH
#define FormSetH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
//---------------------------------------------------------------------------
class TSetForm : public TForm
{
__published:	// IDE-managed Components
private:	// User declarations
public:		// User declarations
        __fastcall TSetForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TSetForm *SetForm;
//---------------------------------------------------------------------------
#endif
