﻿//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "FormSett.h"
#include "TOpO.h"
#include "Form3dExp.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TSettForm *SettForm;
//---------------------------------------------------------------------------
__fastcall TSettForm::TSettForm(TComponent* Owner)
        : TForm(Owner)
{ }
//---------------------------------------------------------------------------
void __fastcall TSettForm::LanguageClick(TObject *Sender)
{
 if(Language->ItemIndex==0)
 {
  Color->Caption=L"Color";
  BackGround->Caption=L"Background";
  WallsColor->Caption=L"Walls";
  WallsColor->Items->Strings[0]=L"grey";
  WallsColor->Items->Strings[1]=L"line color";
  VStep->Caption=L"Vertical step, degrees";

  Exp3dForm->Label4->Caption=L"Color";
 };
 if(Language->ItemIndex==1)
 {
  Color->Caption=L"Цвет";
  BackGround->Caption=L"Фон";
  WallsColor->Caption=L"Стены";
  WallsColor->Items->Strings[0]=L"серые";
  WallsColor->Items->Strings[1]=L"цвета нитки";
  VStep->Caption=L"Шаг по верикали, градусы";

  Exp3dForm->Label4->Caption=L"Цвет";
 };
}
//---------------------------------------------------------------------------

void __fastcall TSettForm::OKClick(TObject *Sender)
{
  switch(BgColor->ItemIndex)        //цвет фона
   {
    case 0  : MainForm1->BgColor = clBlack; break;
    case 1  : MainForm1->BgColor = clWhite; break;
    case 2  : MainForm1->BgColor = clSilver; break;
    case 3  : MainForm1->BgColor = clCream; break;
    default : MainForm1->BgColor = clBlack;
   }
  switch(WallsColor->ItemIndex)        //цвет фона
   {
    case 0  : MainForm1->FlagWallsColor = 0; break;
    case 1  : MainForm1->FlagWallsColor = 1; break;
    default : MainForm1->FlagWallsColor = 0;
   }
// MainForm1->ComColor = clSkyBlue;  // цвет комментария
// MainForm1->LabelColor = clLime;   // цвет комментария
  MainForm1->VStep  = (float)VertStep->Text.ToDouble();   //шаг вращения по вертикали
  SettForm->Close();
  MainForm1->kart(MainForm1->pBitmap);
}
//---------------------------------------------------------------------------
void __fastcall TSettForm::CancelClick(TObject *Sender) {SettForm->Close();}
//---------------------------------------------------------------------------






