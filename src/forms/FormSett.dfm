object SettForm: TSettForm
  Left = 98
  Top = 145
  Caption = 'ToPo - '#1053#1072#1089#1090#1088#1086#1081#1082#1080
  ClientHeight = 200
  ClientWidth = 240
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object VStep: TLabel
    Left = 8
    Top = 140
    Width = 161
    Height = 13
    Alignment = taRightJustify
    Caption = #1064#1072#1075' '#1087#1086#1074#1086#1088#1086#1090#1072' '#1087#1086' '#1074#1077#1088#1090#1080#1082#1072#1083#1080' ('#1075#1088')'
  end
  object OK: TButton
    Left = 76
    Top = 167
    Width = 75
    Height = 25
    Caption = #1054#1050
    TabOrder = 0
    OnClick = OKClick
  end
  object Cancel: TButton
    Left = 157
    Top = 167
    Width = 75
    Height = 25
    Caption = #1054#1090#1084#1077#1085#1072
    TabOrder = 1
    OnClick = CancelClick
  end
  object Color: TGroupBox
    Left = 8
    Top = 8
    Width = 225
    Height = 121
    Caption = #1062#1074#1077#1090#1072
    TabOrder = 2
    object BackGround: TLabel
      Left = 8
      Top = 24
      Width = 23
      Height = 13
      Caption = #1060#1086#1085
    end
    object BgColor: TComboBox
      Left = 88
      Top = 21
      Width = 121
      Height = 21
      TabOrder = 0
      Text = #1063#1077#1088#1085#1099#1081
      Items.Strings = (
        #1063#1077#1088#1085#1099#1081
        #1041#1077#1083#1099#1081
        #1057#1077#1088#1099#1081
        #1041#1077#1078#1077#1074#1099#1081)
    end
    object WallsColor: TRadioGroup
      Left = 0
      Top = 48
      Width = 97
      Height = 65
      Caption = #1057#1090#1077#1085#1099
      ItemIndex = 0
      Items.Strings = (
        #1057#1077#1088#1099#1081
        #1062#1074#1077#1090' '#1089#1098#1077#1084#1082#1080)
      TabOrder = 1
    end
  end
  object Language: TRadioGroup
    Left = 264
    Top = 24
    Width = 113
    Height = 73
    Caption = 'Language'
    ItemIndex = 0
    Items.Strings = (
      'English'
      'Russian')
    TabOrder = 3
    Visible = False
    OnClick = LanguageClick
  end
  object VertStep: TEdit
    Left = 183
    Top = 137
    Width = 49
    Height = 21
    TabOrder = 4
    Text = '2'
  end
end
