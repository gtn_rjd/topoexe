﻿//---------------------------------------------------------------------------

#ifndef FormSettH
#define FormSettH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
//---------------------------------------------------------------------------
class TSettForm : public TForm
{
__published:	// IDE-managed Components
        TButton *OK;
        TButton *Cancel;
        TGroupBox *Color;
        TRadioGroup *Language;
        TComboBox *BgColor;
        TLabel *BackGround;
        TEdit *VertStep;
        TLabel *VStep;
        TRadioGroup *WallsColor;
        void __fastcall LanguageClick(TObject *Sender);
        void __fastcall OKClick(TObject *Sender);
        void __fastcall CancelClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall TSettForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TSettForm *SettForm;
//---------------------------------------------------------------------------
#endif
