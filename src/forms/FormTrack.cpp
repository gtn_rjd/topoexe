﻿//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "FormTrack.h"   
#include "TOpO.h"   
#include "OgreCMCaveModel.h"    
#include "formtrans.h"  
#include <GeographicLib/LocalCartesian.hpp>   
#include <GeographicLib/Geocentric.hpp>     
#include <iostream>
 #include "vrml.h"
 #include "x3d.h"
#include "CMLog.h"
#include "CMHelpers.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TTrackForm *TrackForm;
//---------------------------------------------------------------------------
__fastcall TTrackForm::TTrackForm(TComponent* Owner)
        : TForm(Owner)
{
      
}

//---------------------------------------------------------------------------
void TTrackForm::Reset() {
    ListBox1->Clear();             
    pathJointPikets.clear();
    RebuildTrack();
}

void TTrackForm::AddPiket(UnicodeString name, const P3D* p) {
    ListBox1->Items->Add(name);
    pathJointPikets.push_back(p);
	RebuildTrack();
}

void TTrackForm::RebuildTrack() {
    activePaths.clear();
    for (int i = 0; i < ((int)pathJointPikets.size())-1; i++) {
        const P3D* from = pathJointPikets[i];
        const P3D* to = pathJointPikets[i+1];
        activePaths.push_back(model->getPath(from, to));    
    }

    pathPiketsCache.clear();
    pathEdgesCache.clear();

    pathPiketsCache.insert(pathJointPikets.begin(), pathJointPikets.end());
    const P3D* prevP = nullptr;
    for (int i = 0; i < size; i++) {
        const P3D* p = &P[i];
        int vertId = model->getVerticeId(p);
        if (prevP && prevP->vet == p->vet) {
            for (const auto& path : activePaths) {
                if (model->isInPath(path, prevP, p)) {
                    pathEdgesCache.insert({std::min(prevP, p), std::max(prevP, p)});    
                }    
            }    
        }
        prevP = p;
    }
    
    MainForm1->reDrawKart();

    int numPikets = 0;
    int summLength = 0;
    int depthLength = 0; 
    int depthChange = 0;
    int planLength = 0;
    int planChange = 0;
    
    for(const std::vector<int>& path : activePaths) {
        numPikets += path.size();  
        for (int i = 0; i < ((int)path.size()) - 1; i++) {
            V3 iPos = model->getPiketPos(path[i]);
            V3 niPos = model->getPiketPos(path[i+1]);
            V3 dPos = niPos - iPos;        
            summLength += dPos.length();
            depthLength += abs(dPos.z);
            dPos.z = 0;
            planLength += dPos.length();
        }           
    }
    
    if (!activePaths.empty()) numPikets -= activePaths.size()-1; 
    if (!activePaths.empty() && !activePaths.front().empty()&& !activePaths.back().empty()) {
        V3 beginPos = model->getPiketPos(activePaths.front().front());
        V3 endPos = model->getPiketPos(activePaths.back().back());
        V3 dpos = endPos - beginPos;
        depthChange = dpos.z;
        dpos.z = 0;
        planChange = dpos.length();
    }
                   
    Label7->Caption = FloatToStr(summLength/100.0f);
    Label8->Caption = FloatToStr(depthLength/100.0f); 
    Label11->Caption = FloatToStr(depthChange/100.0f);  
    Label10->Caption = FloatToStr(planLength/100.0f);  
    Label9->Caption = FloatToStr(planChange/100.0f); 
    Label13->Caption = IntToStr(numPikets);

//    if (!pathJointPikets.empty()) {
//        int lastMet = pathJointPikets.back();
//        int vertId = model->getVerticeId(lastMet);
//        V3 pos = model->getPiketPos(vertId) / 100;  
//
//        double lat, lon, h;    
//        localCoordinateSystem->Reverse(pos.y, pos.x, pos.z, lat, lon, h);  
//        Label14->Caption = (L"ln:" + std::to_wstring(lon) + L" lt:" + std::to_wstring(lat) + L" h:" + std::to_wstring(h)).c_str();
//    }
}

bool TTrackForm::isInPath(const P3D* p0, const P3D* p1) const {
//    if(!model) return false;
//    for (const auto& path : activePaths) {
//        if (model->isInPath(path, p0, p1)) {
//            return true;    
//        }    
//    }
//    return false;
    auto it = pathEdgesCache.find(std::min(p0, p1));
    while (it != pathEdgesCache.end() && it->first == std::min(p0, p1)) {
        if (it->second == std::max(p0, p1)) return true;
    }
    return false;
}
      
bool TTrackForm::isJoint(const P3D* p) const {
//    if(!model) return false;
//    int vertId = model->getVerticeId(p);
//    for (int jointMet : pathJointPikets) {
//        if (vertId == model->getVerticeId(jointMet)) return true;
//    }
//    return false;
    return pathPiketsCache.count(p);
}

void __fastcall TTrackForm::FormClose(TObject *Sender, TCloseAction &Action)
{
    MainForm1->ToolButton48->Down = false; 
    if (!pathJointPikets.empty()) {
        MainForm1->reDrawKart();        
    }
    if (model) {
        delete model;
        model = nullptr;
        pathPiketsCache.clear();
        pathEdgesCache.clear();
    }
}
//---------------------------------------------------------------------------
void __fastcall TTrackForm::FormShow(TObject *Sender)
{
    MainForm1->ToolButton48->Down = true;   
    if (model) {
        delete model;
        model = nullptr;
    }

	model = new OgreCaveDataModel(P, size, W, MainForm1->WRazmer, Equates);
	model->setOutputType(OutputType::OT_WALL);
    model->init();     
	defaultName = MainForm1->CurrFileName.c_str();// Pos1();
	int slashPos = defaultName.find_last_of(L'\\');
    slashPos++;
	int pointPos = defaultName.find_last_of(L'.');
	if (pointPos == std::wstring::npos || slashPos > pointPos) pointPos = defaultName.size();
	defaultName = defaultName.substr(slashPos, pointPos - slashPos);

    Edit2->Text = UnicodeString(defaultName.c_str());
}
//---------------------------------------------------------------------------

void __fastcall TTrackForm::FormDestroy(TObject *Sender)
{
    if (model) {
        delete model;
        model = nullptr;
    }
}
//---------------------------------------------------------------------------
void __fastcall TTrackForm::Button1Click(TObject *Sender)
{
    if (!pathJointPikets.empty() && ListBox1->Items->Count > 0) {
        pathJointPikets.pop_back();           
        ListBox1->Items->Delete(ListBox1->Items->Count-1);  
        RebuildTrack();
        
    }
}
//---------------------------------------------------------------------------
void __fastcall TTrackForm::Button3Click(TObject *Sender)
{
	if (activePaths.empty()) {
		MessageDlg(L"Выберите минимум 2 пикета для экспорта кликнув по ним мышкой в главном окне программы", mtInformation, TMsgDlgButtons()<<mbOK, 0);
		return;
	}

    SaveDialog1->FileName = Edit2->Text + L".gpx";
    if(localCoordinateSystem && SaveDialog1->Execute()) {
		std::wofstream ofs;
		std::wstring fname = UnicodeString(SaveDialog1->FileName).c_str();
		ofs.open(fname, std::wofstream::out | std::wofstream::trunc);
		if (ofs.is_open) {
            ofs <<  
			L"<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\" ?> \n"
			L"<gpx xmlns=\"http://www.topografix.com/GPX/1/1\" \n"
			L"     creator=\"Topo\" \n"
			L"     version=\"1.1\" \n"
			L"     xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" \n"
			L"     xsi:schemaLocation=\"http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd\" \n"
			L"> \n"
			L"   <trk> \n"
			L"       <name>" << UnicodeString(Edit2->Text.c_str()).c_str() << L"</name> \n"
			L"       <trkseg> \n ";
       
            int trackPointsMinDist = Edit1->Text.ToInt();
            ofs.precision(17);
            bool skipFirst = false;
            V3 prevPos (0,0,0);
            for(const std::vector<int>& path : activePaths) {
                float summDist = 0;
                for (int i = 0; i < path.size(); i++) {
                    if (skipFirst && i == 0) continue;
                    V3 iPos = model->getPiketPos(path[i]);
                    iPos /= 100.0f; 
                    summDist += prevPos.distance(iPos);
                    prevPos = iPos;
                    if (i != 0 && i != path.size()-1 && summDist < trackPointsMinDist) continue;
                    summDist = 0;  
					std::vector<const P3D*> p3ds = model->getP3DForVertice(path[i]);
                    std::wstring name;
                    if (!p3ds.empty()) name = UnicodeString(StationList->Strings[p3ds.front()->met].c_str()).c_str();
                    double wgsX, wgsY, wgsH;
                    localCoordinateSystem->Reverse(iPos.y, iPos.x, iPos.z, wgsY, wgsX, wgsH);
					ofs << L"            <trkpt lat=\"" << wgsY << L"\" lon=\"" << wgsX << L"\"> \n";
					ofs << L"                <ele>" << wgsH << L"</ele> \n";
					ofs << L"                <name>" << name << L"</name> \n";
					ofs << L"            </trkpt> \n";
                }
                skipFirst = true;
            }              
            
            ofs << 
            "       </trkseg> \n"
            "   </trk> \n"
            "</gpx> \n";
            
            ofs.close();
        }
    }
}

void __fastcall TTrackForm::Button2Click(TObject *Sender)
{
	if (activePaths.empty()) {
		MessageDlg(L"Выберите минимум 2 пикета для экспорта клинкув по ним мышкой в главном окне программы", mtInformation, TMsgDlgButtons()<<mbOK, 0);
		return;
	}

	bool exportThread = CheckBox1->Checked;
	bool exportWall = CheckBox2->Checked;

	if (!exportWall && !exportThread) {
		MessageDlg(L"Выберите экспорт трека или стен", mtInformation, TMsgDlgButtons()<<mbOK, 0);
        return;
	}

	SaveDialog1->FileName = (defaultName + L".wrl").c_str();
	if(SaveDialog1->Execute()) {
		std::wofstream ofs;
		std::wstring fname = UnicodeString(SaveDialog1->FileName).c_str();
		ofs.open (fname, std::wofstream::out | std::wofstream::trunc);
		if (ofs.is_open) {

			vrmlPreambula(ofs);

			V3 minP(FLT_MAX, FLT_MAX, FLT_MAX);
			V3 maxP(-FLT_MAX, -FLT_MAX, -FLT_MAX);

			int prevVerticeId = 0;
			CM::Color prevColor = CM::Color::None;
			V3 prevPos(0, 0, 0);
			int pathVerticeNum = 0;

			for(const std::vector<int>& path : activePaths) {
				for (int i = 0; i < path.size(); i++) {
					int verticeId = path[i];
					if (verticeId == prevVerticeId) continue;

					V3 pos = model->getPiketPos(verticeId) / 100;
					minP.x = std::min(pos.x, minP.x);
					minP.y = std::min(pos.y, minP.y);
					minP.z = std::min(pos.z, minP.z);

					maxP.x = std::max(pos.x, maxP.x);
					maxP.y = std::max(pos.y, maxP.y);
					maxP.z = std::max(pos.z, maxP.z);

					if (exportThread && prevVerticeId > 0) {
						const CM::Color& col = model->getEdgeColor(prevVerticeId, verticeId);
						if (col != prevColor) {
							if (prevColor != CM::Color::None) {
								vrmlEndPath(ofs, pathVerticeNum);
								pathVerticeNum = 0;
							}

							vrmlStartPath(ofs, col);
							vrmlPoint(ofs, prevPos);
							pathVerticeNum++;
							prevColor = col;
						}
						vrmlPoint(ofs, pos);
						pathVerticeNum++;
					}

					prevVerticeId = verticeId;
					prevPos = pos;
				}
			}

			if (pathVerticeNum > 0) vrmlEndPath(ofs, pathVerticeNum);


			if (exportWall) {
				std::set<SortablePair<int>> segmentsToExport;
				int prevVerticeId = 0;
				for(const std::vector<int>& path : activePaths) {
					for (int i = 0; i < path.size(); i++) {
						int verticeId = path[i];
						if (verticeId == prevVerticeId) continue;
						segmentsToExport.emplace(verticeId, prevVerticeId);
						segmentsToExport.emplace(verticeId, verticeId);

						LOG(L"select "<< verticeId << L" " << prevVerticeId );

						prevVerticeId = verticeId;
					}
				}

				const std::vector<CM::OutputPoly>& polygons = model->getOutputPoly(OutputType::OT_WALL);
				for(const auto& poly : polygons) {
                    LOG(poly.pid0 << L" " << poly.pid1 );
					if (segmentsToExport.count(SortablePair<int>(poly.pid0, poly.pid1)) == 1) {
						//TODO: export polygon to vrml here
						vrmlPoly(ofs, (poly.ca+poly.cb+poly.cc)/3
						, model->verticePosToPiketPos(poly.a) / 100
						, model->verticePosToPiketPos(poly.b) / 100
						, model->verticePosToPiketPos(poly.c) / 100);
					}
				}
			}

			vrmlAABB(ofs, minP, maxP);

			vrmlViewPoints(ofs, minP, maxP);

			vrmlNavigation(ofs, (fabs(maxP.x-minP.x) + fabs(maxP.y-minP.y) + fabs(maxP.z-minP.z))/30 +1);

			vrmlBackground(ofs, 0, 0, 0);

            ofs.close();
		}
    }
}
//---------------------------------------------------------------------------
   void __fastcall TTrackForm::Button4Click(TObject *Sender)
{
	if (activePaths.empty()) {
		MessageDlg(L"Выберите минимум 2 пикета для экспорта клинкув по ним мышкой в главном окне программы", mtInformation, TMsgDlgButtons()<<mbOK, 0);
		return;
	}

	bool exportThread = CheckBox1->Checked;
	bool exportWall = CheckBox2->Checked;

	if (!exportWall && !exportThread) {
		MessageDlg(L"Выберите экспорт трека или стен", mtInformation, TMsgDlgButtons()<<mbOK, 0);
        return;
	}

	SaveDialog1->FileName = (defaultName + L".x3d").c_str();
	if(SaveDialog1->Execute()) {
		std::wofstream ofs;
		std::wstring fname = UnicodeString(SaveDialog1->FileName).c_str();
		ofs.open (fname, std::wofstream::out | std::wofstream::trunc);
		if (ofs.is_open) {

			x3dPreambula(ofs);

			V3 minP(FLT_MAX, FLT_MAX, FLT_MAX);
			V3 maxP(-FLT_MAX, -FLT_MAX, -FLT_MAX);

			int prevVerticeId = 0;
			CM::Color prevColor = CM::Color::None;
			CM::Color prevColor1 = CM::Color::None;
			V3 prevPos(0, 0, 0);
			V3 prevPos1(0, 0, 0);
			int pathVerticeNum = 0;
				int prevVerticeId1 = 0;
				int pathVerticeNum1 = 0;

			for(const std::vector<int>& path : activePaths) {
				for (int i = 0; i < path.size(); i++) {
				int n=i;
					int verticeId = path[i];
					if (verticeId == prevVerticeId) continue;

					V3 pos = model->getPiketPos(verticeId) / 100;
					minP.x = std::min(pos.x, minP.x);
					minP.y = std::min(pos.y, minP.y);
					minP.z = std::min(pos.z, minP.z);

					maxP.x = std::max(pos.x, maxP.x);
					maxP.y = std::max(pos.y, maxP.y);
					maxP.z = std::max(pos.z, maxP.z);

					if (exportThread && prevVerticeId > 0) {
						const CM::Color& col = model->getEdgeColor(prevVerticeId, verticeId);
						if (col != prevColor) {
							if (prevColor != CM::Color::None) {
								x3dEndPath(ofs, pathVerticeNum);
							 /////
								 for (int i = (n-pathVerticeNum); i < n; i++) {
									int verticeId1 = path[i];
									if (verticeId1 == prevVerticeId1) continue;

									V3 pos1 = model->getPiketPos(verticeId1) / 100;
									minP.x = std::min(pos.x, minP.x);
									minP.y = std::min(pos.y, minP.y);
									minP.z = std::min(pos.z, minP.z);

									maxP.x = std::max(pos.x, maxP.x);
									maxP.y = std::max(pos.y, maxP.y);
									maxP.z = std::max(pos.z, maxP.z);

									if (exportThread && prevVerticeId1 > 0) {
										const CM::Color& col1 = model->getEdgeColor(prevVerticeId1, verticeId1);
										if (col1 != prevColor1) {
											if (prevColor1 != CM::Color::None) {
												//x3dEndPath(ofs, pathVerticeNum);
												pathVerticeNum1 = 0;
											}

											//x3dStartPath(ofs, col);
											x3dPoint(ofs, prevPos1);
											pathVerticeNum1++;
											prevColor1 = col1;
										}
										x3dPoint(ofs, pos1);
										pathVerticeNum1++;
									}

									prevVerticeId1 = verticeId1;
									prevPos1 = pos1;
								}

/////////////////////////////////////////////
								x3dcoord(ofs);
								pathVerticeNum = 0;
							}

							x3dStartPath(ofs, col);
							//x3dPoint(ofs, prevPos);
							pathVerticeNum++;
							prevColor = col;
						}
						//x3dPoint(ofs, pos);
						pathVerticeNum++;
					}

					prevVerticeId = verticeId;
					prevPos = pos;
				}
			}

			if (pathVerticeNum > 0){ x3dEndPath(ofs, pathVerticeNum);
			for(const std::vector<int>& path : activePaths) {

            for (int i = (path.size()-pathVerticeNum); i < path.size(); i++) {
					int verticeId1 = path[i];
					if (verticeId1 == prevVerticeId1) continue;

					V3 pos1 = model->getPiketPos(verticeId1) / 100;


					if (exportThread && prevVerticeId1 > 0) {
						const CM::Color& col1 = model->getEdgeColor(prevVerticeId1, verticeId1);
						if (col1 != prevColor1) {
							if (prevColor1 != CM::Color::None) {
								//x3dEndPath(ofs, pathVerticeNum);
								pathVerticeNum1 = 0;
							}

							//x3dStartPath(ofs, col);
							x3dPoint(ofs, prevPos1);
							pathVerticeNum1++;
							prevColor1 = col1;
						}
						x3dPoint(ofs, pos1);
						pathVerticeNum1++;
					}

					prevVerticeId1 = verticeId1;
					prevPos1 = pos1;
				}
			}
			x3dcoord(ofs);
            }


			if (exportWall) {
				std::set<SortablePair<int>> segmentsToExport;
				int prevVerticeId = 0;
				for(const std::vector<int>& path : activePaths) {
					for (int i = 0; i < path.size(); i++) {
						int verticeId = path[i];
						if (verticeId == prevVerticeId) continue;
						segmentsToExport.emplace(verticeId, prevVerticeId);
						segmentsToExport.emplace(verticeId, verticeId);

						LOG(L"select "<< verticeId << L" " << prevVerticeId );

						prevVerticeId = verticeId;
					}
				}

				const std::vector<CM::OutputPoly>& polygons = model->getOutputPoly(OutputType::OT_WALL);
				for(const auto& poly : polygons) {
                    LOG(poly.pid0 << L" " << poly.pid1 );
					if (segmentsToExport.count(SortablePair<int>(poly.pid0, poly.pid1)) == 1) {
						//TODO: export polygon to vrml here
						x3dPoly(ofs, (poly.ca+poly.cb+poly.cc)/3
						, model->verticePosToPiketPos(poly.a) / 100
						, model->verticePosToPiketPos(poly.b) / 100
						, model->verticePosToPiketPos(poly.c) / 100);
					}
				}
			}

			x3dAABB(ofs, minP, maxP);

			x3dViewPoints(ofs, minP, maxP);

			x3dNavigation(ofs, (fabs(maxP.x-minP.x) + fabs(maxP.y-minP.y) + fabs(maxP.z-minP.z))/30 +1);

			x3dBackground(ofs, 0, 0, 0);

            ofs.close();
		}
    }
}
//---------------------------------------------------------------------------






