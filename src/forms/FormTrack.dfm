object TrackForm: TTrackForm
  Left = 0
  Top = 0
  Caption = #1042#1099#1073#1086#1088' '#1089#1077#1075#1084#1077#1085#1090#1072
  ClientHeight = 483
  ClientWidth = 210
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  OnClose = FormClose
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 9
    Top = 5
    Width = 43
    Height = 13
    Caption = #1055#1080#1082#1077#1090#1099':'
  end
  object ListBox1: TListBox
    Left = 8
    Top = 24
    Width = 121
    Height = 97
    ItemHeight = 13
    TabOrder = 0
  end
  object Button1: TButton
    Left = 135
    Top = 24
    Width = 75
    Height = 25
    Caption = #1059#1076#1072#1083#1080#1090#1100
    TabOrder = 1
    OnClick = Button1Click
  end
  object GroupBox1: TGroupBox
    Left = 6
    Top = 127
    Width = 202
    Height = 122
    Caption = #1061#1072#1088#1088#1072#1082#1090#1077#1088#1080#1089#1090#1080#1082#1080' '#1089#1077#1075#1084#1077#1085#1090#1072
    TabOrder = 2
    object Label2: TLabel
      Left = 11
      Top = 19
      Width = 84
      Height = 13
      Caption = #1055#1088#1086#1090#1103#1078#1077#1085#1085#1086#1089#1090#1100':'
    end
    object Label3: TLabel
      Left = 11
      Top = 35
      Width = 162
      Height = 13
      Caption = #1057#1091#1084#1084#1072' '#1085#1072#1073#1086#1088#1072' '#1080' '#1089#1073#1088#1086#1089#1072' '#1074#1099#1089#1086#1090#1099':'
    end
    object Label4: TLabel
      Left = 11
      Top = 83
      Width = 116
      Height = 13
      Caption = #1055#1077#1088#1077#1084#1077#1097#1077#1085#1080#1077' '#1074' '#1087#1083#1072#1085#1077':'
    end
    object Label5: TLabel
      Left = 11
      Top = 51
      Width = 60
      Height = 13
      Caption = #1040#1084#1087#1083#1080#1090#1091#1076#1072':'
    end
    object Label6: TLabel
      Left = 11
      Top = 67
      Width = 84
      Height = 13
      Caption = #1044#1083#1080#1085#1085#1072' '#1074' '#1087#1083#1072#1085#1077':'
    end
    object Label7: TLabel
      Left = 185
      Top = 19
      Width = 6
      Height = 13
      Alignment = taRightJustify
      Caption = '0'
    end
    object Label8: TLabel
      Left = 185
      Top = 35
      Width = 6
      Height = 13
      Alignment = taRightJustify
      Caption = '0'
    end
    object Label9: TLabel
      Left = 134
      Top = 83
      Width = 57
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = '0'
    end
    object Label10: TLabel
      Left = 185
      Top = 67
      Width = 6
      Height = 13
      Alignment = taRightJustify
      Caption = '0'
    end
    object Label11: TLabel
      Left = 185
      Top = 51
      Width = 6
      Height = 13
      Alignment = taRightJustify
      Caption = '0'
    end
    object Label12: TLabel
      Left = 11
      Top = 98
      Width = 47
      Height = 13
      Caption = #1055#1080#1082#1077#1090#1086#1074':'
    end
    object Label13: TLabel
      Left = 134
      Top = 98
      Width = 57
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = '0'
    end
  end
  object GroupBox2: TGroupBox
    Left = 6
    Top = 255
    Width = 202
    Height = 130
    Caption = #1069#1082#1089#1087#1086#1088#1090' gps '#1090#1088#1077#1082#1072' '#1074' .gpx'
    TabOrder = 3
    object Label14: TLabel
      Left = 11
      Top = 15
      Width = 59
      Height = 13
      Caption = #1048#1084#1103'  '#1090#1088#1077#1082#1072':'
    end
    object Label15: TLabel
      Left = 11
      Top = 54
      Width = 150
      Height = 13
      Caption = 'Track points '#1085#1077' '#1095#1072#1097#1077' ('#1084#1077#1090#1088#1086#1074')'
    end
    object Button3: TButton
      Left = 99
      Top = 97
      Width = 100
      Height = 25
      Caption = #1069#1082#1089#1087#1086#1088#1090
      TabOrder = 0
      OnClick = Button3Click
    end
    object Edit2: TEdit
      Left = 6
      Top = 30
      Width = 185
      Height = 21
      TabOrder = 1
      Text = 'Edit1'
    end
    object Edit1: TEdit
      Left = 6
      Top = 70
      Width = 185
      Height = 21
      NumbersOnly = True
      TabOrder = 2
      Text = '10'
    end
  end
  object GroupBox3: TGroupBox
    Left = 6
    Top = 391
    Width = 202
    Height = 90
    Caption = #1069#1082#1089#1087#1086#1088#1090' '#1074' vrml/x3d'
    TabOrder = 4
    object CheckBox1: TCheckBox
      Left = 9
      Top = 17
      Width = 97
      Height = 17
      Caption = #1053#1080#1090#1082#1072
      Checked = True
      State = cbChecked
      TabOrder = 0
    end
    object CheckBox2: TCheckBox
      Left = 9
      Top = 37
      Width = 162
      Height = 17
      Caption = #1054#1073#1098#1077#1084#1085#1099#1077' '#1089#1090#1077#1085#1099
      TabOrder = 1
    end
    object Button2: TButton
      Left = 3
      Top = 60
      Width = 92
      Height = 26
      Caption = #1069#1082#1089#1087#1086#1088#1090' vrml'
      TabOrder = 2
      OnClick = Button2Click
    end
    object Button4: TButton
      Left = 112
      Top = 60
      Width = 87
      Height = 25
      Caption = #1069#1082#1089#1087#1086#1088#1090' x3d'
      TabOrder = 3
      OnClick = Button4Click
    end
  end
  object SaveDialog1: TSaveDialog
    Filter = 'All|*.*'
    Title = 'Save track'
    Left = 152
    Top = 72
  end
end
