﻿//---------------------------------------------------------------------------

#ifndef FormTrackH
#define FormTrackH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.Dialogs.hpp>   
#include <string>     
#include <vector>
#include <CaveData.h>

class OgreCaveDataModel;
//---------------------------------------------------------------------------
class TTrackForm : public TForm
{
__published:	// IDE-managed Components
        TListBox *ListBox1;
        TLabel *Label1;
        TButton *Button1;
        TGroupBox *GroupBox1;
        TLabel *Label2;
        TLabel *Label3;
        TLabel *Label4;
        TLabel *Label5;
        TLabel *Label6;
        TLabel *Label7;
        TLabel *Label8;
        TLabel *Label9;
        TLabel *Label10;
        TLabel *Label11;
        TLabel *Label12;
        TLabel *Label13;
    TGroupBox *GroupBox2;
    TButton *Button3;
    TEdit *Edit2;
    TLabel *Label14;
    TSaveDialog *SaveDialog1;
    TLabel *Label15;
    TEdit *Edit1;
	TGroupBox *GroupBox3;
	TCheckBox *CheckBox1;
	TCheckBox *CheckBox2;
	TButton *Button2;
	TButton *Button4;
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
        void __fastcall FormShow(TObject *Sender);
        void __fastcall FormDestroy(TObject *Sender);
        void __fastcall Button1Click(TObject *Sender);
    void __fastcall Button3Click(TObject *Sender);
	void __fastcall Button2Click(TObject *Sender);
	void __fastcall Button4Click(TObject *Sender);


private:	// User declarations
        OgreCaveDataModel* model {nullptr};
        std::vector<std::vector<int> > activePaths; // array of array of verticceId
        std::vector<const P3D*> pathJointPikets; 
        
        std::set<const P3D*> pathPiketsCache;
        std::multimap<const P3D*, const P3D*> pathEdgesCache;
                
public:		// User declarations
        void Reset();
        void AddPiket(UnicodeString name, const P3D* p);  
		void RebuildTrack();
        bool isInPath(const P3D* p0, const P3D* p1) const;
        bool isJoint(const P3D* p) const;
        
		__fastcall TTrackForm(TComponent* Owner);

protected:
    std::wstring defaultName;

};
//---------------------------------------------------------------------------
extern PACKAGE TTrackForm *TrackForm;
//---------------------------------------------------------------------------
#endif
