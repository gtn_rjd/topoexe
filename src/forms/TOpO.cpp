﻿// ---------------------------------------------------------------------------
#include <vcl.h>
#include <process.h>
#include <stdio.h>
#include <stdlib.h>
#include <dir.h>
#include <errno.h>
#include <math.h>
#include <dstring.h>
#pragma hdrstop

#include "TOpO.h"
#include "About.h"
#include "FormInfo.h"
#include "FormHelp.h"
#include "FormEdit.h"
#include "FormPrn.h"
#include "formtrans.h"
#include "Form3dExp.h"
#include "FormSett.h"   
#include "FormTrack.h"
#include <limits>
#include <limits>
#include <string>
#include "OgreCMRenderer.h"

#pragma package(smart_init)
#pragma resource "*.dfm"
// ---------------------------------------------------------------------------

// считываемые параметры
unsigned int size = 0; // число пикетов (массив P[])
unsigned int wsize = 0; // max число пикетов до стен (массив W[])
unsigned int Wsize = 0; // точное число пикетов до стен (массив W[])
// unsigned int sur_col;   // цвет пов. топо
// unsigned int dup_col;   // цвет dup. топо
unsigned int PP[9]; // число пикетов, веток, меток, съемок, колец, #equate, пикетов до стен

// высчитываемые и текущие параметры
long Xmin, Xmax, Ymin, Ymax, Zmin, Zmax, XYmin, XYmax;
long Xmed, Ymed, Zmed, XYmed; // текущие коорд.середины
long OgreXShift = 0;
long OgreYShift = 0;
float H; // текущий масштаб в sm/pixel
float phi = 0, theta = 0; // текущие углы поворота
float C, S, Ct, St; // sin и cos углов поворота
char rot_flag = 0; // 0-stop, 1-right rotatation, 2-left rotation.
float nn = 2; // шаг при вращении
int MouseStation = -1; // номер мышиного пикета

int CaveMouseNo = -1; // номер пикета для сave-мыши

int xx = 0; // число т. bitmap по гориз.
int yy = 0; // и вертикали
int X0 = 0;
int Y0 = 0; // коорд. точки нажатия левой кн. мыши

/*
 struct P3D              // 12+2+()=14 байт  (4.5 тыс. пикетов в 64K)
 {long X,Y,Z;
 unsigned char priz;   // вх. признаки точки
 unsigned char col;    // цвет
 int vet;              // номер ветки
 int sur;              // номер съемки
 //unsigned int  vpoint; // номер т. ветвл. (для разрез-развертки)
 };
 struct Vet
 {int B,E;};
 */
UnicodeString Labels;
UnicodeString Surveys;
UnicodeString Comments;
UnicodeString WComments;
UnicodeString IncFiles;

P3D *P; // указатель массива точек ниток
W3D *W; // указатель массива точек стен
Vet *V; // указатель массива веток
Srv *Surv; // указатель массива съемок

int *Prx = nullptr; // указатели массивов проекций точек на плоскость экрана,
int *Pry = nullptr; //
WPR *Wpr; // указатель массива проекций ежа обрисовки на плоскость экрана
long *SPrx = nullptr; // указатели массивов проекций поверхности на плоскость экрана
long *SPry = nullptr; //
int *FPrx = nullptr; // указатели массивов проекций фикс. точек на плоскость экрана
int *FPry = nullptr; //

TMainForm1 *MainForm1;
PCube PCub[8]; // bounding box
int PrCubx[8];
int PrCuby[8];

// Пути
wchar_t cur_dir[MAXPATH]; // current path (to this program)
UnicodeString caves_path; // путь к файлам данных
// UnicodeString cave_filename;

// declarations
void ResizeBitmap(Graphics::TBitmap *&pBitmap);
// void projection(Graphics::TBitmap *pBitmap);  topo.h
// void kart(Graphics::TBitmap *pBitmap);
void Stations(Graphics::TBitmap *pBitmap);
void calcGridStep(double& Af, int& Hc, char gridSizeMode);
void scale(Graphics::TBitmap *pBitmap, char o);
void ShowLabels(Graphics::TBitmap *pBitmap);
void ShowComments(Graphics::TBitmap *pBitmap);
void ShowBifLabels(Graphics::TBitmap *pBitmap);
void ShowFixPoints(Graphics::TBitmap *pBitmap);
void ShowFixPoints_com(Graphics::TBitmap *pBitmap);
void FindStations(Graphics::TBitmap *pBitmap);

void Read_vxod_par();
void Read_vxod_data();
void TakeData();
void minmax();
// void init_par();  topo.h

void axis();

char ActModa = 0;

TColor SetTColor(int oldColor);

void ReinitCaveModel();

double sqr(double a) {
    return a*a;
}

void PhiPlus(float nn) {
    phi += 0.017453292 * nn;
    if (phi > 6.2831853)
        phi = phi - 6.2831853;
}

void PhiMinus(float nn) {
    phi -= 0.017453292 * nn;
    if (phi < 0)
        phi = phi + 6.2831853;
}

void Circle(Graphics::TBitmap *pBitmap, int x, int y, int r) {
    pBitmap->Canvas->Ellipse(x - r, y - r, x + r, y + r);
}

// **************************************************************************//
// ------------------------Конструктор MainForm1-------------------------------
__fastcall TMainForm1::TMainForm1(TComponent* Owner) : TForm(Owner) {

    // g_pages_mutex.lock();
    std::char_traits<wchar_t>::compare(L"a", L"b", 0);
    std::char_traits<wchar_t>::eof();

    State = STATE_NONE; // устанавливается SetState
    ComColor = clSkyBlue; // цвет комментария
    LabelColor = clLime; // цвет комментария
    BgColor = clBlack; // цвет фона
    VStep = 2; // верт шаг вращения в градусах
    FlagWallsColor = 0; // цвет ежика стен: 0 - серый, 1 - цвет нитки
    // clCream  clGray  clSkyBlue clMoneyGreen clSilver;
}

// ----------------------------------------------------------------------------
void __fastcall TMainForm1::FormCreate(TObject *Sender) {
    ogreRenderer = new OgreRenderer(Handle, false);
    if (!ogreRenderer->init()) {
        ogreRenderer = NULL;
    }

    // Application->OnIdle = MyIdleHandler;
    _wgetcwd(cur_dir, MAXPATH);
    caves_path = cur_dir;
    caves_path += L"\\caves";

    /* wchar_t  *p=NULL;         //current path (to this program)
     try
     {
     p=searchpath(caves_path.c_str());
     if(!p)
     OpenDialog1->InitialDir=cur_dir;
     else */
    OpenDialog1->InitialDir = caves_path;
    CurrFileName = L"";
    /* }
     catch(...)
     {
     OpenDialog1->InitialDir=cur_dir;
     } */

    Timer2->Interval = 60;

    pBitmap = MainForm1->Image1->Picture->Bitmap;
    pBitmap->PixelFormat = pf24bit; // это я перенес из NewBitmap, чтобы сделать это один раз (Свисла)
    ResizeBitmap(pBitmap);

    // ListFileNameHystory = new TList;//список имен последних открываемых файлов

    State = STATE_NONE;

    updateOgreCheckers();
    // updateWallsShadowModeChecker();
    // updateWallsBlowModeChecker();
    // updateWallsPropagateModeChecker();
    // updategenerateWallsForNoWallsPiketsModeChecker();
    // updateWallsSurfaceModeChecker();
    // updateWallsSegmentTriangulationMode();

}

// ---------------------------------------------------------------------------
void __fastcall TMainForm1::Timer2Timer(TObject *Sender) {
    if (rot_flag == 0)
        Timer2->Enabled = false;

    if (rot_flag == 1) {
        PhiMinus(nn);
        projection(pBitmap);
        kart(pBitmap);
    }
    else if (rot_flag == 2) {
        PhiPlus(nn);
        projection(pBitmap);
        kart(pBitmap);
    }
    // Refresh();
}

void TMainForm1::reDrawKart() {
    projection(pBitmap);
    kart(pBitmap);
}
// ********************************* Bitmap ***********************************
//
// ------------------------------- Clear Bitmap  ------------------------------
void ClearBitmap(Graphics::TBitmap *&pBitmap, bool cb) {
    pBitmap->Canvas->Brush->Color = MainForm1->BgColor;
    pBitmap->Canvas->Pen->Color = MainForm1->BgColor;
    pBitmap->Canvas->Rectangle(0, 0, pBitmap->Width, pBitmap->Height);
    if (cb == true) {
        MainForm1->LabelXYZ->Caption = L"";
    }
}

// ------------------------- Bitmap (Re)initialization ------------------------
void ResizeBitmap(Graphics::TBitmap *&pBitmap) // при открытии MainForm1 or resize
{ // assign the initial width and height
    float BOldHeight = 0;
    float BOldWidth = 0;

    if (pBitmap != NULL) {
        BOldHeight = pBitmap->Height;
        BOldWidth = pBitmap->Width;
    }

    /* #if(pBitmap!=NULL){
     BOldHeight = pBitmap->Height; BOldWidth = pBitmap->Width;
     delete pBitmap; }
     pBitmap = new Graphics::TBitmap();
     pBitmap->PixelFormat = pf8bit;   //24 -> 8 bit/pixel //для скорости вращения
    /*# */

    int BHeight = MainForm1->ClientHeight - MainForm1->ControlBar1->Height - MainForm1->StatusBar1->Height;

    if (MainForm1->ogreRenderer)
        MainForm1->ogreRenderer->resize(MainForm1->ClientWidth, MainForm1->ClientHeight, MainForm1->ControlBar1->Height, MainForm1->StatusBar1->Height);

    if (MainForm1->ClientWidth > 300)
        pBitmap->Width = MainForm1->ClientWidth;
    else
        pBitmap->Width = 300;
    if (BHeight > 200)
        pBitmap->Height = BHeight;
    else
        pBitmap->Height = 200;
    pBitmap->Canvas->Font->Color = clTeal; // Font color
    pBitmap->Canvas->Font->Size = 10;

    ClearBitmap(pBitmap, 0);

    // #MainForm1->Image1->Picture->Assign(Bitmap);  //Resize Image1

    H = H * (hypot(BOldWidth, BOldHeight) / hypot((double)pBitmap->Width, (double)pBitmap->Height));

    MainForm1->LabelXYZ->Top = MainForm1->ClientHeight - MainForm1->StatusBar1->Height - MainForm1->LabelXYZ->Height;

    // число т. экрана по гориз. и вертикали, вывод в StatusBar
    xx = pBitmap->Width;
    yy = pBitmap->Height;
    UnicodeString bmpSizeStr = xx;
    bmpSizeStr += L'x';
    bmpSizeStr += yy;
    MainForm1->StatusBar1->Panels->Items[2]->Text = bmpSizeStr;
}

// ******************** Build-.T3D & (Re)initialization P[] *******************
char Build1(void) {
    UnicodeString data_path = L'"'; // файл данных с путем
    data_path += MainForm1->CurrFileName.c_str();
    data_path += L'"'; // " - чтобы в comm. line путь воспринимался как 1 аргумент

    TransForm->ComponentsInit();
    if (RunBuild(MainForm1->CurrFileName.c_str()) == true) {
        // Read_vxod_par();///--------------------------------------
        PP[0] = TransForm->DataLength.pik; // число пикетов
        PP[1] = TransForm->DataLength.vet; // веток
        PP[2] = TransForm->DataLength.met; // меток
        PP[3] = TransForm->DataLength.lps; // колец
        PP[4] = TransForm->DataLength.srv; // съемок
        PP[5] = TransForm->DataLength.equ; // #equate
        PP[6] = TransForm->DataLength.vet0; // пещерных веток (без отвеч. #point)
        PP[7] = TransForm->DataLength.wal; // пикетов ежиков до стен
        PP[8] = TransForm->DataLength.lrud; // данных lrud
        size = PP[0] + PP[6];
        wsize = PP[7] + PP[8];
        MainForm1->Razmer = size; // для внешнего пользования
        MainForm1->StatusBar1->Panels->Items[1]->Text = L"Cave: ";
        MainForm1->StatusBar1->Panels->Items[1]->Text += MainForm1->cave;
        if (Srf.flag) {
            MainForm1->StatusBar1->Panels->Items[1]->Text = L"Region: ";
            MainForm1->StatusBar1->Panels->Items[1]->Text += MainForm1->region;
        }
        if (P != NULL) {
            delete[]Prx;
            delete[]Pry;
            delete[]P;
            delete[]V;
            delete[]Surv;
        }
        P = new P3D[size + 1]; // создание массивов размера size
        Prx = new int[size];
        Pry = new int[size];
        V = new Vet[PP[1] + 2];
        Surv = new Srv[PP[4] + 1];
        if (SPrx != NULL)
            delete[]SPrx; // рельеф
        if (SPry != NULL)
            delete[]SPry;
        if (FPrx != NULL)
            delete[]FPrx; // фикс. точки
        if (FPry != NULL)
            delete[]FPry;
        if (W != NULL)
            delete[]W; // обрисовка - ежики
        if (Wpr != NULL)
            delete[]Wpr; // обрисовка - ежики
        SPrx = new long[Srf.ncols * Srf.nrows];
        SPry = new long[Srf.ncols * Srf.nrows];
        FPrx = new int[TransForm->DataLength.fxp];
        FPry = new int[TransForm->DataLength.fxp];
        W = new W3D[wsize * 4]; // массив точек стен
        Wpr = new WPR[wsize * 4]; // и их проекций
        for (unsigned int s = 0; s < size; ++s) // "обнуляем" массив lrud
        {
            P[s].left = -1;
            P[s].right = -1;
            P[s].up = -1;
            P[s].down = -1;
        }
        // Read_vxod_data();
        TakeData();
        minmax();
        return 1;
    }
    return 0;
}

// -------------------------- LRangle --------------------------------------
void LRangle() // угол для откладывания left - right
{
    float dx1, dx2, dy1, dy2; // горизонт. проекции
    float length1 = 0, length2 = 0, length = 0; // длины
    float lfac1 = 0, lfac2 = 0; // обратные длины
    float sin_a = 0, cos_a = 0;

    for (unsigned int s = 0; s < size; ++s) {
        if ((s > 0) && !(P[s].priz & PRIZ_BRANCH)) // если s - не 1-й пикет ветки
        {
            dx1 = P[s].pos.x - P[s - 1].pos.x;
            dy1 = P[s].pos.y - P[s - 1].pos.y;
            length1 = sqrt(sqr(dx1) + sqr(dy1));
            if (length1 > 0.1)
                lfac1 = 1 / length1;
            else
                lfac1 = 0; // если вертикаль
        }
        else {
            lfac1 = 0;
            dx1 = 0;
            dy1 = 0;
        }
		if ((!(P[s + 1].priz & PRIZ_BRANCH)) && (s < size - 1)) // если s - не последний пикет ветки
        {
            dx2 = P[s + 1].pos.x - P[s].pos.x;
            dy2 = P[s + 1].pos.y - P[s].pos.y;
            length2 = sqrt(sqr(dx2) + sqr(dy2));
            if (length2 > 0.1)
                lfac2 = 1 / length2;
            else
                lfac2 = 0; // если вертикаль
        }
        else {
            lfac2 = 0;
            dx1 = 0;
            dy1 = 0;
        }
        length = sqrt(sqr(dx1 * lfac1 + dx2 * lfac2) + sqr(dy1 * lfac1 + dy2 * lfac2));
        if (length > 0) // если не 0 (не 2 верт подряд)
        {
            cos_a = (dx1 * lfac1 + dx2 * lfac2) / length;
            sin_a = (dy1 * lfac1 + dy2 * lfac2) / length;
			if ((P[s].priz & PRIZ_LR_NORMAL) || (dx1 * dx2 + dy1 * dy2) * lfac1 * lfac2 < -0.94) // если угол очень тупой, то право-лево откл перп-но
                if (P[s].priz & PRIZ_WALLS_FROM) {
                    cos_a = dx2 * lfac2;
                    sin_a = dy2 * lfac2;
                } // -  #walls_from
                else {
                    cos_a = dx1 * lfac1;
                    sin_a = dy1 * lfac1;
                }
        }
        else {
            cos_a = 1;
            sin_a = 0;
		} // временно, пока нет #walls_forward
		P[s].sin_c = cos_a;
        P[s].cos_c = -sin_a;
    }
}

// -------------------------------------------------------------------------
void TakeData() // данные из FormTranse
{
    TrackForm->Close();
    // UnicodeString prosmotr=L"";   //для отладки
    Comments = TransForm->Comment;
    WComments = TransForm->WallComment;
    IncFiles = TransForm->IncFiles;
    MainForm1->sur_col = TransForm->DataLength.sur;
    MainForm1->dup_col = TransForm->DataLength.dup;

    // ---------------------- пикеты P[s]. ------------------------------------
    int s = 0;
    int v = 0;
    int prevCom = 0;
    int walls1 = 0;
    float X = 0, Y = 0, Z = 0, XY = 0;
    while (s < TransForm->DataLength.pik) { // +1 -надо ли??
        if ((s == 0) || (P1[s].Vet != P1[s - 1].Vet)) // если начало ветки - доп. точка
        {
            X = 100 * V1[P1[s].Vet].XB;
            Y = 100 * V1[P1[s].Vet].YB;
            Z = 100 * V1[P1[s].Vet].ZB;
            XY = 100 * V1[P1[s].Vet].XYB;
            P[s + v].pos.x = X;
            P[s + v].pos.y = Y;
            P[s + v].pos.z = Z;
            P[s + v].XY = XY;
            P[s + v].priz = PRIZ_NONE;
            (int&)P[s + v].priz = P[s + v].priz | PRIZ_BRANCH; // v - точка ветвления
            if (P1[s].prz & 64)
                (int&)P[s + v].priz = P[s + v].priz | PRIZ_BEGIN; // t -  #red beg
            if (P1[s].prz & 1)
                (int&)P[s + v].priz = P[s + v].priz | PRIZ_SURFACE; // p -  #sur
            if (P1[s].prz & 16)
                (int&)P[s + v].priz = P[s + v].priz | PRIZ_SIPHON; // s -  #sif beg
            if (P1[s].prz & 4)
                (int&)P[s + v].priz = P[s + v].priz | PRIZ_ENTRY; // e -  #ent beg
            if (P1[s].prz & 2)
                (int&)P[s + v].priz = P[s + v].priz | PRIZ_RIVER; // r -  #riv
            if (P1[s].prz2 & 2)
                (int&)P[s + v].priz = P[s + v].priz | PRIZ_DUPLICATE; // -  #duplicate
            if (P1[s].prz2 & 4)
                (int&)P[s + v].priz = P[s + v].priz | PRIZ_WALLS_FROM; // -  #walls_from
            if (P1[s].prz2 & 8)
                (int&)P[s + v].priz = P[s + v].priz | PRIZ_Z_SURVEY; // -  #z_survey
            if (P1[s].prz2 & 16)
                (int&)P[s + v].priz = P[s + v].priz | PRIZ_Z_TURN; // -  #ztn
            if (P1[s].prz2 & 32)
                (int&)P[s + v].priz = P[s + v].priz | PRIZ_ONLY_CONVEX_WALLS; // -  #start_convex_surf
			if (P1[s].prz2 & 64)
				(int&)P[s + v].priz = P[s + v].priz | PRIZ_LR_NORMAL;
			if (P1[s].prz2 & 128)
				(int&)P[s + v].priz = P[s + v].priz | PRIZ_NO_FAKE_WALLS; // -  #no_autogen_walls
            // if(P1[s].prz2&64)P[s+v].priz = P[s+v].priz | PRIZ_IGNORE_WALLS;

            P3DPriz priz = P[s + v].priz;

            P[s + v].vet = P1[s].Vet;
            P[s + v].col = P1[s].col;
            P[s + v].srv = P1[s].Srv;
            P[s + v].line = P1[s].line;
            P[s + v].met = P1[s].metBeg;
            // если коммент. к началу пикета:
            if (P1[s].Com < 0) {
                P[s + v].com = abs(P1[s].Com);
                prevCom = P[s + v].com;
            }
            else
                P[s + v].com = 0;
            v++;
        }
        X = X + 100 * P1[s].L;
        Y = Y + 100 * P1[s].An;
        Z = Z + 100 * P1[s].Az;
        XY = XY + 100 * P1[s].pr_xy();
        P[s + v].pos.x = X;
        P[s + v].pos.y = Y;
        P[s + v].pos.z = Z;
        P[s + v].XY = XY;
        P[s + v].priz = PRIZ_NONE;
        if (P1[s].prz & 128)
            (int&)P[s + v].priz = P[s + v].priz | PRIZ_BEGIN; // t -  #red beg
        if (P1[s].prz & 1)
            (int&)P[s + v].priz = P[s + v].priz | PRIZ_SURFACE; // p -  #sur
        if (P1[s].prz & 32)
            (int&)P[s + v].priz = P[s + v].priz | PRIZ_SIPHON; // s -  #sif beg
        if (P1[s].prz & 8)
            (int&)P[s + v].priz = P[s + v].priz | PRIZ_ENTRY; // e -  #ent beg
        if (P1[s].prz & 2)
            (int&)P[s + v].priz = P[s + v].priz | PRIZ_RIVER; // r -  #riv
        if (P1[s].prz2 & 2)
            (int&)P[s + v].priz = P[s + v].priz | PRIZ_DUPLICATE; // -  #duplicate
        if (P1[s].prz2 & 4)
            (int&)P[s + v].priz = P[s + v].priz | PRIZ_WALLS_FROM; // -  #walls_from
        if (P1[s].prz2 & 8)
            (int&)P[s + v].priz = P[s + v].priz | PRIZ_Z_SURVEY; // -  #z_survey
        if (P1[s].prz2 & 16)
			(int&)P[s + v].priz = P[s + v].priz | PRIZ_Z_TURN; // -  #ztn
        if (P1[s].prz2 & 32)
			(int&)P[s + v].priz = P[s + v].priz | PRIZ_ONLY_CONVEX_WALLS; // -  #start_convex_surf
		if (P1[s].prz2 & 64)
			(int&)P[s + v].priz = P[s + v].priz | PRIZ_LR_NORMAL;
		if (P1[s].prz2 & 128)
			(int&)P[s + v].priz = P[s + v].priz | PRIZ_NO_FAKE_WALLS; // -  #no_autogen_walls
        // if(P1[s].prz2&64) P[s+v].priz = P[s+v].priz | PRIZ_IGNORE_WALLS;

        P3DPriz priz = P[s + v].priz;

        if (TransForm->DataLength.lrud) // если есть lrud
        {
            if (P1[s].prz2&4)
                walls1 = 1;
            else
                walls1 = 0; // к какому пикету lrud
            P[s + v - walls1].left = 100 * P1[s].left; // переводим в целые см
            P[s + v - walls1].right = 100 * P1[s].right;
            P[s + v - walls1].up = 100 * P1[s].up;
            P[s + v - walls1].down = 100 * P1[s].down;
        }
        P[s + v].vet = P1[s].Vet;
        P[s + v].col = P1[s].col;
        if (P[s].priz & PRIZ_RIVER)
            P[s].col = 3;
        P[s + v].srv = P1[s].Srv;
        P[s + v].line = P1[s].line;
        P[s + v].met = P1[s].metEnd;
        if (P1[s].Com > 0) { // если кооммент. к концу пикета
            P[s + v].com = P1[s].Com;
            if (P1[s].Com - prevCom - 1 > 0)
                P[s + v - 1].com = P1[s].Com - 1;
            prevCom = P[s + v].com;
        }
        if (P1[s].Com <= 0)
            P[s + v].com = 0;
        // prosmotr+=P[s].com;
        s++;
    }
    if (!P1[s].prz & 1)
        (int&)P[size].priz = (P[size].priz + 1); // показ посл. точки
    if (TransForm->DataLength.lrud)
        LRangle(); // если есть lrud
    // -------------------------- Ветки V[s]. --------------------------------
    s = 1;
    while (s <= TransForm->DataLength.vet) {
        V[s].B = V1[s].metBeg;
        V[s].E = V1[s].metEnd;
        s++;
    }
    V[PP[1] + 1].B = 0;
    V[PP[1] + 1].E = 0;
    // ------------------------ съемки Surv[s]. ------------------------------
    s = 1;
    Surveys = L"";
    while (s <= TransForm->DataLength.srv) {
        Surv[s].File = (int)Survey1[s].file;
        Surveys += Survey1[s].title;
        Surveys = Surveys + L"  "; // временно
        Surveys += Survey1[s].team;
        Surveys += L"  ";
        Surveys += Survey1[s].date;
        Surveys += L"\n";
        s++;
    }
    // --------------------------- стены W[s] ---------------------------------
    // X,Y,Z,Station,Piket,color,Attr(lrud,s),Srv,line
    // порядок нумерации - по StationList (сортировка)
    // lrud,s - дописываются к Wall1[.]

    unsigned int r = 0;
    for (unsigned int t = 0; t < StationList->Count; ++t) // ----------- обрисовка (массив W[])
    {
        for (unsigned int s = 0; s < PP[7]; ++s) // -----------  ежик к met=t
        {
            if (Wall1[s].met == t) {
                W[r].type = L'w'; // (ежик w,lrud,s)
                W[r].col = Wall1[s].col; // цвет
                W[r].srv = Wall1[s].Srv; // номер съемки
                W[r].com = Wall1[s].com; // номер комментария
                W[r].line = Wall1[s].line; // номер строки в файле данных
                W[r].ignoreAt3d = Wall1[s].ignoreAt3d;
                W[r].met = Wall1[s].met;
                if (W[r].met > 0) // если стена "пришита" к нитке, находим W[s].pik
                    for (unsigned int s1 = 0; s1 <= size; ++s1) {
                        if (W[r].met == P[s1].met) {
                            W[r].pik = s1;
                            break;
                        }
                    } // первый пикет с данной меткой
                W[r].pos.x = Wall1[s].L * 100 + P[W[r].pik].pos.x;
                W[r].pos.y = Wall1[s].An * 100 + P[W[r].pik].pos.y;
                W[r].pos.z = Wall1[s].Az * 100 + P[W[r].pik].pos.z;
                r++;
            }
        }
        unsigned int s = 0;
        while (s < size) // -----------  lrud к met=t
        {
            if ((P[s].met == t)) // P[s].met=0 - пикеты без метки (режим #label)
            {
                std::wstring wType;
                std::vector<V3>w;
                int lidx = 0;
				int uidx = 0;
				int ridx = 0;
				int bidx = 0;
                lidx = uidx = ridx = bidx = -1;

                if (P[s].left > 0) {
                    lidx = wType.size();
                    wType.push_back(L'l');
                    w.push_back(V3(P[s].pos.x - P[s].left*P[s].cos_c, P[s].pos.y - P[s].left*P[s].sin_c, P[s].pos.z));
                }
                if (P[s].up > 0) {
                    uidx = wType.size();
                    wType.push_back(L'u');
                    w.push_back(V3(P[s].pos.x, P[s].pos.y, P[s].pos.z + P[s].up));
                }
                if (P[s].right > 0) {
                    ridx = wType.size();
                    wType.push_back(L'r');
                    w.push_back(V3(P[s].pos.x + P[s].right*P[s].cos_c, P[s].pos.y + P[s].right*P[s].sin_c, P[s].pos.z));
                }
                if (P[s].down > 0) {
                    bidx = wType.size();
                    wType.push_back('r');
                    w.push_back(V3(P[s].pos.x, P[s].pos.y, P[s].pos.z - P[s].down));
                }

                // if (uidx != -1 && ridx != -1) {
                // wType.push_back('L');
                // w.push_back(P[s].pos + ((w[uidx] + w[ridx])/2 - P[s].pos) * 1.5);
                // }
                //
                // if (ridx != -1 && bidx != -1) {
                // wType.push_back('R');
                // w.push_back(P[s].pos + ((w[ridx] + w[bidx])/2 - P[s].pos) * 1.5);
                // }
                //
                // if (bidx != -1 && lidx != -1) {
                // wType.push_back('R');
                // w.push_back(P[s].pos + ((w[bidx] + w[lidx])/2 - P[s].pos) * 1.5);
                // }
                //
                // if (lidx != -1 && uidx != -1) {
                // wType.push_back('R');
                // w.push_back(P[s].pos + ((w[lidx] + w[uidx])/2 - P[s].pos) * 1.5);
                // }

                for (int i = 0; i < wType.size(); i++) {
                    W[r].col = P[s].col; // цвет
                    W[r].com = 0; // номер комментария
                    W[r].srv = P[s].srv; // номер съемки
                    W[r].line = P[s].line; // номер строки в файле данных
                    W[r].met = P[s].met;
                    W[r].pik = s; // пикет, к которому относится lrud
                    W[r].type = wType[i]; // (ежик w,lrud,s)
                    W[r].pos = w[i];
                    r++;
                }

            }
            s++;
            // !здесь для объемной картинки надо добавлять сам пикет, если он вне многоугольника обрисовки
        }
        Wsize = r;
        MainForm1->WRazmer = Wsize; // для внешнего использования
    }

    // CaveViewPrefs prefs;
    ////  prefs.wallsPropagateMode = WPM_10D;
    // prefs.wallsShadowMode = WSM_SMOOTH;
    // UnicodeString(StationList->Strings[metBeg]).c_str());
    ReinitCaveModel();
}

void ReinitCaveModel() {
	std::vector<std::string>piketNames;
    piketNames.reserve(StationList->Count);
    for (int i = 0; i < StationList->Count; i++) {
		piketNames.push_back(UTF8Encode(StationList->Strings[i]).c_str());
    }

    MainForm1->prefs.surfColorId = MainForm1->sur_col;
    MainForm1->prefs.dupColorId = MainForm1->dup_col;
    bool converToExtendedElevation = (MainForm1->State == STATE_VIEW_EXT_PROFILE);
    if (MainForm1->ogreRenderer) {
        MainForm1->ogreRenderer->setCaveData(P, size, W, Wsize, Equates, piketNames, MainForm1->prefs, converToExtendedElevation);
        MainForm1->ogreRenderer->refresh();
    }
}

// ****************************** Resize **************************************
// --------------------------- Resize Main Form -------------------------------
void __fastcall TMainForm1::FormResize(TObject *Sender) {
    ResizeBitmap(pBitmap);
    if ((State == STATE_VIEW_NORMAL) || (State == STATE_VIEW_EXT_PROFILE)) {
        projection(pBitmap);
        kart(pBitmap);
    }
    // #else MainForm1->Image1->Canvas->(0,0,Bitmap); это я убрал, потому что мы и так рисуем в Битмапу Имиджа
}

// --------------------------- Resize Control Bar -----------------------------
void __fastcall TMainForm1::ControlBar1Resize(TObject *Sender) {
    MainForm1->Image1->Top = MainForm1->ControlBar1->Height;
    ResizeBitmap(pBitmap);
    if ((State == STATE_VIEW_NORMAL) || (State == STATE_VIEW_EXT_PROFILE)) {
        projection(pBitmap);
        kart(pBitmap);
    }
    // else MainForm1->Image1->Canvas->Draw(0,0,Bitmap);   это я убрал, потому что мы и так рисуем в Битмапу Имиджа
}

// ****************************** Set State ************************************
// ------------------------ Enable/Disable View Tools --------------------------
void TMainForm1::EnableView(char ev) {
    if (ev == 0) {
        MainForm1->RotBar->Visible = false;
        MainForm1->RotationControls1->Enabled = false;
        MainForm1->ShiftBar->Visible = false;
        MainForm1->ShiftControls1->Enabled = false;
        MainForm1->ZoomBar->Visible = false;
        MainForm1->SurfaceTool1->Enabled = false;
        MainForm1->SurfBar->Visible = false;
        MainForm1->InfoTools1->Enabled = false;
        MainForm1->Compass1->Enabled = false;
        MainForm1->WallsBar->Visible = false;
        MainForm1->ToolBar2->Visible = false;
        MainForm1->ToolBar3->Visible = false;
    }
    if (ev == 1) {
        MainForm1->RotBar->Visible = true;
        MainForm1->RotationControls1->Enabled = true;
        MainForm1->ShiftBar->Visible = true;
        MainForm1->ShiftControls1->Enabled = true;
        MainForm1->ZoomBar->Visible = true;
        MainForm1->InfoTools1->Enabled = true;
        MainForm1->Compass1->Enabled = true;
        if (Srf.flag)
            MainForm1->SurfBar->Visible = true;
        MainForm1->ToolButton19->Down = false;
        if (Srf.flag)
            MainForm1->SurfaceTool1->Enabled = true;
        MainForm1->ToolButton20->Down = false;
        MainForm1->WallsBar->Visible = true;
        updateOgreCheckers();
        // MainForm1->ToolBar2->Visible=true;
        // MainForm1->ToolBar3->Visible=true;
        // MainForm1->ToolButton23->Down=false;
    }
    if (ev == -2) {
        Stop->Enabled = false;
        StepRight->Enabled = false;
        StepLeft->Enabled = false;
        PlayRight->Enabled = false;
        PlayLeft->Enabled = false;
        ToolButton3->Enabled = false;
        ToolButton9->Enabled = false;
        ToolButton13->Enabled = false;
        ToolButton15->Enabled = false;
    }
    if (/*true) {/*/ev == 2) {
        Stop->Enabled = true;
        StepRight->Enabled = true;
        StepLeft->Enabled = true;
        PlayRight->Enabled = true;
        PlayLeft->Enabled = true;
        ToolButton3->Enabled = true;
        ToolButton9->Enabled = true;
        ToolButton13->Enabled = true;
        ToolButton15->Enabled = true;
    }
}

// ---------------------------- Set State -------------------------------------
void TMainForm1::SetState(StateEnum ST) {
    // 1-Opened, 2-EditNew, 3-view&edit, 4 - built, 5- view, 6 - ext. profile
    switch (ST) {
    case STATE_NONE:
        rot_flag = 0;
        State = STATE_NONE;
        break;
    case STATE_OPENED:
        EnableView(0);
        rot_flag = 0;
        MainForm1->Edit1->Enabled = true;
        MainForm1->ToolButt2->Enabled = true;
        MainForm1->Build->Enabled = true;
        MainForm1->ToolButt3->Enabled = true;
        MainForm1->View1->Enabled = true;
        MainForm1->ToolButt4->Enabled = true;
        MainForm1->Print1->Enabled = false;
        MainForm1->ToolButt5->Enabled = false;
        MainForm1->N2Dpsbmp1->Enabled = false;
        MainForm1->N3Dvrml1->Enabled = false; 
        MainForm1->N6->Enabled = false;
        MainForm1->N7->Enabled = false;
        ClearBitmap(pBitmap, 1);
        // MainForm1->Image1->Canvas->Draw(0,0,Bitmap); это я убрал, потому что мы и так рисуем в Битмапу Имиджа
        EditForm->ToolButton13->Enabled = true;
        State = STATE_OPENED;
        break;
    case STATE_EDIT_NEW:
        EditForm->ToolButton13->Enabled = false;
        State = STATE_EDIT_NEW;
        break;
    case STATE_VIEW_EDIT:
        State = STATE_VIEW_EDIT;
        break;
    case STATE_BUILD:
        MainForm1->Print1->Enabled = true;
        MainForm1->ToolButt5->Enabled = true;
        MainForm1->N2Dpsbmp1->Enabled = true;
        MainForm1->N3Dvrml1->Enabled = true; 
        MainForm1->N6->Enabled = true;
        MainForm1->N7->Enabled = true;
        phi = 0;
        theta = 0;
        State = STATE_BUILD;
        break;
    case STATE_VIEW_NORMAL:
        EnableView(2);
        EnableView(1);
        // MainForm1->View1->Enabled=false; MainForm1->ToolButt4->Enabled=false;
        MainForm1->KeyPreview = true;
        State = STATE_VIEW_NORMAL;
        break;
	case STATE_VIEW_EXT_PROFILE:
		EnableView(2);
        EnableView(1);
        MainForm1->KeyPreview = true;
		theta = 0;//M_PI_2;
		Ct = 1;
        State = STATE_VIEW_EXT_PROFILE;
        break;
    default:
        break;
    };
}

// ****************************-Menu-****************************************/
// --------------------Open-Dialog-(Load-file)---------------------------------

void __fastcall TMainForm1::Openfile1Click(TObject *Sender) {
    if (CurrFileName != L"")
        OpenDialog1->InitialDir = CurrFileName.SubString(1, CurrFileName.LastDelimiter(L"\\"));

    if (MainForm1->OpenDialog1->Execute()) {
        OpenCurrFile(OpenDialog1->FileName);
    }
    // cave_filename=OpenDialog1->FileName;
}

// ------------------------------- New ----------------------------------------
void __fastcall TMainForm1::New1Click(TObject *Sender) {
    if (EditForm->Visible) {
        if (EditForm->Confirm() == 2) {
            EditForm->Show();
            return;
        }
    }

    EditForm->Show();
    EditForm->SetFileName(L"new file");
    EditForm->RichEdit1->Text = L"";

    if (State == STATE_NONE)
        SetState(STATE_EDIT_NEW);
}

// ------------------------------- Edit ---------------------------------------
void __fastcall TMainForm1::Edit1Click(TObject *Sender) {
    if (EditForm->Visible) {
        if ((EditForm->edit_flag == 0) || (EditForm->FFileName == CurrFileName) || (EditForm->Confirm() == 2)) {
            EditForm->Show();
            EditForm->edit_flag = 0;
            return;
        }
    }
	EditForm->edit_flag = 0;
	LoadFromFileUTF8Test(EditForm->RichEdit1->Lines, CurrFileName);
    EditForm->SetFileName(CurrFileName);
    EditForm->OpenDialog1->FileName = CurrFileName;
    EditForm->Show();
}

// --------------------------(Re)Build из меню---------------------------------
void __fastcall TMainForm1::BuildClick(TObject *Sender) {
    SetState(STATE_OPENED);
    // if( Build1() ) SetState(4);
    if (Build1() == 0)
        SetState(STATE_NONE); // ----- эта и след. функции - пока примерно :)
    else // -----
    {
        SetState(STATE_BUILD);
        SetState(STATE_VIEW_NORMAL); // !
        init_par(); // !
        // MainForm1->Hide();
        projection(pBitmap);
        kart(pBitmap); // !
        // MainForm1->SetFocus();
        MainForm1->Hide();
        MainForm1->Show();
    }
}

// ------------------------------- View ---------------------------------------
void __fastcall TMainForm1::View1Click(TObject *Sender) {
    if (State != STATE_NONE) {
        if (State == STATE_OPENED && Build1() == 0)
            return;
        SetState(STATE_BUILD);
        SetState(STATE_VIEW_NORMAL);
        init_par();
        projection(pBitmap);
        kart(pBitmap);
    }
    // ViewForm->Show(); ViewForm->Update();
}

// --------------------------------Print---------------------------------------
void __fastcall TMainForm1::Print1Click(TObject *Sender) {
    HelpForm->Close();
    PrnForm->SetParam(size, theta, phi, State);
    PrnForm->ShowModal();
}

// ---------------------------------Exit---------------------------------------
void __fastcall TMainForm1::Exit1Click(TObject *Sender) {
    MainForm1->Close();
}

// ----------------------------------------------------------------------------
void __fastcall TMainForm1::FormCloseQuery(TObject *Sender, bool &CanClose) {
    if (EditForm->Confirm() == 2)
        CanClose = false; // Если исх. файл не сохранен
    else {
        if (P != NULL) {
            SetState(STATE_NONE);
            delete[]P;
            delete[]Prx;
            delete[]Pry;
            delete[]V;
            delete[]Surv;
        }
        if (W != NULL) {
            delete[]W;
        } // точки стен
        if (Wpr != NULL) {
            delete[]Wpr;
        } // проекции, ежики
        if (SPrx != NULL) {
            delete[]SPrx;
            delete[]SPry;
        }
        if (FPrx != NULL) {
            delete[]FPrx;
            delete[]FPry;
        }
        if (V1 != NULL)
            delete[]V1;
        if (StationList != NULL)
            delete StationList;

        if (Srf.Z != NULL)
            delete[]Srf.Z;
        if (Srf.X != NULL)
            delete[]Srf.X;
        if (Srf.Y != NULL)
            delete[]Srf.Y;
        if (FixPnt != NULL)
            delete[]FixPnt;
        if (Survey1 != NULL)
            delete[]Survey1;
        if (P1 != NULL)
            delete[]P1;
        if (Wall1 != NULL)
            delete[]Wall1;
    }
}

// =============================Functions=====================================

// ------------------------------- Name[N] -----------------------------------
UnicodeString TMainForm1::Name(UnicodeString Astr, unsigned int N) {
    int i = 1;
    unsigned int maxN = 0;
    while (i <= Astr.Length()) {
        if (Astr[i] == L'\n')
            maxN++;
        i++;
    }
    if (N > maxN)
        return L"";
    if (N == 0)
        return L"^"; // защита от вылета
    unsigned int s = 0, k = 1;
    while (s < N - 1) {
        if (Astr[k] == L'\n')
            s = s + 1;
        ++k;
    }
    s = k;
    k = 0;
    while (1 == 1) {
        ++k;
        if (Astr[s + k] == L'\n')
            break;
    }
    return Astr.SubString(s, k);
}

// ********************* Определение естественного масштаба ******************
void minmax() {
    long XN;
    if (!Srf.flag) {
        Xmax = Xmin = P[0].pos.x;
        Ymax = Ymin = P[0].pos.y;
        Zmax = Zmin = P[0].pos.z;
        XYmax = XYmin = P[0].XY;
        for (unsigned int s = 0; s < size; ++s) {
            XN = P[s].pos.z;
            if (XN > Zmax)
                Zmax = XN;
            if (XN < Zmin)
                Zmin = XN;
            XN = P[s].pos.y;
            if (XN > Ymax)
                Ymax = XN;
            if (XN < Ymin)
                Ymin = XN;
            XN = P[s].pos.x;
            if (XN > Xmax)
                Xmax = XN;
            if (XN < Xmin)
                Xmin = XN;
            XN = P[s].XY;
            if (XN > XYmax)
                XYmax = XN;
            if (XN < XYmin)
                XYmin = XN;
        }
    }
    if (Srf.flag) // если есть поверхность - габариты опред. по ней
    {
        Zmin = Srf.Z[0];
        Zmax = Srf.Z[0];
        Ymin = Srf.X[0];
        Ymax = Srf.X[0];
        Xmin = Srf.Y[0];
        Xmax = Srf.Y[0];
        for (int i = 0; i < Srf.ncols * Srf.nrows; ++i) {
            if (Srf.X[i] > Ymax)
                Ymax = Srf.X[i];
            if (Srf.X[i] < Ymin)
                Ymin = Srf.X[i];
            if (Srf.Y[i] > Xmax)
                Xmax = Srf.Y[i];
            if (Srf.Y[i] < Xmin)
                Xmin = Srf.Y[i];
            if (Srf.Z[i] > Zmax)
                Zmax = Srf.Z[i];
            if (Srf.Z[i] < Zmin)
                Zmin = Srf.Z[i];
        }
        Xmax = Xmax * 100;
        Ymax = Ymax * 100;
        Zmax = Zmax * 100;
    }

    PCub[0].X = Xmin;
    PCub[0].Y = Ymin;
    PCub[0].Z = Zmax; // bounding box
    PCub[1].X = Xmax;
    PCub[1].Y = Ymin;
    PCub[1].Z = Zmax;
    PCub[2].X = Xmax;
    PCub[2].Y = Ymax;
    PCub[2].Z = Zmax;
    PCub[3].X = Xmin;
    PCub[3].Y = Ymax;
    PCub[3].Z = Zmax;
    PCub[4].X = Xmin;
    PCub[4].Y = Ymin;
    PCub[4].Z = Zmin;
    PCub[5].X = Xmax;
    PCub[5].Y = Ymin;
    PCub[5].Z = Zmin;
    PCub[6].X = Xmax;
    PCub[6].Y = Ymax;
    PCub[6].Z = Zmin;
    PCub[7].X = Xmin;
    PCub[7].Y = Ymax;
    PCub[7].Z = Zmin;
}

// -------------------------------------------------------------------------
void TMainForm1::init_par() {
    Ymed = (Ymax + Ymin) / 2;
    Xmed = (Xmax + Xmin) / 2;
    Zmed = (Zmax + Zmin) / 2;
    XYmed = (XYmax + XYmin) / 2; // опред. центра
    int xy = xx - 10;
    if (yy < xx)
        xy = yy - 10;
    long xyzMax = (Xmax - Xmin);
    if ((Ymax - Ymin) > (Xmax - Xmin))
        xyzMax = (Ymax - Ymin);
    if (State == STATE_VIEW_EXT_PROFILE)
        xyzMax = (XYmax - XYmin);
    if ((Zmax - Zmin) > xyzMax)
        xyzMax = (Zmax - Zmin); // определение шага
    H = (xyzMax / (float)xy); // (в см/pixel)
}
// ************************ Массив проекций *********************************

void TMainForm1::projection(Graphics::TBitmap *pBitmap) {
    // double  Pi = 3.14159265358979;
    if (State != 6) {
        C = cos(phi);
        S = sin(phi);
        Ct = cos(theta);
        St = sin(theta);
        for (unsigned int s = 0; s < size; ++s) // ----------- пещера
        {
            Prx[s] = (int)(-((P[s].pos.x - Xmed) * S - (P[s].pos.y - Ymed) * C) / H + xx / 2);
            Pry[s] = (int)(-(((P[s].pos.x - Xmed) * C + (P[s].pos.y - Ymed) * S) * St + (P[s].pos.z - Zmed) * Ct) / H + yy / 2);
        }
        if ((MainForm1->ToolButton31->Down == true) && (Wsize)) // walls - ежик и lrud
        {
            for (unsigned int s = 0; s < Wsize; ++s) // ----------- обрисовка
            {
                Wpr[s].X = (int)(-((W[s].pos.x - Xmed) * S - (W[s].pos.y - Ymed) * C) / H + xx / 2);
                Wpr[s].Y = (int)(-(((W[s].pos.x - Xmed) * C + (W[s].pos.y - Ymed) * S) * St + (W[s].pos.z - Zmed) * Ct) / H + yy / 2);
            }
        }
        if (H != 0)
            for (unsigned int s = 0; s < 8; ++s) // ------------ ограничивающий кубик
            {
                PrCubx[s] = (int)(-((PCub[s].X - Xmed) * S - (PCub[s].Y - Ymed) * C) / H + xx / 2);
                PrCuby[s] = (int)(-(((PCub[s].X - Xmed) * C + (PCub[s].Y - Ymed) * S) * St + (PCub[s].Z - Zmed) * Ct) / H + yy / 2);
            }
        if (Srf.flag) // ------------- рельеф поверхности
        {
            double x = 0, y = 0;
            for (int i = 0; i < Srf.nrows * Srf.ncols; ++i) {
                x = Srf.Y[i] * 100 - Xmed; // x и y поменялись местами - форматы не сошлись
                y = Srf.X[i] * 100 - Ymed;
                SPrx[i] = (int)(-(x * S - y * C) / H + xx / 2);
                SPry[i] = (int)(-((x * C + y * S) * St + (Srf.Z[i] * 100 - Zmed) * Ct) / H + yy / 2);
            }
        }
        if (/*(Srf.flag) &&*/ (TransForm->DataLength.fxp)) // ----- фикс. GPS-точки (заданные координатами)
        {
            for (int i = 0; i < TransForm->DataLength.fxp; ++i) {
                FPrx[i] = (int)(-((FixPnt[i].localY * 100 - Xmed) * S - (FixPnt[i].localX * 100 - Ymed) * C) / H + xx / 2);
                FPry[i] = (int)(-(((FixPnt[i].localY * 100 - Xmed) * C + (FixPnt[i].localX * 100 - Ymed) * S) * St + (FixPnt[i].localZ * 100 - Zmed) * Ct) / H + yy / 2);
            }
        }
    }
    else // ----- ext. profile
    {
        Ct = 1;
        St = 0;
        for (unsigned int s = 0; s < size; ++s) {
            Prx[s] = (int)(-(P[s].XY - XYmed) / H + xx / 2);
            Pry[s] = (int)(-(P[s].pos.z - Zmed) / H + yy / 2);
        }
        if ((MainForm1->ToolButton31->Down == true) && (Wsize)) // walls - ежик
        {
            for (unsigned int s = 0; s < Wsize; ++s) {
                Wpr[s].X = (int)(-(P[W[s].pik].XY - XYmed) / H + xx / 2);
                Wpr[s].Y = (int)(-(W[s].pos.z - Zmed) / H + yy / 2);
            }
        }
    }
    // #ClearBitmap(pBitmap,0);
    MainForm1->LabelXYZ->Caption = L"";
    CaveMouseNo = -1;
}

// ---------------------------------- Arrow -----------------------------------
void Arrow(Graphics::TBitmap *&pBitmap, int Ax, int Ay, int Az, TColor Acolor) {
    int ALen = sqrt(sqr(Ax) + sqr(Ay) + sqr(Az));
    int Xs = -Ax * S + Ay * C;
    int Ys = (Ax * C + Ay * S) * St + Az * Ct;
    pBitmap->Canvas->MoveTo(xx - ALen - 1 - Xs, ALen + 1 + Ys);
    pBitmap->Canvas->Pen->Color = Acolor;
    pBitmap->Canvas->LineTo(xx - ALen - 1 + Xs, ALen + 1 - Ys);
}

// ************************ Картинка (Draw Bitmap) ****************************
void TMainForm1::kart(Graphics::TBitmap *pBitmap) {
    // MainForm1->Caption=IntToStr(MainForm1->Tag++);

    if (MainForm1->ogreRenderer) {
        char gridMode = -1;
        if (MainForm1->ToolButton19->Down)
            gridMode = 0;
        else if (MainForm1->ToolButton20->Down)
            gridMode = 1;
        if (gridMode >= 0) {
            double Af;
            int Hc = 0;
            calcGridStep(Af, Hc, gridMode);
            ogreRenderer->setGridMode(Hc);
        }
        else {
            ogreRenderer->setGridMode(0);
        }

        if (State != 6) { // не разрез-развертка
            ogreRenderer->setRotation(phi, theta);
            ogreRenderer->setScale(H);
            ogreRenderer->setCaveCenter(Xmed, Ymed, Zmed);
        }
        else {
            ogreRenderer->setRotation(M_PI / 2, theta);//-M_PI / 2 + M_PI / 8);
            ogreRenderer->setScale(H);
            ogreRenderer->setCaveCenter(XYmed, 0, Zmed);
        }
        if (MainForm1->ogreRenderer->isEnabled())
            ogreRenderer->refresh();
    }
    ClearBitmap(pBitmap, 0); // #-- это я добавил
    if (MainForm1->ToolButton14->Down)
        Stations(pBitmap);
    if (MainForm1->ToolButton19->Down)
        scale(pBitmap, 0);
    if (MainForm1->ToolButton20->Down)
        scale(pBitmap, 1);

    // ---------- draw lines & circles -------------------------------------------
    for (unsigned int s = 0; s < size; ++s) {
		pBitmap->Canvas->Pen->Color = SetTColor(P[s].col);
        pBitmap->Canvas->Brush->Color = BgColor;

        if ((P[s].priz & PRIZ_SURFACE))
            pBitmap->Canvas->Pen->Color = SetTColor(sur_col);

        if ((P[s].priz & PRIZ_DUPLICATE))
            pBitmap->Canvas->Pen->Color = SetTColor(dup_col);

        if (s && ToolButton48->Down && TrackForm && TrackForm->isInPath(&P[s-1], &P[s])) {   
            pBitmap->Canvas->Pen->Color = clWhite;
        }
            
        if (s && // чтобы не было выхода за пределы
            !(P[s].priz & PRIZ_BRANCH)) // если нет т. ветвления
        {
            pBitmap->Canvas->MoveTo(Prx[s - 1], Pry[s - 1]);
            pBitmap->Canvas->LineTo(Prx[s], Pry[s]);
        }
        if (P[s].priz & PRIZ_ENTRY) { // LIGHTGREEN   - вход
            pBitmap->Canvas->Pen->Color = clLime;
            Circle(pBitmap, Prx[s], Pry[s], 2);
        };
        if (P[s].priz & PRIZ_SIPHON) { // CYAN  - сифон
            pBitmap->Canvas->Pen->Color = clAqua;
            Circle(pBitmap, Prx[s], Pry[s], 2);
        };
        if (P[s].priz & PRIZ_BEGIN) { // LIGHTRED  - красный кружок
            pBitmap->Canvas->Pen->Color = (TColor)0x000000FF;
            Circle(pBitmap, Prx[s], Pry[s], 3);
        };
        if (ToolButton48->Down && TrackForm && TrackForm->isJoint(&P[s])) {   
            pBitmap->Canvas->Pen->Color = clWhite;
            Circle(pBitmap, Prx[s], Pry[s], 3);
        }

    }
    // ------------------- обрисовка (ежики и lrud стен) ---------------------------
    pBitmap->Canvas->Pen->Color = clDkGray;
    if ((MainForm1->ToolButton31->Down == true) && (Wsize)) {
        pBitmap->Canvas->Pen->Color = clDkGray;
        for (unsigned int s = 0; s < Wsize; ++s) {
            if (W[s].met >= 0) // нулевая метка - пикеты без меток (режим #label)
            {
                if (FlagWallsColor)
                    pBitmap->Canvas->Pen->Color = SetTColor(P[W[s].pik].col); // цвет по нитке
                pBitmap->Canvas->MoveTo(Prx[W[s].pik], Pry[W[s].pik]);
                pBitmap->Canvas->LineTo(Wpr[s].X, Wpr[s].Y);
            }
        }
    }
    // рельеф поверхности
    if ((Srf.flag) && ((MainForm1->ToolButton27->Down == true) || (MainForm1->ToolButton28->Down == true))) {
        int i_ = 0;
        int ii = 0;
        int poi = 0;
        pBitmap->Canvas->Pen->Color = SetTColor(sur_col);
        ii = (int)(H * 10 / Srf.xcellsize / 100) + 1; // 10 - min число pix между линиями
        // если меньше - линии реже
        if (MainForm1->ToolButton28->Down == true)
            ii = ii * 2; // редкая сетка
        poi = (int)(Srf.xcellsize * 100 / H) + 1; // число точек на ячейку
        for (int i = 0; i <= Srf.nrows / ii; ++i) {
            i_ = i * Srf.ncols;
            for (int k = 1; k < Srf.ncols / ii; ++k) {
                if ((i_ * ii + (k - 1) * ii < Srf.ncols * Srf.nrows) && (abs((double)SPrx[i_ * ii + (k - 1) * ii] - xx / 2) < (xx / 2 + poi)) &&
                    (abs((double)SPry[i_ * ii + (k - 1) * ii] - yy / 2) < (yy / 2 + poi))) { // только не вылезающие за экран
                    pBitmap->Canvas->MoveTo(SPrx[i_*ii + (k - 1)*ii], SPry[i_*ii + (k - 1)*ii]);
                    pBitmap->Canvas->LineTo(SPrx[i_*ii + k*ii], SPry[i_*ii + k*ii]);
                }
            }
        }
        for (int i = 0; i < (Srf.nrows - 1) / ii; ++i) {
            i_ = i * Srf.ncols;
            for (int k = 1; k <= Srf.ncols / ii; ++k) {
                if ((abs((double)SPrx[i_ * ii + (k - 1) * ii] - xx / 2) < (xx / 2 + poi)) && (abs((double)SPry[i_ * ii + (k - 1) * ii] - yy / 2) < (yy / 2 + poi)))
                { // только не вылезающие за экран
                    pBitmap->Canvas->MoveTo(SPrx[i_*ii + (k - 1)*ii], SPry[i_*ii + (k - 1)*ii]);
                    pBitmap->Canvas->LineTo(SPrx[i_*ii + (k - 1 + Srf.ncols)*ii], SPry[i_*ii + (k - 1 + Srf.ncols)*ii]);
                }
            }
        }
    }
//    if (Srf.flag) {
        if (TransForm->DataLength.fxp) // фикс. точки
        {
            for (int i = 0; i < TransForm->DataLength.fxp; ++i) {
                pBitmap->Canvas->Pen->Color = (TColor)0x0020C0C0; // khaki
                if (FixPnt[i].prz&1)
                    pBitmap->Canvas->Pen->Color = clLime; // ent - LIGHTGREEN
                if (FixPnt[i].prz&2)
                    pBitmap->Canvas->Pen->Color = clCream; // way - Cream
                if (FixPnt[i].prz&4)
                    pBitmap->Canvas->Pen->Color = clAqua; // sif - Aqua
                Circle(pBitmap, FPrx[i], FPry[i], 2);
                if (FixPnt[i].prz & 8) {
                    pBitmap->Canvas->Pen->Color = clRed;
                    Circle(pBitmap, FPrx[i], FPry[i], 3);
                }
            }
        }
//    }
    if ((State != 6) && (MainForm1->ToolButton26->Down == true)) // ограничивающий кубик
    {
        pBitmap->Canvas->Pen->Color = (TColor)0x000000AA;
        pBitmap->Canvas->MoveTo(PrCubx[0], PrCuby[0]);
        pBitmap->Canvas->LineTo(PrCubx[1], PrCuby[1]);
        pBitmap->Canvas->LineTo(PrCubx[2], PrCuby[2]);
        pBitmap->Canvas->LineTo(PrCubx[3], PrCuby[3]);
        pBitmap->Canvas->LineTo(PrCubx[0], PrCuby[0]);
        pBitmap->Canvas->LineTo(PrCubx[4], PrCuby[4]);
        pBitmap->Canvas->LineTo(PrCubx[5], PrCuby[5]);
        pBitmap->Canvas->LineTo(PrCubx[6], PrCuby[6]);
        pBitmap->Canvas->LineTo(PrCubx[7], PrCuby[7]);
        pBitmap->Canvas->LineTo(PrCubx[4], PrCuby[4]);
        pBitmap->Canvas->MoveTo(PrCubx[5], PrCuby[5]);
        pBitmap->Canvas->LineTo(PrCubx[1], PrCuby[1]);
        pBitmap->Canvas->MoveTo(PrCubx[6], PrCuby[6]);
        pBitmap->Canvas->LineTo(PrCubx[2], PrCuby[2]);
        pBitmap->Canvas->MoveTo(PrCubx[7], PrCuby[7]);
        pBitmap->Canvas->LineTo(PrCubx[3], PrCuby[3]);
    }

    // ---------- compass or arrow
    if (State != 6) {
        if (MainForm1->Compass1->Checked & MainForm1->Compass1->Enabled) // compass
        {
            int ALen = 40;
            Arrow(pBitmap, 0, 0, ALen, clOlive); // верх
            Arrow(pBitmap, 0, ALen, 0, clBlue); // запад
            Arrow(pBitmap, ALen, 0, 0, clBlue); // север
            pBitmap->Canvas->Brush->Color = clBlue; // точка вверху
            pBitmap->Canvas->Pen->Color = clBlue;
            pBitmap->Canvas->Ellipse(xx - ALen - 1 - 9, ALen + 1 - 9*St, xx - ALen - 1 + 9, ALen + 1 + 9*St);
            pBitmap->Canvas->Brush->Color = (TColor)0x0000FF00;
            pBitmap->Canvas->Ellipse(xx - ALen - 3, ALen + 1 - ALen*Ct - 1, xx - ALen + 1 + 1, ALen + 1 - ALen*Ct + 2);
            pBitmap->Canvas->Brush->Color = TColor(0x0000C000 - 0x00000800 * ((int)(0x8 * C)));
            pBitmap->Canvas->Pen->Color = TColor(0x00C0C000 - 0x00000800 * ((int)(0x8 * C)));
            Circle(pBitmap, xx - ALen - 1 - ALen*S, ALen + 2 - ALen*C*St, 2); // точка на север

            if (theta > 0)
                pBitmap->Canvas->MoveTo(xx - ALen - 1, ALen + 2 - ALen*Ct);
            else
                pBitmap->Canvas->MoveTo(xx - ALen - 1, ALen + 1 + ALen*Ct);

            pBitmap->Canvas->Pen->Color = clOlive;
            pBitmap->Canvas->LineTo(xx - ALen - 1, ALen + 1);
            pBitmap->Canvas->Brush->Color = BgColor;
        }
        else // arrow (на север)
        {
            int Xs = 0;
			int Ys = 0;
			int Xs1 = 0;
			int Ys1 = 0;
			int Xs2 = 0;
			int Ys2 = 0;
            Xs = (xx - 41 - 40 * S);
            Ys = (41 - 40 * C);
            Xs1 = (xx - 41 - 34 * sin(phi - 0.1));
            Ys1 = (41 - 34 * cos(phi - 0.1));
            Xs2 = (xx - 41 - 34 * sin(phi + 0.1));
            Ys2 = (41 - 34 * cos(phi + 0.1));

            pBitmap->Canvas->MoveTo(xx - 41, 41);
            pBitmap->Canvas->Pen->Color = clAqua; // CYAN;
            pBitmap->Canvas->LineTo(Xs, Ys);
            pBitmap->Canvas->LineTo(Xs1, Ys1);
            pBitmap->Canvas->MoveTo(Xs, Ys);
            pBitmap->Canvas->LineTo(Xs2, Ys2);
        }
    }

    // ---------- show angles
    pBitmap->Canvas->Font->Color = clLime; // Font color
    wchar_t angle[20];
    if (State == STATE_VIEW_EXT_PROFILE) {
        wcscpy(angle, L"extended profile");
        pBitmap->Canvas->TextOut(8, 1, angle);
    }
    else {
        if (fabs(phi * 57.2958 - (int)(phi * 57.2958)) < 0.1) // если угол близок к целому
                swprintf(angle, L"View towards% 5.0f", (phi*57.2958 + 0.01));
        else
            swprintf(angle, L"View towards% 6.1f", (phi*57.2958 + 0.01));
        pBitmap->Canvas->TextOut(8, 1, angle);
        if (fabs(theta * 57.2958 - (int)(theta * 57.2958)) < 0.1) // если угол близок к целому
                swprintf(angle, L"View point  % 5.0f", (theta*57.2958 + 0.001));
        else
            swprintf(angle, L"View point  % 6.1f", (theta*57.2958 + 0.001));
        pBitmap->Canvas->TextOut(8, 11*pBitmap->Canvas->Font->PixelsPerInch / 72, angle);
    }
    pBitmap->Canvas->Pen->Color = BgColor;

    // #MainForm1->Image1->Canvas->Draw(0,0,Bitmap);
    // это я убрал, так как мы уже непосредственно рисуем на Битмапу Имиджа

    if (MainForm1->ToolButton7->Down == true)
        ShowBifLabels(pBitmap);
    if (MainForm1->ToolButton12->Down == true)
        ShowLabels(pBitmap);
    if (MainForm1->ToolButton24->Down == true)
        ShowComments(pBitmap);
    if (MainForm1->ToolButton25->Down == true)
        FindStations(pBitmap);
    if (MainForm1->ToolButton29->Down == true)
        ShowFixPoints(pBitmap);
    if (MainForm1->ToolButton30->Down == true)
        ShowFixPoints_com(pBitmap);
}
// --------------------------- Color conversion ------------------------------

TColor SetTColor(int oldColor) {
    switch (oldColor) {
    case 0:
        return clBlack;
    case 1:
        return clBlue;
    case 2:
        return clGreen;
    case 3:
        return clAqua;
    case 4:
        return clRed;
    case 5:
        return clPurple;
    case 6:
        return (TColor)0x008080F0; // soft red
    case 7:
        return clLtGray;
    case 8:
        return clDkGray;
    case 9:
        return (TColor)0x00FFAA00; // soft blue
    case 10:
        return clLime;
    case 11:
        return (TColor)0x0020C0C0; // khaki
    case 12:
        return (TColor)0x000000FF;
    case 13:
        return clFuchsia;
    case 14:
        return clYellow;
    case 15:
        return clWhite;
    case 16:
        return (TColor)0x002AAFF0; // оранжевый
        // clCream  clGray  clMaroon clSkyBlue clMoneyGreen (TColor)0x00FF7070;
    default:
        return clLime;
    }
}
// ---------------------------------------------------------------------------

// ************************** Click Events (HELP) ****************************

void __fastcall TMainForm1::GeneralOverview1Click(TObject *Sender) {
    HelpForm->SetHelpNum(1);
    HelpForm->Show();
}

void __fastcall TMainForm1::Data1Click(TObject *Sender) {
    HelpForm->SetHelpNum(2);
    HelpForm->Show();
}

void __fastcall TMainForm1::CommandsClick(TObject *Sender) {
    HelpForm->SetHelpNum(3);
    HelpForm->Show();
}

void __fastcall TMainForm1::Corrections1Click(TObject *Sender) {
    HelpForm->SetHelpNum(4);
    HelpForm->Show();
}

void __fastcall TMainForm1::HelpViewClick(TObject *Sender) {
    HelpForm->SetHelpNum(5);
    HelpForm->Show();
}

void __fastcall TMainForm1::Print2Click(TObject *Sender) {
    HelpForm->SetHelpNum(6);
    HelpForm->Show();
}

void __fastcall TMainForm1::Loops1Click(TObject *Sender) {
    HelpForm->SetHelpNum(7);
    HelpForm->Show();
}

void __fastcall TMainForm1::ProfileClick(TObject *Sender) {
    HelpForm->SetHelpNum(8);
    HelpForm->Show();
}

void __fastcall TMainForm1::SurfGeometry1Click(TObject *Sender) {
    HelpForm->SetHelpNum(9);
    HelpForm->Show();
}

void __fastcall TMainForm1::Walls1Click(TObject *Sender) {
    HelpForm->SetHelpNum(10);
    HelpForm->Show();
}
// ---------------------------------------------------------------------------

// ---------------------------------------------------------------------------

// ---------------------------------------------------------------------------

// ---------------------------------------------------------------------------

void __fastcall TMainForm1::AboutProgram1Click(TObject *Sender) {
    AboutBox->Show();
}

// ************************* Click Events (TOOLS) ****************************

void __fastcall TMainForm1::Viewtools1Click(TObject *Sender) {
    if (ToolBar1->Visible == false)
        MainForm1->BasicTool1->Checked = false;
    else
        MainForm1->BasicTool1->Checked = true;
    if (RotBar->Visible == false)
        MainForm1->RotationControls1->Checked = false;
    else
        MainForm1->RotationControls1->Checked = true;
    if (ZoomBar->Visible == false)
        MainForm1->InfoTools1->Checked = false;
    else
        MainForm1->InfoTools1->Checked = true;
    if (ShiftBar->Visible == false)
        MainForm1->ShiftControls1->Checked = false;
    else
        MainForm1->ShiftControls1->Checked = true;
    if (ZoomBar->Visible == false)
        MainForm1->InfoTools1->Checked = false;
    else
        MainForm1->InfoTools1->Checked = true;
    if (SurfBar->Visible == false)
        MainForm1->SurfaceTool1->Checked = false;
    else
        MainForm1->SurfaceTool1->Checked = true;
}

// ----------------------------- Basic panel ---------------------------------
void __fastcall TMainForm1::BasicTool1Click(TObject *Sender) {
    if (ToolBar1->Visible) {
        MainForm1->BasicTool1->Checked = false;
        ToolBar1->Visible = false;
    }
    else {
        MainForm1->BasicTool1->Checked = true;
        ToolBar1->Visible = true;
    }
}

// ---------------------------- Rotation Panel -------------------------------
void __fastcall TMainForm1::RotationControls1Click(TObject *Sender) {
    if (RotBar->Visible) {
        MainForm1->RotationControls1->Checked = false;
        RotBar->Visible = false;
    }
    else {
        MainForm1->RotationControls1->Checked = true;
        RotBar->Visible = true;
    }
}

// ---------------------------- Shift Panel ----------------------------------
void __fastcall TMainForm1::ShiftControls1Click(TObject *Sender) {
    if (ShiftBar->Visible) {
        MainForm1->ShiftControls1->Checked = false;
        ShiftBar->Visible = false;
    }
    else {
        MainForm1->ShiftControls1->Checked = true;
        ShiftBar->Visible = true;
    }
}

// ----------------------------- Surface Panel -------------------------------
void __fastcall TMainForm1::SurfaceTool1Click(TObject *Sender) {
    if (SurfBar->Visible) {
        MainForm1->SurfaceTool1->Checked = false;
        SurfBar->Visible = false;
    }
    else {
        MainForm1->SurfaceTool1->Checked = true;
        SurfBar->Visible = true;
    }
}

// ------------------------- Info&Export Panel -------------------------------
void __fastcall TMainForm1::InfoTools1Click(TObject *Sender) {
    if (ZoomBar->Visible) {
        MainForm1->InfoTools1->Checked = false;
        ZoomBar->Visible = false;
    }
    else {
        MainForm1->InfoTools1->Checked = true;
        ZoomBar->Visible = true;
    }
}

// ---------------------------- 3DCompass ------------------------------------
void __fastcall TMainForm1::Compass1Click(TObject *Sender) {
    if (MainForm1->Compass1->Checked)
        MainForm1->Compass1->Checked = false;
    else
        MainForm1->Compass1->Checked = true;

    projection(pBitmap);
    kart(pBitmap);
}

// ---------------------------------------------------------------------------
void __fastcall TMainForm1::Settings1Click(TObject *Sender) {
    SettForm->Show();
}

// --------------------------- Menu - 2D Export ------------------------------
void __fastcall TMainForm1::N2Dpsbmp1Click(TObject *Sender) {
    HelpForm->Close();
    PrnForm->SetParam(size, theta, phi, State);
    PrnForm->ShowModal();
}
// --------------------------- Menu - 3D Export ------------------------------

void __fastcall TMainForm1::N3Dvrml1Click(TObject *Sender) {
    // Exp3dForm->SetNum(1);
	Exp3dForm->Show();
	Exp3dForm->Left = 100;
	Exp3dForm->Top  = 100 ;
}

// ************************** Topo View Functions ******************************
// ---------------------------------- Axis ------------------------------------
void axis() // ось вращения
{
    int k = 0; // число точек пещеры, видимых на экране
    double Y1 = 0, xmed1, ymed1;
    double ymed2;
    xmed1 = Ymed * C - Xmed * S;
    ymed1 = Ymed * S + Xmed * C;
    for (unsigned int s = 0; s < size; ++s) { // если т. видна на экране
        if ((abs(Prx[s] - xx / 2) < (xx / 2 - xx / 6)) && (abs(Pry[s] - yy / 2) < (yy / 2 - yy / 6))) {
            Y1 = Y1 + P[s].pos.y * S + P[s].pos.x * C;
            k = k + 1;
        };
    }
    /* if(k!=0){
     ymed1=Y1/k*Ct*Ct+ymed1*St*St;   //Y1/k - средняя коорд.(перп. экрану)
     Xmed=ymed1*C-xmed1*S;           //для точек, видимых на экране}
     Ymed=ymed1*S+xmed1*C;};
     */

    int l = 0; // число точек узлов решетки, видимых на экране
    int i_ = 0;
    double Y2 = 0;
    double shift_cav = 0, shift_sur = 0;
    if (MainForm1->ToolButton28->Down == true || MainForm1->ToolButton27->Down == true)
        // если показыв. поверхн. сетка
    {
        for (int i = 0; i < Srf.nrows; ++i) {
            i_ = i * Srf.ncols;
            for (int k = 1; k < Srf.ncols; ++k) {
                if ((abs((double)SPrx[i_ + (k - 1)] - xx / 2) < (xx / 2 + xx / 6)) && (abs((double)SPry[i_ + (k - 1)] - yy / 2) < (yy / 6)))
                    // только не вылезающие за экран с рамками xx/6
                {
                    Y2 = Y2 + (Srf.xcellsize * k * 100) * S + (Srf.ycellsize * (Srf.nrows - i) * 100) * C;
                    l = l + 1;
                };
            }
        }
    }
    shift_cav = Y1 * k / ((k + 0.001) * (k + 0.001)) * 0.2 * k / (l + 1 + 0.2 * k);
    shift_sur = Y2 * l / ((l + 0.001) * (l + 0.001)) * (l + 1) / (l + 1 + 0.2 * k);
    // ymed2=(Y1*k/((k+0.001)*(k+0.001))+Y2*l/((l+0.001)*(l+0.001)))*Ct*Ct+ymed1*St*St;
    ymed2 = (shift_cav + shift_sur) * Ct * Ct + ymed1 * St * St;
    Xmed = ymed2 * C - xmed1 * S;
    Ymed = ymed2 * S + xmed1 * C; // далее компенсируем бросок по высоте с пом. Z
    // Zmed=Zmed-( Y1*k/((k+0.001)*(k+0.001))+Y2*l/((l+0.001)*(l+0.001)) -ymed1 )*Ct*St;
    Zmed = Zmed - (shift_cav + shift_sur - ymed1) * Ct * St;
}

// ************************* Cave Info Functions *****************************
// ------------------------------AboutCave------------------------------------
void TMainForm1::AboutCave(char k) {
    long Hmax, Hmin;
    float L_pr = 0, H_pr = 0, L = 0;
    Hmax = -1000000l;
    Hmin = 1000000l;

    for (unsigned int s = 0; s < size; ++s) // нахождение max высокой
    { // и max низкой т. пещеры
        if (!(P[s].priz & PRIZ_SURFACE) || (P[s].priz & PRIZ_ENTRY)) { // если не поверхность
            if (P[s].pos.z > Hmax)
                Hmax = P[s].pos.z;
            if (P[s].pos.z < Hmin)
                Hmin = P[s].pos.z;
        }
    }
    if (Hmax == -1000000l)
        Hmax = 0;
    if (Hmin == 1000000l)
        Hmin = 0;

    for (unsigned int s = 1; s < size; ++s) // нахождение длины, проект. длины и высоты
    { // если не поверхность, не дубль или не т. ветвления
        // if(!(P[s-1].priz & PRIZ_SURFACE) && !(P[s].priz & PRIZ_SURFACE) && !(P[s].priz & PRIZ_BRANCH))
        if (!(P[s].priz & PRIZ_SURFACE) && !(P[s].priz & PRIZ_DUPLICATE) && !(P[s].priz & PRIZ_BRANCH)) {
            L_pr += hypot(P[s].pos.x - P[s - 1].pos.x, P[s].pos.y - P[s - 1].pos.y);
            H_pr += labs(P[s].pos.z - P[s - 1].pos.z);
            L += sqrt(sqr(P[s].pos.x - P[s - 1].pos.x) + sqr(P[s].pos.y - P[s - 1].pos.y) + sqr(P[s].pos.z - P[s - 1].pos.z));
        }
    }
    wchar_t comment[25];
    wchar_t* NamInf[6] = {L"Hmin :", L"Hmax :", L"Amplituda :", L"L :", L"L pr :", L"H pr :"};
    float NumInf[6] = {Hmin / 100.0f, Hmax / 100.0f, (Hmax - Hmin) / 100.0f, L / 100.0f, L_pr / 100.0f, H_pr / 100.0f};
    UnicodeString InfoStr;
    InfoStr = L"Cave:    ";
    InfoStr += MainForm1->cave;
    InfoStr.Insert(L"\r\n", InfoStr.Length() + 1); // вставляем "\r"
    InfoStr += L"Region:  ";
    InfoStr += MainForm1->region;
    InfoStr.Insert(L"\r\n", InfoStr.Length() + 1);
    InfoStr += L"\r\n";

    if ((k == -1) || (k & 1)) {
        for (char i = 0; i < 6; i++) {
            wcscpy(comment, L"           ");
            wcsncpy(comment, NamInf[i], wcslen(NamInf[i])); // чтобы не лез конец строки
            swprintf(comment + 11, L"% 9.2f", NumInf[i]);
            InfoStr += comment;
            InfoStr += L"\r\n";
        }
        InfoStr += L"\r\nstations/пикетов   ";
        InfoStr += (PP[0] - PP[5]);
        InfoStr += L"\r\nbranchs/веток      ";
        InfoStr += (PP[1] - PP[5]);
        InfoStr += L"\r\nlabels/меток       ";
        InfoStr += PP[2];
        InfoStr += L"\r\nloops/колец        ";
        InfoStr += PP[3];
        InfoStr += L"\r\nsurveys/съемок     ";
        InfoStr += PP[4];
        InfoStr += L"\r\n\r\nFile: " + CurrFileName;
        InfoStr += L"\r\n";
    }
    if ((k == -1) || (k & 2)) {
        InfoStr += L"\r\nSurveys:\r\n";
        for (unsigned int i = 1; i <= PP[4]; i++) {
            InfoStr = InfoStr + UnicodeString(i) + L". " + MainForm1->Name(Surveys, i) + L"\r\n";
        }
    }
    if (k == -1)
        InfoForm->Memo1->Text = InfoStr;
    else
        Exp3dForm->txtList->Text = InfoStr;
}

// -------------------------------------------------------------------------
void calcGridStep(double& Af, int& Hc, char gridSizeMode) {
    switch (gridSizeMode) {
    case 0: {
            Af = 500.0 / H;
            Hc = 5; // Af=100*10/HZ - число строк на 10 метров
            if (Af < 30) {
                Af = 2 * Af;
                Hc = 10;
            };
            if (Af < 30) {
                Af = 2.5 * Af;
                Hc = 25;
            }; // 30<A
            if (Af < 30) {
                Af = 2 * Af;
                Hc = 50;
            };
            if (Af < 30) {
                Af = 2 * Af;
                Hc = 100;
            };
            if (Af < 30) {
                Af = 2.5 * Af;
                Hc = 250;
            };
            if (Af < 30) {
                Af = 2 * Af;
                Hc = 500;
            };
        };
        break; // 100*HZ метров на строку
    case 1: {
            Af = 50000.0 / H;
            Hc = 500;
            if (Af > 190) {
                Af = Af / 2;
                Hc = 250;
            };
            if (Af > 190) {
                Af = Af / 2.5;
                Hc = 100;
            }; // A<190
            if (Af > 190) {
                Af = Af / 2;
                Hc = 50;
            };
            if (Af > 190) {
                Af = Af / 2;
                Hc = 25;
            };
            if (Af > 190) {
                Af = Af / 2.5;
                Hc = 10;
            };
        };
        break;
    };
}

void scale(Graphics::TBitmap *pBitmap, char o) // координатная сетка через ... м.
{
    int x_sc = 0;
	int y_sc = 0;
	int Hc = 0;
    double Af;
    calcGridStep(Af, Hc, o);
    // rot_flag=0;     //остановка вращения
    x_sc = 1;
    y_sc = 1;

    pBitmap->Canvas->Pen->Color = clTeal; // clGray
    char n = 1;
    do {
        pBitmap->Canvas->MoveTo(1, y_sc);
        pBitmap->Canvas->LineTo(xx, y_sc);
        pBitmap->Canvas->MoveTo(x_sc, 1);
        pBitmap->Canvas->LineTo(x_sc, yy);
        x_sc = 1 + (int)(n * Af);
        y_sc = 1 + (int)(n * Af);
        n++;
    }
    while (((x_sc < xx) || (y_sc < yy)) && ((int)(n * Af) != 0));
    pBitmap->Canvas->Pen->Color = MainForm1->BgColor;
    wchar_t comment[15];
    swprintf(comment, L"scale  %d m", Hc); // Hc m/sm
    pBitmap->Canvas->Font->Color = clTeal;
    pBitmap->Canvas->TextOut(8, 22*pBitmap->Canvas->Font->PixelsPerInch / 72, comment);
}

// -------------------------- Show Stations ---------------------------------
void Stations(Graphics::TBitmap *pBitmap) {
    for (unsigned int s = 0; s < size; ++s) {
        pBitmap->Canvas->Pen->Color = clTeal; // clGray
        if (((P[s].priz & PRIZ_BRANCH) && (V[P[s].vet].B) != 0) || (((P[s + 1].priz & PRIZ_BRANCH) || (s == size - 1)) && (V[P[s].vet].E) != 0)) // s+1!!!
                pBitmap->Canvas->Pen->Color = clYellow;
        if (P[s].com)
            pBitmap->Canvas->Pen->Color = (TColor)0x002AAFF; // оранжевый
        pBitmap->Canvas->Ellipse(Prx[s] - 2, Pry[s] - 2, Prx[s] + 2, Pry[s] + 2);
    }

    pBitmap->Canvas->Pen->Color = MainForm1->BgColor;
}

// ------------------------ Show All Labels ----------------------------------
void ShowLabels(Graphics::TBitmap *pBitmap) {
    pBitmap->Canvas->Font->Color = MainForm1->LabelColor; // Font color
    pBitmap->Canvas->Font->Size = 8;
    for (unsigned int s = 0; s < size; ++s) {
        if (P[s].met > 0)
            pBitmap->Canvas->TextOut(Prx[s] + 2, Pry[s] + 2, StationList->Strings[P[s].met]);
    }
    pBitmap->Canvas->Font->Size = 10;
}

// --------------------------- Show Bif Stations ------------------------------
void ShowBifLabels(Graphics::TBitmap *pBitmap) {
    pBitmap->Canvas->Font->Color = MainForm1->LabelColor; // Font color
    pBitmap->Canvas->Font->Size = 8;
    for (unsigned int s = 0; s < size; ++s) {
        if ((P[s].priz & PRIZ_BRANCH) && (V[P[s].vet].B) != 0) {
            pBitmap->Canvas->Pen->Color = clYellow;
            pBitmap->Canvas->Ellipse(Prx[s] - 2, Pry[s] - 2, Prx[s] + 2, Pry[s] + 2);
            pBitmap->Canvas->Font->Color = MainForm1->LabelColor;
            pBitmap->Canvas->TextOut(Prx[s] + 2, Pry[s] + 2, StationList->Strings[V[P[s].vet].B]);
        }
        if (((P[s + 1].priz & PRIZ_BRANCH) || (s == size - 1)) && (V[P[s].vet].E) != 0) {
            pBitmap->Canvas->Pen->Color = clYellow;
            pBitmap->Canvas->Ellipse(Prx[s] - 2, Pry[s] - 2, Prx[s] + 2, Pry[s] + 2);
            pBitmap->Canvas->Font->Color = MainForm1->LabelColor;
            pBitmap->Canvas->TextOut(Prx[s] + 2, Pry[s] + 2, StationList->Strings[V[P[s].vet].E]);
        }
    }
    pBitmap->Canvas->Font->Size = 10;
    // #MainForm1->Image1->Canvas->Draw(0,0,Bitmap); это я убрал, потому что мы и так рисуем в Битмапу Имиджа
}

// --------------------------- Show Comments --------------------------------
void ShowComments(Graphics::TBitmap *pBitmap) {
    pBitmap->Canvas->Font->Color = MainForm1->ComColor; // Font color
    pBitmap->Canvas->Font->Size = 8;
    for (unsigned int s = 0; s < size; ++s) {
        if (P[s].com)
            pBitmap->Canvas->TextOut(Prx[s] + 2, Pry[s] + 2, MainForm1->Name(Comments, P[s].com).c_str());
    }
    if ((MainForm1->ToolButton31->Down == true) && (Wsize)) // walls
        for (unsigned int s = 0; s < Wsize; ++s) {
            if (W[s].com)
                pBitmap->Canvas->TextOut(Wpr[s].X + 2, Wpr[s].Y + 2, MainForm1->Name(WComments, W[s].com).c_str());
        }
    pBitmap->Canvas->Font->Size = 10;
    pBitmap->Canvas->Font->Color = clLime;
}

// --------------------------- Show FixPoints --------------------------------
void ShowFixPoints(Graphics::TBitmap *pBitmap) // показ меток FixPoints
{
    pBitmap->Canvas->Font->Color = MainForm1->LabelColor; // Font color
    pBitmap->Canvas->Font->Size = 8;
    for (int s = 0; s < TransForm->DataLength.fxp; ++s) {
        if (FixPnt[s].label != L"")
            pBitmap->Canvas->TextOut(FPrx[s] + 2, FPry[s] + 2, FixPnt[s].label.c_str());
    }
    pBitmap->Canvas->Font->Size = 10;
    pBitmap->Canvas->Font->Color = clLime;
}

// --------------------------- Show FixPoints_com --------------------------------
void ShowFixPoints_com(Graphics::TBitmap *pBitmap) // показ комментария FixPoints
{
    pBitmap->Canvas->Font->Color = MainForm1->ComColor; // Font color
    pBitmap->Canvas->Font->Size = 8;
    for (int s = 0; s < TransForm->DataLength.fxp; ++s) {
        if (FixPnt[s].com != L"")
            pBitmap->Canvas->TextOut(FPrx[s] + 2, FPry[s] + 2, FixPnt[s].com.c_str());
    }
    pBitmap->Canvas->Font->Size = 10;
    pBitmap->Canvas->Font->Color = clLime;
}

// ----------------------------- Find Stations ------------------------------
void FindStations(Graphics::TBitmap *pBitmap) {
    int n = 0;
    for (unsigned int s = 0; s < size; ++s) {
        if (P[s].met > 0) {
            if ((MainForm1->CheckBox1->Checked == true && UnicodeString(StationList->Strings[P[s].met]).Compare(MainForm1->Edit4->Text.Trim()) == 0) ||
                (MainForm1->CheckBox1->Checked == false && StationList->Strings[P[s].met].Pos(MainForm1->Edit4->Text.Trim()) > 0)) {
                pBitmap->Canvas->Pen->Color = clCream; // clYellow;
                pBitmap->Canvas->Ellipse(Prx[s] - 3, Pry[s] - 3, Prx[s] + 3, Pry[s] + 3);
                if (MainForm1->CheckBox2->Checked == true)
                    pBitmap->Canvas->TextOut(Prx[s] + 2, Pry[s] + 2, StationList->Strings[P[s].met]);
                n++;
            }
        }
    }
    MainForm1->StaticTextFound->Caption = L"Found: " + UnicodeString(n);
}

// ----------------------------------------------------------------
float sqr_(float x) {
    return x*x;
}

// ----------------------------------------------------------------
bool belong2piece(float x0, float y0, // координаты точки
    float x1, float y1, float x2, float y2 // координаты отрезка
    ) { /* _Принадлежность точки отрезку._
     Считаем расстояние от данной точки до отрезка, и сравниваем с eps.
     1) Квадрат расстояния меньше sqr(eps)
     2) Находится между перпендикулярными прямыми, проведенными через концы
     отрезка.
     Проверяется через с.п./* */

    float eps = 4, eps2 = 16, t;

    float Temp = (sqr_(x2 - x1) + sqr_(y2 - y1));
    if (Temp < 1e-5) // т.е. точки отрезка фактически составляют одну точку
            t = sqr_(x0 - x1) + sqr_(y0 - y1);
    else
        t = sqr_((x0 - x1) * (y2 - y1) - (y0 - y1) * (x2 - x1)) / Temp;

    if (t > eps2)
        return false;

    if (((x0 - x1) * (x2 - x1) + (y0 - y1) * (y2 - y1)) * ((x0 - x2) * (x2 - x1) + (y0 - y2) * (y2 - y1)) < eps)
        return true;
    else
        return false;
}

// --------------------------- Cave Mouse ------------------------------------
unsigned int TMainForm1::CaveMouse(Graphics::TBitmap *&pBitmap, int X, int Y, bool drawPrev) {
    int s1 = -1; // номер пикета, на котором мышь

    /* unsigned int l1=6;    //расстояние точка - мышь
     unsigned int l2;


     //находим ближайший пикет (вернее первый из попавшихся пикетов, что расположен к курсору мыши на расстоянии меньшем l1)
     for(unsigned int s=0; s<size; s++)
     {
     l2=abs(X-Prx[s])+abs(Y-Pry[s]);

     if(l2<l1)
     {
     l1=l2;
     s1=s;
     break;
     }
     }/* */

    for (unsigned int s = 1; s < size; s++)
        if (belong2piece(X, Y, // координаты точки
            Prx[s - 1], Pry[s - 1], Prx[s], Pry[s] // координаты отрезка
            ))
            if (!(P[s].priz & PRIZ_BRANCH)) {
                s1 = s;
                break;
            }

    int s2 = -1;
    if (s1 == 0 || (sqr_(X - Prx[s1]) + sqr_(Y - Pry[s1])) < (sqr_(X - Prx[s1 - 1]) + sqr_(Y - Pry[s1 - 1]))) {
        s2 = s1;
    } else {
        s2 = s1 - 1;
    }
    
    if (s2 > 0 && s1 != CaveMouseNo) // номер предыдущего найденного пикета
    {
        CaveMouseNo = s1;
        kart(pBitmap);

        // pBitmap->Canvas->Pen->Color = SetTColor(P[s1].col);
        if (drawPrev) {
            pBitmap->Canvas->Pen->Color = clFuchsia;
            pBitmap->Canvas->Ellipse(Prx[s1] - 2, Pry[s1] - 2, Prx[s1] + 2, Pry[s1] + 2);
            pBitmap->Canvas->Ellipse(Prx[s1 - 1] - 2, Pry[s1 - 1] - 2, Prx[s1 - 1] + 2, Pry[s1 - 1] + 2);
            pBitmap->Canvas->MoveTo(Prx[s1 - 1], Pry[s1 - 1]);
            pBitmap->Canvas->LineTo(Prx[s1], Pry[s1]);
            pBitmap->Canvas->Pen->Color = BgColor;
        }

        // по расст. выбираем s1 или s1-1 соотв. подсвечиваем и выводим о нем инфо
        pBitmap->Canvas->Pen->Color = clYellow;
        ShowInfoForCurrPicket(s2); // выводим на Экран Инфу про текущий пикет
        Circle(pBitmap, Prx[s2], Pry[s2], 2);
    }
    return s2;
}

// -------------------------------------------------------------------------
void __fastcall TMainForm1::ShowInfoForCurrPicket(unsigned int s1) { // выводим на Экран Инфу про текущий пикет

    wchar_t comment[256];

    swprintf(comment, L"% 8.2f N ", (P[s1].pos.x / 100.0));
    swprintf(comment + wcslen(comment), L"% 8.2f E ", (P[s1].pos.y / 100.0));
    swprintf(comment + wcslen(comment), L"% 8.2f H", (P[s1].pos.z / 100.0));
    swprintf(comment + wcslen(comment), L"  ");

    if (s1 < size - 1) // !!!!только тогда корректно использовать номер (s1+1)!!!
    {
        if (!(P[s1 + 1].priz & PRIZ_BRANCH))
            swprintf(comment + wcslen(comment), UnicodeString(StationList->Strings[V[P[s1].vet].B]).c_str());
        if (!(P[s1 + 1].priz & PRIZ_BRANCH) && !(P[s1].priz & PRIZ_BRANCH))
            swprintf(comment + wcslen(comment), L" - ");
    }

    if (!(P[s1].priz & PRIZ_BRANCH))
        swprintf(comment + wcslen(comment), UnicodeString(StationList->Strings[V[P[s1].vet].E]).c_str());

    swprintf(comment + wcslen(comment), L",  survey: ");
    if (wcslen(comment) + wcslen(MainForm1->Name(Surveys, P[s1].srv).c_str()) < 170)
        // огранич. на длину comment до 256 - защита от вылета
            swprintf(comment + wcslen(comment), MainForm1->Name(Surveys, P[s1].srv).c_str());
    swprintf(comment + wcslen(comment), L",  File: ");
    swprintf(comment + wcslen(comment), MainForm1->Name(IncFiles, Surv[P[s1].srv].File + 1).c_str());
    swprintf(comment + wcslen(comment), L"  line: ");
    swprintf(comment + wcslen(comment), L"% .d", (P[s1].line));
    MainForm1->LabelXYZ->Caption = comment; // надо огранич. на длину comment до 256 - защиту от вылета!!!

    if (P[s1].met > 0) // показ меток
            pBitmap->Canvas->TextOut(Prx[s1] + 2, Pry[s1] + 2, StationList->Strings[P[s1].met]);

    if (P[s1].com) // показ комментария
    {
        pBitmap->Canvas->Font->Color = MainForm1->ComColor;
        pBitmap->Canvas->TextOut(Prx[s1] + 2, Pry[s1] + 2, MainForm1->Name(Comments, P[s1].com).c_str());
        pBitmap->Canvas->Font->Color = MainForm1->LabelColor;
    }
}

// ****************************** View control ********************************//
// ******************** (Key & Mouse events in view state) ********************//

// ---------------- Actions (Shifts & Step Rotations & so on) ------------------
void TMainForm1::Act(char moda) {
    StateEnum prevState = State;
    if (State > 4) {
        switch (moda) {
        case 1:
            axis();
            rot_flag = 0;
            PhiPlus(nn);
            break; // rotation
        case 2:
            axis();
            rot_flag = 0;
            PhiMinus(nn);
            break;
        case 3:
            if (theta < 1.5359)
                theta += 0.017453292 * MainForm1->VStep;
            break;
        case 4:
            if (theta > -1.5360)
                theta -= 0.017453292 * MainForm1->VStep;
            break;
        case 5:
            Ymed = Ymed + 10 * H * C;
            Xmed = Xmed - 10 * H * S;
            XYmed = XYmed - 10 * H;
            break; // left shift
        case 6:
            Ymed = Ymed - 10 * H * C;
            Xmed = Xmed + 10 * H * S;
            XYmed = XYmed + 10 * H;
            break; // right shift
        case 7:
            Zmed = Zmed - 10 * H * Ct;
            Ymed = Ymed - 10 * H * S * St;
            Xmed = Xmed - 10 * H * C * St;
            break;
        case 8:
            Zmed = Zmed + 10 * H * Ct;
            Ymed = Ymed + 10 * H * S * St;
            Xmed = Xmed + 10 * H * C * St;
            break;
        case 9:
            if (H < 5000)
                H = H / 0.9;
            break; // zoom
        case 10:
            if (H > 1)
                H = H * 0.9;
            break;
        case 11:
            rot_flag = 0;
            SetState(STATE_VIEW_NORMAL);
            init_par();
            phi = 0;
            theta = (3.1415 / 2);
            break; // план  'p'
        case 12:
            rot_flag = 0;
            SetState(STATE_VIEW_NORMAL);
            init_par();
            phi = 0;
            theta = 0;
            break; // разрез 'r'
        case 23:
            rot_flag = 0;
			SetState(STATE_VIEW_EXT_PROFILE);
            init_par();
            break; // разрез-развертка
        case 13:
            if (nn < 10)
                nn = nn * 2;
            break; // 'z'
        case 14:
            if (nn > 0.1)
                nn = nn * 0.5;
            break; // 'x'
        case 15:
            AboutCave(-1);
            InfoForm->Show();
            InfoForm->OK->SetFocus();
            break;
        case 16:
            axis();
            rot_flag = 1;
            MainForm1->Timer2->Enabled = true;
            return;
        case 17:
            axis();
            rot_flag = 2;
            MainForm1->Timer2->Enabled = true;
            return;
        case 18:
            if (MainForm1->ToolButton20->Down == true)
                MainForm1->ToolButton20->Down = false;
            break;
        case 19:
            if (MainForm1->ToolButton19->Down == true)
                MainForm1->ToolButton19->Down = false;
            break;
        case 20:
            break;
        case 21:   
            if (TrackForm && TrackForm->Visible) {
                TrackForm->Close();
            }
            break;
        case 22:
            break;
        case 24:
            break;
        case 25:
            break;
        case 26:
            break;
        case 27:
            if (MainForm1->ToolButton27->Down == true) {
                MainForm1->ToolButton28->Down = false;
            }
            break;
        case 28:
            if (MainForm1->ToolButton28->Down == true) {
                MainForm1->ToolButton27->Down = false;
            }
            break;
        case 29:
            break;
        default:
            break;
        }

        prefs.showBox = MainForm1->ToolButton26->Down;
        if (prevState != State && (State == STATE_VIEW_NORMAL || State == STATE_VIEW_EXT_PROFILE)) {
            ReinitCaveModel();
        }
        updateCaveViewPrefs();

        projection(MainForm1->pBitmap);
        kart(MainForm1->pBitmap);
    }
}

// ----------------------------- Mouse events ----------------------------------
// ----------------------------- Mouse Move ------------------------------------
void __fastcall TMainForm1::Image1MouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y) {
    if (Shift.Contains(ssLeft) && rot_flag == 0)
        X0 = X;
    Y0 = Y;
}

// ---------------------------------------------------------------------------
void __fastcall TMainForm1::Image1MouseMove(TObject *Sender, TShiftState Shift, int X, int Y) {
    if ((MainForm1->ToolButton23->Down || MainForm1->ToolButton48->Down) && rot_flag == 0 && State > 1) {
        bool visualiseEdge = MainForm1->ToolButton23->Down;
        MouseStation = CaveMouse(pBitmap, X, Y, visualiseEdge);
    }
    // cave mouse
    if (Shift.Contains(ssLeft) && rot_flag == 0 && State > 1) // shift image by mouse
    {
        Ymed = Ymed - (X - X0) * H * C;
        Xmed = Xmed + (X - X0) * H * S; // shift
        Zmed = Zmed + (Y - Y0) * H * Ct;
        Ymed = Ymed + (Y - Y0) * H * S * St;
        Xmed = Xmed + (Y - Y0) * H * C * St;
        XYmed = XYmed + (X - X0) * H;
        X0 = X;
        Y0 = Y;
        axis();
        projection(pBitmap);
        kart(pBitmap);
    }
    if (Shift.Contains(ssRight) && rot_flag == 0 && State > 1) // rotait image by mouse
    {
        if ((X - X0) > 0)
            PhiPlus(1);
        if ((X - X0) < 0)
            PhiMinus(1);
        if (((Y - Y0) > 1) && (theta < 1.56))
            theta += 0.017453292;
        if (((Y - Y0) < -1) && (theta > -1.56))
            theta -= 0.017453292;
        X0 = X;
        Y0 = Y;
        projection(pBitmap);
        kart(pBitmap);
    }
}
// wchar_t xPos[10];  _itow(X, xPos, 10); Edit3->Text = xPos;

// ---------------------------- Mouse Up/Down & Clik --------------------------
// ------------------------------- Rotations ----------------------------------
void __fastcall TMainForm1::StopClick(TObject *Sender) {
    rot_flag = 0;
}

void __fastcall TMainForm1::PlayLeftClick(TObject *Sender) {
    Act(17);
}

void __fastcall TMainForm1::PlayRightClick(TObject *Sender) {
    Act(16);
}

// ---------------------------- Plan - Profile --------------------------------
void __fastcall TMainForm1::ToolButton2Click(TObject *Sender) {
    Act(11);
} // план

void __fastcall TMainForm1::ToolButton10Click(TObject *Sender) {
    Act(12);
} // разрез

void __fastcall TMainForm1::ToolButton4Click(TObject *Sender) {
    Act(23);
} // разрез-разв.

// ------------------------------ labels --------------------------------------
void __fastcall TMainForm1::ToolButton12Click(TObject *Sender) {
    Act(22);
} // пещ

void __fastcall TMainForm1::ToolButton29Click(TObject *Sender) {
    Act(22);
} // пов
// ---------------------------------------------------------------------------

// ------------------------------  Cave Mouse --------------------------------
void __fastcall TMainForm1::ToolButton23Click(TObject *Sender) {
    Act(21);
}

// ------------------------- Slow - Fast Rotation -----------------------------
void __fastcall TMainForm1::ToolButton15Click(TObject *Sender) {
    Act(14);
} // медленнее

void __fastcall TMainForm1::ToolButton9Click(TObject *Sender) {
    Act(13);
} // быстрее

// ----------------------------- Grid -----------------------------------------
void __fastcall TMainForm1::ToolButton19Click(TObject *Sender) {
    Act(18);
}

void __fastcall TMainForm1::ToolButton20Click(TObject *Sender) {
    Act(19);
}

// ----------------------------- Surf Grid -----------------------------------------
void __fastcall TMainForm1::ToolButton27Click(TObject *Sender) {
    Act(27);
}

void __fastcall TMainForm1::ToolButton28Click(TObject *Sender) {
    Act(28);
}
// ---------------------------------------------------------------------------

// ---------------------------------------------------------------------------
// ---------------------------- Stations --------------------------------------
void __fastcall TMainForm1::ToolButton14Click(TObject *Sender) {
    Act(20);
}

// ---------------------------- Comments --------------------------------------
void __fastcall TMainForm1::ToolButton24Click(TObject *Sender) {
    Act(24);
}

// ---------------------------- Surface Comments ------------------------------
void __fastcall TMainForm1::ToolButton30Click(TObject *Sender) {
    Act(24);
}

// ---------------------------- All Labels ------------------------------------
void __fastcall TMainForm1::ToolButton7Click(TObject *Sender) {
    ToolButton12->Down = false;
    Act(24);
}

// --------------------------- Find Stations ----------------------------------
void __fastcall TMainForm1::Edit4KeyDown(TObject *Sender, WORD &Key, TShiftState Shift) {
    if (Key == VK_SPACE) {
        Edit4->Text = Edit4->Text.Trim();
        Edit4->SelStart = Edit4->Text.Length();
    }
    /* if (Key == VK_RETURN) */ Act(25);
}

void __fastcall TMainForm1::CheckBox1Click(TObject *Sender) {
    Act(25);
}

void __fastcall TMainForm1::CheckBox2Click(TObject *Sender) {
    Act(25);
}

// ---------------------------- Find Stations ---------------------------------
void __fastcall TMainForm1::ToolButton25Click(TObject *Sender) {
    if (ToolButton25->Down) {
        MainForm1->CheckBox1->Visible = true;
        MainForm1->CheckBox2->Visible = true;
        MainForm1->StaticTextFound->Visible = true;
        MainForm1->Edit4->Visible = true;
        MainForm1->StatusBar1->Panels->Items[3]->Text = L"Find:";
        MainForm1->Edit4->SetFocus();
    }
    else {
        MainForm1->CheckBox1->Visible = false;
        MainForm1->CheckBox2->Visible = false;
        MainForm1->StaticTextFound->Visible = false;
        MainForm1->Edit4->Visible = false;
        MainForm1->StatusBar1->Panels->Items[3]->Text = L"";
    }
}

// -------------------------- Save as Bitmap ----------------------------------
void __fastcall TMainForm1::ToolButton1Click(TObject *Sender) {
    SavePictureDialog1->InitialDir = ExtractFilePath(MainForm1->CurrFileName);
    UnicodeString bmpFile = ExtractFileName(MainForm1->CurrFileName);
    bmpFile.Delete(bmpFile.LastDelimiter(L"."), bmpFile.Length() + 1 - bmpFile.LastDelimiter(L"."));
    SavePictureDialog1->FileName = bmpFile;

	if (MainForm1->SavePictureDialog1->Execute()) {
		if (renderSimpleMode == CM::SM_ORIGINAL) {
			Graphics::TBitmap *SaveBitmap;
			SaveBitmap = new Graphics::TBitmap();
			SaveBitmap->Assign(pBitmap);
			SaveBitmap->PixelFormat = pf24bit; // 24 -> 8 bit/pixel
			SaveBitmap->SaveToFile(SavePictureDialog1->FileName);
			delete SaveBitmap;
		} else if (ogreRenderer){
			std::string fname = UTF8Encode(SavePictureDialog1->FileName).c_str();
			ogreRenderer->saveToFile(fname);
		}
    }
}

// ---------------------------- About Cave ------------------------------------
void __fastcall TMainForm1::ToolButton22Click(TObject *Sender) {
    Act(15);
}

// ----------------------------- View help ------------------------------------
void __fastcall TMainForm1::ToolButton5Click(TObject *Sender) {
    HelpForm->SetHelpNum(5);
    HelpForm->Show();
}

// ------------------------------ Viev Box ------------------------------------
void __fastcall TMainForm1::ToolButton26Click(TObject *Sender) {
    Act(26);
}

// ------------------------------ Show Walls ------------------------------------
void __fastcall TMainForm1::ToolButton31Click(TObject *Sender) {
    Act(29);
}
// ---------------------------------------------------------------------------

// ********************** Mouse Button Down & Timer **************************
void __fastcall TMainForm1::Timer1Timer(TObject *Sender) {
    if (ActModa < 0) {
        Timer1->Interval = 50;
    }
    else {
        Timer1->Interval = 600;
        ActModa = (signed wchar_t)(-ActModa);
    }
    Act((wchar_t)abs(ActModa));
}

// --------------------------- On Mouse Down ---------------------------------
void __fastcall TMainForm1::RotLeftMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y) {
    if (Shift.Contains(ssLeft)) {
        ActModa = 1;
        Timer1->Enabled = true;
        Timer1Timer(MainForm1);
    }
}

void __fastcall TMainForm1::RotRightMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y) {
    if (Shift.Contains(ssLeft)) {
        ActModa = 2;
        Timer1->Enabled = true;
        Timer1Timer(MainForm1);
    }
}

void __fastcall TMainForm1::RotUpMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y) {
    if (Shift.Contains(ssLeft)) {
        ActModa = 3;
        Timer1->Enabled = true;
        Timer1Timer(MainForm1);
    }
}

void __fastcall TMainForm1::RotDownMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y) {
    if (Shift.Contains(ssLeft)) {
        ActModa = 4;
        Timer1->Enabled = true;
        Timer1Timer(MainForm1);
    }
}

void __fastcall TMainForm1::ShiftLeftMDown(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y) {
    if (Shift.Contains(ssLeft)) {
        ActModa = 5;
        Timer1->Enabled = true;
        Timer1Timer(MainForm1);
    }
}

void __fastcall TMainForm1::ShiftRightMDown(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y) {
    if (Shift.Contains(ssLeft)) {
        ActModa = 6;
        Timer1->Enabled = true;
        Timer1Timer(MainForm1);
    }
}

void __fastcall TMainForm1::ShiftUpMDown(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y) {
    if (Shift.Contains(ssLeft)) {
        ActModa = 7;
        Timer1->Enabled = true;
        Timer1Timer(MainForm1);
    }
}

void __fastcall TMainForm1::ShiftDownMDown(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y) {
    if (Shift.Contains(ssLeft)) {
        ActModa = 8;
        Timer1->Enabled = true;
        Timer1Timer(MainForm1);
    }
}

void __fastcall TMainForm1::ZoomMMDown(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y) {
    if (Shift.Contains(ssLeft)) {
        ActModa = 9;
        Timer1->Enabled = true;
        Timer1Timer(MainForm1);
    }
}

void __fastcall TMainForm1::ZoomPMDown(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y) {
    if (Shift.Contains(ssLeft)) {
        ActModa = 10;
        Timer1->Enabled = true;
        Timer1Timer(MainForm1);
    }
}

// ---------------------------- On Mouse Up -----------------------------------
void __fastcall TMainForm1::ButtonMouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y) {
    Timer1->Enabled = false;
    ActModa = 0;
}

// --------------------------- Om Mouse Wheel ---------------------------------
void __fastcall TMainForm1::FormMouseWheelDown(TObject *Sender, TShiftState Shift, TPoint &MousePos, bool &Handled) {
    Act(10);
}

// {if(H>1)
// {H=H*0.9;
// Xmed=Xmed+(MousePos.y-Xmed)*0.1;}
// projection(MainForm1->pBitmap); kart(MainForm1->pBitmap);}
// ---------------------------------------------------------------------------
void __fastcall TMainForm1::FormMouseWheelUp(TObject *Sender, TShiftState Shift, TPoint &MousePos, bool &Handled) {
    Act(9);
}
// ---------------------------------------------------------------------------
// -------------------------- Key events (View state) -------------------------

void __fastcall TMainForm1::FormKeyDown(TObject *Sender, WORD &Key, TShiftState Shift) {
    // Edit3->Text=Key;  (key number)
    if ((State == STATE_VIEW_NORMAL) || (State == STATE_VIEW_EXT_PROFILE)) {
        switch (Key) {
        case 80:
            if (!ToolButton25->Down)
                Act(11);
            break; // план  'p'
        case 82:
            if (!ToolButton25->Down)
                Act(12);
            break; // разрез 'r'
        case 69:
            if (!ToolButton25->Down)
                Act(23);
            break; // разрез-развертка 'e'
        case 32:
            if (State != 6) {
                if (rot_flag == 0)
                    Act(16);
                else
                    rot_flag = 0;
            }
            break;
        case 88:
            if (State != 6)
                Act(13);
            break; // 'x'
        case 90:
            if (State != 6)
                Act(14);
            break; // 'z'
        case 188:
            Act(1);
            break; // '<'
        case 190:
            Act(2);
            break; // '>'
        case 33:
            Act(3);
            break; // page up  (rotation up/down)
        case 34:
            Act(4);
            break; // page down
        case 37:
            if (Shift.Contains(ssCtrl))
                Act(1);
            else
                Act(5);
            break; // cursor (shift)
        case 38:
            if (Shift.Contains(ssCtrl))
                Act(3);
            else
                Act(7);
            break; //
        case 39:
            if (Shift.Contains(ssCtrl))
                Act(2);
            else
                Act(6);
            break; //
        case 40:
            if (Shift.Contains(ssCtrl))
                Act(4);
            else
                Act(8);
            break; // end of cursor
        case 109:
            Act(9);
            break; // уменьшение  '-'
        case 107:
            Act(10);
            break; // увеличение  '+'
        case 73:
            if (!ToolButton25->Down)
                Act(15);
            break; // 'i'
        case 113:
            if (MainForm1->ToolButton24->Down == true)
                MainForm1->ToolButton24->Down = false;
            else
                MainForm1->ToolButton24->Down = true;
            Act(24);
            break; // 'F2'
        case 114:
            if (MainForm1->ToolButton12->Down == true)
                MainForm1->ToolButton12->Down = false;
            else
                MainForm1->ToolButton12->Down = true;
            Act(22);
            break; // 'F3'
        case 115:
            if (MainForm1->ToolButton14->Down == true)
                MainForm1->ToolButton14->Down = false;
            else
                MainForm1->ToolButton14->Down = true;
            Act(20);
            break; // 'F4'
        case 116:
            if (MainForm1->ToolButton19->Down == true)
                MainForm1->ToolButton19->Down = false;
            else
                MainForm1->ToolButton19->Down = true;
            Act(18);
            break; // 'F5'
        case 117:
            if (MainForm1->ToolButton20->Down == true)
                MainForm1->ToolButton20->Down = false;
            else
                MainForm1->ToolButton20->Down = true;
            Act(19);
            break; // 'F6'
        case 118:
            if (MainForm1->ToolButton23->Down == true)
                MainForm1->ToolButton23->Down = false;
            else
                MainForm1->ToolButton23->Down = true;
            Act(20);
            break; // 'F7'
        default:
            break;
        }
    }
}
// ------------------------ Edit по Click -------------------------------------

void __fastcall TMainForm1::Image1Click(TObject *Sender) {
    UnicodeString EFile = ExtractFilePath(CurrFileName);
    if (rot_flag == 0 && State > 1) {
        int station = MouseStation; // CaveMouse(pBitmap,X,Y);
        if (station > 0) {// если мышь наведена на точку
            if (MainForm1->ToolButton23->Down) {
                if (EditForm->Visible) {
                    if ((EditForm->edit_flag == 0) || (EditForm->FFileName == EditForm->OpenDialog1->FileName) || (EditForm->Confirm() == 2)) {
                        EditForm->edit_flag = 0;
                    }
                    if (EditForm->Confirm() == 2) // если редактир. файл не сохранен
                    {
                        EditForm->Close();
                        if (EditForm->Visible == true)
                            return;
                    }
                }
                else {
                    EditForm->edit_flag = 0;
                } // если окно edit закрыто
                EFile += MainForm1->Name(IncFiles, Surv[P[station].srv].File + 1);
				LoadFromFileUTF8Test(EditForm->RichEdit1->Lines, EFile); // надо try!!!
                EditForm->SetFileName(EFile);
                EditForm->RichEdit1->CaretPos = Point(0, P[station].line);
                EditForm->RichEdit1->SelLength = EditForm->RichEdit1->Lines->Strings[P[station].line].Length();
                EditForm->Show();
            }
            
            if (MainForm1->ToolButton48->Down && TrackForm) {
                const P3D* p = &P[station];
                UnicodeString name;
                if (p->met > 0) name = StationList->Strings[p->met];
                else name = L"-";
                TrackForm->AddPiket(name, p);       
            }

        }
    }
}

// ------------------------- регистры - имена файлов ---------------------------

void __fastcall TMainForm1::FormActivate(TObject *Sender) {
    int i = 0;
    UnicodeString FilePath;
    // читаем из регистра имена файлов:
    TRegistry *Registry = new TRegistry;
    try {
        // Registry->RootKey = HKEY_LOCAL_MACHINE;
        // false because we do not want to create it if it doesn’t exist
        Registry->OpenKey(L"\\Software\\TOPO\\RecentFiles", false);

        for (i = 0; i < FILES_HISTORY_COUNT; i++) {
            FilePath = Registry->ReadString(IntToStr(i + 1));
            File1->Items[9 + i]->Caption = IntToStr(i + 1) + L" " + FilePath;
        }
    }
    __finally {
        delete Registry;
    }
}

// ---------------------------------------------------------------------------
void __fastcall TMainForm1::File_1Click(TObject *Sender) {
    CurrFileName = File_1->Caption.SubString(4, File_1->Caption.Length() - 3);
    OpenCurrFile(CurrFileName);

}

// ---------------------------------------------------------------------------
void __fastcall TMainForm1::File_2Click(TObject *Sender) {
    CurrFileName = File_2->Caption.SubString(4, File_2->Caption.Length() - 3);
    OpenCurrFile(CurrFileName);
}

// ---------------------------------------------------------------------------
void __fastcall TMainForm1::File_3Click(TObject *Sender) {
    CurrFileName = File_3->Caption.SubString(4, File_3->Caption.Length() - 3);
    OpenCurrFile(CurrFileName);
}

// ---------------------------------------------------------------------------
void __fastcall TMainForm1::File_4Click(TObject *Sender) {
    CurrFileName = File_4->Caption.SubString(4, File_4->Caption.Length() - 3);
    OpenCurrFile(CurrFileName);
}

// ---------------------------------------------------------------------------
void __fastcall TMainForm1::File_5Click(TObject *Sender) {
    CurrFileName = File_5->Caption.SubString(4, File_5->Caption.Length() - 3);
    OpenCurrFile(CurrFileName);
}

// ---------------------------------------------------------------------------
void __fastcall TMainForm1::OpenCurrFile(UnicodeString CurrFileName_) {
    CurrFileName = CurrFileName_;
    SetState(STATE_OPENED);
    EditForm->edit_flag = 1;
    MainForm1->StatusBar1->Panels->Items[0]->Text = L"Project file: " + ExtractFileName(CurrFileName);
    MainForm1->StatusBar1->Panels->Items[1]->Text = L"";

    // 1.читаем из регистра имяена файлов:
    ListFileNameHystory->Clear();
    TRegistry * Registry = new TRegistry;
    UnicodeString FilePath;
    try {
        // Registry->RootKey = HKEY_LOCAL_MACHINE;
        // false because we do not want to create it if it doesn’t exist
        Registry->OpenKey(L"\\Software\\TOPO\\RecentFiles", false);
        for (int i = 0; /* , ids=0; */ i < FILES_HISTORY_COUNT; i++) {
            FilePath = Registry->ReadString(IntToStr(i + 1));
            if (!FilePath.IsEmpty())
                ListFileNameHystory->Lines->Add(FilePath);
        }
    }
    __finally {
        delete Registry;
    }

    // проверим, может это имя (CurrFileName) уже существует в списке. Тогда удаляем его оттуда:
    for (int i = 0; /* , ids=0; */ i < FILES_HISTORY_COUNT; i++)
        if (ListFileNameHystory->Lines->Strings[i] == CurrFileName) {
            ListFileNameHystory->Lines->Delete(i);
            break;
        }
    // тепрь добавляем в список (на нулевую позицию) текущее имя файла:
    ListFileNameHystory->Lines->Insert(0, CurrFileName);

    // перезаприсываем содержимое регистра:
    TRegistry *Reg = new TRegistry;
    try {
        // Reg->RootKey = HKEY_CURRENT_USER;
        if (Reg->OpenKey(L"\\Software\\TOPO\\RecentFiles", true)) {
            for (int i = 0; /* , ids=0; */ i < FILES_HISTORY_COUNT; i++)
                Reg->WriteString(IntToStr(i + 1), ListFileNameHystory->Lines->Strings[i]);
            Reg->CloseKey();
        }
    }
    __finally {
        delete Reg;
    }

    // Обновляем содержимое меню:
    for (int i = 0; i < FILES_HISTORY_COUNT; i++) {
        FilePath = Registry->ReadString(IntToStr(i + 1));
        File1->Items[9 + i]->Caption = IntToStr(i + 1) + L" " + ListFileNameHystory->Lines->Strings[i];
    }
}
// ---------------------------------------------------------------------------

void __fastcall TMainForm1::ToolButton32MouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y) {
    Image1->Visible = !Image1->Visible;

    if (ogreRenderer)
        ogreRenderer->setEnabled(!Image1->Visible);
    if (!Image1->Visible) {
        Image1->Hide();
        if (ogreRenderer)
            ogreRenderer->refresh();
    }
}

// ---------------------------------------------------------------------------
void __fastcall TMainForm1::FormPaint(TObject *Sender) {
    if (ogreRenderer && ogreRenderer->isEnabled())
        ogreRenderer->refresh();
}

// ---------------------------------------------------------------------------
void TMainForm1::setOgreRendererEnabled(bool enbld) {
    Image1->Visible = !enbld;
    if (ogreRenderer)
        ogreRenderer->setEnabled(!Image1->Visible);
    if (!Image1->Visible) {
        Image1->Hide();
        if (ogreRenderer)
            ogreRenderer->refresh();
    }
    // OgreToolButton->Down = ogreRenderer->isEnabled();
}

// ---------------------------------------------------------------------------
void __fastcall TMainForm1::ToolButton34_Click(TObject *Sender) {
    (int&)prefs.wallsShadowMode = (prefs.wallsShadowMode + 1) % CM::WSM_NUM;
    updateWallsShadowModeChecker();
    updateCaveViewPrefs();
}

// ---------------------------------------------------------------------------
void __fastcall TMainForm1::ToolButton32Click(TObject *Sender) {
	(int&)prefs.wallsSurfaceMode = (prefs.wallsSurfaceMode + 1) % CM::WSFM_NUM;
	prefs.showSplay = prefs.wallsSurfaceMode != CM::WSFM_SURF;
    updateWallsSurfaceModeChecker();
    updateCaveViewPrefs();
}

// ---------------------------------------------------------------------------
void TMainForm1::updateWallsShadowModeChecker() {
    switch (prefs.wallsShadowMode) {
    case CM::WSM_ROUGH:
        ToolButton34->ImageIndex = 14;
        break;
    case CM::WSM_SMOOTH:
        ToolButton34->ImageIndex = 15;
        break;
    }
    ToolButton34->Enabled = (renderSimpleMode != CM::SM_ORIGINAL);
}

// ---------------------------------------------------------------------------
void TMainForm1::updateWallsSurfaceModeChecker() {
    switch (prefs.wallsSurfaceMode) {
    case CM::WSFM_NONE:
        ToolButton42->ImageIndex = 16;
        break;
    case CM::WSFM_SURF:
        ToolButton42->ImageIndex = 17;
        break;
    }
    ToolButton42->Enabled = (renderSimpleMode != CM::SM_ORIGINAL);
}

// ---------------------------------------------------------------------------
void TMainForm1::updateWallsBlowModeChecker() {
    switch (prefs.wallsBlowMode) {
    case CM::WBM_NONE:
        ToolButton35->ImageIndex = 2;
        break;
    case CM::WBM_LINEAR:
        ToolButton35->ImageIndex = 3;
        break;
    case CM::WBM_COS2PI:
        ToolButton35->ImageIndex = 4;
        break;
    case CM::WBM_COSCOS2PI:
        ToolButton35->ImageIndex = 5;
        break;
    case CM::WBM_BESIER3:
        ToolButton35->ImageIndex = 18;
        break;
    }
    ToolButton35->Enabled = (renderSimpleMode != CM::SM_ORIGINAL);
}

void TMainForm1::updateWallsColorModeChecker() {
    switch (prefs.wallColoringMode) {
    case CM::WCM_SMOOTH:
        ToolButton47->ImageIndex = 48;
        break;
    case CM::WCM_TIGHTNESS_SMOOTH:
        ToolButton47->ImageIndex = 49;
        break;
    case CM::WCM_DEPTH_SMOOTH:
        ToolButton47->ImageIndex = 50;
        break;
    }
    ToolButton47->Enabled = (renderSimpleMode != CM::SM_ORIGINAL);
}

// ---------------------------------------------------------------------------
void TMainForm1::updateWallsPropagateModeChecker() {
    switch (prefs.wallsPropagateMode) {
    case CM::WPM_NONE:
        ToolButton36->ImageIndex = 6;
        break;
    case CM::WPM_10D:
        ToolButton36->ImageIndex = 9;
        break;
    case CM::WPM_20D:
        ToolButton36->ImageIndex = 10;
        break;
    case CM::WPM_30D:
        ToolButton36->ImageIndex = 11;
        break;
    case CM::WPM_1M:
        ToolButton36->ImageIndex = 19;
        break;
    case CM::WPM_2M:
        ToolButton36->ImageIndex = 7;
        break;
    case CM::WPM_4M:
        ToolButton36->ImageIndex = 8;
        break;
    case CM::WPM_X2:
        ToolButton36->ImageIndex = 12;
        break;
    case CM::WPM_X4:
        ToolButton36->ImageIndex = 13;
        break;
    }
    ToolButton36->Enabled = (renderSimpleMode != CM::SM_ORIGINAL);
}

// ---------------------------------------------------------------------------
void TMainForm1::updategenerateWallsForNoWallsPiketsModeChecker() {
    switch (prefs.generateWallsForNoWallsPiketsMode) {
    case CM::GWNWPM_NONE:
        ToolButton33->ImageIndex = 20;
        break;
    case CM::GWNWPM_SKIP:
        ToolButton33->ImageIndex = 21;
        break;
    case CM::GWNWPM_TRAIL:
        ToolButton33->ImageIndex = 22;
        break;
    case CM::GWNWPM_BUDGE:
        ToolButton33->ImageIndex = 23;
        break;
    }
    ToolButton33->Enabled = (renderSimpleMode != CM::SM_ORIGINAL);
}

// ---------------------------------------------------------------------------
void TMainForm1::updateWallsSegmentTriangulationMode() {
    switch (prefs.wallsSegmentTriangulationMode) {
    case CM::WSTM_ANGLE:
        ToolButton37->ImageIndex = 24;
        break;
    case CM::WSTM_CONVEX_QUAD:
        ToolButton37->ImageIndex = 25;
        break;
    }
    ToolButton37->Enabled = (renderSimpleMode != CM::SM_ORIGINAL);
}

// ---------------------------------------------------------------------------
void TMainForm1::updateRenderSimpleModeChecker() {
    switch (renderSimpleMode) {
    case CM::SM_ORIGINAL:
        ToolButton41->ImageIndex = 29;
        break;
    case CM::SM_ROUGH_WALLS:
        ToolButton41->ImageIndex = 30;
        break;
    case CM::SM_SMOOTH_WALLS:
        ToolButton41->ImageIndex = 31;
        break;
    case CM::SM_SECTIONS_WALLS:
        ToolButton41->ImageIndex = 51;
        break;
    case CM::SM_OUTLINE:
        ToolButton41->ImageIndex = 52;
        break;
//	case CM::SM_CONVEX_HULL_WALLS:
//        ToolButton41->ImageIndex = 100;
//		break;
    }
}

// ---------------------------------------------------------------------------
void TMainForm1::updateTrianglesBlowMode() {
    switch (prefs.wallsTrianglesBlowMode) {
    case CM::WTBM_NONE:
        ToolButton38->ImageIndex = 26;
        break;
    case CM::WTBM_4:
        ToolButton38->ImageIndex = 33;
        break;
    case CM::WTBM_9:
        ToolButton38->ImageIndex = 34;
        break;
    case CM::WTBM_16:
        ToolButton38->ImageIndex = 35;
        break;
    case CM::WTBM_24:
        ToolButton38->ImageIndex = 36;
        break;
    case CM::WTBM_33:
        ToolButton38->ImageIndex = 37;
        break;
    case CM::WTBM_44:
        ToolButton38->ImageIndex = 38;
        break;
    case CM::WTBM_57:
        ToolButton38->ImageIndex = 39;
        break;
    }
    ToolButton38->Enabled = (renderSimpleMode != CM::SM_ORIGINAL);
}

// ---------------------------------------------------------------------------
void TMainForm1::updateWallsSegmentTriangulationStrengthCheccker() {
    switch (prefs.wallsTrianglesBlowStrength) {
    case CM::WTBS_LOW:
        ToolButton39->ImageIndex = 27;
        break;
    case CM::WTBS_MEDIUM:
        ToolButton39->ImageIndex = 40;
        break;
    case CM::WTBS_STRONG:
        ToolButton39->ImageIndex = 41;
        break;
    case CM::WTBS_HUDGE:
        ToolButton39->ImageIndex = 42;
        break;
    }
    ToolButton39->Enabled = (renderSimpleMode != CM::SM_ORIGINAL);
}

// ---------------------------------------------------------------------------
void TMainForm1::updateShowThreadChecker() {
    switch (prefs.showThread) {
    case true:
        ToolButton43->ImageIndex = 43;
        break;
    case false:
        ToolButton43->ImageIndex = 44;
        break;
    }
    ToolButton43->Enabled = (renderSimpleMode != CM::SM_ORIGINAL);
}

// ---------------------------------------------------------------------------
void TMainForm1::updateCaveViewPrefs() {
    setOgreRendererEnabled(renderSimpleMode != CM::SM_ORIGINAL);
    if (ogreRenderer) {
        ogreRenderer->setCaveViewPrefs(prefs);
        if (ogreRenderer->isEnabled()) {
            ogreRenderer->refresh();
        }
    }
}

// ---------------------------------------------------------------------------
void __fastcall TMainForm1::ToolButton35_MouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y) {
    if (Button == TMouseButton::mbLeft) {
        (int&)prefs.wallsBlowMode = (prefs.wallsBlowMode + 1) % CM::WBM_NUM;
    }
    else if (Button == TMouseButton::mbRight) {
        (int&)prefs.wallsBlowMode = prefs.wallsBlowMode - 1;
        if (prefs.wallsBlowMode < 0)
            (int&)prefs.wallsBlowMode = CM::WBM_NUM - 1;
    }
    updateWallsBlowModeChecker();
    updateCaveViewPrefs();
    ToolButton43->Enabled = (renderSimpleMode != CM::SM_ORIGINAL);
}
// ---------------------------------------------------------------------------

void __fastcall TMainForm1::ToolButton36_MouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y) {
    if (Button == TMouseButton::mbLeft) {
        (int&)prefs.wallsPropagateMode = (prefs.wallsPropagateMode + 1) % CM::WPM_NUM;
    }
    else if (Button == TMouseButton::mbRight) {
        (int&)prefs.wallsPropagateMode = prefs.wallsPropagateMode - 1;
        if (prefs.wallsPropagateMode < 0)
            (int&)prefs.wallsPropagateMode = CM::WPM_NUM - 1;
    }
    updateWallsPropagateModeChecker();
    updateCaveViewPrefs();
}

// ---------------------------------------------------------------------------
void __fastcall TMainForm1::ToolButton33_MouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y) {
    if (Button == TMouseButton::mbLeft) {
        (int&)prefs.generateWallsForNoWallsPiketsMode = (CM::GWNWPM_NUM + prefs.generateWallsForNoWallsPiketsMode + 1) % CM::GWNWPM_NUM;
    }
    else if (Button == TMouseButton::mbRight) {
        (int&)prefs.generateWallsForNoWallsPiketsMode = (CM::GWNWPM_NUM + prefs.generateWallsForNoWallsPiketsMode - 1) % CM::GWNWPM_NUM;
    }
    updategenerateWallsForNoWallsPiketsModeChecker();
    updateCaveViewPrefs();
}
// ---------------------------------------------------------------------------

void __fastcall TMainForm1::ToolButton37_MouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y) {
    if (Button == TMouseButton::mbLeft) {
        // prefs.wallsSegmentTriangulationMode = (CM::WSTM_NUM + prefs.wallsSegmentTriangulationMode + 1) % CM::WSTM_NUM;
//        prefs.allOutlineCuts = false;
		prefs.skipNum++;
	}
	else if (Button == TMouseButton::mbRight) {
		// prefs.wallsSegmentTriangulationMode = (CM::WSTM_NUM + prefs.wallsSegmentTriangulationMode - 1) % CM::WSTM_NUM;
//		prefs.allOutlineCuts = true;
        prefs.skipNum--;
    }
//    updateWallsSegmentTriangulationMode();
	updateCaveViewPrefs();
	ReinitCaveModel();
}
// ---------------------------------------------------------------------------

void __fastcall TMainForm1::ToolButton38_MouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y) {
    if (Button == TMouseButton::mbLeft) {
        (int&)prefs.wallsTrianglesBlowMode = (CM::WTBM_NUM + prefs.wallsTrianglesBlowMode + 1) % CM::WTBM_NUM;
    }
    else if (Button == TMouseButton::mbRight) {
        (int&)prefs.wallsTrianglesBlowMode = (CM::WTBM_NUM + prefs.wallsTrianglesBlowMode - 1) % CM::WTBM_NUM;
    }
    updateTrianglesBlowMode();
    updateCaveViewPrefs();
}
// ---------------------------------------------------------------------------

void __fastcall TMainForm1::ToolButton39_MouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y) {
    if (Button == TMouseButton::mbLeft) {
        (int&)prefs.wallsTrianglesBlowStrength = (CM::WTBS_NUM + prefs.wallsTrianglesBlowStrength + 1) % CM::WTBS_NUM;
    }
    else if (Button == TMouseButton::mbRight) {
        (int&)prefs.wallsTrianglesBlowStrength = (CM::WTBS_NUM + prefs.wallsTrianglesBlowStrength - 1) % CM::WTBS_NUM;
    }
    updateWallsSegmentTriangulationStrengthCheccker();
    updateCaveViewPrefs();
}

// ---------------------------------------------------------------------------
void __fastcall TMainForm1::ToolButton32MouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y) {
    if (Button == TMouseButton::mbLeft) {
        (int&)prefs.ambientLightStrength = (CM::ALS_NUM + prefs.ambientLightStrength + 1) % CM::ALS_NUM;
    }
    else if (Button == TMouseButton::mbRight) {
        (int&)prefs.ambientLightStrength = (CM::ALS_NUM + prefs.ambientLightStrength - 1) % CM::ALS_NUM;
    }
    updateCaveViewPrefs();
}

// ---------------------------------------------------------------------------
void __fastcall TMainForm1::ToolButton47MouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y) {
    if (Button == TMouseButton::mbLeft) {
        (int&)prefs.wallColoringMode = (CM::WCM_NUM + prefs.wallColoringMode + 1) % CM::WCM_NUM;
    }
    else if (Button == TMouseButton::mbRight) {
        (int&)prefs.wallColoringMode = (CM::WCM_NUM + prefs.wallColoringMode - 1) % CM::WCM_NUM;
    }
    updateWallsColorModeChecker();
    updateCaveViewPrefs();
}

// ---------------------------------------------------------------------------
void __fastcall TMainForm1::ToolButton40_Click(TObject *Sender) {
    if (!FillTimer->Enabled) {
        prefs.fillRate = -0.2;
        updateCaveViewPrefs();
        FillTimer->Enabled = true;
    }
    else {
        prefs.fillRate = 1;
        updateCaveViewPrefs();
        FillTimer->Enabled = false;
    }
}

// ---------------------------------------------------------------------------
void __fastcall TMainForm1::FillTimerTimer(TObject *Sender) {
    prefs.fillRate += 0.2 / 5;
    updateCaveViewPrefs();
    if (prefs.fillRate >= 1.0f) {
        FillTimer->Enabled = false;
    }
}
// ---------------------------------------------------------------------------

void TMainForm1::updateOgreCheckers() {
    updateWallsShadowModeChecker();
    updateWallsBlowModeChecker();
    updateWallsPropagateModeChecker();
    updategenerateWallsForNoWallsPiketsModeChecker();
    updateWallsSurfaceModeChecker();
    updateWallsSegmentTriangulationMode();
    updateWallsSegmentTriangulationStrengthCheccker();
    updateRenderSimpleModeChecker();
    updateTrianglesBlowMode();
    updateShowThreadChecker();
    updateCaveViewPrefs();
    updateWallsColorModeChecker();

    ToolBar2->Visible = ToolButton44->Down && (renderSimpleMode != CM::SM_ORIGINAL); ;
    ToolBar3->Visible = ToolButton45->Down && (renderSimpleMode != CM::SM_ORIGINAL); ;
    ToolButton46->Down = prefs.showDebug;
    ToolButton44->Enabled = (renderSimpleMode != CM::SM_ORIGINAL);
    ToolButton45->Enabled = (renderSimpleMode != CM::SM_ORIGINAL);

    ToolButton1->Enabled = (renderSimpleMode == CM::SM_ORIGINAL);
    ToolButton24->Enabled = (renderSimpleMode == CM::SM_ORIGINAL);
    ToolButton23->Enabled = (renderSimpleMode == CM::SM_ORIGINAL);
    ToolButton25->Enabled = (renderSimpleMode == CM::SM_ORIGINAL);
    ToolButton31->Enabled = (renderSimpleMode == CM::SM_ORIGINAL);
    ToolButton14->Enabled = (renderSimpleMode == CM::SM_ORIGINAL);
    ToolButton12->Enabled = (renderSimpleMode == CM::SM_ORIGINAL); 
    ToolButton48->Enabled = (renderSimpleMode == CM::SM_ORIGINAL);

    updateToolBarsPositions();
}

// ---------------------------------------------------------------------------
void TMainForm1::updateToolBarsPositions() {
    float left = WallsBar->Left + WallsBar->Width;
    if (ToolBar2->Visible) {
        ToolBar2->Left = left;
        left += ToolBar2->Width;
    }
    else {
        ToolBar2->Left = 999;
    }

    if (ToolBar3->Visible) {
        ToolBar3->Left = left;
        left += ToolBar3->Width;
    }
    else {
        ToolBar3->Left = 999;
    }

    if (SurfBar->Visible) {
        SurfBar->Left = left;
        left += SurfBar->Width;
    }
    else {
        SurfBar->Left = 999;
    }
}

// ---------------------------------------------------------------------------
void __fastcall TMainForm1::ToolButton41MouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y) {
    if (Button == TMouseButton::mbLeft) {
        (int&)renderSimpleMode = (CM::SM_NUM + renderSimpleMode + 1) % CM::SM_NUM;
    }
    else if (Button == TMouseButton::mbRight) {
        (int&)renderSimpleMode = (CM::SM_NUM + renderSimpleMode - 1) % CM::SM_NUM;
    }

    if (renderSimpleMode == CM::SM_ROUGH_WALLS) {
        OgreRenderer::updateForRoughWallsMode(prefs);
    }
    else if (renderSimpleMode == CM::SM_SMOOTH_WALLS) {
        OgreRenderer::updateForSmoothWallsMode(prefs);
    }
    else if (renderSimpleMode == CM::SM_SECTIONS_WALLS) {
        OgreRenderer::updateForSectionsWallsMode(prefs);
    }
    else if (renderSimpleMode == CM::SM_OUTLINE) {
        OgreRenderer::updateForOutlineMode(prefs);
	}
//	else if (renderSimpleMode == CM::SM_CONVEX_HULL_WALLS) {
//		OgreRenderer::updateForConvexRoughWallsMode(prefs);
//	}

    updateOgreCheckers();
    updateCaveViewPrefs();
}

// ---------------------------------------------------------------------------
void __fastcall TMainForm1::ToolButton43MouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y) {
    prefs.showThread = !prefs.showThread;

    updateOgreCheckers();
    updateCaveViewPrefs();
}
// ---------------------------------------------------------------------------

void __fastcall TMainForm1::ToolButton44Click(TObject *Sender) {
    updateOgreCheckers();
}

// ---------------------------------------------------------------------------
void __fastcall TMainForm1::ToolButton45Click(TObject *Sender) {
    updateOgreCheckers();
}

// ---------------------------------------------------------------------------
void __fastcall TMainForm1::ToolButton46Click(TObject *Sender) {
    prefs.showDebug = !prefs.showDebug;
    updateOgreCheckers();
}
// ---------------------------------------------------------------------------
void __fastcall TMainForm1::FormClose(TObject *Sender, TCloseAction &Action) {
    delete ogreRenderer;
    ogreRenderer = NULL;
}
// ---------------------------------------------------------------------------
void __fastcall TMainForm1::RebuildOutlineTimerTimer(TObject *Sender) {
    if (ogreRenderer && ogreRenderer->isEnabled()) {
        ogreRenderer->update(0.05);
    }
}
// ---------------------------------------------------------------------------
void __fastcall TMainForm1::ToolButton48Click(TObject *Sender)
{
    if (!TrackForm->Visible) {
        TrackForm->Show();  
        TrackForm->Reset();
        ToolButton48->Down = true; 
        ToolButton23->Down = false;
    } else {
        TrackForm->Close();       
        ToolButton48->Down = false;
    }

}
//---------------------------------------------------------------------------






