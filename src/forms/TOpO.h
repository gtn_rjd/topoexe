﻿//---------------------------------------------------------------------------
#ifndef TOpOH
#define TOpOH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Menus.hpp>
#include <Dialogs.hpp>
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
#include <Buttons.hpp>
#include <ToolWin.hpp>
#include <ImgList.hpp>
#include <ExtDlgs.hpp>
#include <Graphics.hpp>
#include <OgreCMRenderer.h>
#include <CaveData.h>

#include <Registry.hpp>
#include <System.ImageList.hpp>

#define FILES_HISTORY_COUNT 5

enum StateEnum {         
    STATE_NONE = 0,
    STATE_OPENED = 1,
    STATE_EDIT_NEW = 2,
    STATE_VIEW_EDIT = 3,
    STATE_BUILD = 4, 
    STATE_VIEW_NORMAL = 5,
    STATE_VIEW_EXT_PROFILE = 6
};

//---------------------------------------------------------------------------
class TMainForm1 : public TForm
{
__published:	// IDE-managed Components
        TMainMenu *MainMenu1;
        TMenuItem *File1;
        TMenuItem *Openfile1;
        TMenuItem *Exit1;
        TMenuItem *Help1;
        TOpenDialog *OpenDialog1;
        TMenuItem *Data1;
        TMenuItem *Commands;
        TMenuItem *AboutProgram1;
        TMenuItem *GeneralOverview1;
        TStatusBar *StatusBar1;
        TMenuItem *Print1;
        TMenuItem *Edit1;
        TMenuItem *Build;
        TMenuItem *View1;
        TMenuItem *sep1;
        TMenuItem *N1;
        TEdit *Edit2;
        TImage *Image1;
        TEdit *Edit3;
        TLabel *Scale;
        TControlBar *ControlBar1;
        TToolBar *RotBar;
        TToolButton *StepLeft;
        TToolButton *ToolButton2;
        TToolButton *ToolButton3;
        TToolButton *ToolButton4;
        TToolButton *Stop;
        TToolButton *ToolButton7;
        TToolButton *StepRight;
        TToolButton *PlayRight;
        TToolButton *ToolButton9;
        TToolButton *ToolButton10;
        TToolButton *PlayLeft;
        TToolButton *ToolButton12;
        TToolButton *ToolButton13;
        TToolButton *ToolButton14;
        TToolButton *ToolButton15;
        TImageList *RotImages;
        TToolBar *ZoomBar;
        TToolButton *ToolButton1;
        TToolButton *ToolButton5;
        TMenuItem *Viewtools1;
        TMenuItem *RotationControls1;
        TMenuItem *ShiftControls1;
        TToolBar *ShiftBar;
        TToolButton *ToolButton6;
        TToolButton *ToolButton8;
        TToolButton *ToolButton11;
        TToolButton *ToolButton16;
        TToolButton *ToolButton17;
        TToolButton *ToolButton18;
        TToolButton *ToolButton19;
        TToolButton *ToolButton20;
        TToolButton *ToolButton21;
        TToolButton *ToolButton22;
        TToolButton *ToolButton23;
        TImageList *TransImages;
        TImageList *ZoomImage;
        TToolBar *ToolBar1;
        TToolButton *ToolButt1;
        TToolButton *ToolButt2;
        TToolButton *ToolButt3;
        TToolButton *ToolButt4;
        TToolButton *ToolButt5;
        TImageList *ImageList1;
        TMenuItem *BasicTool1;
        TMenuItem *N2;
        TTimer *Timer1;
        TMenuItem *Compass1;
        TMenuItem *HelpView;
        TLabel *LabelXYZ;
        TSavePictureDialog *SavePictureDialog1;
        TMenuItem *New1;
        TMenuItem *N3;
        TTimer *Timer2;
        TMenuItem *Corrections1;
        TMenuItem *Print2;
        TMenuItem *Loops1;
        TMenuItem *Profile;
        TMenuItem *InfoTools1;
        TToolButton *ToolButton24;
        TToolButton *ToolButton25;
        TEdit *Edit4;
        TCheckBox *CheckBox1;
        TCheckBox *CheckBox2;
        TStaticText *StaticTextFound;
        TMenuItem *Export1;
        TMenuItem *N2Dpsbmp1;
        TMenuItem *N3Dvrml1;
        TToolButton *ToolButton26;
        TMenuItem *N4;
        TMenuItem *N5;
        TMenuItem *File_1;
        TMemo *ListFileNameHystory;
        TMenuItem *File_2;
        TMenuItem *File_3;
        TMenuItem *File_4;
        TMenuItem *File_5;
        TMenuItem *SurfGeometry1;
        TToolBar *SurfBar;
        TToolButton *ToolButton27;
        TToolButton *ToolButton28;
        TToolButton *ToolButton29;
        TToolButton *ToolButton30;
        TImageList *SurfImage;
        TMenuItem *SurfaceTool1;
        TToolButton *ToolButton31;
        TMenuItem *Walls1;
    TImageList *ImageList2;
    TToolBar *WallsBar;
    TImage *Image2;
    TTimer *FillTimer;
    TToolButton *ToolButton41;
    TToolBar *ToolBar2;
    TToolButton *ToolButton34;
    TToolButton *ToolButton38;
    TToolButton *ToolButton39;
    TToolBar *ToolBar3;
    TToolButton *ToolButton35;
    TToolButton *ToolButton36;
    TToolButton *ToolButton37;
    TToolButton *ToolButton42;
    TToolButton *ToolButton32;
    TToolButton *ToolButton43;
    TToolButton *ToolButton44;
    TToolButton *ToolButton45;
    TToolButton *ToolButton46;
    TToolButton *ToolButton47;
	TTimer *RebuildOutlineTimer;
	TToolButton *ToolButton33;
	TToolButton *ToolButton40;
        TToolButton *ToolButton48;
    TMenuItem *N6;
    TMenuItem *N7;
    TMenuItem *N8;
    TMenuItem *N9;
        void __fastcall Openfile1Click(TObject *Sender);
        void __fastcall BuildClick(TObject *Sender);
        void __fastcall Exit1Click(TObject *Sender);
        void __fastcall AboutProgram1Click(TObject *Sender);
        void __fastcall View1Click(TObject *Sender);
        void __fastcall Print1Click(TObject *Sender);
        void __fastcall CommandsClick(TObject *Sender);
        void __fastcall FormResize(TObject *Sender);
        void __fastcall Data1Click(TObject *Sender);
        void __fastcall GeneralOverview1Click(TObject *Sender);
        void __fastcall Edit1Click(TObject *Sender);
        void __fastcall StopClick(TObject *Sender);
        void __fastcall PlayLeftClick(TObject *Sender);
        void __fastcall PlayRightClick(TObject *Sender);
        void __fastcall ToolButton10Click(TObject *Sender);
        void __fastcall ToolButton15Click(TObject *Sender);
        void __fastcall ToolButton9Click(TObject *Sender);
        void __fastcall RotationControls1Click(TObject *Sender);
        void __fastcall Viewtools1Click(TObject *Sender);
        void __fastcall ShiftControls1Click(TObject *Sender);
        void __fastcall ControlBar1Resize(TObject *Sender);
        void __fastcall ToolButton19Click(TObject *Sender);
        void __fastcall ToolButton20Click(TObject *Sender);
        void __fastcall BasicTool1Click(TObject *Sender);
        void __fastcall RotUpMouseDown(TObject *Sender,
          TMouseButton Button, TShiftState Shift, int X, int Y);
        void __fastcall ButtonMouseUp(TObject *Sender,
          TMouseButton Button, TShiftState Shift, int X, int Y);
        void __fastcall Timer1Timer(TObject *Sender);
        void __fastcall RotLeftMouseDown(TObject *Sender,
          TMouseButton Button, TShiftState Shift, int X, int Y);
        void __fastcall RotDownMouseDown(TObject *Sender,
          TMouseButton Button, TShiftState Shift, int X, int Y);
        void __fastcall RotRightMouseDown(TObject *Sender,
          TMouseButton Button, TShiftState Shift, int X, int Y);
        void __fastcall ShiftLeftMDown(TObject *Sender,
          TMouseButton Button, TShiftState Shift, int X, int Y);
        void __fastcall ShiftRightMDown(TObject *Sender,
          TMouseButton Button, TShiftState Shift, int X, int Y);
        void __fastcall ShiftUpMDown(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y);
        void __fastcall ShiftDownMDown(TObject *Sender,
          TMouseButton Button, TShiftState Shift, int X, int Y);
        void __fastcall ZoomMMDown(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y);
        void __fastcall ZoomPMDown(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y);
        void __fastcall FormKeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);
        void __fastcall Compass1Click(TObject *Sender);
		void __fastcall HelpViewClick(TObject *Sender);
        void __fastcall Image1MouseMove(TObject *Sender, TShiftState Shift,
          int X, int Y);
        void __fastcall ToolButton4Click(TObject *Sender);
        void __fastcall ToolButton2Click(TObject *Sender);
        void __fastcall ToolButton7Click(TObject *Sender);
        void __fastcall FormCloseQuery(TObject *Sender, bool &CanClose);
        void __fastcall ToolButton12Click(TObject *Sender);
        void __fastcall New1Click(TObject *Sender);
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall Timer2Timer(TObject *Sender);
        void __fastcall Corrections1Click(TObject *Sender);
        void __fastcall Print2Click(TObject *Sender);
        void __fastcall ToolButton14Click(TObject *Sender);
        void __fastcall Loops1Click(TObject *Sender);
        void __fastcall ToolButton5Click(TObject *Sender);
        void __fastcall ToolButton1Click(TObject *Sender);
        void __fastcall Image1MouseDown(TObject *Sender,
          TMouseButton Button, TShiftState Shift, int X, int Y);
        void __fastcall ProfileClick(TObject *Sender);
        void __fastcall FormMouseWheelDown(TObject *Sender,
          TShiftState Shift, TPoint &MousePos, bool &Handled);
        void __fastcall FormMouseWheelUp(TObject *Sender,
          TShiftState Shift, TPoint &MousePos, bool &Handled);
        void __fastcall ToolButton22Click(TObject *Sender);
        void __fastcall ToolButton23Click(TObject *Sender);
        void __fastcall InfoTools1Click(TObject *Sender);
        void __fastcall Image1Click(TObject *Sender);
        void __fastcall ToolButton24Click(TObject *Sender);
        void __fastcall Edit4KeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);
        void __fastcall CheckBox1Click(TObject *Sender);
        void __fastcall CheckBox2Click(TObject *Sender);
        void __fastcall ToolButton25Click(TObject *Sender);
        void __fastcall N2Dpsbmp1Click(TObject *Sender);
        void __fastcall N3Dvrml1Click(TObject *Sender);
        void __fastcall ToolButton26Click(TObject *Sender);
        void __fastcall Settings1Click(TObject *Sender);
        void __fastcall FormActivate(TObject *Sender);
        void __fastcall File_5Click(TObject *Sender);
        void __fastcall File_1Click(TObject *Sender);
        void __fastcall File_2Click(TObject *Sender);
        void __fastcall File_3Click(TObject *Sender);
        void __fastcall File_4Click(TObject *Sender);
        void __fastcall SurfGeometry1Click(TObject *Sender);
        void __fastcall ToolButton30Click(TObject *Sender);
        void __fastcall ToolButton29Click(TObject *Sender);
        void __fastcall ToolButton27Click(TObject *Sender);
        void __fastcall ToolButton28Click(TObject *Sender);
        void __fastcall SurfaceTool1Click(TObject *Sender);
        void __fastcall ToolButton31Click(TObject *Sender);
        void __fastcall Walls1Click(TObject *Sender);
	void __fastcall ToolButton32MouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y);
	void __fastcall FormPaint(TObject *Sender);
        void __fastcall ToolButton34_Click(TObject *Sender);
        void __fastcall ToolButton32Click(TObject *Sender);
        void __fastcall ToolButton35_MouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
              int X, int Y);
        void __fastcall ToolButton36_MouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
              int X, int Y);
        void __fastcall ToolButton33_MouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
              int X, int Y);
        void __fastcall ToolButton37_MouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
              int X, int Y);
        void __fastcall ToolButton38_MouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
              int X, int Y);
        void __fastcall ToolButton39_MouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
              int X, int Y);
        void __fastcall ToolButton32MouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
              int X, int Y);
        void __fastcall ToolButton40_Click(TObject *Sender);
        void __fastcall FillTimerTimer(TObject *Sender);
        void __fastcall ToolButton41MouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
              int X, int Y);
        void __fastcall ToolButton43MouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
              int X, int Y);
        void __fastcall ToolButton44Click(TObject *Sender);
        void __fastcall ToolButton45Click(TObject *Sender);
        void __fastcall ToolButton46Click(TObject *Sender);
        void __fastcall ToolButton47MouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y);
	void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
	void __fastcall RebuildOutlineTimerTimer(TObject *Sender);
        void __fastcall ToolButton48Click(TObject *Sender);

private:	// User declarations

        void EnableView(char ev);
        void init_par();
        void projection(Graphics::TBitmap *pBitmap);
        void Act(char moda);
protected:

public:		// User declarations
        void updateOgreCheckers();
        void updateWallsShadowModeChecker();  
        void updateWallsBlowModeChecker();   
        void updateWallsPropagateModeChecker(); 
        void updategenerateWallsForNoWallsPiketsModeChecker();
        void updateWallsSurfaceModeChecker(); 
        void updateWallsSegmentTriangulationMode(); 
        void updateWallsSegmentTriangulationStrengthCheccker();
        void updateTrianglesBlowMode(); 
        void updateRenderSimpleModeChecker();   
        void updateShowThreadChecker();      
        void updateWallsColorModeChecker(); 
        void updateCaveViewPrefs();     

        void updateToolBarsPositions();

        void setOgreRendererEnabled(bool enbld);
        void reDrawKart();
        
        __fastcall TMainForm1(TComponent* Owner);

        Graphics::TBitmap *pBitmap;
        void kart(Graphics::TBitmap *pBitmap);
        TColor BgColor, ComColor, LabelColor;
        bool FlagWallsColor;
        UnicodeString cave, region;
        UnicodeString CurrFileName;
        void  __fastcall OpenCurrFile(UnicodeString CurrFileName_);

        unsigned int sur_col;   // цвет пов. топо
        unsigned int dup_col;   // цвет dup. топо
        double VStep;           // шаг вращени¤ по вертикали
        unsigned int Razmer;    // число пикетов
        unsigned int WRazmer;     // число пикетов на стены (ежик) 
        StateEnum State;
        void SetState(StateEnum ST);
        void AboutCave(char k);
        UnicodeString Name(UnicodeString Astr, unsigned int N);
        unsigned int CaveMouse(Graphics::TBitmap *&pBitmap,int X, int Y, bool drawPrev);
	void  __fastcall ShowInfoForCurrPicket(unsigned int s1);//выводим на Ёкран »нфу про текущий пикет
        OgreRenderer* ogreRenderer;
        
        CaveViewPrefs prefs;
	RenderSimpleMode renderSimpleMode;

};


struct WPR   {long X,Y;};  //проекции стен
struct Vet   {int B, E;};
struct Srv   {int File, Prefix;};
struct PCube {long X,Y,Z;}; // bounding box
//---------------------------------------------------------------------------
extern PACKAGE TMainForm1 *MainForm1;
extern PACKAGE P3D *P;    
extern PACKAGE unsigned int size;
extern PACKAGE W3D *W;
extern PACKAGE Vet *V;
extern PACKAGE UnicodeString Labels;
extern PACKAGE UnicodeString Comments;
extern PACKAGE UnicodeString WComments;

//---------------------------------------------------------------------------
#endif
