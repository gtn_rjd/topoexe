﻿//---------------------------------------------------------------------------

#ifndef UnitInsertNewPiketH
#define UnitInsertNewPiketH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Grids.hpp>
#include <Buttons.hpp>
//---------------------------------------------------------------------------
class TFormInsertNewPiket : public TForm
{
__published:	// IDE-managed Components
        TStringGrid *StringGrid1;
        TBitBtn *BitBtnAddRaw;
        void __fastcall BitBtnAddRawClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall TFormInsertNewPiket(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFormInsertNewPiket *FormInsertNewPiket;
//---------------------------------------------------------------------------
#endif
