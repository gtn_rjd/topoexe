﻿//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "UnitPrintScalePosition.h"
#include "FormPrn.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFormPrintScalePosition *FormPrintScalePosition;
//---------------------------------------------------------------------------
__fastcall TFormPrintScalePosition::TFormPrintScalePosition(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TFormPrintScalePosition::FormActivate(TObject *Sender)
{
  EditScale->Text =  PrnForm->mashtab*100;
  EditPrintedScale->Text =  PrnForm->mashtab;
  RealScale = OldRealScale = PrnForm->mashtab;
}
//---------------------------------------------------------------------------
void __fastcall TFormPrintScalePosition::EditPrintedScaleChange(
      TObject *Sender)
{
  try
  {
    RealScale = StrToFloat(EditPrintedScale->Text);
    EditScale->Text = RealScale* 100.;
  }
  catch(...)
  {
    RealScale = StrToFloat(EditScale->Text)/100.;
  }
  Caption = L"M 1:" +FloatToStr(RealScale*100);
}
//---------------------------------------------------------------------------
void __fastcall TFormPrintScalePosition::EditScaleChange(TObject *Sender)
{
  try
  {
    RealScale = StrToFloat(EditScale->Text)/100.;
    EditPrintedScale->Text = RealScale;
  }
  catch(...)
  {
    RealScale = StrToFloat(EditPrintedScale->Text);
  }
  Caption = L"M 1:" +FloatToStr(RealScale*100);
}
//---------------------------------------------------------------------------
void __fastcall TFormPrintScalePosition::BitBtnOKClick(TObject *Sender)
{
 if ( RealScale < 0.5 )
   Application->MessageBox(L"Некорректный масштаб.", L"TOpO Print: Error", MB_OK);
 else
   PrnForm->mashtab = RealScale;

}
//---------------------------------------------------------------------------
void __fastcall TFormPrintScalePosition::BitBtnCancelClick(TObject *Sender)
{
PrnForm->mashtab = OldRealScale;
}
//---------------------------------------------------------------------------
