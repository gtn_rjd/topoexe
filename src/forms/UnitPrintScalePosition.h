﻿//---------------------------------------------------------------------------

#ifndef UnitPrintScalePositionH
#define UnitPrintScalePositionH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
//---------------------------------------------------------------------------
class TFormPrintScalePosition : public TForm
{
__published:	// IDE-managed Components
        TPanel *Panel1;
        TGroupBox *GroupBox1;
        TLabel *Label1;
        TLabel *Label2;
        TLabel *Label3;
        TEdit *EditScale;
        TEdit *EditPrintedScale;
        TBitBtn *BitBtnOK;
        TBitBtn *BitBtnCancel;
        void __fastcall FormActivate(TObject *Sender);
        void __fastcall EditPrintedScaleChange(TObject *Sender);
        void __fastcall EditScaleChange(TObject *Sender);
        void __fastcall BitBtnOKClick(TObject *Sender);
        void __fastcall BitBtnCancelClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall TFormPrintScalePosition(TComponent* Owner);
        double RealScale;
        double OldRealScale;
};
//---------------------------------------------------------------------------
extern PACKAGE TFormPrintScalePosition *FormPrintScalePosition;
//---------------------------------------------------------------------------
#endif
