﻿#include "vrml.h"

void vrmlPreambula(std::wostream& s) {
	s <<  L"#VRML V2.0 utf8\n";
	s <<  L"\n";
	s <<  L"Transform { children [\n";
	s <<  L"\n";
}

void vrmlStartPath(std::wostream& s, const Color& color) {
	std::wstring col = std::to_wstring(color.r) + L" " + std::to_wstring(color.g) + L" " + std::to_wstring(color.b);
	vrmlStartPath(s, col.c_str());
}

void vrmlStartPath(std::wostream& s, const wchar_t* color) {
	s << L"Shape {\n";
	s << L"appearance Appearance {\n";
	s << L"   material Material {\n";
	s << L"      emissiveColor " << color << L"\n";
	s << L"     }\n";
	s << L"   }\n";
	s << L"geometry IndexedLineSet {\n";
	s << L"coord Coordinate {\n";
	s << L"   point [\n";
}

void vrmlEndPath(std::wostream& s, int vNum) {
	s << L"   ]\n";
	s << L"}\n";
	s << L"   coordIndex [ ";
	for(int i=0; i < vNum; i++)
		s << i << L",L";
	s << L"-1,L";
	s << L"   ]\n";
	s << L"}\n";
	s << L"}\n";
	s << L"\n\n";
}

void vrmlPoint(std::wostream& s, V3 p) {
	s << p.y << L" " << p.x << L" " << p.z << L",\n";
}

void vrmlAABB(std::wostream& s, V3 min, V3 max) {
	s << L"Shape {\n";
	s << L"appearance Appearance {\n";
	s << L"   material Material {\n";
	s << L"      emissiveColor 1.0 0.0 0.0\n";
	s << L"     }\n";
	s << L"   }\n";
	s << L"geometry IndexedLineSet {\n";
	s << L"coord Coordinate {\n";
	s << L"   point [\n";
	s << min.y << L" " << min.x << L" " << min.z <<"\n";
	s << max.y << L" " << min.x << L" " << min.z <<"\n";
	s << max.y << L" " << max.x << L" " << min.z <<"\n";
	s << min.y << L" " << max.x << L" " << min.z <<"\n";
	s << min.y << L" " << min.x << L" " << max.z <<"\n";
	s << max.y << L" " << min.x << L" " << max.z <<"\n";
	s << max.y << L" " << max.x << L" " << max.z <<"\n";
	s << min.y << L" " << max.x << L" " << max.z <<"\n";
	s << L"   ]\n";
	s << L"}\n";
	s << L"coordIndex [\n";
	s << L"0,1,2,3,0,4,5,6,7,4,-1,\n";
	s << L"1,5,-1,\n";
	s << L"2,6,-1,\n";
	s << L"3,7,-1\n";
	s << L"   ]\n";
	s << L"}\n";
	s << L"}\n";
	s << L"]\n";
	s << L"\n";
	s << L"translation ";
	s << -(max.y + min.y)/2 << L" ";
	s << -(max.x + min.x)/2 << L" ";
	s << -(max.z + min.z)/2;
	s << L" } #end of transf. to center\n\n";
}

void vrmlViewPoint(std::wostream& s, const wchar_t* descr, V3 p, V3 orientationAxis, float orientationAngle, V3 rotateAxis, float rotateAngle) {
	if (!rotateAxis.isZeroLength()) {
		s << L"Transform {\n";
		s << L"   rotation ";
		s << rotateAxis.x << L" ";
		s << rotateAxis.y << L" ";
		s << rotateAxis.z << L" ";
		s << rotateAngle;
		s << L"   children\n";
	}
	s << L"Viewpoint {\n";
	s << L"   fieldOfView 0.42\n";
	s << L"   jump TRUE\n";
	s << L"   position " << p.x << L" " << p.y << L" " <<p.z << L"\n";
	if (!orientationAxis.isZeroLength()) {
		s << L"   orientation ";
		s << orientationAxis.x << L" ";
		s << orientationAxis.y << L" ";
		s << orientationAxis.z << L" ";
		s << orientationAngle << L"\n	";
	}
	s << L"   description \"" << descr << L"\" \n";
	s << L"}\n";

	if (!rotateAxis.isZeroLength()) {
		s << L"}\n";
	}
	s << L"\n";
}

void vrmlViewPoints(std::wostream& f, V3 min, V3 max) {
	V3 sz(fabs(max.x - min.x), fabs(max.y - min.y), fabs(max.z - min.z));

	vrmlViewPoint(f, L"Up", V3(0, 0,  (2*sz.x+ 2*sz.y + sz.z) * 0.5f));
	vrmlViewPoint(f, L"Down", V3(0, 0, (-2*sz.x - 2*sz.y - sz.z) * 0.5f), V3(1, 0, 0), M_PI);
	vrmlViewPoint(f, L"North",  V3(0, (-sz.x- 2*sz.y - 2*sz.z) * 0.5f, 0), V3(1, 0, 0), M_PI_2);

	vrmlViewPoint(f, L"South", V3(0, (-sz.x - 2*sz.y - 2*sz.z) * 0.5f, 0), V3(1, 0, 0), M_PI_2, V3(0, 0, 1), M_PI);
	vrmlViewPoint(f, L"West",  V3(0, (-2*sz.x - sz.y - 2*sz.z) * 0.5f, 0), V3(1, 0, 0), M_PI_2, V3(0, 0, 1), M_PI_2);
	vrmlViewPoint(f, L"East",  V3(0, (-2*sz.x - sz.y - 2*sz.z) * 0.5f, 0), V3(1, 0, 0), M_PI_2, V3(0, 0, 1), -M_PI_2);
}

void vrmlNavigation(std::wostream& s, float speed) {
	s << L"NavigationInfo {\n";
	s << L"headlight TRUE\n";
	s << L"speed " << speed << L" \n";
	s << L"}\n\n";
}

void vrmlBackground(std::wostream& s, float r, float g, float b) {
	s << L"Background {skyColor ";
	s << r << L" ";
	s << g << L" ";
	s << b << L" ";
	s << L"}\n\n";
}

void vrmlPoly(std::wostream& s, const Color& color, V3 p0, V3 p1, V3 p2) {
	s << L"Shape {\n";
	s << L"	appearance Appearance {\n";
	s << L"	   material Material {\n";
	s << L"	      diffuseColor  " << std::to_wstring(color.r) + L" " + std::to_wstring(color.g) + L" " + std::to_wstring(color.b) << L"\n";
	s << L"	     }\n";
	s << L"	   }\n";
	s << L"	geometry IndexedFaceSet {\n";
	s << L"	coord Coordinate {\n";
	s << L"	   point [\n";
	s << p0.y << L"		 " << p0.x << L" " << p0.z << L",\n";
	s << p1.y << L"		 " << p1.x << L" " << p1.z << L",\n";
	s << p2.y << L" 		 " << p2.x << L" " << p2.z << L",\n";
	s << L"	   ]\n";
	s << L"	}\n";
	s << L"	coordIndex [0,2,1,-1]\n";
	s << L"	}\n";
	s << L"}\n";
}

void vrmlPoly(std::wostream& s, const std::vector<Color>& color, std::vector<V3> pt, std::vector<int> idx) {
	s << L"Shape {\n";
	s << L"	appearance Appearance {\n";
	s << L"	   material Material {\n";
	//s << L"	      diffuseColor  " << std::to_wstring(color.r) + L" " + std::to_wstring(color.g) + L" " + std::to_wstring(color.b) << L"\n";
	s << L"	     }\n";
	s << L"	   }\n";
	s << L"	geometry IndexedFaceSet {\n";
	s << L"	coord Coordinate {\n";
	s << L"	   point [\n";

	for (const auto& p : pt) {
		s << p.y << L" " << p.x << L" " << p.z << L",\n";
	}

	s << L"	   ]\n";
	s << L"	}\n";
	s << L"	coordIndex [";

	for (int i = 0; i < idx.size(); i+=3) {
		s << idx[i] << L",L" << idx[i + 1] << L",L" << idx[i + 2] << L",-1,L";

	}

	s << L"]\n";

	s << L"	color Color {\n";
	s << L"	   color [\n";

	for (const auto& c : color) {
		s << std::to_wstring(c.r) + L" " + std::to_wstring(c.g) + L" " + std::to_wstring(c.b)<< L",\n";
	}

	s << L"	   ]\n";
	s << L"	}\n";

	s << L"	}\n";
	s << L"}\n";
}


















