﻿#pragma once
#include <iostream>
#include "CMTypes.h"

using namespace CM;

void vrmlPreambula(std::wostream& s);
void vrmlStartPath(std::wostream& s, const wchar_t* color);
void vrmlStartPath(std::wostream& s, const Color& color);
void vrmlEndPath(std::wostream& s, int vNum);
void vrmlPoint(std::wostream& s, V3 p);
void vrmlAABB(std::wostream& s, V3 min, V3 max);
void vrmlViewPoint(std::wostream& s, const wchar_t* descr, V3 p, V3 orientationAxis = V3::ZERO, float orientationAngle = 0, V3 rotateAxis = V3::ZERO, float rotateAngle = 0);
void vrmlViewPoints(std::wostream& f, V3 min, V3 max);
void vrmlNavigation(std::wostream& s, float speed);
void vrmlBackground(std::wostream& s, float r, float g, float b);
void vrmlPoly(std::wostream& s, const Color& color, V3 p0, V3 p1, V3 p2);
void vrmlPoly(std::wostream& s, const std::vector<Color>& color, std::vector<V3> pt, std::vector<int> idx);
