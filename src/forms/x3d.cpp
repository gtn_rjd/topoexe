﻿#include "x3d.h"

void x3dPreambula(std::wostream& s) {
	s <<  L"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
	s <<  L"\n";
	s << L"<X3D profile='Immersive' version='3.3'> ";
	s <<  L"\n";
	s <<  L"<Scene>\n";


}

void x3dStartPath(std::wostream& s, const Color& color) {
	std::wstring col = std::to_wstring(color.r) + L" " + std::to_wstring(color.g) + L" " + std::to_wstring(color.b);
	x3dStartPath(s, col.c_str());
}

void x3dStartPath(std::wostream& s, const wchar_t* color) {
	s <<  L"<Transform>\n";
	s << L"<Shape> \n";
	s << L"<Appearance>\n";
	s << L"   <Material emissiveColor='"<< color <<"'/>\n";

	s << L"     </Appearance>\n";

	s << L"<IndexedLineSet\n";

}

void x3dEndPath(std::wostream& s, int vNum) {
	s << L"coordIndex=' ";
	for(int i=0; i < vNum; i++)
		s << i << L" ";
	s << L"-1'";
	s << L">\n";
	s << L"<Coordinate point='";
}

void x3dcoord(std::wostream& s) {
	s << L"'/>\n";
	s << L"</IndexedLineSet>\n" ;
	s <<  L"</Shape> \n"         ;
	s <<  L"</Transform>\n"       ;

}

void x3dPoint(std::wostream& s, V3 p) {
	s << p.y << L" " << p.x << L" " << p.z << L"\n";
}

void x3dAABB(std::wostream& s, V3 min, V3 max) {
	s <<   "<Transform>  ";
   /*	s << L"translation=' ";
	s << -(max.y + min.y)/2 << L" ";
	s << -(max.x + min.x)/2 << L" ";
	s << -(max.z + min.z)/2;
	s <<   "'>\n";  */
	s << L"<Shape> \n";
	s << L"<Appearance>\n";
	s << L"   <Material emissiveColor='1.0 0.0 0.0'/>\n";
	s << L"     </Appearance>\n";

	s << L"<IndexedLineSet\n";
	s << L"coordIndex ='\n";
	s << L"0 1 2 3 0 4 5 6 7 4 -1\n";
	s << L"1 5 -1 \n";
	s << L"2 6 -1 \n";
	s << L"3 7 -1\n";
	s <<"'>\n";
	s << L"<Coordinate point='";
	s << min.y << L" " << min.x << L" " << min.z <<"\n";
	s << max.y << L" " << min.x << L" " << min.z <<"\n";
	s << max.y << L" " << max.x << L" " << min.z <<"\n";
	s << min.y << L" " << max.x << L" " << min.z <<"\n";
	s << min.y << L" " << min.x << L" " << max.z <<"\n";
	s << max.y << L" " << min.x << L" " << max.z <<"\n";
	s << max.y << L" " << max.x << L" " << max.z <<"\n";
	s << min.y << L" " << max.x << L" " << max.z <<"\n";
	s << L"'/>\n";
	s << L"</IndexedLineSet>\n" ;
	s <<  L"</Shape> \n"         ;
	s <<  L"</Transform>\n"       ;

}

void x3dViewPoint(std::wostream& s, const wchar_t* descr, V3 p, V3 orientationAxis, float orientationAngle, V3 rotateAxis, float rotateAngle) {
	if (!rotateAxis.isZeroLength()) {
		s << L"<Transform rotation='";

		s << rotateAxis.x << L" ";
		s << rotateAxis.y << L" ";
		s << rotateAxis.z << L" ";
		s << rotateAngle;
		s << L"  '>";
	}
	s << L"<Viewpoint";
	s << L"   fieldOfView='0.42' ";
   //	s << L"   jump TRUE\n";
	s << L"   position=' " << p.x << L" " << p.y << L" " <<p.z << L"'\n";
	if (!orientationAxis.isZeroLength()) {
		s << L"   orientation=' ";
		s << orientationAxis.x << L" ";
		s << orientationAxis.y << L" ";
		s << orientationAxis.z << L" ";
		s << orientationAngle << L"' ";
	}
	s << L"   description='" << descr << L"' ";


	if (!rotateAxis.isZeroLength()) {
		s << L"/>\n";
		s << L"</Transform>";
	} else {
		s << L"/>\n";
	}
}

void x3dViewPoints(std::wostream& f, V3 min, V3 max) {
	V3 sz(fabs(max.x - min.x), fabs(max.y - min.y), fabs(max.z - min.z));

	x3dViewPoint(f, L"Up", V3(0, 0,  (2*sz.x+ 2*sz.y + sz.z) * 0.5f));
	x3dViewPoint(f, L"Down", V3(0, 0, (-2*sz.x - 2*sz.y - sz.z) * 0.5f), V3(1, 0, 0), M_PI);
	x3dViewPoint(f, L"North",  V3(0, (-sz.x- 2*sz.y - 2*sz.z) * 0.5f, 0), V3(1, 0, 0), M_PI_2);

	x3dViewPoint(f, L"South", V3(0, (-sz.x - 2*sz.y - 2*sz.z) * 0.5f, 0), V3(1, 0, 0), M_PI_2, V3(0, 0, 1), M_PI);
	x3dViewPoint(f, L"West",  V3(0, (-2*sz.x - sz.y - 2*sz.z) * 0.5f, 0), V3(1, 0, 0), M_PI_2, V3(0, 0, 1), M_PI_2);
	x3dViewPoint(f, L"East",  V3(0, (-2*sz.x - sz.y - 2*sz.z) * 0.5f, 0), V3(1, 0, 0), M_PI_2, V3(0, 0, 1), -M_PI_2);
}

void x3dNavigation(std::wostream& s, float speed) {
	s << L"	 <Transform >";
	s << L"<NavigationInfo headlight='true' ";
	s << L" speed=' " << speed << L"'/>\n ";
	s << L"	 </Transform >";
}

void x3dBackground(std::wostream& s, float r, float g, float b) {
	s << L"<Background skyColor='";
	s << r << L" ";
	s << g << L" ";
	s << b << L" ";
	s << L"'/>\n";
	s << L"</Scene>";

	s <<  L"</X3D>";
}

void x3dPoly(std::wostream& s, const Color& color, V3 p0, V3 p1, V3 p2) {
	s <<	 " <Transform> \n ";
	s << L"<Shape>\n";
	s << L"<Appearance>\n";
	s << L"<Material ";
	s << L"	 diffuseColor='" << std::to_wstring(color.r) + L" " + std::to_wstring(color.g) + L" " + std::to_wstring(color.b) << L"'/>\n";
	s << L"     </Appearance>\n";
	s << L"<IndexedFaceSet \n";
	s << L"	coordIndex='0 2 1 -1'>\n";
	s << L"	<Coordinate \n";
	s << L" point ='\n";
	s << p0.y << L" " << p0.x << L" " << p0.z << L"\n";
	s << p1.y << L" " << p1.x << L" " << p1.z << L"\n";
	s << p2.y << L" " << p2.x << L" " << p2.z << L"\n";
	s << L" '/>\n";
	s << L"	</IndexedFaceSet>\n";

	s << L"	</Shape>\n";
	s << L"</Transform> \n";
}

void x3dPoly(std::wostream& s, const std::vector<Color>& color, std::vector<V3> pt, std::vector<int> idx) {
	s <<  L" <Transform> \n ";
	s << L"<Shape>\n";
	s << L"<Appearance>\n";
	s << L"<Material />";
	s << L"</Appearance>\n";
	s << L"<IndexedFaceSet \n";
		s << L"	coordIndex=' ";

	for (int i = 0; i < idx.size(); i+=3) {
		s << idx[i] << L" " << idx[i + 1] << L" " << idx[i + 2] << L" -1 ";

	}

	s << L"'>\n";

	s << L"	<Coordinate\n";
	s << L"	   point='\n";

	for (const auto& p : pt) {
		s << p.y << L" " << p.x << L" " << p.z << L" \n";
	}

	s << L"	   '/>\n";


	s << L"	<Color \n";
	s << L"	   color='\n";

	for (const auto& c : color) {
		s << std::to_wstring(c.r) + L" " + std::to_wstring(c.g) + L" " + std::to_wstring(c.b)<< L" \n";
	}

	s << L"	   '/>\n";
		s << L"	</IndexedFaceSet>\n";

	s << L"	</Shape>\n";
	s << L"</Transform> \n";
}








