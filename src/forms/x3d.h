﻿#pragma once
#include <iostream>
#include "CMTypes.h"

using namespace CM;

void x3dPreambula(std::wostream& s);
void x3dStartPath(std::wostream& s, const wchar_t* color);
void x3dStartPath(std::wostream& s, const Color& color);
void x3dEndPath(std::wostream& s, int vNum);
void x3dcoord(std::wostream& s);
void x3dPoint(std::wostream& s, V3 p);
void x3dAABB(std::wostream& s, V3 min, V3 max);
void x3dViewPoint(std::wostream& s, const wchar_t* descr, V3 p, V3 orientationAxis = V3::ZERO, float orientationAngle = 0, V3 rotateAxis = V3::ZERO, float rotateAngle = 0);
void x3dViewPoints(std::wostream& f, V3 min, V3 max);
void x3dNavigation(std::wostream& s, float speed);
void x3dBackground(std::wostream& s, float r, float g, float b);
void x3dPoly(std::wostream& s, const Color& color, V3 p0, V3 p1, V3 p2);
void x3dPoly(std::wostream& s, const std::vector<Color>& color, std::vector<V3> pt, std::vector<int> idx);

