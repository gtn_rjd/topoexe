//---------------------------------------------------------------------------
#include "OgreCMCaveModel.h"                  
#include "CMCave.h"    
#include "CMAssertions.h"     
#include "CMLog.h"

using namespace std;  
using namespace CM;


OgreCaveDataModel::OgreCaveDataModel(P3D* data, int size, W3D* wData, int wallsSize, const EquatesArrayType& equates, const std::vector<std::string>& names, const CM::CaveViewPrefs& prefs, bool convertToExtendedElevation):
    piketsData(data, data + size), 
    wallsData(wData, wData + wallsSize),
    piketsNames(names), 
    equatesList(equates),
    convertToExtendedElevation(convertToExtendedElevation),
    cmCave(new CM::Cave) {

    LOG("create cave with:");
    LOG("\t" << size << " pikets");
    LOG("\t" << wallsSize << " walls");
    LOG("\t" << equates.size() << " equates");

    DLOG("raw pikets:");

    std::vector<P3D>&piketsDataMut = const_cast<std::vector<P3D>&>(piketsData);
    std::vector<P3D>::iterator p = piketsDataMut.begin();
    for (; p != piketsDataMut.end(); p++) {
        p->pos.x = OGRE_IMPORT_AXIS_X(p->pos.x);
        p->pos.y = OGRE_IMPORT_AXIS_Y(p->pos.y);
        p->pos.z = OGRE_IMPORT_AXIS_Z(p->pos.z);

        DLOG("\tmet: " << p->met << " vet:" << p->vet << " line: " << p->line << " name: " << getP3DLabel(p->met) << " pos:" << p->pos);
    }

    std::vector<W3D>&wallDataMut = const_cast<std::vector<W3D>&>(wallsData);
    std::vector<W3D>::iterator w = wallDataMut.begin();
    for (; w != wallDataMut.end(); w++) {
        w->pos.x = OGRE_IMPORT_AXIS_X(w->pos.x);
        w->pos.y = OGRE_IMPORT_AXIS_Y(w->pos.y);
        w->pos.z = OGRE_IMPORT_AXIS_Z(w->pos.z);
	}

    setCaveViewPrefs(prefs);
}

void OgreCaveDataModel::setOutputType(CM::OutputType ot) { 
    AssertReturn(!wasInited, return);
    outputType = ot; 
};

void OgreCaveDataModel::init() {  
    AssertReturn(p3dToPiketId.empty(), return);
    AssertReturn(p3dToVerticeId.empty(), return);
                  
    cmCave->setShouldConvertToExtendedElevation(convertToExtendedElevation);
      
    buildEquatesMap();
    buildPikets();
    
    wasInited = true;
}

bool OgreCaveDataModel::setCaveViewPrefs(CM::CaveViewPrefs prefs) {
	caveViewPrefs = prefs;
    return cmCave->setCaveViewPrefs(prefs);
}
                                
bool OgreCaveDataModel::isOutputEnabled(CM::OutputType type) {
    return cmCave->isOutputEnabled(type);
}

bool OgreCaveDataModel::isOutputChanged(CM::OutputType type) {  
    return cmCave->isOutputChanged(type);
}

const std::vector<CM::OutputPoly>& OgreCaveDataModel::getOutputPoly(CM::OutputType type) {    
    return cmCave->getOutputPoly(type);
}

const std::vector<CM::OutputLine>& OgreCaveDataModel::getOutputLine(CM::OutputType type) { 
    return cmCave->getOutputLine(type);
}

OgreCaveDataModel::~OgreCaveDataModel() {
    delete cmCave;
}


void OgreCaveDataModel::buildEquatesMap() {
    equates.clear();
    DLOG("Equates:");
    std::vector<std::vector<int> >equateBunchs;
    for (int i = 0; i < equatesList.size(); i++) {
        DLOG("\t" << "met " << equatesList[i].first << " = met " << equatesList[i].second);
        const P3D* firstP3D = getP3DbyMet(equatesList[i].first);
        const P3D* secondP3D = getP3DbyMet(equatesList[i].second);
        if (!firstP3D || !secondP3D || firstP3D->pos.distance(secondP3D->pos) > 0.5f) {
            DLOG("\t\t" << "disconnected");
            continue;
        }
        bool added = false;
        for (int j = 0; j < equateBunchs.size(); j++) {
            if (find(equateBunchs[j].begin(), equateBunchs[j].end(), equatesList[i].first) != equateBunchs[j].end()) {
                equateBunchs[j].push_back(equatesList[i].second);
                added = true;
                break;
            }
            if (find(equateBunchs[j].begin(), equateBunchs[j].end(), equatesList[i].second) != equateBunchs[j].end()) {
                equateBunchs[j].push_back(equatesList[i].first);
                added = true;
                break;
            }
        }
        if (!added) {
            equateBunchs.push_back(std::vector<int>());
            equateBunchs.back().push_back(equatesList[i].first);
            equateBunchs.back().push_back(equatesList[i].second);
        }
    }

    DLOG("Equates map:");
    for (int i = 0; i < equateBunchs.size(); i++) {
        if (equateBunchs[i].empty())
            continue;
        int min = *std::min_element(equateBunchs[i].begin(), equateBunchs[i].end());
        for (int j = 0; j < equateBunchs[i].size(); j++) {
            // AssertReturn(equates.count(equateBunchs[i][j]) == 0, continue);
            if (equates.count(equateBunchs[i][j]) != 0)
                continue;
            if (equateBunchs[i][j] != min) {
                equates[equateBunchs[i][j]] = min;
                DLOG("\t" << equateBunchs[i][j] << "->" << min);
            }
        }
    }
}

void OgreCaveDataModel::buildPikets() {

    for (int i = 0; i < piketsData.size(); i++) {
		const P3D* p3d = getP3D(i);
		assert(p3d);

		if (p3d->priz & PRIZ_DUPLICATE) continue;

		int verticeId = getVerticeId(p3d);

		CM::PiketInfo newPiketInfo;
		newPiketInfo.name = getP3DLabel(p3d->met);
		newPiketInfo.label = "";
		newPiketInfo.pos = p3d->pos;
		newPiketInfo.extendedElevationX = p3d->XY;
		newPiketInfo.priz = CM::MARK_NONE;
		if (p3d->priz & PRIZ_Z_SURVEY)
			newPiketInfo.priz = (CM::PiketMark)(newPiketInfo.priz | CM::MARK_Z_SURVEY);
		if (p3d->priz & PRIZ_Z_TURN)
			newPiketInfo.priz = (CM::PiketMark)(newPiketInfo.priz | CM::MARK_Z_TURN);
		if (p3d->priz & PRIZ_Z_SURVEY_FAKE)
			newPiketInfo.priz = (CM::PiketMark)(newPiketInfo.priz | CM::MARK_Z_SURVEY_FAKE);
		if (p3d->priz & PRIZ_ONLY_CONVEX_WALLS)
			newPiketInfo.priz = (CM::PiketMark)(newPiketInfo.priz | CM::MARK_ONLY_CONVEX_WALLS);
		if (p3d->priz & PRIZ_SURFACE || p3d->priz & PRIZ_NO_FAKE_WALLS)
			newPiketInfo.priz = (CM::PiketMark)(newPiketInfo.priz | CM::MARK_NO_FAKE_WALLS);

        if (verticeId <= 0) {
            // pikets.insert(make_pair(newPiketInfo.id, newPiketInfo));
            verticeId = newPiketInfo.id;
            cmCave->addVertice(newPiketInfo);
        }
        else {
            cmCave->addVertice(newPiketInfo, verticeId);
        }
        AssertReturn(p3d->p3did > 0, ;);
        p3dToPiketId[p3d->p3did] = newPiketInfo.id;
        p3dToVerticeId[p3d->p3did] = verticeId; 
        if (p3d->met > 0) {
            metToVerticeId[p3d->met] = verticeId; 
        }
        verticeIdToP3D.insert({verticeId, p3d});
		if (i > 0) {
			const P3D* prevP3D = getP3D(i - 1);
			if (prevP3D->vet != p3d->vet)
				continue;
			if (prevP3D->priz & PRIZ_DUPLICATE)
				continue;

			int prevVerticeId = getVerticeId(prevP3D);
			AssertReturn(prevVerticeId > 0, continue);

			if (prevVerticeId == verticeId)
				continue;
                                                        
			CM::Color col = getThreadColour(p3d->col);
                
			CM::EdgeInfo edgeInfo(prevVerticeId, verticeId, col);
			cmCave->addEdge(edgeInfo);

        }
    }

    std::vector<W3D>::const_iterator wIt = wallsData.begin();
    while (wIt != wallsData.end()) {
        const P3D* wallP3D = getP3D(wIt->pik);
        const W3D* wallW3D = &*wIt;

        wIt++;
        AssertReturn(wallP3D, continue);
        int verticetId = getVerticeId(wallP3D);

        cmCave->addWall(CM::Wall(wallW3D->pos), verticetId, getPiketId(wallP3D));
    }

    cmCave->finishInit(outputType);
}


const P3D* OgreCaveDataModel::getP3D(int id) const {
    const P3D* result = NULL;
    if (id >= 0 && id < piketsData.size()) {
        result = &piketsData.at(id);
    }
    Assert(result);
    return result;
}

int OgreCaveDataModel::getPiketId(const P3D* p3d) const {
    if (p3d && p3d->p3did > 0) {
        auto p3dIdFindRes = p3dToPiketId.find(p3d->p3did);
        if (p3dIdFindRes != p3dToPiketId.end()) {
            return p3dIdFindRes->second;
        }
    }
    return 0;
}

int OgreCaveDataModel::getVerticeId(const P3D* sampleP3d) const {
    return getVerticeId(sampleP3d, sampleP3d->met);
}

int OgreCaveDataModel::getVerticeId(int met) const {
	return getVerticeId(NULL, met);
}

int OgreCaveDataModel::getVerticeId(const P3D* sampleP3d, int met) const {
    if (sampleP3d != NULL && sampleP3d->p3did > 0) {
        auto p3dIdFindRes = p3dToVerticeId.find(sampleP3d->p3did);
		if (p3dIdFindRes != p3dToVerticeId.end()) {
            return p3dIdFindRes->second;
        }
    }

    if (met > 0) {
        static int loopCounter = 0;
        AssertReturn(loopCounter < 1000, "probably infinite loop detected");
        std::tr1::unordered_map<int, int>::const_iterator equateFind = equates.find(met);
        if (equateFind != equates.end() && loopCounter < 1000) {
            loopCounter++;
            int piketId = getVerticeId(equateFind->second);
            loopCounter--;
            return piketId;
        }
    }

    if (met > 0) {
        auto metIdFindRes = metToVerticeId.find(met);
        if (metIdFindRes != metToVerticeId.end()) {
            return metIdFindRes->second;
        }
//        std::tr1::unordered_map< const P3D*, int>::const_iterator p3dit;
//        for (p3dit = p3dToVerticeId.begin(); p3dit != p3dToVerticeId.end(); p3dit++) {
//            const P3D* p3d = p3dit->first;
//            if (p3d->met == met) {
//                return p3dit->second;
//            }
//        }
    }

    return -1;
}

V3 OgreCaveDataModel::getPiketPos(int vertId) const {
	return verticePosToPiketPos(cmCave->getVerticePos(vertId));
}

V3 OgreCaveDataModel::verticePosToPiketPos(V3 pos) const {
	pos.x = OGRE_EXPORT_AXIS_X(pos.x);
    pos.y = OGRE_EXPORT_AXIS_Y(pos.y);
	pos.z = OGRE_EXPORT_AXIS_Z(pos.z);
	return pos;
}

const Color& OgreCaveDataModel::getEdgeColor(int from, int to) const {
	return cmCave->getColorForEdge(from, to);
}

std::vector<const P3D*> OgreCaveDataModel::getP3DForVertice(int verticeId) const {
    std::vector<const P3D*> res;
    auto it = verticeIdToP3D.find(verticeId);
    while (it != verticeIdToP3D.end() && it->first == verticeId) {
        res.push_back(it->second);
        it++;
    }
    return res;
}
     
//std::string OgreCaveDataModel::getPiketName(int id) const {    
//   return cmCave->getVerticeName(id);
//}

const P3D* OgreCaveDataModel::getP3DbyMet(int met) const {
    for (int i = 0; i < piketsData.size(); i++) {
        if (piketsData[i].met == met)
            return &piketsData[i];
    }
    return NULL;
}


const std::string& OgreCaveDataModel::getP3DLabel(int met) {
    const static std::string nullString;
    if (met < piketsNames.size())
        return piketsNames[met];
    else
        return nullString;
}

std::vector<CM::CrossPiketLineBesier3> OgreCaveDataModel::calcOutineBesier() {
    if (!wasInited) {
        init();
    }
    return cmCave->calcOutineBesier();
}


const CM::Color& OgreCaveDataModel::getThreadColour(int color, P3DPriz priz) const {
    static const Color BlackThread(0.2, 0.2, 0.2);
    static const Color BlueThread(0.0, 0.0, 1.0);
    static const Color GreenThread(0.0, 0.5, 0.0);
    static const Color AquaThread(0.0, 1, 1);
    static const Color RedThread(0.5, 0.0, 0.0);
    static const Color PurpleThread(0.5, 0.0, 0.5);
    static const Color SoftRedThread(0.94, 0.5, 0.5); //0x008080F0
    static const Color LightGrayThread(0.76, 0.76, 0.76);
    static const Color DarkGrayThread(0.5, 0.5, 0.5);
    static const Color PureBlueThread(0, 0.666, 1);
    static const Color LimeThread(0, 1, 0.0);
    static const Color KhakiThread(0.76, 0.76, 0.124);
    static const Color FuchsiaThread(1, 0.0, 1);
    static const Color YellowThread(1, 1, 0.0);
    static const Color WhiteThread(1, 1, 1);
    static const Color OrangeThread(1, 0.5, 0);/////0.94, 0.666, 0);

    if (priz & PRIZ_SURFACE)
        return getThreadColour(caveViewPrefs.surfColorId);

    if (priz & PRIZ_DUPLICATE)
        return getThreadColour(caveViewPrefs.dupColorId);

    switch (color) {
    case 0:
        return BlackThread;
    case 1:
        return BlueThread;
    case 2:
        return GreenThread;
    case 3:
        return AquaThread;
    case 4:
        return RedThread;
    case 5:
        return PurpleThread;
    case 6:
        return SoftRedThread;
    case 7:
        return LightGrayThread;
    case 8:
        return DarkGrayThread;
    case 9:
        return PureBlueThread;
    case 10:
        return LimeThread;
    case 11:
        return KhakiThread;
    case 12:
        return RedThread;
    case 13:
        return FuchsiaThread;
    case 14:
        return YellowThread;
    case 15:
        return WhiteThread;
    case 16:
        return OrangeThread;
    default:
        return LimeThread;
    }
}

std::vector<int> OgreCaveDataModel::getPath(const P3D* from, const P3D* to) const {
    LOG("path from met: " << from->met << "  to met:" << to->met);
    std::vector<int> res = cmCave->getPath(getVerticeId(from), getVerticeId(to));
    return res;
}             

//std::vector<int> OgreCaveDataModel::getPath(int metFrom, int metTo) const {
//    LOG("path from met: " << metFrom << "  to met:" << metTo);
//    std::vector<int> res = cmCave->getPath(getVerticeId(metFrom), getVerticeId(metTo));
//    return res;
//}

bool OgreCaveDataModel::isInPath(const std::vector<int>& path, const P3D* p0, const P3D* p1) const {
   int p0VertId = getVerticeId(p0);
   int p1VertId = getVerticeId(p1);
   auto p0it = find(path.begin(), path.end(), p0VertId);
   if (p0it != path.end() && (
        ((p0it+1) != path.end() && *(p0it+1) == p1VertId) ||
        ((p0it) != path.begin() && *(p0it-1) == p1VertId)
      )) {
      return true;
   } else {
      return false;
   }
}
