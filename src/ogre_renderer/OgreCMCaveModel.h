#pragma once       
#include "CMTypes.h"
#include "CaveData.h"
#include <vector>           
#include <map>               
//#include <multimap>          
#include <unordered_map>
#include <unordered_set> 

namespace CM {  
    struct Cave;
    struct OutputLine;
    struct CrossPiketLineBesier3;
}


class OgreCaveDataModel {
public:
    OgreCaveDataModel(P3D* data, int size, W3D* wallsData, int wallsSize, const EquatesArrayType& equates, const std::vector<std::string>& names = {}, const CM::CaveViewPrefs& prefs = CM::CaveViewPrefs(), bool convertToExtendedElevation = false);
    ~OgreCaveDataModel();
    void init();

    void setOutputType(CM::OutputType ot);

    bool setCaveViewPrefs(CM::CaveViewPrefs prefs);
    
    bool isCnvertToExtendedElevation() const { return convertToExtendedElevation; }           
    const CaveViewPrefs& getCaveViewPrefs() const { return caveViewPrefs; }
                                           
    bool isOutputEnabled(CM::OutputType type);
    bool isOutputChanged(CM::OutputType type);
    const std::vector<CM::OutputPoly>& getOutputPoly(CM::OutputType type);
    const std::vector<CM::OutputLine>& getOutputLine(CM::OutputType type);    
    std::vector<CM::CrossPiketLineBesier3> calcOutineBesier();
                         
    int getVerticeId(const P3D* p3d, int met) const ;
    int getVerticeId(const P3D* p3d) const ;
    int getVerticeId(int met) const ;
	V3 getPiketPos(int vertId) const;
	const CM::Color& getEdgeColor(int from, int to) const;
    std::vector<const P3D*> getP3DForVertice(int verticeId) const;
    std::vector<int> getPath(const P3D* from, const P3D* to) const; 
//    std::vector<int> getPath(int metFrom, int metTo) const;
    bool isInPath(const std::vector<int>& path, const P3D* p0, const P3D* p1) const;

	V3 verticePosToPiketPos(V3 p) const;

protected:
    // ������� ���������� �����
    const P3D* getP3D(int id) const ;
    const P3D* getP3DbyMet(int met) const ;
    int getPiketId(const P3D* p3d) const ;
    // CM::PiketInfo* getPiketMut(int id);
    // const CM::PiketInfo* getPiket(int id);
    const std::string& getP3DLabel(int met);
    
    void buildEquatesMap();
    void buildPikets(); // ������� ���� ������� �� �������� P3D � W3D
                                              
    const CM::Color& getThreadColour(int color, P3DPriz priz = PRIZ_NONE) const ;
    
protected:
    const std::vector<P3D>piketsData;
    const std::vector<W3D>wallsData;

    const EquatesArrayType equatesList;

    const std::vector<std::string> piketsNames; /* key - 'met' */

    // key- first equate group 'met', value- some other equate group 'met'
    std::tr1::unordered_map<int, int> equates;
    // key- vertice id, value- piket
    // std::tr1::unordered_map<int, CM::PiketInfo> pikets;
    // key- p3dId, value- piket id
    std::tr1::unordered_map<int, int> p3dToPiketId;
    // key- p3dId, value- vertice id
    std::tr1::unordered_map<int, int> p3dToVerticeId;     
    // key- met, value- vertice id
    std::tr1::unordered_map<int, int> metToVerticeId; 
    // key- vertice id, value- P3D
    std::multimap<int, const P3D*> verticeIdToP3D;   


    bool convertToExtendedElevation { false };

    CM::OutputType outputType {CM::OT_NONE};     
    CM::Cave* cmCave {nullptr};  
                            
    CM::CaveViewPrefs caveViewPrefs;
    
    bool wasInited {false}; 
};
