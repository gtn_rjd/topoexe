// ---------------------------------------------------------------------------

#define OGRE_DEBUG_MODE 1

#include "OgreCMCaveView.h"
#include "OgreRoot.h"
#include "OgreManualObject.h"
#include "CMAssertions.h"
#include "stdio.h"           
#include "CMHelpers.h"
#include "OgreMath.h"     
#include "wykobi.hpp"     
#include "wykobi_wrap.h"   
#include "CMtext.h"
#include "CMcommon.h"
#include "CMLog.h"
#include <iostream>      

using namespace std;
using namespace tr1;
using namespace Ogre;
using namespace wykobi;  


OgreCaveView::OgreCaveView(Ogre::SceneManager* sceneManager, P3D * data, int size, W3D * wData, int wSize, const EquatesArrayType& equates, const std::vector<std::string>& names,
    const CaveViewPrefs & prefs, bool convertToExtendedElevation) : 
    verts(NULL), 
    caveNode(NULL), 
    dataModel(data, size, wData, wSize, equates, names, prefs, convertToExtendedElevation),
    sceneManager(sceneManager), 
    caveViewPrefs(prefs) {
                               
    dataModel.setOutputType(CM::OT_ALL); 
    
    if (sceneManager) {
        caveNode = sceneManager->createSceneNode();

        wallsCutsNode = sceneManager->createSceneNode();
        caveNode->addChild(wallsCutsNode);

        debug = sceneManager->createManualObject();
        caveNode->attachObject(debug);

        debug2 = sceneManager->createManualObject();
        caveNode->attachObject(debug2);

        verts = sceneManager->createManualObject();
        caveNode->attachObject(verts);

        lines = sceneManager->createManualObject();
        caveNode->attachObject(lines);

        cube = sceneManager->createManualObject();
        caveNode->attachObject(cube);

        walls = sceneManager->createManualObject();
        caveNode->attachObject(walls);

        wallsCuts = sceneManager->createManualObject();
        caveNode->attachObject(wallsCuts);

        box = sceneManager->createManualObject();
        caveNode->attachObject(box);

        outline = sceneManager->createManualObject();
        caveNode->attachObject(outline);

        outlineCuts = sceneManager->createManualObject();
		caveNode->attachObject(outlineCuts);

		splays = sceneManager->createManualObject();
		caveNode->attachObject(splays);
    }
}

void OgreCaveView::enable() {
    if (!wasInited) {
        init();
    }
}

void OgreCaveView::init() {
    dataModel.setCaveViewPrefs(caveViewPrefs);
    dataModel.init();  
    
    rebuildVisualObjets();

    wasInited = true;
}

OgreCaveView::~OgreCaveView() {
    if (sceneManager) {
        sceneManager->destroySceneNode(caveNode);
        sceneManager->destroyManualObject(verts);
        sceneManager->destroyManualObject(lines);
        sceneManager->destroyManualObject(cube);
        sceneManager->destroyManualObject(walls);
        sceneManager->destroyManualObject(wallsCuts);
		sceneManager->destroySceneNode(wallsCutsNode);
        sceneManager->destroyManualObject(debug);
        sceneManager->destroyManualObject(debug2);
        sceneManager->destroyManualObject(box);
		sceneManager->destroyManualObject(outline);
		sceneManager->destroyManualObject(outlineCuts);
		sceneManager->destroyManualObject(splays);
    }
}

void OgreCaveView::setCaveViewPrefs(CM::CaveViewPrefs prefs) {
    bool fillRateChanged = (prefs.fillRate != caveViewPrefs.fillRate);
    if (caveViewPrefs != prefs) {
        bool shouldRebuild = dataModel.setCaveViewPrefs(prefs);
        caveViewPrefs = prefs;
        if (shouldRebuild || fillRateChanged) {
            rebuildVisualObjets();
        }
        else {
            updateVisualObjetsVisibility();
        }
    }
}

void OgreCaveView::drawPoly(const std::string& material, Ogre::ManualObject* object, const std::vector<CM::OutputPoly>& data, float fillRate) {
    float fillHeight = -FLT_MAX;
    if (fillRate != -1) {
        float maxHeight = -FLT_MAX / 10;
        float minHeight = FLT_MAX / 10;
        std::vector<CM::OutputPoly>::const_iterator it = data.begin();
        for (; it != data.end(); it++) {
            const CM::OutputPoly& triangle = *it;
            maxHeight = std::max(maxHeight, std::max((triangle.a.z), std::max((triangle.b.z), (triangle.c.z))));
            minHeight = std::min(minHeight, std::min((triangle.a.z), std::min((triangle.b.z), (triangle.c.z))));
        }
        fillHeight = minHeight + (maxHeight - minHeight) * caveViewPrefs.fillRate;
    }

    object->clear();
    object->begin(material, RenderOperation::OT_TRIANGLE_LIST);
    std::vector<CM::OutputPoly>::const_iterator it = data.begin();
    for (; it != data.end(); it++) {
        if (fillRate == -1 || ((it->a.z) < fillHeight && (it->b.z) < fillHeight && (it->c.z) < fillHeight)) {
            object->position(it->a);
            object->colour(ColourValue(it->ca.r, it->ca.g, it->ca.b, it->ca.a));
            if (!it->an.isZeroLength())
                object->normal(it->an);
            object->position(it->b);
            object->colour(ColourValue(it->cb.r, it->cb.g, it->cb.b, it->cb.a));
            if (!it->an.isZeroLength())
                object->normal(it->bn);
            object->position(it->c);
            object->colour(ColourValue(it->cc.r, it->cc.g, it->cc.b, it->cc.a));
            if (!it->an.isZeroLength())
                object->normal(it->cn);
        }
    }
    object->end();
}

void OgreCaveView::drawTransparentPoly(const std::string& material, Ogre::SceneNode* node, const std::vector<CM::OutputPoly>& data) {
    while (node->numAttachedObjects() > 0) {
        ManualObject* mo = dynamic_cast<ManualObject*>(node->detachObject((unsigned short)0));
        if (mo)
            sceneManager->destroyManualObject(mo);
    }
    std::vector<CM::OutputPoly>::const_iterator it = data.begin();
    for (; it != data.end(); it++) {
        Ogre::ManualObject* object = sceneManager->createManualObject();
        object->begin(material, RenderOperation::OT_TRIANGLE_LIST);

        object->position(it->a);
        object->colour(ColourValue(it->ca.r, it->ca.g, it->ca.b, 0.5f));
        // if (!it->an.isZeroLength()) object->normal(it->an);

        object->position(it->b);
        object->colour(ColourValue(it->cb.r, it->cb.g, it->cb.b, 0.5f));
        // if (!it->an.isZeroLength()) object->normal(it->bn);

        object->position(it->c);
        object->colour(ColourValue(it->cc.r, it->cc.g, it->cc.b, 0.5f));
        // if (!it->an.isZeroLength()) object->normal(it->cn);

        object->end();
        node->attachObject(object);
    }
}

void OgreCaveView::drawLine(const std::string& material, Ogre::ManualObject* object, const std::vector<CM::OutputLine>& data, float colorMult) {
    object->clear();
    object->begin(material, RenderOperation::OT_LINE_LIST);
    std::vector<CM::OutputLine>::const_iterator it = data.begin();
    for (; it != data.end(); it++) {
        assert(it->a == it->a);
        assert(it->b == it->b);
        object->position(it->a);
        object->colour(ColourValue(it->ca.r * colorMult, it->ca.g * colorMult, it->ca.b * colorMult, it->ca.a));
        object->position(it->b);
        object->colour(ColourValue(it->cb.r * colorMult, it->cb.g * colorMult, it->cb.b * colorMult, it->cb.a));
    }
    object->end();
}

void OgreCaveView::rebuildVisualObjets() {
    if (!sceneManager)
        return;

    if (dataModel.isOutputChanged(CM::OT_THREAD))
        drawLine("Thread", lines, dataModel.getOutputLine(CM::OT_THREAD));
    if (dataModel.isOutputChanged(CM::OT_DEBUG))
        drawLine("BaseWhiteNoLighting", debug, dataModel.getOutputLine(CM::OT_DEBUG));
    if (dataModel.isOutputChanged(CM::OT_DEBUG2))
        drawLine("BaseWhiteNoLighting", debug2, dataModel.getOutputLine(CM::OT_DEBUG2));
    if (dataModel.isOutputChanged(CM::OT_WALL_CUTS))
        drawLine("BaseWhiteNoLighting", wallsCuts, dataModel.getOutputLine(CM::OT_WALL_CUTS));

    if (dataModel.isOutputChanged(CM::OT_WALL))
        drawPoly(getWallMaterial(), walls, dataModel.getOutputPoly(CM::OT_WALL), caveViewPrefs.fillRate);
    if (dataModel.isOutputChanged(CM::OT_WALL_CUTS))
        drawTransparentPoly("TranspWall", wallsCutsNode, dataModel.getOutputPoly(CM::OT_WALL_CUTS));

    if (dataModel.isOutputChanged(CM::OT_BOX))
        drawLine("BaseWhiteNoLighting", box, dataModel.getOutputLine(CM::OT_BOX));

    if (dataModel.isOutputChanged(CM::OT_OUTLINE))
        drawLine("Thread", outline, dataModel.getOutputLine(CM::OT_OUTLINE), 0.5);
    if (dataModel.isOutputChanged(CM::OT_OUTLINE_CUT))
		drawLine("Thread", outlineCuts, dataModel.getOutputLine(CM::OT_OUTLINE_CUT), 0.5);

	if (dataModel.isOutputChanged(CM::OT_SPLAY))
		drawLine("Thread", splays, dataModel.getOutputLine(CM::OT_SPLAY), 0.75);

    updateVisualObjetsVisibility();
}

void OgreCaveView::updateVisualObjetsVisibility() {
    if (lines)
        lines->setVisible(dataModel.isOutputEnabled(CM::OT_THREAD));
    if (walls)
        walls->setVisible(dataModel.isOutputEnabled(CM::OT_WALL));
    if (wallsCuts)
        wallsCuts->setVisible(dataModel.isOutputEnabled(CM::OT_WALL_CUTS));
    if (wallsCutsNode)
        wallsCutsNode->setVisible(dataModel.isOutputEnabled(CM::OT_WALL_CUTS));
    if (box)
        box->setVisible(dataModel.isOutputEnabled(CM::OT_BOX));
    if (outline)
		outline->setVisible(dataModel.isOutputEnabled(CM::OT_OUTLINE));
	if (outlineCuts)
		outlineCuts->setVisible(dataModel.isOutputEnabled(CM::OT_OUTLINE_CUT));
	if (splays)
		splays->setVisible(dataModel.isOutputEnabled(CM::OT_SPLAY));

    if (debug)
		debug->setVisible(dataModel.isOutputEnabled(CM::OT_DEBUG));
	if (debug2)
		debug2->setVisible(dataModel.isOutputEnabled(CM::OT_DEBUG2));
}


std::string OgreCaveView::getWallMaterial() {
    return "ColoredWall";
}


void OgreCaveView::attachToNode(Node* node) {
    if (caveNode->getParent())
        caveNode->getParent()->removeChild(caveNode);
    node->addChild(caveNode);
}

