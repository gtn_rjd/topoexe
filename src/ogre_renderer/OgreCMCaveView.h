// ---------------------------------------------------------------------------
#pragma once

#include "OgrePrerequisites.h" 
#include <OgreCMCaveModel.h>



class OgreCaveView {
public:
    OgreCaveView(Ogre::SceneManager* sceneManager, P3D* data, int size, W3D* wallsData, int wallsSize, const EquatesArrayType& equates, const std::vector<std::string>& names,
    const CaveViewPrefs & prefs, bool convertToExtendedElevation);
    ~OgreCaveView();
    void attachToNode(Ogre::Node* node);
    void setCaveViewPrefs(CM::CaveViewPrefs prefs);
    void enable();
    
protected:
    void init();
    void rebuildVisualObjets();
    void updateVisualObjetsVisibility();
    void drawLine(const std::string& material, Ogre::ManualObject* object, const std::vector<CM::OutputLine>& data, float colorMult = 1.0f);
    void drawPoly(const std::string& material, Ogre::ManualObject* object, const std::vector<CM::OutputPoly>& data, float fillRate = -1);
    void drawTransparentPoly(const std::string& material, Ogre::SceneNode* node, const std::vector<CM::OutputPoly>& data);

    // ������� ��������� ����
    std::string getWallMaterial();

    CM::CaveViewPrefs caveViewPrefs;

	Ogre::ManualObject* verts {nullptr};
	Ogre::ManualObject* lines {nullptr};
	Ogre::ManualObject* cube {nullptr};
	Ogre::ManualObject* walls {nullptr};
	Ogre::ManualObject* wallsCuts {nullptr};
	Ogre::SceneNode* wallsCutsNode {nullptr};
	Ogre::ManualObject* debug {nullptr};
	Ogre::ManualObject* debug2 {nullptr};
	Ogre::ManualObject* box {nullptr};
	Ogre::ManualObject* outline {nullptr};
	Ogre::ManualObject* outlineCuts {nullptr};
	Ogre::ManualObject* splays {nullptr};
	Ogre::SceneNode* caveNode {nullptr};
    Ogre::SceneManager* sceneManager {nullptr};

    std::string curWallsMaterial;
    
    OgreCaveDataModel dataModel; 
     
    bool wasInited {false}; 
};
