//---------------------------------------------------------------------------
#include "CMAssertions.h"
#include "OgreCMLog.h"
#include "OgreLogManager.h"

Ogre::Log* gameLog = NULL;
bool writeDebug = false;


void initLogSystem(bool debug) {
	if (gameLog == NULL) {
		gameLog = Ogre::LogManager::getSingleton().createLog("game.log", false, false, false);
		writeDebug = debug;
		OgreCMLog::init();
	}
}

void logMessage(const std::string& msg){
	AssertReturn(gameLog != NULL, return);
	gameLog->logMessage(msg);
}

void logMessageDebug(const std::string& msg) {
	if (writeDebug) logMessage(msg);

}

void OgreCMLog::init() {
	instatnce = new OgreCMLog;
}

void OgreCMLog::log( const std::string& message) {
	logMessage(message);
}

void OgreCMLog::logDebug( const std::string& message) {
	logMessageDebug(message);
}


