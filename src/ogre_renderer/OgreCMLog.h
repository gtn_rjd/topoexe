#pragma once
#include "CMLog.h"

void initLogSystem(bool debug);
void logMessage(const std::string& msg);
void logMessageDebug(const std::string& msg);

class OgreCMLog: public CMLog {
public:
	static void init();
   //	static CMLog* inst();
	virtual void log( const std::string& message);
    virtual void logDebug( const std::string& message);

};


