#ifdef _DEBUG
#pragma link "ogre_dbg.lib"
#pragma link "OgreGLPlugin_dbg.lib"
#pragma link "free_image_dbg.lib"
#pragma link "freetype_dbg.lib"
#pragma link "zlib_dbg.lib"   
#pragma link "GeographicLib_dbg.lib"
#pragma link "qhull_dbg.lib"
#else
#pragma link "ogre_rel.lib"
#pragma link "OgreGLPlugin_rel.lib"
#pragma link "free_image_rel.lib"
#pragma link "freetype_rel.lib"
#pragma link "zlib_rel.lib"
#pragma link "GeographicLib_rel.lib"
#pragma link "qhull_rel.lib"
#endif

#include "OgreCMRenderer.h"
#include "OgreEntity.h"
#include "RenderSystems/GL/OgreGLPlugin.h"
#include "OgreConfigFile.h"
#include "OgreRoot.h"
#include "OgreViewport.h"
#include "OgreRenderWindow.h"
#include "OgreCamera.h"
#include "CMAssertions.h"
#include <cmath>
#include "OgreCMCaveView.h"
#include "OgreManualObject.h"
#include "OgreCMLog.h"

using namespace Ogre;

OgreRenderer::OgreRenderer(HWND hWnd, bool debugMode) : hWnd(hWnd), root(NULL), gLPlugin(NULL), renderWindow(NULL), sceneMgr(NULL), camera(NULL), viewport(NULL), ogreEntity(NULL),
    cavesNode(NULL), light(NULL), grid(NULL), rootNode(NULL),
    // gridMode(GridMode::NONE),
    cavesProxyNode(NULL), cave(NULL), debugMode(debugMode) {
}

OgreRenderer::~OgreRenderer() {
    deinit();
}

void OgreRenderer::loadResources(const Ogre::String& configFilename) {
    // Add all the resources in the resource configuration file
    Ogre::ConfigFile configFile;
    configFile.load(configFilename);
    Ogre::ConfigFile::SectionIterator sectionIter = configFile.getSectionIterator();
    Ogre::String secName, typeName, archName;
    while (sectionIter.hasMoreElements()) {
        secName = sectionIter.peekNextKey();
        Ogre::ConfigFile::SettingsMultiMap* settings = sectionIter.getNext();
        Ogre::ConfigFile::SettingsMultiMap::iterator i;
        for (i = settings->begin(); i != settings->end(); ++i) {
            typeName = i->first;
            archName = i->second;
            Ogre::ResourceGroupManager::getSingleton().addResourceLocation(archName, typeName, secName);
        }
    }

    // Index all added resources
    Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups(); ;
}

bool OgreRenderer::init() {
    try {

        AssertReturn(!root, return false);
        root = new Root("plugins_d.cfg", "ogre.cfg", "ogre.log");

        initLogSystem(true);

        gLPlugin = new GLPlugin();
        Root::getSingleton().installPlugin(gLPlugin);

        // choose renderer
        // root->showConfigDialog();
        root->restoreConfig();
        root->initialise(false);

        NameValuePairList misc;
        misc["externalWindowHandle"] = StringConverter::toString((int)hWnd);

        renderWindow = root->createRenderWindow("Main RenderWindow", 10, 10, false, &misc);

        loadResources("resources_d.cfg");

        sceneMgr = root->createSceneManager(Ogre::ST_GENERIC);
        // add a camera
        camera = sceneMgr->createCamera("MainCam");
        camera->setProjectionType(PT_ORTHOGRAPHIC);

        Quaternion q;
        q.FromAxes(Vector3::UNIT_Y, Vector3::NEGATIVE_UNIT_Z, Vector3::NEGATIVE_UNIT_X);
        camera->setOrientation(q);
        camera->setPosition(-10000, 0, 0);
        camera->setNearClipDistance(0.0000000001);
        camera->setFarClipDistance(20000);
        // add viewport
        viewport = renderWindow->addViewport(camera);
        if (debugMode)
            viewport->setBackgroundColour(Ogre::ColourValue(0.5, 0, 0));
        else
            viewport->setBackgroundColour(Ogre::ColourValue(0, 0, 0));

        camera->setAutoAspectRatio(true);

        rootNode = sceneMgr->getRootSceneNode()->createChildSceneNode();

        cavesProxyNode = rootNode->createChildSceneNode();
        cavesNode = cavesProxyNode->createChildSceneNode();
        if (debugMode) {
            ogreEntity = sceneMgr->createEntity("ogrehead.mesh");
            ogreNode = rootNode->createChildSceneNode();
            ogreNode->attachObject(ogreEntity);
            ogreNode->setOrientation(q); // look to camera;
        }

        gridNode = rootNode->createChildSceneNode();

        sceneMgr->setAmbientLight(Ogre::ColourValue(0.9, 0.9, 0.9));

        light = sceneMgr->createLight("MainLight");
        light->setPosition(camera->getPosition() + V3(0, 10000, -5000));

        setEnabled(false);

        LOG("OgreRenderer inited");

    }
    catch (...) {
        return false;
    }

    return true;
}

void OgreRenderer::deinit() {

    root->destroyRenderTarget(renderWindow);
    root->destroySceneManager(sceneMgr);

    root = NULL;
    gLPlugin = NULL;
    renderWindow = NULL;
    sceneMgr = NULL;
    camera = NULL;
    viewport = NULL;
    ogreEntity = NULL;
    cavesNode = NULL;
    light = NULL;
    grid = NULL;
    rootNode = NULL;
}

void OgreRenderer::resize(int width, int height, int toolsHeight, int statusHeight) {
    AssertReturn(viewport, return);
    renderWindow->windowMovedOrResized();

    float actualHeight = height - toolsHeight - statusHeight;
    viewport->setDimensions(0, (float)toolsHeight / height, 1, actualHeight / height);

    AssertReturn(camera, return);
    camera->setOrthoWindow(width, actualHeight);
    refresh();
}

void OgreRenderer::refresh() {
    AssertReturn(root, return);
    root->renderOneFrame();
}

int powOf2(float v) {
    return pow(2, ceil(log(v) / log(2.0f)));
}

void OgreRenderer::updateGrid(int step) {
    // return;
    AssertReturn(sceneMgr, return);
    AssertReturn(rootNode, return);

    if (step == 0) {
        if (grid) {
            sceneMgr->destroyManualObject(grid);
            grid = NULL;
        }
        gridStep = step;
    }
    else {
        if (!grid) {
            grid = sceneMgr->createManualObject("manual");
            gridNode->attachObject(grid);
            grid->setRenderQueueGroup(RenderQueueGroupID::RENDER_QUEUE_BACKGROUND); // AndPriority(0);
        }
        gridNode->setScale(cavesProxyNode->getScale());
        gridNode->setPosition(5000, 0, 0);

        Vector2 sceneSize(camera->getOrthoWindowWidth(), camera->getOrthoWindowHeight());
        sceneSize.x = /* powOf2 */ (sceneSize.x / gridNode->getScale().x);
        sceneSize.y = /* powOf2 */ (sceneSize.y / gridNode->getScale().y);

        if (sceneSize == gridSize && gridStep == step)
            return;

        gridSize = sceneSize;
        gridStep = step;

        grid->clear();
        grid->begin("BaseWhiteNoLighting", RenderOperation::OT_LINE_LIST);
        grid->colour(0, 80 / 255.0, 80 / 255.0, 1);
        // define vertex position of index 0..3

        // if (gridMode == GridMode::SMALL)
        // gridStep = 5 * 100 ;
        // if (gridMode == GridMode::HUDGE)
        // gridStep = 10 * 100;

        float i = -sceneSize.x / 2;
        while (i <= sceneSize.x / 2) {
            float j = -sceneSize.y / 2;
            while (j <= sceneSize.y / 2) {
                grid->position(0.0, -sceneSize.x, j);
                grid->position(0.0, sceneSize.x, j);

                // grid->position(0.0, -sceneSize.x, j);
                // grid->position(0.0, sceneSize.x, j);

                grid->position(0.0, i, -sceneSize.y);
                grid->position(0.0, i, sceneSize.y);

                // grid->position(0.0, -i, -sceneSize.y);
                // grid->position(0.0, -i, sceneSize.y);

                j += gridStep * 100;
            }

            i += gridStep * 100;
        }

        // // define usage of vertices by refering to the indexes
        // grid->index(0);
        // grid->index(1);
        // grid->index(2);
        // grid->index(3);
        // grid->index(0);

        grid->end();
    }
}

void OgreRenderer::setRotation(float rotZ, float rotY) {
    cavesProxyNode->resetOrientation();
    cavesProxyNode->rotate(Vector3::UNIT_Y, Ogre::Radian(rotY));
    cavesProxyNode->rotate(Vector3::UNIT_Z, Ogre::Radian(-rotZ));

    if (caveViewPrefs.showOutline) {
        setCaveViewPrefs(caveViewPrefs);
        if (timeToRefresh <= 0) {
            if (wsize < 500)
                timeToRefresh = 0.25;
            else if (wsize < 1000)
                timeToRefresh = 0.5;
            else if (wsize < 2000)
                timeToRefresh = 1;
//            else
//                timeToRefresh = 2;
        }
    }
}

Ogre::Vector3 OgreRenderer::getLocalLookDir() const {
    V3 lookDir = camera->getDirection();
    V3 localLookDir = cavesProxyNode->getOrientation().Inverse() * lookDir;
    localLookDir.normalise();
    return localLookDir;
}

void OgreRenderer::setCaveCenter(float x, float y, float z) {
    cavesNode->setPosition(OGRE_IMPORT_AXIS_X(-x), OGRE_IMPORT_AXIS_Y(-y), OGRE_IMPORT_AXIS_Z(-z));
    // ogreNode->setPosition(OGRE_IMPORT_AXIS_X(x), OGRE_IMPORT_AXIS_Y(y), OGRE_IMPORT_AXIS_Z(z));
}

void OgreRenderer::setScale(float scale) {
    scale = 1 / scale;
    cavesProxyNode->setScale(scale, scale, scale);
    if (gridStep != 0) {
        updateGrid(gridStep);
    }
}

void OgreRenderer::setGridMode(int gridSize) {
    updateGrid(gridSize);
}

bool OgreRenderer::isEnabled() {
    return rootNode && rootNode->getParent() != NULL;
}

void OgreRenderer::setEnabled(bool enabled) {
    if (isEnabled() != enabled) {
        if (enabled)
            sceneMgr->getRootSceneNode()->addChild(rootNode);
        else
            rootNode->getParent()->removeChild(rootNode);

        if (enabled && cave) {
            cave->enable();
        }

        refresh();
    }
}

void OgreRenderer::setCaveData(P3D* data, int size, W3D* wdata, int wsize, const EquatesArrayType& equates, const std::vector<std::string>names, const CaveViewPrefs& prefs,
    bool convertToExtendedElevation) {
    delete cave;
    cave = new OgreCaveView(sceneMgr, data, size, wdata, wsize, equates, names, prefs, convertToExtendedElevation);
	cave->attachToNode(cavesNode);
	setCaveViewPrefs(caveViewPrefs);
    if (isEnabled())
        cave->enable();
    OgreRenderer::wsize = wsize;
}

void OgreRenderer::setCaveViewPrefs(const CaveViewPrefs& prefs) {
    float ambient = 1.0;
    switch (prefs.ambientLightStrength) {
    case CM::ALS_100:
        ambient = 1.0f;
        break;
    case CM::ALS_90:
        ambient = 0.9f;
        break;
    case CM::ALS_75:
        ambient = 0.75f;
        break;
    case CM::ALS_50:
        ambient = 0.5f;
        break;
    case CM::ALS_30:
        ambient = 0.3f;
        break;
    case CM::ALS_10:
        ambient = 0.1f;
        break;
    case CM::ALS_00:
        ambient = 0.0f;
        break;
    }
    sceneMgr->setAmbientLight(Ogre::ColourValue(ambient, ambient, ambient));
    V3 prevLookDir = caveViewPrefs.lookDirrection;
    caveViewPrefs = prefs;
    if (caveViewPrefs.lookDirrection.isZeroLength() || (caveViewPrefs.showOutline && timeToRefresh <= 0)) {
        caveViewPrefs.lookDirrection = getLocalLookDir();
    }
    else {
        caveViewPrefs.lookDirrection = prevLookDir;
    }
    if (cave) {
        cave->setCaveViewPrefs(caveViewPrefs);
    }
}

void OgreRenderer::update(float dt) {
    if (timeToRefresh > 0 && timeToRefresh <= dt) {
        setCaveViewPrefs(caveViewPrefs);
    }
    timeToRefresh = std::max(0.0f, timeToRefresh - dt);
}

//void OgreRenderer::updateForConvexRoughWallsMode(CaveViewPrefs& caveViewPrefs) {
//    caveViewPrefs.wallsPropagateMode = CM::WPM_NONE;
//    caveViewPrefs.wallsShadowMode = CM::WSM_ROUGH;
//    caveViewPrefs.wallsBlowMode = CM::WBM_NONE;
//    caveViewPrefs.wallsSurfaceMode = CM::WSFM_SURF;
//	caveViewPrefs.wallsTrianglesBlowMode = CM::WTBM_NONE;//WTBM_9;
//	caveViewPrefs.wallsTrianglesBlowStrength = CM::WTBS_LOW;
//	caveViewPrefs.wallsSegmentTriangulationMode = CM::WSTM_CONVEX_HULL;
//	caveViewPrefs.generateWallsForNoWallsPiketsMode = CM::GWNWPM_SKIP;
//	caveViewPrefs.showSections = false;
//    caveViewPrefs.showWallLines = false;
//	caveViewPrefs.showOutline = false;
//}

void OgreRenderer::updateForRoughWallsMode(CaveViewPrefs& caveViewPrefs) {
    caveViewPrefs.wallsPropagateMode = CM::WPM_NONE;
    caveViewPrefs.wallsShadowMode = CM::WSM_ROUGH;
    caveViewPrefs.wallsBlowMode = CM::WBM_NONE;
    caveViewPrefs.wallsSurfaceMode = CM::WSFM_SURF;
    caveViewPrefs.wallsTrianglesBlowMode = CM::WTBM_NONE;
	caveViewPrefs.wallsTrianglesBlowStrength = CM::WTBS_LOW;
	caveViewPrefs.wallsSegmentTriangulationMode = CM::WSTM_CONVEX_HULL;
//	caveViewPrefs.generateWallsForNoWallsPiketsMode = CM::GWNWPM_BUDGE;
    caveViewPrefs.showSections = false;
	caveViewPrefs.showSplay = false;
	caveViewPrefs.showOutline = false;
}

void OgreRenderer::updateForSmoothWallsMode(CaveViewPrefs& caveViewPrefs) {
	caveViewPrefs.wallsPropagateMode = CM::WPM_NONE;
	caveViewPrefs.wallsShadowMode = CM::WSM_ROUGH;
	caveViewPrefs.wallsBlowMode = CM::WBM_NONE;
	caveViewPrefs.wallsSurfaceMode = CM::WSFM_SURF;
	caveViewPrefs.wallsTrianglesBlowMode = CM::WTBM_9;
	caveViewPrefs.wallsTrianglesBlowStrength = CM::WTBS_LOW;
	caveViewPrefs.wallsSegmentTriangulationMode = CM::WSTM_CONVEX_HULL;
//	caveViewPrefs.generateWallsForNoWallsPiketsMode = CM::GWNWPM_BUDGE;
	caveViewPrefs.showSections = false;
	caveViewPrefs.showSplay = false;
	caveViewPrefs.showOutline = false;
}

void OgreRenderer::updateForSectionsWallsMode(CaveViewPrefs& caveViewPrefs) {
	caveViewPrefs.wallsPropagateMode = CM::WPM_NONE;
	caveViewPrefs.wallsShadowMode = CM::WSM_ROUGH;
	caveViewPrefs.wallsBlowMode = CM::WBM_NONE;
	caveViewPrefs.wallsSurfaceMode = CM::WSFM_NONE;
	caveViewPrefs.wallsTrianglesBlowMode = CM::WTBM_NONE;
	caveViewPrefs.wallsTrianglesBlowStrength = CM::WTBS_LOW;
	caveViewPrefs.wallsSegmentTriangulationMode = CM::WSTM_CONVEX_HULL;
  //	caveViewPrefs.generateWallsForNoWallsPiketsMode = CM::GWNWPM_BUDGE;
	caveViewPrefs.showSections = true;
	caveViewPrefs.showSplay = true;
	caveViewPrefs.showOutline = false;
}

void OgreRenderer::updateForOutlineMode(CaveViewPrefs& caveViewPrefs) {
	caveViewPrefs.wallsPropagateMode = CM::WPM_NONE;
	caveViewPrefs.wallsShadowMode = CM::WSM_ROUGH;
	caveViewPrefs.wallsBlowMode = CM::WBM_NONE;
	caveViewPrefs.wallsSurfaceMode = CM::WSFM_NONE;
	caveViewPrefs.wallsTrianglesBlowMode = CM::WTBM_NONE;
	caveViewPrefs.wallsTrianglesBlowStrength = CM::WTBS_LOW;
	caveViewPrefs.wallsSegmentTriangulationMode = CM::WSTM_CONVEX_HULL;
//	caveViewPrefs.generateWallsForNoWallsPiketsMode = CM::GWNWPM_BUDGE;
	caveViewPrefs.showSections = false;
    caveViewPrefs.showSplay = false;
    caveViewPrefs.showOutline = true;
}

void OgreRenderer::saveToFile(const std::string& file) {
	renderWindow->writeContentsToFile(file);
}
