#pragma once
#include "windows.h"
#include "OgrePrerequisites.h"
#include "OgreVector2.h"
#include "CaveData.h"
#include <vector>

// #pragma package(smart_init)

// enum class GridMode {
// NONE,
// SMALL,
// HUDGE
// };

class OgreCaveView;

class OgreRenderer {

public:
	//static void updateForConvexRoughWallsMode(CaveViewPrefs& caveViewPrefs);
    static void updateForRoughWallsMode(CaveViewPrefs& caveViewPrefs);
    static void updateForSmoothWallsMode(CaveViewPrefs& caveViewPrefs);
    static void updateForSectionsWallsMode(CaveViewPrefs& caveViewPrefs);
    static void updateForOutlineMode(CaveViewPrefs& caveViewPrefs);

    OgreRenderer(HWND hWnd, bool debugMode);
    ~OgreRenderer();

    void resize(int width, int height, int toolsHeight, int statusHeight);
    void refresh();
    void setEnabled(bool enabled);
    bool isEnabled();
    void setRotation(float rotZ, float rotY);
    void setGridMode(int gridStep);
    void setScale(float zoom);
    void setCaveCenter(float x, float y, float z);
    void setCaveData(P3D* data, int size, W3D* wdata, int wsize, const EquatesArrayType& equates, const std::vector<std::string>names, const CaveViewPrefs& prefs,
        bool convertToExtendedElevation);

    bool isDebugMode() {
        return debugMode;
    }
    void setCaveViewPrefs(const CaveViewPrefs& prefs);

    bool init();

    void update(float dt);

	void saveToFile(const std::string& file);

protected:
    void deinit();

    void loadResources(const Ogre::String& configFilename);
    void updateGrid(int gridSize);
    Ogre::Vector3 getLocalLookDir() const ;

    HWND hWnd;

    Ogre::Root *root;
    Ogre::Plugin* gLPlugin;
    Ogre::RenderWindow* renderWindow;
    Ogre::SceneManager* sceneMgr;
    Ogre::Camera* camera;
    Ogre::Viewport* viewport;
    Ogre::Entity* ogreEntity;
    Ogre::Light* light;

    Ogre::SceneNode* rootNode;
    Ogre::SceneNode* cavesProxyNode;
    Ogre::SceneNode* cavesNode;
    Ogre::SceneNode* ogreNode;

    Ogre::ManualObject* grid;
    Ogre::SceneNode* gridNode;
    Ogre::Vector2 gridSize;
    int gridStep {
        0
    };
    // GridMode gridMode;

    OgreCaveView* cave;
    CaveViewPrefs caveViewPrefs;

    const bool debugMode;

    float timeToRefresh {
        0
    };

    int wsize {
        0
    };
};
