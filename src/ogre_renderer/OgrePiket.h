//---------------------------------------------------------------------------

#pragma once 
#include "CaveData.h"
#include <vector>           
#include <map>
#include "OgrePrerequisites.h" 
#include "CMHelpers.h"
#include <unordered_map> 



//struct OgrePiket {
//	struct P3DInfo {
//		P3DInfo(const P3D* p): p3d(p), hasWalls(0) { }
//		const P3D* p3d;
//		int hasWalls;
//	};
//public:
//	OgrePiket(int id, const P3D* piket) :
//	id(id),
//	piket(piket) {
//	//wallsCenter(0, 0, 0),
//	//wallsMassCenter(0, 0, 0),
////	piketEffectivePos(0, 0, 0),
////	dirrection(0, 0, 0) {
//		allP3D.push_back(P3DInfo(piket));
//	}
//
////    void preProcessWalls(const CaveViewPrefs& caveViewPrefs);
////    void recalcPosCenterDirrection();
////    bool isInactive() const;
//
//    // ��������
//    // hasPriz(priz) != !hasNoPriz(priz) � ����� ������ !!!
//    // ��������
////    bool hasPriz(P3DPriz priz) const;
////    bool hasNoPriz(P3DPriz priz) const;
////
////    bool addP3D(const P3D* addinP3D) { allP3D.push_back(P3DInfo(addinP3D)); }
////    bool addW3D(const P3D* parentP3D, const W3D* w3d);
////
////    std::vector<const OgrePiket*> getAdjPiketsWithPriz(P3DPriz prz) const;
////    std::vector<const OgrePiket*> getAdjPiketsWithoutPriz(P3DPriz prz) const;
////	P3DPriz getSumPriz() const;
////
////	int getPrevailWallColor() const;
////	int getColorOfP3DWithPriz(P3DPriz priz) const;
////
////    float getMaxDimension() const;
//
//protected:
////    void processPiketPosAsWall();
////    void updateEffectivePos(); // ���������� ����� ����
////    void updateWallsCenter(); // ���������� ����� ����
////    void updateDirrection(); // ���������� ������� � ������� ���� ������������� �������
////    void propagateWalls(WallsPropagateMode propMode, WallsBlowMode blowMode); // ���������� �����
////    std::vector<PiketWall> propagateWallAngleAbove(int wallId1, int wallId2, int num, WallsBlowMode blowMode ); // ������� ����������� ���� �� ������ ��������� ��� � �������
////    std::vector<PiketWall> propagateWallBesier3(int h, int i, int j, int k, int addWallsNum, float strong = 0.4f); // �������� ����������� ���� �� ������ ����� 3 �������
////    void classifyWalls(); // ��������� �� ���������� � ������� �����
////    //V3 getWallsMassCenter(V3 dirrection); // �������� ����������� �������� ����� ���� �������������� ����
////
////    void debugDraw(V3 a, V3 b, Ogre::ColourValue col = Ogre::ColourValue(1, 1, 0, 1));
//
//public:
//    const int id;
//
//	const P3D* const piket;
////
//    std::vector<const OgrePiket*> adjPikets;
////    std::vector<const OgrePiket*> adjFakePikets;
//    std::vector<const W3D*> allWalls;
//    std::vector<P3DInfo> allP3D;
////    V3 wallsCenter;
////    //V3 wallsMassCenter;
////    V3 piketEffectivePos;
////    V3 dirrection;
////
////	std::vector<PiketWall> classifiedWalls;
////
////    static Ogre::ManualObject* debugManualObject;
//};
