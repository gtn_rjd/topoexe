﻿//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include <math.h>
#include <stdio.h>

#include "FormEdit.h"
#include "formtrans.h"
#include "TOpO.h"
#include "vgs_utm.cpp"    // перевод WGS - UTM
#include <GeographicLib/LocalCartesian.hpp>
#include <GeographicLib/Geocentric.hpp>
#include <GeographicLib/UTMUPS.hpp>
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TTransForm *TransForm;

// ************************** Глобальные переменные *************************
bool cvx = false; // флаг файла проекта (может сод. рельеф)
bool cav = false; // флаг - если есть пещерные пикеты
int state = 0; // состояние
int pik = 0; // номер текущего пикета (число пикетов)
int vet = 1; // номер текущей ветки (число веток)
int vet0 = 1; // число пещерных веток (т.е. без отвеч. point)
int fxp = 0;             // текущее число фикс. точек (число точек)
int srv = 1;             // текущее число съемок (число съемок)
int wal = 0;             // текущее число измер. до стен (число изм. до стен)
int fil = 0;             // текущее число includ-файлов (число includ-файлов)
int com_num=0;           // текущее число комментариев
int wcom_num=0;          // текущее число комментариев к стенам
float magnit=0;          // магнитное склонение
int pre_pik=0;           // размер массива пикетов (опред. в pre())
int pre_pnt=0;           // размер массива фикс. точек (опред. в pre())
int pre_srv=0;           // размер массива съемок (опред. в pre())
int pre_wal=0;           // размер массива точек стен (опред. в pre())

//-----
int   line_number = 0;     // номер текущей строки текущего файла
char  color=2,sur_color=8; // текущий цвет и цвет пов. топо (Green и DarkGray по умолч.)
char  dup_color=8;         // цвет duplicate. топо (DarkGray по умолч.)
float corrL=1,corrA,corrC; // перем. плавной коррекции длины и азимута
float corrLabs=0;          // переменная для обгрызанных рулеток
float corrH=0;             // переменная коррекции высоты
char  corrA1 = 0;
char  corrA2 = 0;       // перем. дискр. коррекции азимута, поверхности,
char  corrC1 = 0;
char  corrC0 = 0;       // угла
int   walls=0;             // число "иголок" ежика обрисовки
int   lrud=0;              // число данных обрисовки (lrud)
bool  walls_from_to=true;  // флаг - к какому пикету относится lrud
bool  sur = false;
bool  duplic = false;          // флаги для #surface, #duplicate
bool  new_survey = false;          // флаг для начала новой съемки
signed char truth=0;       // перем. достоверности съемки
bool  z_survey=false;      // индикатор зигзаговой съемки зала
bool  ignore_walls=false;  // игнорирование отстрелов до стен при построении поверхности
bool  only_convex_walls=false;  // отбрасывать невыпуклые стены при построении объема
bool  lr_normal=false;      // стены left и right перпендикулярны пикету


char  number = 0;              // перекл. ввода с/без номеров пикетов
bool  angle = 1;             // перекл. угол наклона/перепад
char  ex_prz = 0;              // выход из обраб. текущего файла по #exit
char  comment = 0;           // признак комментария
char  razvertka = 1;         // признак развертки
float razv_angle = 0;        // "угол" развертки
wchar_t order[]=L"12300000";     // порядок ввода (data_order)

wchar_t *stroka0=NULL;        // текущая строка (для обработки ошибок)
//-----
UnicodeString cave_name = L"";   // название пещеры
UnicodeString region_name = L""; // region
UnicodeString fix_label = L"";   // метка точки с фикс. координатами
UnicodeString full_prefix =L"."; // полный префикс
UnicodeString curr_prefix = L""; // текущий префикс
UnicodeString survey_prefix=L""; // префикс текущей съемки
UnicodeString AscFile=L"";       // имя файла поверхности
UnicodeString CavePath=L"";      // путь к заглавному файлу проекта
wchar_t cave_file[cave_file_Len]=L"";  // имя файла данных (включая путь)

TStringList* FileList;     // include files
TStringList* PrefList;     // префиксы include files
TStringList* StationList;  // имена (метки) пикетов
TStringList* WallStationList;  // имена (метки) пикетов, к кот. привязаны стены
struct FileParam {float Fmagnit = 0.0f; char Fnumber = 0;};   // прочие параметры include files
bool  *BifStation;         // т.бифуркации: 0-проходной пикет, 1-развилка или конец.
FError FirstError;         // строка и файл 1-й ошибки, общее число ошибок

//------------------------- декларации ----------------------------------------
bool InitData();                    //инициализируем данные перед работой
void error(int k, const wchar_t *stroka, const wchar_t *slovo);
int AddStation(wchar_t *slovo);
void SetStation(wchar_t *slovo, char i);
void end_prefix(wchar_t* stroka);

//*****************************  массивы *************************************
//struct Cave { name, region; };
//---------------------------------------------------------------------------
Survey *Survey1;   // указатель массива съемок
Piket *P1;         // указатель массива пикетов
Surface Srf;       // поверхность
Vetka *V1;         // указатель массива веток
GeographicLib::LocalCartesian* localCoordinateSystem = nullptr;
FixPoint *FixPnt;  // указатель массива фикс. точек
Wall *Wall1;     // указатель массива стен

//массив меток пикетом объединенных коммандой equate
EquatesArrayType Equates;

//------------------------------- Rings -------------------------------------
ring  *R = nullptr;          //указатель для массива т. замыкания колец и их веток, размер - число веток
int *VRN = nullptr;          //массив числа веток в кольце, размер примерно = числу колец
int *VR = nullptr;           //указатель для динамич. массива веток в кольцах

//****************************************************************************
//--------------------------- Описание функций Класса пикетов --------------------
void Piket::deCart(char mode)
{ //---перевод данных пикета в декартовы коорд.
   float x,y,z;
   if(mode){            //из сферических
    x=L*cos(An)*cos(Az);       y=L*cos(An)*sin(Az);       z=L*sin(An);}
   else{               //из цилиндрических
    x=sqrt(L*L-An*An)*cos(Az); y=sqrt(L*L-An*An)*sin(Az); z=An;}
   L=x; An=y; Az=z;
   if(corrH){Az = Az + corrH;}     // коррекция на пикете по высоте
}
//-----------------------
float Piket::pr_xy()
{  //проекция для разреза-развертки
 switch (proj)
  {case  1001: return  sqrt(L*L+An*An);
   case -1001: return  -sqrt(L*L+An*An);
   default:    return  L*cos(proj * 0.0174532)-An*sin(proj * 0.0174532);
  }
}
//-----------------------
void Piket::control()
{ //---проверка и коррекция текущего пикета  (корр. по высоте в deCart)
   char err=0;
   if(L<0)                            {error( 25,stroka0,L""); err=1;}
   if((Az<0)||(Az>360))               {error(-26,stroka0,L""); err=1;}
//   if( angle && ((An<-90)||(An>90)) ) {error( 27,stroka0,L""); err=1;}
   if(!angle && (abs(An)>L) )         {error( 28,stroka0,L""); err=1;}
   col =  col | color;            //цвет
   if(sur==1)    prz = prz | 1;   //поверхность
   if(duplic==1) prz2 = prz2 | 2; //дублирование
   if(walls_from_to) prz2 = prz2 | 4; //walls от 1-го пикетам
   if(z_survey) prz2 = prz2 | 8; //съемка зигзагом
   if(only_convex_walls) prz2 = prz2 | 32;
   if(lr_normal) prz2 = prz2 | 64;
//   if(ignore_walls) prz2 = prz2 | 64;
   prz1 = -truth;                 //точность  //cout <<(int)prz1
   if(err==1) {L=0.01; Az=0; An=0; return;}        // :)
   if(razvertka== 1) proj = 1001;
   if(razvertka==-1) proj =-1001;
   if(razvertka==0)  proj = razv_angle;

   if((metEnd>0)&&(metEnd == metBeg)) //если имя нач. и конца одинаково

   {metEnd=0; error(12,stroka0, UnicodeString(StationList->Strings[metBeg]).c_str()); }
   if(L==0) {/*L=0.01;*/ Az=0; An=0; return;}  // с L=0 может вылететь на кольцах! :)
   Az = Az + corrA;
   L  = L  * corrL + corrLabs;
   up = up  * corrL + corrLabs;
   down = down * corrL + corrLabs;
   left = left * corrL + corrLabs;
   right = right * corrL + corrLabs;
   if( angle && corrC1==1) An=90-An;
   if( angle && (abs(corrC) < (90-abs(An))) ) An=An+corrC;
   if( angle && ((An<-90)||(An>90)) ) {error( 27,stroka0,L""); err=1;}
   if( angle && corrC0==1) An=-An;
   if(corrA2==1) Az = 360 - Az;
   if(corrA1==1) Az = Az + 180;
   Az  = (Az + magnit)*0.0174532; //магн. скл.
   if(angle) An  =  An * 0.0174532;
}
//-----------------------
void Piket::MemSet0()
{//обнуление всех данных
   L=0;                     //потом переводим
   An=0;                    //в
   Az=0;                    //декартовы
   left=-1;
   right=-1;
   up=-1;
   down=-1;
   col=0;   //цвет
   prz=0;   //вх. признаки точки
   prz2=0;
   prz1=0;  //точность ( =-truth )
   Vet=0;   //номер текущей ветки
   Srv=0;   //номер текущей съемки
   line=0;  //номер строки в файле данных
   proj=0;  //переменная разреза-развертки
   Com=0;    //номер комментария
   metBeg=0; //номера меток начала
   metEnd=0; //и конца ветки
}
//--------------------------* длина вектора *-------------------------------
double L_vect(double x, double y, double z) {return sqrt(x*x+y*y+z*z);}

//--------------------------- Описание функций Класса стен --------------------
void Wall::deCart(char mode)
{ //---перевод данных пикета в декартовы коорд.
   float x,y,z;
   if(mode){            //из сферических
    x=L*cos(An)*cos(Az);       y=L*cos(An)*sin(Az);       z=L*sin(An);}
   else{               //из цилиндрических
    x=sqrt(L*L-An*An)*cos(Az); y=sqrt(L*L-An*An)*sin(Az); z=An;}
   L=x; An=y; Az=z;
   if(corrH){Az = Az + corrH;}     // коррекция на пикете по высоте
}
//-----------------------
void Wall::control()
{ //---проверка и коррекция текущего пикета  (корр. по высоте в deCart)
   char err=0;
   if(L<0)                            {error( 25,stroka0,L""); err=1;}
   if((Az<0)||(Az>360))               {error(-26,stroka0,L""); err=1;}
//   if( angle && ((An<-90)||(An>90)) ) {error( 27,stroka0,L""); err=1;}
   if(!angle && (abs(An)>L) )         {error( 28,stroka0,L""); err=1;}
   col =  col | color;            //цвет
   if(err==1) {L=0.01; Az=0; An=0; return;}        // заглушка :)
//   if(razvertka== 1) proj = 1001;
//   if(razvertka==-1) proj =-1001;
//   if(razvertka==0)  proj = razv_angle;
   if(L==0) {/*L=0.01;*/ Az=0; An=0; return;}  // с L=0 может вылететь на кольцах! :)
   Az = Az + corrA;
   L  = L  * corrL + corrLabs;
   if( angle && corrC1==1) An=90-An;
   if( angle && (abs(corrC) < (90-abs(An))) ) An=An+corrC;
   if( angle && ((An<-90)||(An>90)) ) {error( 27,stroka0,L""); err=1;}
   if( angle && corrC0==1) An=-An;
   if(corrA2==1) Az = 360 - Az;
   if(corrA1==1) Az = Az + 180;
   Az  = (Az + magnit)*0.0174532; //магн. скл.
   if(angle) An  =  An * 0.0174532;
}
//-----------------------


//--------------------------- Описание функций Класса веток ----------------------
//--------------------------- Описание функций Класса веток ----------------------
void Vetka::MemSet0()
{ //обнуление всех данных
   metBeg=0; metEnd=0;   //номера меток начала и конца ветки
   XB=0;YB=0;ZB=0;  XE=0;YE=0;ZE=0; //коорд. начала и конца веток
   XYB=0;                //коорд. начала проекции для развертки
   pikBeg=0; pikEnd=0;   //номера пикето начала и конца ветки
   con=0;                //признаки соединения с др. ветками
}
//-----------------------
void Vetka::Shift()
{ //используется при обработке колец
     XB=X(); YB=Y(); ZB=Z();
     XE=0;   YE=0;   ZE=0;
     con=0;
}
//--------------------------* vektor(vetka) *-------------------------------
float Vetka::vx(char k)  //вектор ветки v покомпонентно
{float x=0;
 for(int p=pikBeg; p<=pikEnd; p++)
   {
   switch(k)
    {case 1: x=x+P1[p].L;  break; // x
     case 2: x=x+P1[p].An; break; // y
     case 3: x=x+P1[p].Az; break; // z
     case 4: x=x+L_vect(P1[p].L,P1[p].An,P1[p].Az);  break; // L
     case 5: x=x+L_vect(P1[p].L,P1[p].An,0);        break; // Lxy
     case 6: x=x+abs(P1[p].Az);                    break; // Lz
     // для обработки с учетом точности съемки:
     case 7: x=x+ldexp(L_vect(P1[p].L,P1[p].An,P1[p].Az),P1[p].prz1); break; // L
     case 8: x=x+ldexp(L_vect(P1[p].L,P1[p].An,0),P1[p].prz1);       break; // Lxy
     case 9: x=x+ldexp(abs(P1[p].Az),P1[p].prz1);                   break; // Lz
     case 10: x=x+P1[p].pr_xy(); break;      //проекция ветки в гориз. плоскости
    }
   }
 return x;
}
//***************************************************************************
//
//                              ERRORS
//
//***************************** Errors ***************************************
int AddLine(UnicodeString textstr)
{
  return TransForm->Memo1->Lines->Add(textstr);
}
//----------------------------------------------------------------------------
void error(int k, const wchar_t *stroka, const wchar_t *slovo)
{
  UnicodeString Slovo = slovo; Slovo.TrimRight();
  UnicodeString Stroka = stroka; Stroka.TrimRight();
  UnicodeString Cave_file = cave_file;
  UnicodeString textstr = L"";
  UnicodeString pass = L" При обработке этот пикет будет пропущен.";

  if(k>0) textstr = L"Error:   ";
   else   textstr = L"Warning: ";
  if(abs(k)<60)
   {
	textstr += L"File " + Cave_file + "   Line " + UnicodeString(line_number+1);
    AddLine(textstr);
    AddLine(Stroka);
   }
  state=-1;  //указывает на ошибку при построчной обработке
  switch (k)
  {
// ошибки, выявляемые при считывании строки
   case 1: textstr = L" Unknown expression:  " + Slovo; break;
   case 2: textstr = L" Неправильно введен азимут:  " + Slovo; break;
   case 3: textstr = L" Неправильно введен угол:  " + Slovo; break;
   case 4: textstr = L" Строка содержит численные данные более чем на 1 пикет.\ \r\n";
           textstr += L" Возможно, неверно установлен режим ввода (при сплошной нумерации пикетов используйте команду #from_to)."; break;
   case 5: textstr = L" Строка содержит численные данные более чем на 1 пикет.\ \r\n";
           textstr += L" При обработке излишние данные будут пропущены."; break;
   case 6: {textstr = L" Строка оканчивается до ввода всех данных пикета.\ \r\n";
           textstr += pass; break;}
   case-6: {textstr = L" Строка оканчивается до ввода данных по ширине и высоте."; break;}
   case 7: textstr = L" Оператор  " + Slovo + "  не может стоять между численными данными одного пикета."; break;
   case 8: textstr = L" Метка  " + Slovo + "  не может стоять между численными данными одного пикета."; break;
   case 9: textstr = L" Неверно введен численный параметр " + Slovo; break;
   //   case-9:  textstr = L" Лишний знак конца комментария  } ."; break;
   case 10:textstr = L" Неверный аргумент команды #"+ Slovo + ".\ \r\n";
           textstr += L" Будет установлен порядок по умолчанию L Az An, что при обработке данных может привести к ошибкам."; break;
   case 11:textstr = L" При режиме ввода с номерами пикетов (#from_to) метки (^"+ Slovo + ") не учитываются.\ \r\n";  break;
   case 12:textstr = L" Одинаковые метки (номера) начала и конца пикета: "+ Slovo + ".\ \r\n";
   case 13:textstr = L" При режиме ввода с номерами пикетов (#from_to) команда #"+ Slovo + ") не используется.\ \r\n";  break;
   case 14:textstr = L" Команда #equate должна иметь два аргумента (метки пикетов).";  break;
   case 15:textstr = L" Команда #equate должна находиться в отдельной строке.";  break;

   case 18:textstr = L" При измерениях до стен надо указать ровно одну метку (станцию) и один знак -.\ \r\n";
           textstr += pass; break;
//   case 20:textstr = L" Две метки подряд: метка  "+ Slovo + "  не отделена от предыдущей данными пикета."; break;
   case 19:textstr = L" Две метки конца пикета."; break;
   case 20:textstr = L" Более двух меток на один пикет."; break;

   case 21:textstr = L" Неправильный ввод данных измерения до стены: должно быть ровно 3 параметра."; break;
   case 22:textstr = L" Ветка, оканчивающаяся этой строкой, не имеет меток,\ \r\n";
           textstr += L" а следовательно не может быть связана с другими пикетами."; break;
   case 23:textstr = L" При введении измерений до стен команды и метки не используются.";  break;
   case 24:textstr = L" Параметр x команды #walls[x] должен быть в интервале от 1 до 100.";  break;
   case 25:textstr = L" Длина пикета не может быть отрицательной.\ \r\n"; textstr += pass; break;
   case-26:textstr = L" Азимут вне промежутка [0,360].\ \r\n"; textstr += pass; break;
   case 27:textstr = L" Угол вне промежутка [-90,90].\ \r\n"; textstr += pass; break;
   case 28:textstr = L" Перепад высоты больше длины пикета.\ \r\n"; textstr += pass; break;
   case 29:textstr = L" Неполные данные точки (должны быть указаны метка, широта, долгота, высота).\ \r\n"; break;
   case 30:textstr = L" Неизвестный оператор #" + Slovo; break;
   case-31:textstr = L" Оператор  #" + Slovo + "  дважды применяется к одному пикету."; break;
   case 32:textstr = L" Оператор конца ветки  #end  должен находиться в строке после данных пикета,\ \r\n";
           textstr += L" а стоит перед данными нового пикета. При обработке он будет пропущен."; break;
   case 33:textstr = L" Оператор  #" + Slovo + "  использован без параметра.\ \r\n";
           textstr +=  " Он должен иметь вид #" + Slovo + "[x]  где  x - численный параметр."; break;
   case 34:textstr = L" Неверно введен численный параметр при операторе " + Slovo + ".\ \r\n";
           textstr +=  " Оператор должен иметь вид #" + Slovo + "[x]  где  x - численный параметр."; break;
   case 35:textstr = L" Оператор, задающий цвет, должен иметь вид #" + Slovo + "[n], 0<=n<=16, целое."; break;
   case 36:textstr = L" Коэффициент коррекции длины (параметр при #corr_L) должен быть положительным!"; break;
   case-37:textstr = L" Длина метра вашей рулетки сильно отличается от эталонного!\ \r\n";
           textstr +=  " Вы уверены, что правильно задали аргумент #corr_L ?"; break;
   case 38:textstr = L" Аргумент команды #truth[n] должен быть целым числом из интервала [-7,7]."; break;
   case 39:textstr = L" Магнитное склонение отличается от указанного ранее."; break;
   case-40:textstr = L" Оператор  #" + Slovo + "  может быть использован только 1 раз.\ \r\n";
           textstr +=  " При повторном использовании команда  #" + Slovo + "  игнорируется."; break;
   case 41:textstr = L" После оператора  #fix  должна следовать метка."; break;
   case 42:textstr = L" Строка оканчивается после оператора  #include  до ввода имени файла."; break;
   case 43:textstr = L" Неверный тип файла данных " + Slovo + " (возможны .dat, .cav, .asc)"; break;
   case-44:textstr = L" Операторы и метки между оператором #"+ Slovo + " и концом строки не обрабатываются."; break;
   case 45:textstr = L" Файл данных поверхности .asc может содержаться только в файлах типа .cvx"; break;
   case-46:textstr = L" Введено большое значение для коррекции угла наклона.\ \r\n";
           textstr +=  " Вы уверены, что правильно задали аргумент #corr_clino ?"; break;
   case-47:textstr = L" Аргумент команды #PR вне промежутка [0,360]."; break;
   case-48:textstr = L" Команда #"+ Slovo + " использована без указания аргумента."; break;
   case-49:textstr = L" Два комментария к одному пикету."; break;
   case 50:textstr = L" Префикс "+ Slovo + " не устанавливался или уже отменен."; break;
   case 52:textstr = L" В строке с оператором " + Slovo + " не должно быть других операторов или данных,\ \r\n";
           textstr +=  " кроме метки пикета (начинающейся с ^), с которого проводились имерения до стен."; break;


//прочие ошибки
   case-50:textstr = L" Отсутствует или закомментирован знак конца комментария } "; break;
   case-51:textstr = L" Метки " + Slovo + " в данных топосъемки нет"; break;
   case 57:textstr = L" Неверный тип файла данных ." + Slovo + " (возможны .dat, .cav, .cvx)"; break;

   case 60:textstr = L" There is not file  " + Slovo + "  \ \r\n";
           textstr +=  " File not open.\ \r\n"; break;
   case 61:textstr = L" File " + Slovo + " not open\n"; break;
   case 62:textstr = L" Невозможно записать файл данных cave.t3d."; break;
//   case 64:textstr = L" Топосъемка состоит из несвязанных между собой частей.\ \r\n";
   case 65:textstr = L" Метки  ^" + Slovo + ",  стоящей после оператора #fix,\ \r\n";
           textstr +=  " в данных топосъемки нет."; break;
   case-66:textstr = L" Кольцо  ^" + Slovo +  "  имеет длину меньше 10см."; break;
//   case 67:textstr = L" "Следующие измерения до стен не привязаны к нитке хода:\ \r\n"
   case 67:textstr = L" Ошибка конвертации обработки коордианат пикета " + Slovo; break;
   case 68:textstr = L" Ошибка привязки рельефа при пересчете координат"; break;
   case 69:textstr = L" Ошибка привязки рельефа, невозможно однозначено определить UTM зону"; break;

                     //sur_color,fix,mag_A - только один раз
  }
  AddLine(textstr); AddLine(L" ");
  if(abs(k)<60)
   {
   if( (FirstError.num==0)||(k>0)&&(FirstError.item<0) )//если не было ошибок
    {
     wcscpy(FirstError.file,cave_file);    //file
     FirstError.line=line_number;             //line
     FirstError.item=k;                       //err number
    }
    FirstError.num ++;    //общее число ошибок
   }
}
//****************************************************************************
//
//                              COMMANDS
//
//*************** Считывание параметра в [] при операторе ********************
wchar_t*  op_par(wchar_t *slovo)
{
 if( !wcschr(slovo,L'[') || !wcschr(slovo,L']') ) return L"err"; NULL;
 if( wcschr(slovo,L']') <= wcschr(slovo,L'[')+1 ) return L"err"; NULL;
 slovo = wcschr(slovo,L'[')+1;
 wcscpy(wcschr(slovo,L']'),L"");   //cout<<slovo<<'\n';
 return slovo;
}
//************************** Operators *************************************
void operat(wchar_t *oper, char k)
{
 wchar_t *err_symb = NULL;           //для обраб. ошибок в числ. данных
 int intN = 0;
 float floatN;         //             -//-

 oper = oper+1; //отбрасываем #
//операторы признаков одной точки (пикета)
// if(wcslen(oper)==3)     //k=0 если нач. точка пикета, -1 -конечная
// {
  if(!wcscmp(oper,L"end"))
   {
    if(number==-1) {error(13,stroka0,L"end"); return;} // в режиме from_to
    if(k==0) {error(32,stroka0,oper); return;}   //если в начале строки (до ввода данных)
    SetStation(L"",0);
    return;
   }
  char i=0;
  char j=0;
  if(!wcscmp(oper,L"sur")) i=1;
  if(!wcscmp(oper,L"riv")) i=2;
  if(!wcscmp(oper,L"ent")) {if(k==0) i=4;  else i=8;  }
  if(!wcscmp(oper,L"sif")) {if(k==0) i=16; else i=32; }
  if(!wcscmp(oper,L"red")) {if(k==0) i=64; else i=128;}
  if(!wcscmp(oper,L"ztn")) j=16;
  if(!wcscmp(oper,L"no_autogen_walls")) j=128;
  //if(i==0 && j==0) {error(30,stroka0,oper); return;}

  if(i != 0 || j != 0) {
	  if((P1[pik].prz & i) == i && (P1[pik].prz2 & j) == j) {
		error(-31, stroka0,oper);
	  } else {
		P1[pik].prz = P1[pik].prz | i;
		P1[pik].prz2 = P1[pik].prz2 | j;
	  }
      return;
  }

//  return;
// }
// else
// {
//"шапка"
  if(!wcscmp(oper,L"begin"))  return;   //для совместимости с предш. версией
  if(!wcscmp(oper,L"label"))  {number=0; return;}
  if(!wcscmp(oper,L"number")) {number=1; return;}
  if(!wcscmp(oper,L"from_to")){number=-1; return;}
  if(!wcscmp(oper,L"angle"))  {angle=1; return;}
  if(!wcscmp(oper,L"fall"))   {angle=0; return;}
  if(!wcscmp(oper,L"walls_from"))    {walls_from_to=1; return;}
  if(!wcscmp(oper,L"walls_to"))      {walls_from_to=0; return;}
  if(!wcscmp(oper,L"begin_z_survvey"))      {z_survey=true; return;}
  if(!wcscmp(oper,L"start_z_survvey"))      {z_survey=true; return;}
  if(!wcscmp(oper,L"end_z_survvey"))      {z_survey=false; return;}
  if(!wcscmp(oper,L"begin_lr_normal"))      {lr_normal=true; return;}
  if(!wcscmp(oper,L"start_lr_normal"))      {lr_normal=true; return;}
  if(!wcscmp(oper,L"end_lr_normal"))      {lr_normal=false; return;}
  if(!wcscmp(oper,L"begin_convex_surf"))      {only_convex_walls=true; return;}
  if(!wcscmp(oper,L"start_convex_surf"))      {only_convex_walls=true; return;}
  if(!wcscmp(oper,L"end_convex_surf"))      {only_convex_walls=false; return;}
  if(!wcscmp(oper,L"begin_ignore_walls"))      {ignore_walls=true; return;}
  if(!wcscmp(oper,L"start_ignore_walls"))      {ignore_walls=true; return;}
  if(!wcscmp(oper,L"end_ignore_walls"))      {ignore_walls=false; return;}
//коррекция и признаки
  if(!wcscmp(oper,L"corr_A_180"))    {corrA1=1; return;}
  if(!wcscmp(oper,L"end_corr_A_180")){corrA1=0; return;}
  if(!wcscmp(oper,L"corr_A_g"))      {corrA2=1; return;}
  if(!wcscmp(oper,L"end_corr_A_g"))  {corrA2=0; return;}
  if(!wcscmp(oper,L"end_corr_A"))    {corrA1=0; corrA2=0; corrA=0; return;}
  if(!wcscmp(oper,L"end_corr_L"))    {corrL=0;  return;}
  if(!wcscmp(oper,L"end_corr_L_beg")){corrLabs=0;return;}
  if(!wcscmp(oper,L"end_corr_H"))    {corrH=0;return;}
  if(!wcscmp(oper,L"end_corr"))
      {corrA1=0; corrA2=0; corrA=0; corrL=1; corrC=0; corrC1=0; corrC0=0;
       corrH=0; return;}
  if(!wcscmp(oper,L"corr_clino_90")) {corrC1=1;  return;}
  if(!wcscmp(oper,L"end_corr_clino_90")){corrC1=0;  return;}
  if(!wcscmp(oper,L"corr_clino_-")) {corrC0=1;  return;}
  if(!wcscmp(oper,L"end_corr_clino_-")){corrC0=0;  return;}
  if(!wcscmp(oper,L"end_corr_clino")){corrC=0; corrC0=0; corrC1=0; return;}
  if(!wcscmp(oper,L"end_color"))     {color=2;  return;}  //станд. зеленый цвет
  if(!wcscmp(oper,L"surface"))       {sur=1;    return;}
  if(!wcscmp(oper,L"end_surface"))   {sur=0;    return;}
  if(!wcscmp(oper,L"duplicate"))     {duplic=1; return;}
  if(!wcscmp(oper,L"end_duplicate")) {duplic=0; return;}
  if(!wcscmp(oper,L"end_declination"))    {magnit=0; return;}
  if(!wcscmp(oper,L"exit"))          {ex_prz=1; return;}
  if(!wcscmp(oper,L"R0"))   {razvertka= 1; return; } //для развертки
  if(!wcscmp(oper,L"R180")) {razvertka=-1; return; } //    -//-
  if(!wcscmp(oper,L"RR"))   {razvertka=-razvertka; return; } //развертка наоборот к текущей

  if((!wcscmp(oper,L"sur_color"))||(!wcscmp(oper,L"color"))     ||
     (!wcscmp(oper,L"corr_A"))   ||(!wcscmp(oper,L"corr_L"))    ||
     (!wcscmp(oper,L"declination"))   ||(!wcscmp(oper,L"corr_L_beg")) ||
     (!wcscmp(oper,L"corr_clino")) || (!wcscmp(oper,L"dup_color")) )
     {error(33,stroka0,oper); return;}

  if(!wcsncmp(oper,L"declination",11))    //магнитное склонение//!!оговорить повт. исп-е! (если меняется после ввода 1-го пикета)
   {floatN=wcstod(op_par(oper),&err_symb);
    if(*err_symb) {error(34,stroka0,L"declination"); return;}
    if(magnit==0) magnit=floatN;
     else if(magnit!=floatN) {error(39,stroka0,L"");}
    return;}
  if(!wcsncmp(oper,L"PR",2))    //угол проектирования
   {floatN=wcstod(op_par(oper),&err_symb);
    if(*err_symb) {error(34,stroka0,L"PR"); return;}
    if((floatN>360)||(floatN<-360)) {error(-47,stroka0,L""); return;}
     else {razv_angle=(int)floatN; razvertka = 0;}
    return;}
  if( (!wcsncmp(oper,L"sur_color",9))
     &&(sur_color==8) )           //!!если цвет по умолчанию - можно менять...
   {intN=int(wcstol(op_par(oper),&err_symb,10));
    if((*err_symb)||(intN>16)||(intN<0)) {error(35,stroka0,L"sur_color"); return;}
    sur_color=intN; return;}
  if( (!wcsncmp(oper,L"dup_color",9))
     &&(dup_color==8) )           //!!если цвет по умолчанию - можно менять...
   {intN=int(wcstol(op_par(oper),&err_symb,10));
    if((*err_symb)||(intN>16)||(intN<0)) {error(35,stroka0,L"dup_color"); return;}
    dup_color=intN; return;}
  if(!wcsncmp(oper,L"color",5))
   {intN=int(wcstol(op_par(oper),&err_symb,10));
    if((*err_symb)||(intN>16)||(intN<0)) {error(35,stroka0,L"color"); return;}
    color=intN; return;}
  if(!wcsncmp(oper,L"corr_A",6))
   {floatN=wcstod(op_par(oper),&err_symb);
    if(*err_symb) error(34,stroka0,L"corr_A"); else corrA=floatN; return;}
  if(!wcsncmp(oper,L"corr_L_beg",10))
   {floatN=wcstod(op_par(oper),&err_symb);
    if(*err_symb) { error(34,stroka0,L"corr_L_abs"); return; }
    corrLabs=floatN;
    return;}
  if(!wcsncmp(oper,L"corr_L",6))
   {floatN=wcstod(op_par(oper),&err_symb);
    if(*err_symb) { error(34,stroka0,L"corr_L"); return; }
	if(floatN<=0)  { error(36,stroka0,L""); return; }
    corrL=floatN; if((corrL>1.5)||(corrL<0.01)) error(-37,stroka0,L"");
    return;}
  if(!wcsncmp(oper,L"truth",5))
   {floatN=wcstod(op_par(oper),&err_symb);
    if(*err_symb) { error(34,stroka0,L"truth"); return; }
    if((abs(floatN)>7)||((int)(floatN)!=floatN)) {error(38,stroka0,L""); return;}
    truth=floatN; return;}
  if(!wcsncmp(oper,L"corr_clino",10))
   {floatN=wcstod(op_par(oper),&err_symb);
    if(*err_symb) error(34,stroka0,L"corr_A"); else
     {corrC=floatN; if(abs(corrC)>9) error(-46,stroka0,L"");} return;}
  if(!wcsncmp(oper,L"corr_H",6))
   {floatN=wcstod(op_par(oper),&err_symb);
    if(*err_symb) { error(34,stroka0,L"corr_H"); return; }
    corrH=floatN;
    return;}
 //}
 error(30,stroka0,oper);
// fix, cave, region, include, walls -- обработка непосредственно в строке
}
//***************************************************************************
//------------------------------- Slova(wchar_t) -----------------------------
UnicodeString StrIzSlov(wchar_t* stroka)  // разбивка на отдельные слова через пробел
{
 UnicodeString astroka=L"";
 wchar_t* slovo; const wchar_t *separators = L"\t\v\r\n ";
 slovo = wcstok(stroka,separators);
  while (slovo!=NULL)
  { astroka += slovo; astroka += L" "; slovo = wcstok(NULL,separators); }
 return astroka;
}
//------------------------------- Slovo[Astr,n] -----------------------------
UnicodeString Slovo(UnicodeString Astr, unsigned int n)
{
 int i=1; unsigned int maxn=0;
 while (i<=Astr.Length())
 {if(Astr[i]==L' ') maxn++; i++; }
 if(n>maxn) return L""; if(n==0) return NULL; //защита от вылета
 unsigned int s=0, k=1;
 while(s<n-1) { if(Astr[k]==L' ') s=s+1;  ++k; }
 s=k; k=0;
 while(1==1)  { ++k; if(Astr[s+k]==L' ') break; }
 return Astr.SubString(s,k);        //что-то не так, не всегда выдает 2-е слово
}
//------------------------------- FindLast -----------------------------
int FindLast(UnicodeString Astr, UnicodeString slovo) // посл. вхождение слова в строку
{
 int index = 0;
 int k=0;
 index = Astr.Pos(slovo); k += index;
 while(index!=0)
 {
  Astr = Astr.SubString(index+slovo.Length()-1, Astr.Length()-slovo.Length()-index+2);
  index = Astr.Pos(slovo); if(index!=0)   k += index -2 + slovo.Length();
 }
 return k;
}
//****************************** include files *****************************
void include(wchar_t* slovo)
{
 UnicodeString FPref = full_prefix;
 UnicodeString slovo1 = slovo;        // имя includ-файла
 UnicodeString PartPath = cave_file;  // нах. разницу между путями
 PartPath.Delete(1, CavePath.Length());
 PartPath.Delete(PartPath.LastDelimiter(L"\\")+1, PartPath.Length() );

 slovo1.Delete( 1, slovo1.LastDelimiter(L".") );
 if ( slovo1.CompareIC(L"cav")*slovo1.CompareIC(L"dat")==0 ) // отбор пещерных файлов
 {
  if( FileList->IndexOf(slovo)<0 ) {
   FileList->Add(PartPath+UnicodeString(slovo)); fil = fil+1;    // добавление файла в список
//   PrefList->Add(FPref.Delete(FPref.Length(),1));
   FileParam *FilePar = new FileParam; //{float Fmagnit; char FNumber;}
   FilePar->Fmagnit = magnit; FilePar->Fnumber = number;  //префикс, склонение, формат ввода
   PrefList->AddObject(FPref.Delete(FPref.Length(),1), (TObject*)FilePar);
//   PrefList->Strings[fil].Delete(PrefList->Strings[fil].Length()-1,1);
   } //префикс файла
   return;
 }
 if (slovo1.CompareIC(L"asc")==0)                   // отбор файла поверхности
 {
  if(cvx==false) {error(45,stroka0,slovo); return;}
  if(AscFile==L"") AscFile=PartPath+UnicodeString(slovo);
  return;
 }
 error(43,stroka0,slovo);    // неверный тип файла
}
//------------------------------ fix ^label --------------------------------
void fix(wchar_t* slovo)
{
 if(fix_label.Length()==0)   {
  if ( (slovo[0]==L'^')&&(wcslen(slovo)>1) ) {fix_label = slovo+1; return;}
  if (slovo[0]!=L'^') {fix_label = slovo; return;}
  error(41,stroka0,L"fix");   }
 else if(!cvx) error(-40,stroka0,L"fix");
}
//------------------------------ point ^label --------------------------------
bool point(wchar_t* stroka1, FIX_POINT_MODE mode)   //  mode 0 - WGS84, 1 - UTM
{
  wchar_t *slovo;
  UnicodeString Stroka0=stroka1;           // резервная копия
  const wchar_t *separators = L"\t\v\r\n ";
  wchar_t *err_symb = NULL;                //для обраб. ошибок в числ. данных

  double x = 0, y = 0, z = 0;
  if(wcslen(stroka1)!=0) {
   slovo = wcstok(stroka1,separators);
   if(slovo==NULL) {error(29,stroka0,L"point");  return false;}
   if(slovo[0]==L'^') slovo=slovo+1;
   if(wcslen(slovo)==0) {error(29,stroka0,L"point");  return false;}
   FixPnt[fxp].label = slovo;
  if((slovo[0]!=L'\\')&&(curr_prefix!=L"")) FixPnt[fxp].label = curr_prefix + FixPnt[fxp].label;
  if(slovo[0]==L'\\') FixPnt[fxp].label.Delete(1,1);   //добавл. (или не добавл.) префикс
   slovo = wcstok(NULL,separators);
   if(slovo!=NULL){ y = wcstod(slovo,&err_symb);
                    if(*err_symb) {error(9,stroka0,slovo); return false;}}
    else {error(29,stroka0,L"point"); return false;}
   slovo = wcstok(NULL,separators);
   if(slovo!=NULL){ x = wcstod(slovo,&err_symb);
                    if(*err_symb) {error(9,stroka0,slovo); return false;}}
    else {error(29,stroka0,L"point"); return false;}
   slovo = wcstok(NULL,separators);
   if(slovo!=NULL){ z = wcstod(slovo,&err_symb);
                    if(*err_symb) {error(9,stroka0,slovo); return false;}}
    else {error(29,stroka0,L"point"); return false;}
   slovo = wcstok(NULL,separators);
   while(slovo!=NULL)  //счит. команд
   {
    bool i=0;
    if(wcscmp(slovo,L"#ent")==0) {FixPnt[fxp].prz = FixPnt[fxp].prz | 1 ; i=1;}
    if(wcscmp(slovo,L"#way")==0) {FixPnt[fxp].prz = FixPnt[fxp].prz | 2 ; i=1;}
	if(wcscmp(slovo,L"#sif")==0) {FixPnt[fxp].prz = FixPnt[fxp].prz | 4 ; i=1;}
	if(wcscmp(slovo,L"#red")==0) {FixPnt[fxp].prz = FixPnt[fxp].prz | 8 ; i=1;}
	if(wcscmp(slovo,L"#ztn")==0) {FixPnt[fxp].prz = FixPnt[fxp].prz | 16 ; i=1;}
	if(wcscmp(slovo,L"#no_autogen_walls")==0) {FixPnt[fxp].prz = FixPnt[fxp].prz | 128 ; i=1;}
	if(wcscmp(slovo,L"#com")==0)
     {if(stroka1!=NULL)
       FixPnt[fxp].com = Stroka0.SubString(Stroka0.Pos(L"#com")+5,Stroka0.Length());
      i=1; break;}
    if(!i) error(1,stroka0,slovo);
    slovo = wcstok(NULL,separators);
   }

    if (mode==FPM_WGS84) {
        FixPnt[fxp].wgsX = x;
        FixPnt[fxp].wgsY = y;
        FixPnt[fxp].wgsH = z;
        FixPnt[fxp].sourceCS = FPM_WGS84;
    } else if (mode==FPM_UTM) {
        FixPnt[fxp].utmX = x;
        FixPnt[fxp].utmY = y;
        FixPnt[fxp].utmH = z;
        FixPnt[fxp].sourceCS = FPM_UTM;
    }

   fxp++;
   return true; }
  else {error(29,stroka0,L"point");
   return false;}
}
//------------------------------ CalcFixPointsLocalCoords) --------------------------------
void CalcFixPointsLocalCoordinates() {
    double medWGSX = 0;
    double medWGSY = 0;
    double medWGSH = 0;
    int numWGSpoints = 0;
    int utmZone = 0; // общая для всех координат утм зона
    bool utmNorth = true; // общее для всех коордират полушарие
    // считаем WGS координаты для всех кого можем
    for (int i = 0; i < fxp; i++) {
        FixPoint& point = FixPnt[i];
        try {
            if (point.sourceCS == FIX_POINT_MODE::FPM_WGS84) {
                bool northp;
                GeographicLib::UTMUPS::Forward(point.wgsY, point.wgsX, point.utmZone, point.utmNorth, point.utmX, point.utmY);
                point.utmH = point.wgsH;
            } else if (point.sourceCS == FIX_POINT_MODE::FPM_UTM && point.utmZone > 0) {
                GeographicLib::UTMUPS::Reverse(point.utmZone, point.utmNorth, point.utmX, point.utmY, point.wgsY, point.wgsX);
                point.utmH = point.wgsH;
            } else {
                continue;
            }

            medWGSX += point.wgsX;
            medWGSY += point.wgsY;
            medWGSH += point.wgsH;
            numWGSpoints ++;
            if ( point.utmZone > 0) {
                if (utmZone == 0) utmZone = point.utmZone;
                else if (utmZone != point.utmZone) utmZone = -1;
            }
        } catch (const std::exception& e) {
            error(67, point.label.c_str(), L"");
            std::cerr << "Caught exception: " << e.what() << "\n";
        }
    }

    // если utm зона для всех прочих координа одна
    // считаем WGS координаты для UTM тчек без указанной зоны
    if (utmZone > 0) {
        Srf.utmZone = utmZone;
        Srf.utmNorth = utmNorth;
        for (int i = 0; i < fxp; i++) {
            FixPoint& point = FixPnt[i];
            try {
                if (point.sourceCS == FIX_POINT_MODE::FPM_UTM && point.utmZone == 0) {
                    point.utmZone = utmZone;
                    point.utmNorth = utmNorth;
                    GeographicLib::UTMUPS::Reverse(point.utmZone, point.utmNorth, point.utmX, point.utmY, point.wgsY, point.wgsX);
                    point.utmH = point.wgsH;

                    medWGSX += point.wgsX;
                    medWGSY += point.wgsY;
                    medWGSH += point.wgsH;
                    numWGSpoints ++;
                }
            } catch (const std::exception& e) {
                error(67, point.label.c_str(), L"");
                std::cerr << "Caught exception: " << e.what() << "\n";
            }
        }
    }

    // строим локальную систему координат
    if (localCoordinateSystem) delete localCoordinateSystem;
    localCoordinateSystem = nullptr;

    medWGSX /= numWGSpoints;
    medWGSY /= numWGSpoints;
    medWGSH /= numWGSpoints;

    // если есть файл рельефа и есть однозначное указание зоны
    // то считаем началом локальной системы левый нижний угол рельефа
    if (Srf.flag) {
        if (utmZone > 0) {
            try {
                GeographicLib::UTMUPS::Reverse(utmZone, utmNorth, Srf.xllcorner, Srf.yllcorner, medWGSY, medWGSX);
                medWGSH = 0;
            } catch (const std::exception& e) {
                error(68, L"", L"");
                std::cerr << "Caught exception: " << e.what() << "\n";
            }
        } else {
             error(69, L"", L"");
        }
    }

    localCoordinateSystem = new GeographicLib::LocalCartesian(medWGSY, medWGSX, medWGSH);

    // считаем позицию всех точек в локальной системе
    for (int i = 0; i < fxp; i++) {
        FixPoint& point = FixPnt[i];
        try {
            if (point.wgsX == 0 && point.wgsY == 0 && point.wgsH == 0) continue;
            localCoordinateSystem->Forward(point.wgsY, point.wgsX, point.wgsH, point.localX, point.localY, point.localZ);
        } catch (const std::exception& e) {
            error(67, point.label.c_str(), L"");
            std::cerr << "Caught exception: " << e.what() << "\n";
        }
    }

    if (Srf.flag && Srf.utmZone > 0) {
        // пересчитываем рельеф в локальную систему координат
        Srf.X = new long[Srf.ncols*Srf.nrows];
        Srf.Y = new long[Srf.ncols*Srf.nrows];
        for(int i=0; i < Srf.nrows; ++i) {
            for(int k=0; k < Srf.ncols; ++k) {
                int x0 = Srf.xcellsize*k;
                int y0 = Srf.ycellsize*(Srf.nrows-i-1);

                double wgsX, wgsY;
                GeographicLib::UTMUPS::Reverse(Srf.utmZone, Srf.utmNorth, x0 + Srf.xllcorner, y0 + Srf.yllcorner, wgsY, wgsX);

                double localX, localY, localZ;
                localCoordinateSystem->Forward(wgsY, wgsX, Srf.Z[i*Srf.ncols+k], localX, localY, localZ);
                Srf.Z[i*Srf.ncols+k]= localZ;
                Srf.Y[i*Srf.ncols+k]= localY;
                Srf.X[i*Srf.ncols+k]= localX;
            }
        }
    }
}
//------------------------------ FixPoint::GradToM() --------------------------------
//bool FixPoint::GradToM()
//{
// double  Pi = 3.14159265358979;  // Число Пи
//// double aW = 6378137;            // Большая полуось эллипсоида WGS84, м
//// double alW = 1 / 298.257223563; // Сжатие
//// double e2W = 2*alW - alW*alW;   // Квадрат эксцентриситета
// double C,S;
// int zone;                        // номер зоны в UTM
// long double Nn,Ee;
//
// if (Srf.ncols == 0) return true;
//
// C=cos(Srf.convergence*M_PI/180);  S=sin(Srf.convergence*M_PI/180);
// for(int i=0; i<fxp; i++)
// {if(FixPnt[i].x==0)              // если в град. WGS - перев в UTM
//   {
//    zone = 31 + int(FixPnt[i].xGrad/6); //Номер зоны = 31 + int(Долг_в_гр/6) , Бзыбь - 37 зона.
//    wgs_to_utm(FixPnt[i].yGrad,FixPnt[i].xGrad,zone,&Nn,&Ee);
//    FixPnt[i].x = Ee;
//    FixPnt[i].y = Nn;
//   }
//            // UTM - в "локальные" коорд. (в метрах от llcorner)
//   FixPnt[i].topoUtmX = FixPnt[i].x;
//   FixPnt[i].topoUtmY = FixPnt[i].y;
//   float dx = FixPnt[i].x - Srf.xllcorner;
//   float dy = FixPnt[i].y - Srf.yllcorner;
//   FixPnt[i].x = ( (dx)*C + (dy)*S); //*100;
//   FixPnt[i].y = (-(dx)*S + (dy)*C); //*100;
//
//    //double Ccon=C;//cos(Srf.convergence*M_PI/180);
//    //double Scon=S;//sin(Srf.convergence*M_PI/180);
//    //double topoUtmX = (FixPnt[i].x*Ccon - FixPnt[i].y*Scon) + Srf.xllcorner;
//    //double topoUtmY= ( FixPnt[i].x*Scon + FixPnt[i].y*Ccon) + Srf.yllcorner;
//
//   /*{                     //примерный перевод из град. в UTM
//     // по долготе
//    x = (FixPnt[i].xGrad - Srf.xllcorner_wgs)* Pi/180*(aW/sqrt( 1 - e2W*pow(sin(FixPnt[i].yGrad),2) ) + FixPnt[i].z) *cos(FixPnt[i].yGrad);
//     // по широте
//    y = (FixPnt[i].yGrad - Srf.yllcorner_wgs)* Pi/180*aW*sqrt( 1 - e2W*pow(sin(FixPnt[i].yGrad),2) );
//    FixPnt[i].x = x; //*100;
//    FixPnt[i].y = y; //*100;
//   } */
// }
// return true;  // пока без проверки диапазона изменения
//}

//------------------------------- comment ----------------------------------
void com(wchar_t* stroka, char k)
{
 if( (wcschr(stroka,L'#'))||(wcschr(stroka,L'^')) ) error(-44,stroka0,L"com");
 if(wcslen(stroka)==0) {error(-48,stroka0,L"com"); return;}
 TransForm->Comment += stroka; TransForm->Comment += L"\n";
 com_num++;
 if(k==0){    //если комм. к начальной т. пикета (до ввода его данных)
  if (P1[pik].Com!=0) error(-49,stroka0,L"com");
  P1[pik].Com = -com_num;}
 else P1[pik].Com = com_num;
}
//------------------------------- comment-wall -------------------------------
void comWall(wchar_t* stroka, char k)
{
 if( (wcschr(stroka,L'#'))||(wcschr(stroka,L'^')) ) error(-44,stroka0,L"com");
 if(wcslen(stroka)==0) {error(-48,stroka0,L"com"); return;}
 TransForm->WallComment += stroka; TransForm->WallComment += L"\n";
 wcom_num++;
 Wall1[wal].com = wcom_num;
}
//------------------------------ cave name ---------------------------------
void cave(wchar_t* stroka)
{
 if( (wcschr(stroka,L'#'))||(wcschr(stroka,L'^')) ) error(-44,stroka0,L"cave");
 if(wcslen(cave_name.c_str())==0) cave_name = stroka;
 else if(fil==1) error(-40,stroka0,L"cave");
}
//-------------------------------- region ----------------------------------
void region(wchar_t* stroka)
{
 if( (wcschr(stroka,L'#'))||(wcschr(stroka,L'^')) ) error(-44,stroka0,L"region");
 if(wcslen(region_name.c_str())==0) region_name = stroka;
 else if(fil==1) error(-40,stroka0,L"region");
}
//------------------------------ prefix ---------------------------------
void prefix(wchar_t* stroka)      // пока отбр. хвост, надо его возвр.!!!
{
 wchar_t *slovo;
 int index = 0;
 const wchar_t *separators = L"\t\v\r\n ";
 slovo = wcstok(stroka,separators);
 if(slovo==NULL) {error(-48,stroka0,L"prefix"); return;}
 if(slovo[0]==L'^') slovo=slovo+1;
  if(wcslen(slovo)==0) {error(-48,stroka0,L"prefix"); return;}
 full_prefix += slovo; full_prefix += L"."; //полный префикс
 index = full_prefix.LastDelimiter(L"\\");  //текущий - отсекаем все левее посл. \\ и .
 if(index==0) curr_prefix = full_prefix;
  else curr_prefix = full_prefix.SubString(index+1, full_prefix.Length()-index);
 if((curr_prefix!=L"")&&(curr_prefix[1]==L'.')) curr_prefix.Delete(1,1);
}
//------------------------------ end_prefix ---------------------------------
void end_prefix(wchar_t* strokaa)
{
 wchar_t *slovo;
 int index = 0;
 const wchar_t *separators = L"\t\v\r\n ";
 UnicodeString Slovo=L"";
 slovo = wcstok(strokaa,separators);
 if(slovo==NULL) {error(-48,stroka0,L"end_prefix"); return;}
  if(slovo[0]==L'^') slovo=slovo+1;
  if(wcslen(slovo)==0) {error(-48,stroka0,L"end_prefix"); return;}
 Slovo = L"."; Slovo += slovo; Slovo += L".";
 index = FindLast(full_prefix,Slovo);
// index = full_prefix.Pos(Slovo);
 if(index==0) {error(50,stroka0,slovo); return;}    //если такого нет
 if (wcscmp(survey_prefix.c_str(), slovo)==0) survey_prefix=L"";
 full_prefix = full_prefix.Delete(index+1, wcslen(slovo)+1);
 curr_prefix = full_prefix;
 index = full_prefix.LastDelimiter(L"\\");
 if(index!=0)                //отсекаем все левее посл.
  curr_prefix = full_prefix.SubString(index+1, full_prefix.Length()-index);
 if((curr_prefix!=L"")&&(curr_prefix[1]==L'.')) curr_prefix.Delete(1,1);
}
//------------------------------ survey ---------------------------------
void survey(wchar_t* stroka)
{
  UnicodeString Astr = StrIzSlov(stroka);
  UnicodeString slovo=L"";
  if( wcschr(stroka,L'#') ) error(-44,stroka0,L"survey");
//  if( (pik!=0)&&(P1[pik-1].Srv==srv) )//если предыд. съемка содержит хоть 1 пикет
  if(new_survey==1)
   {srv=srv+1; Survey1[srv].file=Survey1[srv-1].file;}
  if(survey_prefix!=L"")
   {UnicodeString spref = survey_prefix; end_prefix(spref.c_str()); survey_prefix=L"";}

  slovo = Slovo(Astr,1);
  if(slovo==L"") {error(-48,stroka0,L"survey"); return;}
  if( (slovo[1]==L'^')&&(slovo.Length()>1) )
     {prefix(slovo.c_str()+1); Astr=Astr.Delete(1,slovo.Length());
      survey_prefix=slovo.Delete(1,1);}
  Survey1[srv].team = Astr; Survey1[srv].team = Survey1[srv].team.Trim();
 //if(Survey1[srv].team==L"") {Survey1[srv].team = L"?"; error(-48,stroka0,L"survey");}
}
//---------------------------------------------------------------------------
void end_survey()
 {
  UnicodeString spref = survey_prefix; if(survey_prefix!=L"") end_prefix(spref.c_str());
 }
//---------------------------------------------------------------------------
void survey_title(wchar_t* stroka)
{
  Survey1[srv].title = stroka; Survey1[srv].title = Survey1[srv].title.Trim();
}
//---------------------------------------------------------------------------
void survey_team(wchar_t* stroka)
{
  Survey1[srv].team += L"  "; Survey1[srv].team += stroka;
}
//---------------------------------------------------------------------------
void survey_date(wchar_t* stroka)
{
  Survey1[srv].date = stroka; Survey1[srv].date = Survey1[srv].date.Trim();
}
//--------------------------------- equate ---------------------------------
wchar_t equate(wchar_t* stroka)
{
 wchar_t *slovo;
 int index = 0;
 const wchar_t *separators = L"\t\v\r\n ";
 wchar_t *Stan1;
 slovo = wcstok(stroka,separators);
 if(slovo==NULL) {error(14,stroka0,L""); return 0;}
 if(slovo[0]==L'^') slovo=slovo+1;
 if(wcslen(slovo)==0) {error(14,stroka0,L""); return 0;}
 Stan1 = slovo;
 slovo = wcstok(NULL,separators);
 if(slovo==NULL) {error(14,stroka0,L""); return 0;}
 if(slovo[0]==L'^') slovo=slovo+1;
 if(wcslen(slovo)==0) {error(14,stroka0,L""); return 0;}
 P1[pik].metBeg = AddStation(Stan1);
  if( (pik!=0)&&(P1[pik-1].metEnd==0) ) P1[pik-1].metEnd = -1;
 P1[pik].metEnd = AddStation(slovo); P1[pik+1].metBeg = -1;
 P1[pik].prz2 = P1[pik].prz2+1; // признак #equate   P1[pik].prz2 | 1

 Equates.push_back(std::make_pair(std::min(P1[pik].metBeg, P1[pik].metEnd), std::max(P1[pik].metBeg, P1[pik].metEnd)));
 return -2; //обычно k=-1, потом добавим признак equate  по -2;
}
//------------------------------ data_order ---------------------------------
void data_order(wchar_t *stroka)
{
  wchar_t *slovo;
  const wchar_t *separators = L"\t\v\r\n ";  //сепараторы - табуляции, конец строки и пробел
  const wchar_t *spisok[] = {L"L",L"Az",L"An",L"D",L"l",L"r",L"u",L"d"}; //"left",L"right",L"up",L"down"};
  wchar_t jj[]=L"0"; char flag=0;

   wcscpy(order,L"00000000");
   {for(int i=0; i<7; i++)          //i<3
	{
	 slovo=wcstok(NULL,separators); flag=0;
	 if((slovo==NULL)&&(i<3)) {wcscpy(order,L"12300000"); error(10,stroka0,L"data_order"); break;}
	  // если нет первых трех параметров
	 if(slovo==NULL) break; // если аргументы кончились
     for(int j=0; j<8; j++)         //j<4
      if(wcscmp(slovo,spisok[j])==0) {_itow(j+1,jj,10); order[i]=*jj; flag=1;}
     if (!(flag)) {error(10,stroka0,L"data_order"); wcscpy(order,L"1230000"); break;} //если нет в списке
		}
    }
   if(wcschr(order,L'3')) angle=1; else angle=0; //автом. перекл. режимов при смене формата
   if( wcschr(order,L'4')&& wcschr(order,L'3') )  // An и D не могут входить одновременно
    {wcscpy(order,L"12300000"); error(10,stroka0,L"data_order");}
   if(!(wcschr(order,L'1')&& wcschr(order,L'2') && (wcschr(order,L'3')|| wcschr(order,L'4'))))
     // L, Az и An или D должны обязательно входить
     {wcscpy(order,L"12300000"); angle=1; error(10,stroka0,L"data_order");}
   for(int j=1; j<8; j++)      // один аргумент не может входить более одного раза
    {_itow(j,jj,10);
     if( wcschr(order,*jj) != wcsrchr(order,*jj) )
     {wcscpy(order,L"12300000"); error(10,stroka0,L"data_order"); break;}
    }
  //if(wcscspn(order,L"0")>3) lrud=true;
}
//**************************************************************************
//
//                            DATA PROCESSING
//
//************************** Запись новых меток и веток ********************
//--------------------------------------------------------------------------
int Index_Of(UnicodeString Slovo)  //работает быстрее IndexOf, т.к. одинак. пикеты обычно рядом
{                               //это важно при большом числе пикетов (> 1.5-2 тыс)
 for(int N=StationList->Count-1; N>0; N--) // i - номер пикета
 {if(Slovo==StationList->Strings[N]) return N;}
 return -1;                    // и в отл от IndexOf различает регистры
}                              // что можно исп. для перекл. - разл.регистры или нет
//--------------------------------------------------------------------------
int AddStation(wchar_t *slovo)  // проверка наличия и присв. станции номера
{
  if(slovo==NULL) return 0; //чтобы пока не вылетало (иногда при ошибках в тексте)
  UnicodeString Slovo = slovo;  // добавляем префикс
  if((slovo[0]!=L'\\')&&(curr_prefix!=L"")) Slovo = curr_prefix + Slovo;
  if(slovo[0]==L'\\') Slovo.Delete(1,1);
  int N = 0;                     // проверяем наличие
//  N=StationList->IndexOf(Slovo); if(N<0)  // Find - глючная функция, не исп.!!!
  N=Index_Of(Slovo); if(N<0)  // Find - глючная функция, не исп.!!!
  {StationList->Add(Slovo); N=StationList->Count-1;} // 0 зарезерв.
// StationList->Text; // для отладки
 return N;
}
//--------------------------------------------------------------------------
void SetStation(wchar_t *slovo, char i)
{
 if(i==1){ P1[pik].metBeg = AddStation(slovo);   // начало пикета
  if((number!=-1)&&(pik!=0)&&(P1[pik-1].metEnd==0)) P1[pik-1].metEnd = AddStation(slovo); }

 if(i==2){ P1[pik].metEnd = AddStation(slovo); } // конец пикета

 if(i==-2)                         // 2-я метка в #label
  if(P1[pik].metEnd<=0) {P1[pik].metEnd = AddStation(slovo);
     if(P1[pik+1].metBeg==0) P1[pik+1].metBeg = AddStation(slovo);}
   else error(19,stroka0,slovo);   // 2 метки конца

 if(i==0) {P1[pik+1].metBeg = -1;        //по команде #end
           if(P1[pik].metEnd==0) P1[pik].metEnd = -1;}

 if(i==3) error(20,stroka0,slovo); // 3 метки
}
//--------------------------------------------------------------------------
int AddWallStation(wchar_t *slovo)  // проверка наличия и присв. Wall-станции номера
{
  if(slovo==NULL) return 0; //чтобы пока не вылетало (иногда при ошибках в тексте)
  UnicodeString Slovo = slovo;  // добавляем префикс
  if((slovo[0]!=L'\\')&&(curr_prefix!=L"")) Slovo = curr_prefix + Slovo;
  if(slovo[0]==L'\\') Slovo.Delete(1,1);
  int N = 0;
  N=Index_Of(Slovo);        // проверяем наличие slovo в StationList
  if(N<0)  // если slovo нет в StationList - ищем в WallStationList
   {
    for(int N1=WallStationList->Count-1; N1>0; N1--) // i - номер пикета
     {if(Slovo==WallStationList->Strings[N1]) return -N1;}
    WallStationList->Add(Slovo); N=-(WallStationList->Count-1); // 0 зарезерв.
   }
 WallStationList->Text; // для отладки
 return N;     //+: N из StationList, -: -N из WallStationList
}
//--------------------------------------------------------------------------
//************************** Анализ текущей строки *************************
void analiz(wchar_t stroka[256])
{
 wchar_t *err_symb = NULL;            //для обраб. ошибок в числ. данных
 wchar_t X[7][10];                    //7 строк для считывания данных
 double W[3];                      //для считывания данных стен
 char k=0;                         //число правильно счит. данных пикета
 char StaCount=0;                  //счит. ли названия пикетов в #from_to и #number
 UnicodeString Sta1=L"";               // 1-я и 2-я станции пикета
 UnicodeString Sta2=L"";
 char Nmet=0;                      //число счит. меток в режиме #label
 wchar_t *slovo = nullptr;
 const wchar_t *separators = L"\t\v\r\n ";   //сепараторы - табуляции, конец строки и пробел
 char DataCount=wcscspn(order,L"0");//число данных пикета (вкл lrud)


 int j=wcslen(stroka);            //отсекаем символ конца строки и обобщ. пробелы
 for (; (isspace(stroka[j-1])&&(j>0)); ) {j=j-1;}
 stroka[j]=NULL;

 stroka0 = _wcsdup(stroka);        //резервная копия стр. для обраб. ошибок

 if(wcschr(stroka,L'%')) wcscpy(wcschr(stroka,L'%'),L"");// отсекаем комментарий
 if(wcschr(stroka,L';')) wcscpy(wcschr(stroka,L';'),L"");//  -//-
 if( (comment==1) && wcschr(stroka,L'}') )    //если конец комментария
  {stroka=wcschr(stroka,L'}')+1; comment=0;}
 if(wcschr(stroka,L'{'))                      //если начало комментария
  {if(!wcschr(stroka,L'}')) {wcscpy(wcschr(stroka,L'{'),L"");  comment=1;}
    else  wcscpy( wcschr(stroka,L'{'), (wcschr(stroka,L'}')+1) );
  }
 if(comment==0)
 {
  slovo = wcstok(stroka,separators);
 // if( slovo!=NULL && number==-1 && slovo[0]==L'^') //если метка в режиме from_to
 //     {error(8,stroka0,slovo);  if(stroka0) free(stroka0); stroka0=NULL; return;}

  if(walls==0)                     // если обычный пикет (не стена)
  while (slovo!=NULL)              // разбивка на отдельные слова
  {
  if((number!=0)&&(StaCount==0))    // если еще не считали станции
   {
    if( slovo!=NULL && slovo[0]!=L'#' && ( number==-1 || (number==1 && slovo[0]!=L'^') ))
     {
        if(number==-1) {Sta1=slovo;} StaCount=1;
        slovo=wcstok(NULL,separators);
        if(slovo==NULL) {error(6,stroka0,L""); if(stroka0) free(stroka0); stroka0=NULL; return;}
		if(slovo[0]==L'#')  // если оператор между именами станций
         {
          error(7,stroka0,slovo);
          slovo=wcstok(NULL,separators);
          if(slovo==NULL) {error(6,stroka0,L""); if(stroka0) free(stroka0); stroka0=NULL; return;}
         }
		if(slovo[0]!=L'#')
        {StaCount=2;
         if(number==-1)
         {
          Sta2=slovo; StaCount=2;
          if((Sta1==L"-")&&(Sta2==L"-")) {error(18,stroka0,L""); if(stroka0) free(stroka0); stroka0=NULL; return;} //
          if((Sta1==L"-")||(Sta2==L"-")) // если стена
           {
            walls=1;
            slovo=wcstok(NULL,separators); // счит. следующее слово
            if(slovo==NULL) {error(6,stroka0,L""); if(stroka0) free(stroka0); stroka0=NULL; return;}
            if(Sta1==L"-") {Wall1[wal].met = AddWallStation(Sta2.c_str()); Wall1[wal].beg=1;}
             else Wall1[wal].met=AddWallStation(Sta1.c_str());
            break;                         // выходим из цикла и затем переходим в особый режим обработки стен
           }
          SetStation(Sta1.c_str(),1);   // если не стена
          SetStation(Sta2.c_str(),2);
         }
        }
        slovo=wcstok(NULL,separators);
        if(slovo==NULL) {error(6,stroka0,L""); if(stroka0) free(stroka0); stroka0=NULL; return;}
     }
   }
   switch (slovo[0])
    {case L'#':  if(((k>=1)&&(k<3))||(StaCount==1)) {error(7,stroka0,slovo); break;}
                if(!wcscmp(slovo,L"#include"))
                 {slovo=wcstok(NULL,separators);
                  if(slovo==NULL) {error(42,stroka0,L"#include"); break;}
                  include(slovo); break;
                 }
                if(!wcscmp(slovo,L"#cave"))   {cave(slovo+6);slovo=NULL; break;}
                if(!wcscmp(slovo,L"#region")) {region(slovo+8);slovo=NULL; break;}
                if(!wcscmp(slovo,L"#com"))
                {com(slovo+5,k);slovo=NULL; break;}
                if(!wcscmp(slovo,L"#survey")) {survey(slovo+8);slovo=NULL; break;}
                if(!wcscmp(slovo,L"#survey_team")) {survey_team(slovo+13);slovo=NULL; break;}
                if(!wcscmp(slovo,L"#survey_title")) {survey_title(slovo+14);slovo=NULL; break;}
                if(!wcscmp(slovo,L"#survey_date")) {survey_date(slovo+13);slovo=NULL; break;}
                if(!wcscmp(slovo,L"#end_survey"))  {end_survey();slovo=NULL; break;}
                if(!wcscmp(slovo,L"#prefix")) {prefix(slovo+8);slovo=NULL; break;}
                if(!wcscmp(slovo,L"#end_prefix")) {end_prefix(slovo+12);slovo=NULL; break;}
                if(!wcscmp(slovo,L"#point")){point(slovo+7,FPM_WGS84);slovo=NULL; break;}
                if(!wcscmp(slovo,L"#point_UTM")){point(slovo+11,FPM_UTM);slovo=NULL; break;}
                if(!wcscmp(slovo,L"#equate")) {if(k==0){k=equate(slovo+8);slovo=NULL; break;}
                                              else error(15,stroka0,L"");}
				if(!wcscmp(slovo,L"#data_order")) {data_order(slovo+11); slovo=NULL; break;}
                if(!wcscmp(slovo,L"#fix"))
                 {slovo=wcstok(NULL,separators);
                  if(slovo!=NULL) {fix(slovo); break;}
                  error(41,stroka0,L"#fix"); break;
                 }
                if(!wcscmp(slovo,L"#walls")){error(33,stroka0,L"walls"); break;}
                if(!wcsncmp(slovo,L"#walls[",7))
                 {
                  walls=wcstod(op_par(slovo),&err_symb);   // число стен
                  walls=-walls;  // - чтобы переходить на след. строку
                  if(*err_symb) { error(34,stroka0,L"walls"); slovo=NULL; break; }
                  if(walls<-100 || walls>-1) {error(24,stroka0,L"walls"); walls=0; break;}
                  slovo=wcstok(NULL,separators);
                  if(slovo==NULL)      // если нет метки - то стены текущего пикета
                   {
                    if(walls_from_to==true) Wall1[wal].met=P1[pik].metBeg;
                    else Wall1[wal].met=P1[pik].metEnd;
                    break;
                   }
                  if( (slovo[0]==L'^')&&(wcslen(slovo)>1) )
                   {slovo=slovo+1;
                   Wall1[wal].met = AddWallStation(slovo);}              // метка пикета стены
                   else error(52,stroka0,L"#walls[x]");
                   slovo=wcstok(NULL,separators);
                  if(slovo!=NULL) error(52,stroka0,L"#walls[x]");
                  slovo=NULL; break;
                 }
                operat(slovo,k); break;           //прочие операторы

	 case L'^':  if(number==-1) {error(11,stroka0,slovo); break;}
                if((k>0)&&(k<3)) {error(8,stroka0,slovo); break;} // если не счит. осн. данные
                if((k==0)&&(Nmet==0))                  //до данных пикета, 1-я метка
                 {slovo=slovo+1; SetStation(slovo,1); Nmet++; break;}
                if( (k==-1)||(Nmet==1) )               // 2-я метка
                 {slovo=slovo+1; SetStation(slovo,-2); Nmet++; break;}
                Nmet++; slovo=slovo+1; SetStation(slovo,Nmet); break;

     default :                  //обработка числ. данных и ошибок формата ввода
//          if( !(isdigit(slovo[0]))&&(wcscmp(slovo,L"-")!=0)&&(k==0) ) //если k=0 и не число
          wcstod(slovo,&err_symb);
          if( (k==0)&&*err_symb && (wcscmp(slovo,L"-")!=0) ) //если k=0 и не число
             {error(1,stroka0,slovo); break;}

          if(k==-1){ wcstod(slovo,&err_symb);  //если данные должны кончиться, но не кончились
                 if(*err_symb) error(1,stroka0,slovo); else
                 {if(number==0) error(4,stroka0,slovo); else error(5,stroka0,slovo);} }
           else if((k>=0)&&(k<DataCount)) //если нет ошибок в осн данных
            {wcscpy(X[k],slovo); k++;}  //считывание данных пикета
          if(k==3)                      //запись в массив пикетов
          {
           P1[pik].L = wcstod(X[wcscspn(order,L"1")],&err_symb);
           if(*err_symb)  {error(1,stroka0,X[wcscspn(order,L"1")]); k=-3;}
           if (_wcsnicmp(X[wcscspn(order,L"34")],L"down",4)==0) wcscpy(X[wcscspn(order,L"34")],L"-90"); //временно
           if (_wcsnicmp(X[wcscspn(order,L"34")],L"up",2)==0) wcscpy(X[wcscspn(order,L"34")],L"+90");
           P1[pik].An = wcstod(X[wcscspn(order,L"34")],&err_symb);    // угол
           if(*err_symb)  {error(3,stroka0,X[wcscspn(order,L"34")]); k=-3;}
            else
            {if( !(!angle && (abs(P1[pik].An)==P1[pik].L)) && !(angle && (abs(P1[pik].An)==90) ) )
              {P1[pik].Az = wcstod(X[wcscspn(order,L"2")],&err_symb);  // азимут
               if(*err_symb) {error(2,stroka0,X[wcscspn(order,L"2")]); k=-3;} }
             if( angle && (abs(P1[pik].An)==90) )  // Ая счит. и для вертикалей, если он есть
              {wcstod(X[wcscspn(order,L"2")],&err_symb);
                 if(!(*err_symb)) P1[pik].Az = wcstod(X[wcscspn(order,L"2")],&err_symb);}
            }
           if(k==-3) {P1[pik].L=0.1; P1[pik].Az=0; P1[pik].An=0;}   //0-й пикет, если ошибки
           if(DataCount==3) {k=-1;} //break; //если без ширины и высоты
          }
          if(k==DataCount)   //запись ширины и высоты  (lrud)
          {
            if(wcscspn(order,L"5") < DataCount)       //left
            {P1[pik].left = wcstod(X[wcscspn(order,L"5")],&err_symb);
             lrud++;
             if(*err_symb) {P1[pik].left = -1;
              if(wcscmp(X[wcscspn(order,L"5")],L"-")!=0) error(1,stroka0,X[wcscspn(order,L"5")]);}
            }
           if(wcscspn(order,L"6") < DataCount)       //right
            {P1[pik].right = wcstod(X[wcscspn(order,L"6")],&err_symb);
             lrud++;
             if(*err_symb) {P1[pik].right = -1;
              if(wcscmp(X[wcscspn(order,L"6")],L"-")!=0) error(1,stroka0,X[wcscspn(order,L"6")]);}
            }
           if(wcscspn(order,L"7") < DataCount)       //up
            {P1[pik].up = wcstod(X[wcscspn(order,L"7")],&err_symb);
             lrud++;
             if(*err_symb) {P1[pik].up = -1;
              if(wcscmp(X[wcscspn(order,L"7")],L"-")!=0) error(1,stroka0,X[wcscspn(order,L"7")]);}
            }
           if(wcscspn(order,L"8") < DataCount)       //down
            {P1[pik].down = wcstod(X[wcscspn(order,L"8")],&err_symb);
             lrud++;
             if(*err_symb) {P1[pik].down = -1;
              if(wcscmp(X[wcscspn(order,L"8")],L"-")!=0) error(1,stroka0,X[wcscspn(order,L"8")]);}
            }
            k=-1;
          }
    }
    if(slovo!=NULL) slovo=wcstok(NULL,separators); //новое слово
    if((slovo==NULL)&&(k>0)&&(k<3))   //если строка кончилась посередь осн числ данных
      {error(6,stroka0,L""); P1[pik].L=0.1; P1[pik].Az=0; P1[pik].An=0; }
    if((slovo==NULL)&&(k>=3))  // если строка кончилась до ввода lrud
      {error(-6,stroka0,L"");}
    if(((slovo==NULL)&&(k!=0))||(k==-3)) //если конец строки при прав. или неправ. введенном пикете
    {
     P1[pik].Srv = srv;   //номер съемки
     P1[pik].control();
     P1[pik].deCart(angle);
     P1[pik].line = line_number;
      if(!(P1[pik].prz2&1)) new_survey=1;
     pik = pik+1; Nmet=0;
    }
    if(walls<0) {walls=-walls; if(stroka0) free(stroka0); stroka0=NULL; return;}
  }

   if(walls!=0)                        // отдельный режим считывания данных до стен
  {
   int i=0;
   while (slovo!=NULL)              // разбивка на отдельные слова
   {
    if(slovo[0]==L'#' || slovo[0]==L'^') {error(23,stroka0,slovo); break;}
    else
     {
      wcscpy(X[i],slovo);
      if(i>2) {error(21,stroka0,slovo); break;}    // если слишком много данных
      else {i++;}
     }
    slovo=wcstok(NULL,separators);
   }
   if(i==3)
    {
     Wall1[wal].L = wcstod(X[wcscspn(order,L"1")],&err_symb);      // расстояние
      if(*err_symb)  {error(1,stroka0,X[wcscspn(order,L"1")]); i--;}
     Wall1[wal].An = wcstod(X[wcscspn(order,L"34")],&err_symb);    // угол
      if(*err_symb)  {error(3,stroka0,X[wcscspn(order,L"34")]); i--;}
      else
       {if( !(!angle && (abs(Wall1[wal].An)==Wall1[wal].L)) && !(angle && (abs(Wall1[wal].An)==90) ) )
          {Wall1[wal].Az = wcstod(X[wcscspn(order,L"2")],&err_symb);  // азимут
           if(*err_symb) {error(2,stroka0,X[wcscspn(order,L"2")]); i--;}
        if( angle && (abs(Wall1[wal].An)==90) )  // Аz счит. и для вертикалей, если он есть
          {wcstod(X[wcscspn(order,L"2")],&err_symb);
           if(!(*err_symb)) Wall1[wal].Az = wcstod(X[wcscspn(order,L"2")],&err_symb);
           else Wall1[wal].Az = 0;
          }
        }
    }
   if(i==3)
//     {Wall1[wal].L=W[wcscspn(order,L"1")];
//      Wall1[wal].Az=W[wcscspn(order,L"2")];
//      Wall1[wal].An=W[wcscspn(order,L"34")];
      if(Wall1[wal].beg==1)
       {if(Wall1[wal].Az<=180) Wall1[wal].Az=180+Wall1[wal].Az; else Wall1[wal].Az=-180+Wall1[wal].Az;
        Wall1[wal].An=-Wall1[wal].An;}
      Wall1[wal].control();
      Wall1[wal].deCart(angle);
      Wall1[wal].Srv = srv;
	  Wall1[wal].line = line_number;
	  Wall1[wal].ignoreAt3d = ignore_walls;
	  wal++;}
     else error(21,stroka0,slovo);  // если слишком мало данных
    walls--;
   }
 }
 if(stroka0) free(stroka0);
 stroka0=NULL;
}
// k=0,1,2,... - число считанных данных
// k=-3 - считали 3 с ошибками
// k=-1 - считали (с ошибками или без) и записали пикет
//------------------------ add points --------------------------------------
void AddFixPnt()  // добавл. точек из FixPnt[n] как пикетов от лев. нижн. угла
{
 curr_prefix=L"";   // т.к. префикс уже записан в массиве FixPnt[n].label в point(...)
 for(int n=0; n<fxp; n++) // пещ точки от 0 до pik-1, FixPnt от pik до pik+fxp-1
 {
 P1[pik+n].L = FixPnt[n].localY;//FixPnt[n].y;  //x и y меняем местами
 P1[pik+n].An = FixPnt[n].localX;//FixPnt[n].x;
 P1[pik+n].Az = FixPnt[n].localZ;//FixPnt[n].z;
 P1[pik+n].prz = 1;       // пока 1 - поверхность
 P1[pik+n].prz1 = - 7;      // точность поверх. пока 2^(7), потом надо думать !!!
 P1[pik+n].prz2 = 2;      // пока 2 - дублирование
 P1[pik+n].Srv = -1;
 P1[pik+n].metBeg = AddStation(L"%GPS%");    // метка для т. с коорд. 000
 P1[pik+n].metEnd = AddStation(FixPnt[n].label.c_str());
                                       // пока не используются
 P1[pik+n].line = 0;  // = line   (строка в файле данных)
 P1[pik+n].col = 0;
 P1[pik+n].Vet = 0;
 P1[pik+n].proj = 0;
 }
}
//--------------------------------------------------------------------------
void SetBifurcStations()  //находим точки ветвления (и точки начала/конца)
{
 BifStation = new bool[StationList->Count+1];  // создаем массив т.бифуркации
 for(int n=0; n<=StationList->Count; n++) BifStation[n]=0;  // уст. в 0
 bool *BFlag;                                  // вспомогат. массив
 BFlag = new bool[StationList->Count+1];
 for(int n=0; n<=StationList->Count; n++) BFlag[n]=0;  // уст. в 0

 if(P1[0].metBeg>0) BifStation[P1[0].metBeg]=1;                  // 1-я т. =1
 if(P1[pik-1+fxp].metEnd>0) BifStation[P1[pik-1+fxp].metEnd]=1;  // посл-я т.
 for(int i=0; i<pik+fxp; i++)           // i - номер пикета
 {
  if(P1[i].metEnd >0)
   {
    if(BFlag[P1[i].metEnd]==1) BifStation[P1[i].metEnd]=1;
     else BFlag[P1[i].metEnd]=1;
    if(P1[i+1].metBeg==-1) BifStation[P1[i].metEnd]=1;
    if(P1[i+1].metBeg >0)
    {
     if( P1[i].metEnd != P1[i+1].metBeg )
       {BifStation[P1[i].metEnd]=1; BifStation[P1[i+1].metBeg]=1;}
    }
   }
  else if(P1[i+1].metBeg >0)
   {
    if(BFlag[P1[i+1].metBeg]==1) BifStation[P1[i+1].metBeg]=1;
     else BFlag[P1[i+1].metBeg]=1;
    if(P1[i].metEnd==-1) BifStation[P1[i+1].metBeg]=1;
   }
 }
 int NF=0;                   // фиксируемая точка - 0 0 0
 if( fix_label.Length()!=0 ){
  NF=StationList->IndexOf(fix_label);
  if(NF<0) error(65,L"",fix_label.c_str()); else BifStation[NF]=1; }
// UnicodeString testik=L"";      // для отладки
// for(int n=0; n<=StationList->Count; n++) testik+=(int)BifStation[n];
// StationList->Strings[190];
 delete [] BFlag;
}
//--------------------------------------------------------------------------
void WriteVetka()
{
 if(pik+fxp>0) SetBifurcStations();                                           // + fxp!!!
 vet=0;
 for(int i=0; i<pik+fxp; i++)    // подсчет числа веток; i - номер пикета      // + fxp!!!
  if( (P1[i].metEnd==-1)||(BifStation[P1[i].metEnd]>0) ) vet++;
 if(V1!=NULL) delete [] V1;  // создаем массив веток
 V1 = new Vetka[vet+2];
  for(int i=0; i<=vet+1; i++) V1[i].MemSet0(); //обнуление всех данных
 vet=1; vet0=1;
 for(int i=0; i<pik+fxp; i++)    // i - номер пикета
 {
  P1[i].Vet=vet;
  if(P1[i].metBeg==-1)  {V1[vet].metBeg=0; V1[vet].pikBeg=i;}
   else if(BifStation[P1[i].metBeg]==1)
    {V1[vet].metBeg=P1[i].metBeg; V1[vet].pikBeg=i;}
  if(P1[i].metEnd==-1) {V1[vet].metEnd=0; V1[vet].pikEnd=i; V1[vet+1].pikBeg=i+1; vet++; if(i<pik) vet0++;}
   else if(BifStation[P1[i].metEnd]==1)
    {V1[vet].metEnd=P1[i].metEnd; V1[vet].pikEnd=i; vet++;  if(i<pik) vet0++; }
 }
}
//---------------------------------------------------------------------------
int equate_control()  // подсчет числа #equate и контроль наличия таких меток
{
 int n=0; UnicodeString stroka00;  bool flag;
 for(int i=0; i<pik+fxp; i++)                                               // +fxp!!!
 {if(P1[i].prz2&1)
  {
   n++;
   flag=0;
   for(int k=0; k<pik+fxp; k++)
    {if( ((P1[i].metBeg==P1[k].metBeg)||(P1[i].metBeg==P1[k].metEnd))&& !(P1[k].prz2&1) ) {flag=1;break;}}
   if(flag==0){
    wcscpy(cave_file,(CavePath + FileList->Strings[Survey1[P1[i].Srv].file]).c_str()); line_number=P1[i].line;
	stroka00 = L"#equate " + StationList->Strings[P1[i].metBeg]+" "+ StationList->Strings[P1[i].metEnd];
	error(-51, stroka00.c_str(), UnicodeString(StationList->Strings[P1[i].metBeg]).c_str() );  }
   flag=0;
   for(int k=0; k<pik+fxp; k++)                                             //+fxp!!!
	{if( ((P1[i].metEnd==P1[k].metBeg)||(P1[i].metEnd==P1[k].metEnd))&& !(P1[k].prz2&1) ) {flag=1;break;}}
   if(flag==0){
    wcscpy(cave_file,(CavePath + FileList->Strings[Survey1[P1[i].Srv].file]).c_str()); line_number=P1[i].line;
	stroka00 = L"#equate " + StationList->Strings[P1[i].metBeg]+" "+StationList->Strings[P1[i].metEnd];
	error(-51, stroka00.c_str(), UnicodeString(StationList->Strings[P1[i].metEnd]).c_str() );  }
  }
 }
 return n; // число #equate
}

//*********************** Считывание данных из файла asc ************************
//------------------------------ read_asc_file ---------------------------------
bool read_asc_file(UnicodeString asc_file, UnicodeString CavePath)
{
  Srf.ncols=0; Srf.nrows=0;
  Srf.xllcorner=0; Srf.yllcorner=0; Srf.xllcorner_wgs=0; Srf.yllcorner_wgs=0;
  Srf.xcellsize=0; Srf.ycellsize=0;  Srf.convergence=0;
  Srf.flag=false;

  FILE* fdat;
  const wchar_t *separators = L"\t\v\r\n ";
  wchar_t stroka1[10000];
  wchar_t* slovo;

  fdat=_wfopen( (CavePath + asc_file).c_str(),L"rt" ); // присоед. текущей дир + открытие
  if(fdat==NULL)                                    // Проверка ошибки
   {error(61,L"",asc_file.c_str());  return false;}  //exit(EXIT_FAILURE);

 for(;fgetws(stroka1,10000,fdat)!=NULL; line_number++)  // считывание и обработка строки
  {
   slovo = wcstok(stroka1,separators);           // данные формата asc
   if( wcscmp (slovo, L"ncols")==0)
    {slovo = wcstok(NULL,separators); Srf.ncols=UnicodeString(slovo).ToInt(); }
   if( wcscmp (slovo, L"nrows")==0)
    {slovo = wcstok(NULL,separators); Srf.nrows=UnicodeString(slovo).ToInt(); }
   if( wcscmp (slovo, L"xllcorner")==0)
	{slovo = wcstok(NULL,separators); Srf.xllcorner=_wtof(slovo); }
   if( wcscmp (slovo, L"yllcorner")==0)
	{slovo = wcstok(NULL,separators); Srf.yllcorner=_wtof(slovo); }
   if( wcscmp (slovo, L"cellsize")==0)
	{slovo = wcstok(NULL,separators); Srf.xcellsize=_wtof(slovo); Srf.ycellsize=_wtof(slovo);}
   if( wcscmp (slovo, L"xdim")==0)
	{slovo = wcstok(NULL,separators); Srf.xcellsize=_wtof(slovo);}
   if( wcscmp (slovo, L"ydim")==0)
	{slovo = wcstok(NULL,separators); Srf.ycellsize=_wtof(slovo);}

   if( wcscmp (slovo, L"xllcorner_wgs")==0)     // добавленные данные
	{slovo = wcstok(NULL,separators); Srf.xllcorner_wgs=_wtof(slovo); }
   if( wcscmp (slovo, L"yllcorner_wgs")==0)
	{slovo = wcstok(NULL,separators); Srf.yllcorner_wgs=_wtof(slovo); }

   if( wcscmp (slovo, L"convergence")==0)
	{slovo = wcstok(NULL,separators); Srf.convergence=_wtof(slovo); }

   if( wcscmp (slovo, L"NODATA_value")==0) break;
   if(ex_prz) break;                               // выход по #exit
  }    // выше нужна защита от вылета при неполных данных
  if( (Srf.ncols*Srf.nrows)!=0 )
  {
   if(Srf.Z!=NULL) delete[] Srf.Z; Srf.Z=NULL;
   if(Srf.X!=NULL) delete[] Srf.X; Srf.X=NULL;
   if(Srf.Y!=NULL) delete[] Srf.Y; Srf.Y=NULL;
   Srf.Z = new long[Srf.ncols*Srf.nrows];

   for(int i=0; i<Srf.nrows; i++)      //-- считываем массив Srf.Z высот рельефа
   {
	fgetws(stroka1,10000,fdat);
    slovo = wcstok(stroka1,separators);
       for(int k=0; k<Srf.ncols; k++){
        Srf.Z[i*Srf.ncols+k]=_wtof(slovo);  // высота в м
        slovo = wcstok(NULL,separators);}
   }
  }
  fclose(fdat);
  Srf.flag=true;
  return true;
}
//---------------------------------------------------------------------------
//*********************** Считывание данных из файла ************************


bool read_data() {

	TStringList* fdat = new TStringList();
	UnicodeString stroka; // текущая строка обрабатываемого файла
	CavePath = cave_file; // путь к заглавному файлу проекта

	if (wcschr(cave_file, L'\\')) {
		FileList->Strings[0] = wcsrchr(cave_file, L'\\') + 1;
		// запись (без пути) в массив файлов
		CavePath.Delete(CavePath.LastDelimiter(L"\\") + 1, CavePath.Length());
	} // запись пути
	else
		FileList->Strings[0] = cave_file; // запись (без пути) в массив файлов
	fil = 1;
	char k = 0;
	while (k < fil) {
		if (k != 0) // присоед. текущей директории
				wcscpy(cave_file, (CavePath + FileList->Strings[k]).c_str());
		try {
			LoadFromFileUTF8Test(fdat, cave_file);
		} catch (const EFOpenError& ) {
        	if (k == 0) {
				error(61, L"", cave_file);
                delete fdat;
				return false;
			} // exit(EXIT_FAILURE);
			error(60, L"", cave_file);
			k++;
			continue;
		}

		end_survey();
		if (pik != 0)
			srv = srv + 1; // новая съемка - при начале файла
		Survey1[srv].team = L"?";
		Survey1[srv].file = k;
		new_survey = 0;

		if ((k > 1) && PrefList->Strings[k - 1] != L"")
			end_prefix(UnicodeString(PrefList->Strings[k - 1]).c_str() + 1);
		full_prefix = PrefList->Strings[k] + full_prefix;
		// добавл. префикс файла
		int index = full_prefix.LastDelimiter(L"\\");
		// текущий - отсекаем все левее посл. \\ и .
		if (index == 0)
			curr_prefix = full_prefix;
		else
			curr_prefix = full_prefix.SubString(index + 1,
			full_prefix.Length() - index);
		if ((curr_prefix != L"") && (curr_prefix[1] == L'.'))
			curr_prefix.Delete(1, 1);

		if (k > 0)
			magnit = ((FileParam*)(PrefList->Objects[k]))->Fmagnit;
		// склонение
		if (k > 0)
			number = ((FileParam*)(PrefList->Objects[k]))->Fnumber;
		// режим ввода

		for (line_number = 0; line_number < fdat->Count; line_number++) // считывание
		{
			stroka = (*fdat)[line_number] ;
			analiz(stroka.c_str()); // и обработка строки
			if (ex_prz)
				break;
		} // выход по #exit

		k++;
		if (comment == 1)
			error(-50, L"\n", L"");
		// для след. файла:
		if (pik != 0) // новый файл - новая ветка
		{
			if (P1[pik - 1].metEnd == 0)
				P1[pik - 1].metEnd = -1;
			P1[pik].metBeg = -1;
		}
		corrA1 = 0;
		corrA2 = 0;
		corrA = 0;
		corrL = 1;
		corrC = 0;
		corrC0 = 0;
		corrC1 = 0;
		corrH = 0; // уст. коррекций в 0
		color = 2; // станд. цвет
		line_number = 0;
		number = 0;
		angle = 1; // уст. режима в стандартный
		comment = 0;
		ex_prz = 0;
		sur = 0;
		duplic = 0;
		truth = 0;
		walls_from_to = true;
		z_survey = false;
		lr_normal = false;
		ignore_walls = false;
		only_convex_walls = false;
		wcscpy(order, L"12300000"); // станд. порядок данных L Az An
	}
	if (AscFile != L"")
		read_asc_file(AscFile, CavePath); // счит. файла рельефа
	if (fxp > 0) { // если есть FixPoints
		CalcFixPointsLocalCoordinates();
		AddFixPnt(); // допис. FixPoints в P1[]
		fix_label = L"%GPS%"; // уст. точку с коорд 000 (лев ниж угол рельефа)
		// int idx = Index_Of(L"%GPS%")+1;
		// if(!Srf.flag) fix_label = StationList->Strings[idx];
	}
	TransForm->DataLength.equ = equate_control();
	WriteVetka();
    delete fdat;
	return true;
}
//************************** walls connection **********************************
char Walls_Connection() //возвр.: 0 - есть не привяз. пикеты, 1 - все привяз. к пикетам нитки
{                       // привязка меток из WallsStationList к нитке (StationList)
  UnicodeString textstr;
  char flag=1;   // 0 - есть ненайденные метки
  char flag1=0;  // 0 - не найдена текущая метка
  int max_err=0; // max число выводимых ошибок (непривязанных стен)
  for(int N1=0; N1<WallStationList->Count; N1++)
   {
    flag1=0;
    for(int N=0; N<StationList->Count-1; N++)
     {
      if(WallStationList->Strings[N1]==StationList->Strings[N]) // если нашли в StationList
       {
        for(int k=0; k<wal; k++) {if(Wall1[k].met==-N1) Wall1[k].met=N;} // пишем в Wall1[k]
        flag1=1;
        break;    // и переходим к следующему N1
       }
     }
    //если не нашли в StationList и это первая не найденная станция
    if( (flag1==0)&&(flag==1) )
       {
        flag=0;
        textstr = L"Следующие измерения до стен не привязаны к нитке хода:\ ";  //\ \r\n";
        TransForm->Memo1->Lines->Add(textstr);
        if( (FirstError.num==0)||(FirstError.item<0) )//если не было ошибок
         {
          for(int k=0; k<wal; k++)
           {
            if(Wall1[k].met==-N1)
             {
              wcscpy( cave_file,(CavePath + FileList->Strings[Survey1[ Wall1[k].Srv ].file]).c_str() );
              wcscpy(FirstError.file,cave_file);        //file
              FirstError.line=Wall1[k].line;   //line
              break;
             }
            }
         }
       }
    if( (flag1==0)&&(max_err<11) )// вывод не найденных станций (не > 10)
    for(int k=0; k<wal; k++)
     {
      if(Wall1[k].met==-N1)
      {
       textstr = WallStationList->Strings[N1] + "  ";
       textstr += L"  File: "; textstr += FileList->Strings[Survey1[ Wall1[k].Srv ].file]; //!!!!!!!!!!!!!!!!!!
       textstr += L"  Line: "; textstr += Wall1[k].line;
       TransForm->Memo1->Lines->Add(textstr);
       FirstError.num ++;    //общее число ошибок
       FirstError.item=67;   //err number
       max_err++;
       break;
      }
     }
   }
  return flag;
}

//**************************************************************************
//*                                                                        *
//*                         CONNECTION                                     *
//*                                                                        *
//**************************************************************************
//************************** connection *************************************
char connection()    //возвр.: 0 - не связ. пикеты, 1 - все связ.
{
 if(localCoordinateSystem/*Srf.flag*/){     // если поверхность
   V1[0].XB=0; V1[0].YB=0; V1[0].ZB=0; // фиктивная нулевая ветка
   V1[0].metBeg=Index_Of(L"%GPS%"); //  метка т. 000 (левый нижн. угол рельефа)
   V1[0].metEnd=0; V1[0].con=1;}
 else{             // если нет поверхности
   V1[1].XB=0; V1[1].YB=0; V1[1].ZB=0;
   V1[1].XE=V1[1].vx(1); V1[1].YE=V1[1].vx(2); V1[1].ZE=V1[1].vx(3);
   V1[1].con=1; }
 int i = 0;
 char connect=1; //если за цикл новые ветки не пришиваются, connect=0 или -1
 while(connect>0)
 {
  connect=0;
  for(int cV=0; cV<vet; cV++)  //cV - текущая пришиваемая ветка
  {
   if((cV==0)&&(!localCoordinateSystem/*Srf.flag*/)) cV=1; // нет пов - нет фикт. ветки
   if(!V1[cV].con)                // если текущая ветка не присоединена
  {
   if(connect==0) connect=-1;
   for(int k=cV-1; k>(cV-vet); k--)
   {i=k; if(k<0) i=cV-k;
    if(V1[i].con)     //если i-я ветка присоединена
    {
     if(V1[cV].metBeg!=0)  //0-я метка - это отсутствие метки
     {
	  if(V1[cV].metBeg==V1[i].metBeg)
       {V1[cV].XB=V1[i].XB;  V1[cV].XE=V1[i].XB+V1[cV].vx(1);
        V1[cV].YB=V1[i].YB;  V1[cV].YE=V1[i].YB+V1[cV].vx(2);
        V1[cV].ZB=V1[i].ZB;  V1[cV].ZE=V1[i].ZB+V1[cV].vx(3);
        V1[cV].XYB=V1[i].XYB;
        V1[cV].con=1; connect=1; break;}
      if(V1[cV].metBeg==V1[i].metEnd)
       {V1[cV].XB=V1[i].XE;  V1[cV].XE=V1[i].XE+V1[cV].vx(1);
        V1[cV].YB=V1[i].YE;  V1[cV].YE=V1[i].YE+V1[cV].vx(2);
        V1[cV].ZB=V1[i].ZE;  V1[cV].ZE=V1[i].ZE+V1[cV].vx(3);
        V1[cV].XYB=V1[i].XYB+V1[i].vx(10);
        V1[cV].con=1; connect=1; break;}
     }
     if(V1[cV].metEnd!=0)
     {
      if(V1[cV].metEnd==V1[i].metBeg)
       {V1[cV].XE=V1[i].XB;  V1[cV].XB=V1[i].XB-V1[cV].vx(1);
        V1[cV].YE=V1[i].YB;  V1[cV].YB=V1[i].YB-V1[cV].vx(2);
        V1[cV].ZE=V1[i].ZB;  V1[cV].ZB=V1[i].ZB-V1[cV].vx(3);
        V1[cV].XYB=V1[i].XYB-V1[cV].vx(10);
        V1[cV].con=1; connect=1; break;}
      if(V1[cV].metEnd==V1[i].metEnd)
       {V1[cV].XE=V1[i].XE;  V1[cV].XB=V1[i].XE-V1[cV].vx(1);
        V1[cV].YE=V1[i].YE;  V1[cV].YB=V1[i].YE-V1[cV].vx(2);
        V1[cV].ZE=V1[i].ZE;  V1[cV].ZB=V1[i].ZE-V1[cV].vx(3);
        V1[cV].XYB=V1[i].XYB-V1[cV].vx(10)+V1[i].vx(10);
        V1[cV].con=1; connect=1; break;}
     }
    }
   }
  }
  }
 }
 if(connect==-1)       //какие метки не связаны (не >20) {error(64,L"",L"");
 {
  UnicodeString textstr;
  textstr = L" Топосъемка состоит из несвязанных между собой частей.\ \r\n";
  if(!localCoordinateSystem/*Srf.flag*/)    // если нет поверхности
   {textstr +=  "Следующие ветки не соединяются с первой:  ";}
  else
   {textstr +=  "Следующие ветки не привязаны к поверхности:  ";}
  TransForm->Memo1->Lines->Add(textstr);
  char k=0; int cV=1;
  while((cV<vet)&&(k<20))
   {if(V1[cV].con==0){
     textstr = StationList->Strings[V1[cV].metBeg] +"  "+StationList->Strings[V1[cV].metEnd];
     textstr += L"  File: "; textstr += FileList->Strings[Survey1[ P1[V1[cV].pikBeg].Srv ].file]; //!!!!!!!!!!!!!!!!!!
     textstr += L"  Line: "; textstr += P1[V1[cV].pikBeg].line;
     TransForm->Memo1->Lines->Add(textstr);
     k++;}
    cV++;
   }
  TransForm->Memo1->Lines->Add(L"");
  //для выхода на строку с ошибкой по Exit&Edit
  cV=1;
  if( (FirstError.num==0)||(FirstError.item<0) )//если не было ошибок
  while((V1[cV].con!=0)&&(cV<vet))
    {
     cV++;
     if((V1[cV].con==0))
      {
      k=Survey1[ P1[V1[cV].pikBeg].Srv ].file;
      wcscpy( cave_file,(CavePath + FileList->Strings[k]).c_str() );
      wcscpy(FirstError.file,cave_file);        //file
      FirstError.line=P1[V1[cV].pikBeg].line;   //line
      }
    }
  FirstError.num ++;    //общее число ошибок
  FirstError.item=64;   //err number
  return 0;
 }
 return 1;
}
//-------------------------------- fix --------------------------------------
void fix()
{
 float X,Y,Z;
 int k = 0;
 k=StationList->IndexOf(fix_label);
 if(k<0) return; //если метка не найдена
 for(int cV=0; cV<vet; cV++)
  {
   if(V1[cV].metBeg==k) {X=V1[cV].XB; Y=V1[cV].YB; Z=V1[cV].ZB; break;}
   if(V1[cV].metEnd==k) {X=V1[cV].XE; Y=V1[cV].YE; Z=V1[cV].ZE; break;}
  }
 for(int i=0; i<vet; i++)
  {V1[i].XB=V1[i].XB-X; V1[i].YB=V1[i].YB-Y; V1[i].ZB=V1[i].ZB-Z;
   V1[i].XE=V1[i].XE-X; V1[i].YE=V1[i].YE-Y; V1[i].ZE=V1[i].ZE-Z;}
}

//------------------ запись данных для Form View ----------------------------
void write_data()
{
 for(int p=0; p<pik; p++)        // подгот. к записи P1 - P
  if(P1[p+1].Vet==P1[p].Vet)     //если не конец ветки, то beg(p+1)->end(p)
   {if(P1[p+1].prz&4 )  P1[p].prz = P1[p].prz|8;
    if(P1[p+1].prz&16)  P1[p].prz = P1[p].prz|32;
    if(P1[p+1].prz&64)  P1[p].prz = P1[p].prz|128;}

 MainForm1->cave = cave_name; MainForm1->region = region_name;
 for(int i=0; i<fil; i++) //запись имен файлов
   {TransForm->IncFiles += FileList->Strings[i]; TransForm->IncFiles += L"\n";}
 Labels=L""; Labels=StationList->Text;
 TransForm->DataLength.pik=pik;
 TransForm->DataLength.vet=vet-1; TransForm->DataLength.vet0=vet0-1;
 TransForm->DataLength.met=StationList->Count-1; TransForm->DataLength.srv=srv;
 TransForm->DataLength.fil=fil;
 TransForm->DataLength.com =com_num;
 TransForm->DataLength.wcom =wcom_num;
 TransForm->DataLength.fxp=fxp;
 TransForm->DataLength.sur=sur_color;  TransForm->DataLength.dup=dup_color;
 TransForm->DataLength.declin=magnit;
 TransForm->DataLength.lrud=lrud;
 TransForm->DataLength.wal=wal;
 //TransForm->DataLength.equ=equate_control(); запис. в read_data()
// TransForm->DataLength.lps=VRN[0];
}

//************************ write to file .t3d *******************************
void write_file()
{
 //-----------------------------------------------------------------------
 FILE* fdat;
 if(wcschr(cave_file,L'\\')) wcscpy(wcsrchr(cave_file,L'\\')+1, L"cave.t3d");
  else wcscpy(cave_file,L"cave.t3d");
 fdat=_wfopen(cave_file,L"wt");
 if(fdat==NULL) {error(62,L"",L""); state=-11 ; return;} //Проверка ошибки
 wchar_t buffer[100];
 swprintf(buffer,L"%d \n", pik+vet-1); fputws(buffer,fdat);//число пикетов+веток-1
 swprintf(buffer,L"%d \n", sur_color); fputws(buffer,fdat);//цвет пов. топо
 if(!wcschr(cave_name.c_str(),L'\n')) wcscat(cave_name.c_str(),L"\n");  //добавляем конец строки
 fputws(cave_name.c_str(), fdat);                              //назв. пещеры
 if(!wcschr(region_name.c_str(),L'\n')) wcscat(region_name.c_str(),L"\n"); //добавляем конец строки
 fputws(region_name.c_str(), fdat);                              //назв. пещеры

 float X=0, Y=0, Z=0, XY=0;
 for(int p=0; p<pik; p++)
 {
  if((p==0)||(P1[p].Vet!=P1[p-1].Vet)) // если начало ветки - дополн. точка
   {                                 // и v - призн.начала)
    X=100*V1[P1[p].Vet].XB;
    Y=100*V1[P1[p].Vet].YB;
    Z=100*V1[P1[p].Vet].ZB;
    XY=100*V1[P1[p].Vet].XYB; //р-развертка
    swprintf(buffer,L"%.0f  %.0f  %.0f  %.0f v", X, Y, Z, XY);
    if(P1[p].prz&1 ) wcscat(buffer,L" p"); //sur
    if(P1[p].prz&2 ) wcscat(buffer,L" r"); //riv
    if(P1[p].prz&4 ) wcscat(buffer,L" e"); //ent beg
    if(P1[p].prz&16) wcscat(buffer,L" s"); //sif beg
    if(P1[p].prz&64) wcscat(buffer,L" t"); //red beg
    if((P1[p].col&31)!=2)                 //цвет
     {wcscat(buffer,L" c"); _itow((int)(P1[p].col&31),buffer+wcslen(buffer),10);}
    swprintf(buffer+wcslen(buffer),L" V%.d  ", P1[p].Vet);
    swprintf(buffer+wcslen(buffer),L" S%.d  ", P1[p].Srv);
    wcscat(buffer,L"\n");
    fputws(buffer,fdat);
   }
  X=X+ 100*P1[p].L;
  Y=Y+ 100*P1[p].An;
  Z=Z+ 100*P1[p].Az;
  XY=XY+ 100*P1[p].pr_xy();   //р-развертка
  swprintf(buffer,L"%.0f  %.0f  %.0f  %.0f", X, Y, Z, XY);
/*  if(P1[p+1].Vet==P1[p].Vet)     //если не конец ветки, то beg(p+1)->end(p)
   {if(P1[p+1].prz&4 )  P1[p].prz = P1[p].prz|8;
    if(P1[p+1].prz&16)  P1[p].prz = P1[p].prz|32;
    if(P1[p+1].prz&64)  P1[p].prz = P1[p].prz|128;} */
  if(P1[p].prz&1  ) wcscat(buffer,L" p");
  if(P1[p].prz&2  ) wcscat(buffer,L" r");
  if(P1[p].prz&8  ) wcscat(buffer,L" e"); //ent end
  if(P1[p].prz&32 ) wcscat(buffer,L" s"); //sif end
  if(P1[p].prz&128) wcscat(buffer,L" t"); //red end
  if((P1[p].col&31)!=2)                  //цвет
   {wcscat(buffer,L" c"); _itow((int)(P1[p].col&31),buffer+wcslen(buffer),10);}
  swprintf(buffer+wcslen(buffer),L" V%.d  ", P1[p].Vet);
  swprintf(buffer+wcslen(buffer),L" S%.d  ", P1[p].Srv);
  wcscat(buffer,L"\n");
  fputws(buffer,fdat);
 }
 swprintf(buffer,L"%.d  %.d  %.d %.d %.01d\n", pik, vet-1, StationList->Count-1, srv, TransForm->DataLength.lps);
 fputws(buffer,fdat); //число пикетов, веток, меток, съемок, колец
 for(int p=1; p<vet; p++) //массив веток
 {
  swprintf(buffer,L"%.01d  %.01d\n", V1[p].metBeg, V1[p].metEnd);
  fputws(buffer,fdat);
 }
 for(int p=1; p<StationList->Count; p++) //массив имен меток
 {
  wcscpy(buffer,UnicodeString(StationList->Strings[p]).c_str());
  wcscat(buffer,L"\n");
  fputws(buffer,fdat);
 }
 for(int p=1; p<srv+1; p++) //массив имен съемок
 {
  wcscpy(buffer,Survey1[p].team.c_str());
  wcscat(buffer,L"\n");
  fputws(buffer,fdat);
 }
 if(fclose(fdat)!=0) error(62,L"",L"");
}
//**************************************************************************
//**************************************************************************
//*                                                                        *
//*                               КОЛЬЦА                                   *
//*                                                                        *
//**************************************************************************
//**************************************************************************
//------------------- нахождение т. замыкания колец ------------------------
int find_rings() //возвр. число одинак. меток с разными коорд. и
{                 //заполняет массив R[] меток (и их веток) отв-х т. замыкания колец
// struct ring  {int me; int ve;}; //метка(+/- начало/конец) и номера веток
// ring R[max_rin];                //от max_rin/2 до max_rin-1 колец
 int o=0; int j=0;         //текущее число колец  и вспом. переменная
 R[0].me=0;                  //присв. 0-й номер несуществ. метки
 for(int k=1; k<= StationList->Count; k++)   //k - номер метки   экономнее искать не по всем меткам  !!!!!!!!!!!!!!!!!!!!!!
 {
 if(BifStation[k]==1)
 {
  for(int i=0; i<=vet; i++)  //i - номер ветки; сначала пишем ветки-кольца
   {if((V1[i].metEnd==k)&&(V1[i].metBeg==k)) {o++; R[o].me=k; R[o].ve=i;} }
  for(int i=0; i<=vet; i++)      //i - номер ветки
  {                          //просм. все ветки на предмет k-й метки
   if((V1[i].metBeg==k)&&(V1[i].metEnd!=k)&&(V1[i].metEnd!=0)) //нашли ветку с меткой metBeg=k
   {
    j=o;
    while(abs(R[j].me)==k) //сравн. с коорд. уже имеющихся в R[] точек
     {                     //с меткой k (k>0 - Beg)
      if(R[j].me>0)        //если коорд. совп. с началом уже имеющ-ся в R[] ветки
      if( (V1[i].XB==V1[R[j].ve].XB)&&(V1[i].YB==V1[R[j].ve].YB)&&(V1[i].ZB==V1[R[j].ve].ZB) )
       break;
      if(R[j].me<0)        //если с концом
      if( (V1[i].XB==V1[R[j].ve].XE)&&(V1[i].YB==V1[R[j].ve].YE)&&(V1[i].ZB==V1[R[j].ve].ZE) )
       break;
      j--;
     }
    if(abs(R[j].me)!=k) //записываем, если нет совп. координат с предыд. записями
     {o++; R[o].me=k; R[o].ve=i;} //для нач-конец ветки исп. знак метки
   }
   if((V1[i].metEnd==k)&&(V1[i].metBeg!=k)&&(V1[i].metBeg!=0)) //нашли ветку с меткой metEnd=k
   {
    j=o;
    while(abs(R[j].me)==k)  //сравн. с имеющимися точками с меткой k (k>0 - Beg)
     {
      if(R[j].me>0)         //если с началом
      if( (V1[i].XE==V1[R[j].ve].XB)&&(V1[i].YE==V1[R[j].ve].YB)&&(V1[i].ZE==V1[R[j].ve].ZB) )
       break;
      if(R[j].me<0)         //если с концом
      if( (V1[i].XE==V1[R[j].ve].XE)&&(V1[i].YE==V1[R[j].ve].YE)&&(V1[i].ZE==V1[R[j].ve].ZE) )
       break;
      j--;
     }
    if(abs(R[j].me)!=k)
     {o++; R[o].me=-k; R[o].ve=i;} //для нач-конец ветки можно исп. знак метки
   }
  }
  if(o>0) //o-- устраняем единичное упоминание, если нет колец с данной меткой
  if( (V1[R[o].ve].metEnd!=V1[R[o].ve].metBeg)&&(abs(R[o].me)!=abs(R[o-1].me)) ) o--;
 }
 }
 R[0].ve=o; // оставляем инф. о числе записей в R[0].ve
 return o;  // (+/-)met vet - массив на выходе
}

//**********************- атрибут ветки V1[i].con -**************************
int vet_con()   //возвр. (число веток с метками на обоих концах +1)
{               //перезапис. атрибут ветки V1[i].con
 int k=1;
 for(int i=0; i<vet; i++)
 {if((V1[i].metBeg==0)||(V1[i].metEnd==0)) V1[i].con=0; else{V1[i].con=1;k++;} }
 return k;
}
//------------------------- поиск веток кольца -----------------------------
float vet_ring(int o, char mode, int j)
    //ищет ветки кольца (от номера o в массиве колец), возвр. длину кольца
{                     //o - нач., o+1 - конец ветки
 struct posVet        //возм. ветка кольца и ей предшеств.
  {int vet = 0;
   int pre = 0;};         //+/-vet - нач/кон; pre - номер из PV, с кот. пришли
 posVet *PV;
 PV = new posVet[vet_con()+1]; //массив возм. веток кольца [числа "двустор." веток]
 int k=1;                 //текущее число записанных возм. веток кольца
 for(int i=1; i<vet; i++) //запись веток 0-го уровня (т.е. с исх. меткой и коорд-ми)
 if(V1[i].con!=0)          //если оба конца ветки с метками
 {
  if((V1[i].metBeg==R[o].me)
   &&(V1[i].XB==V1[R[o].ve].XB)&&(V1[i].YB==V1[R[o].ve].YB)&&(V1[i].ZB==V1[R[o].ve].ZB) )
      {PV[k].vet=i; V1[i].con=-1; k++; }
  if((V1[i].metBeg==-R[o].me)
   &&(V1[i].XB==V1[R[o].ve].XE)&&(V1[i].YB==V1[R[o].ve].YE)&&(V1[i].ZB==V1[R[o].ve].ZE) )
      {PV[k].vet=i; V1[i].con=-1; k++; }
  if((V1[i].metEnd==R[o].me)
   &&(V1[i].XE==V1[R[o].ve].XB)&&(V1[i].YE==V1[R[o].ve].YB)&&(V1[i].ZE==V1[R[o].ve].ZB) )
      {PV[k].vet=-i; V1[i].con=-1; k++; }
  if((V1[i].metEnd==-R[o].me)
   &&(V1[i].XE==V1[R[o].ve].XE)&&(V1[i].YE==V1[R[o].ve].YE)&&(V1[i].ZE==V1[R[o].ve].ZE) )
      {PV[k].vet=-i; V1[i].con=-1; k++; }
 }
  //AddLine(UnicodeString (R[o].ve)); AddLine(UnicodeString (R[o+1].ve));

 for(int i=1; i<=k; i++) PV[i].pre=0; //предш. 0-му ур. - не существ. 0-ветка
   // cout <<k<<"\n";    //для отладки - число веток 0-го уровня
 if(mode==0) j=1;                 //перем. для записи в массив VR[]
 int fla=1;
 for(int ku=1; ku<vet; ku++)      //пока рак на горе не свистнет
 {                       //!корректнее ku<vet_con+1, ku-для перемещ. по PV[]
  for(int i=1; i<vet; i++)   //запись веток высших уровней
  if(V1[i].con>0)         //если ветку еще не записали
   {

    if(PV[ku].vet<0)     //если пришивали по метке на конце,
     {if(V1[i].metBeg==V1[abs(PV[ku].vet)].metBeg) //сравниваем с меткой начала непришитой
       if( (V1[i].XB==V1[abs(PV[ku].vet)].XB)&&(V1[i].YB==V1[abs(PV[ku].vet)].YB)&&(V1[i].ZB==V1[abs(PV[ku].vet)].ZB) )
        {PV[k].vet=i; PV[k].pre=ku; k++; V1[i].con=-1; //пишем в PV[]
         if(i==abs(R[o+1].ve)) {if(R[o+1].me<0) fla=0; goto wr;}}
      if(V1[i].metEnd==V1[abs(PV[ku].vet)].metBeg) //сравниваем с меткой конца
       if( (V1[i].XE==V1[abs(PV[ku].vet)].XB)&&(V1[i].YE==V1[abs(PV[ku].vet)].YB)&&(V1[i].ZE==V1[abs(PV[ku].vet)].ZB) )
        {PV[k].vet=-i; PV[k].pre=ku; k++; V1[i].con=-1; //пишем в PV[]
         if(i==abs(R[o+1].ve)) {if(R[o+1].me>0) fla=0; goto wr; }}
     }//(если попали на конец кольца) (метка замыкания на начале, и пришли с конца-wr)

    if(PV[ku].vet>0)    //если пришивали по метке на начале,
     {if(V1[i].metBeg==V1[abs(PV[ku].vet)].metEnd)
       if( (V1[i].XB==V1[abs(PV[ku].vet)].XE)&&(V1[i].YB==V1[abs(PV[ku].vet)].YE)&&(V1[i].ZB==V1[abs(PV[ku].vet)].ZE) )
        {PV[k].vet=i; PV[k].pre=ku; k++; V1[i].con=-1;
         if(i==abs(R[o+1].ve)) {if(R[o+1].me<0) fla=0; goto wr; }}
      if(V1[i].metEnd==V1[abs(PV[ku].vet)].metEnd)
       if( (V1[i].XE==V1[abs(PV[ku].vet)].XE)&&(V1[i].YE==V1[abs(PV[ku].vet)].YE)&&(V1[i].ZE==V1[abs(PV[ku].vet)].ZE) )
        {PV[k].vet=-i; PV[k].pre=ku; k++; V1[i].con=-1;
         if(i==abs(R[o+1].ve)) {if(R[o+1].me>0) fla=0; goto wr; }}
     }
   }
 }
wr: if(fla==1) k=PV[k-1].pre; //отбрасываем последнюю ветку, т.к. она не входит в кольцо
    else k--; //cout <<k<<"\n"; //число веток, перебранных до соединения
  float L=0;                 //длина кольца
  int GPS=Index_Of(L"%GPS%");
  while(k!=0)                //подсчет и запись числа веток кольца
   {if(mode==1) VR[j]=PV[k].vet; //cout << PV[k].vet <<" "<<L<<" "; getch();
    if( (GPS<0)||(V1[abs(PV[k].vet)].metBeg!=GPS) ) // L без учета GPS-привязки к лев ниж. углу
     L=L+V1[abs(PV[k].vet)].vx(4);
    k=PV[k].pre;
    j++;}
 if(mode==0) VRN[0]=j-1;    //запись числа веток в кольце // cout<<"\n"<<(j-1)<<"\n";
  /* for(i=1;i<=k;i++)      //потенц. ветки кольца (для отладки)
     {cout<<PV[i].vet<<" "<< StationList->Strings[V1[abs(PV[i].vet)].metBeg]<<"  "
     << StationList->Strings[V1[abs(PV[i].vet)].metEnd]<<"  "<<PV[i].pre<<"\n"; getch();} */
   // for(int k2=1; k2<vet; k2++)
   //  AddLine(UnicodeString (PV[k2].vet) + " " + UnicodeString (PV[k2].pre));
 delete [] PV;
 return L;
}
//**************************************************************************/
//*                           Information                                  */
//**************************************************************************/
//---------------------------вывод невязок----------------------------------
void info1(int k,double L,double Lo,double LoR,double LoZ)
{
 wchar_t buffer[100];
 AddLine( "кольцо  ^" + StationList->Strings[abs(R[k].me)] );
  TransForm->txtLoops += L"кольцо  ^" + StationList->Strings[abs(R[k].me)];
 swprintf(buffer,L"  %.2fm  невязка %.2fm (%.2fm / %.2fm), или %.2f\% ", L,Lo,LoR,LoZ,100*Lo/L);
 AddLine(UnicodeString(buffer));
  TransForm->txtLoops += UnicodeString(buffer) + "\r\n";
 if(Lo/L>0.11){
  AddLine(L"Невязка кольца слишком велика. Возможно, два РАЗНЫХ ");
  AddLine(L"пикета помечены одной меткой либо неверно введены");
  AddLine(L"данные (например, перепутаны прямые и обратные азимуты).");
  }
 if(L<0.1) error(-66, L"", UnicodeString(StationList->Strings[abs(R[k].me)]).c_str() ); //если кольцо<10cm
 //? надо защиту от L->0 ?
}
//----------------------------метки кольца----------------------------------
void infoCir(int k)
{
 int ko=1;
 UnicodeString textstr;
 for(int i=1;i<k;i++) ko=ko+VRN[i]; //номер 1-й ветки k-го кольца в VR[]
 textstr=L"";
  {for(int i=ko;i<ko+VRN[k];i++)           //ветки колец
   {if(VR[i]<0) textstr+= StationList->Strings[V1[abs(VR[i])].metBeg] + " - ";
     else       textstr+= StationList->Strings[V1[abs(VR[i])].metEnd] + " - ";}
    if(VR[ko]<0)textstr+= StationList->Strings[V1[abs(VR[ko])].metBeg];
     else       textstr+= StationList->Strings[V1[abs(VR[ko])].metEnd];
   }
 AddLine(textstr);

 //----- просмотр массива VR ----------------
//  for(int i=0;i<VRN[1]+1;i++) AddLine(VR[i]);
//  AddLine(StationList->Strings[V1[10].metBeg]);
//------------------------------------------
}
//----------------------запрос на обработку колец---------------------------
void info2(int i)
{
 UnicodeString textstr;
 textstr = L"\r\nТопосъемка содержит незамкнутые кольца в количестве ";
 textstr += UnicodeString ((int)i) + " шт.\ \r\n";
 //textstr += L"Если не хотите обрабатывать кольца, нажмите пробел;\ \r\n";
 //textstr += L"для просмотра меток колец нажмите F1;\ \r\n";
 //textstr += L"для обработки колец нажмите Enter.";
 textstr += L"Выберите режим обработки колец \r\n";
 AddLine(textstr);
}
//--------------------------------------------------------------------------
void info3(int k, float maxEr)  //пока не используем
{wchar_t buffer[80];
 swprintf(buffer,L"\r\nНевязка кольца слишком велика и составляет %.2f\%", 100*maxEr);
 AddLine(UnicodeString (buffer));
 AddLine(L"Возможно, два РАЗНЫХ пикета помечены одной меткой либо неверно введены");
 AddLine(L"данные (например, перепутаны прямые и обратные азимуты). Ветки кольца:");
 infoCir(k); AddLine(L"");
}
//**************************************************************************
void circle(char mode)        //опред. колец, max невязки и вывод на печать
{
 int k=1;  //номер тек. записи в R[]
 int i=1;  //номер тек. кольца
 int j=1;            //номер тек. записи в VR[]
 float L,Lo,LoR,LoZ; //длина и невязка кольца (общая, горизонтальная, вертикальная)
 float  maxEr=0;     //max относит. невязка (ошибка)
 int maxErCir = 0;       //номер кольца с max относит. невязкой

 while(k<=R[0].ve)   //выдача абс. невязок, цикл до числа записей в R[]
  {
   if(V1[R[k].ve].metBeg==V1[R[k].ve].metEnd) //если замык.на одной ветке
    {Lo=L_vect(V1[R[k].ve].X(), V1[R[k].ve].Y(), V1[R[k].ve].Z());
     LoR=L_vect(V1[R[k].ve].X(), V1[R[k].ve].Y(),0); LoZ=fabs(V1[R[k].ve].Z());
     VRN[i]=1;  L=V1[R[k].ve].vx(4);
     if(mode==0) info1(k,L,Lo,LoR,LoZ);
      else {VR[j]=R[k].ve; j++;
       if(Lo/L > maxEr) {maxEr=Lo/L; maxErCir=i;}}    //нах. max невязки
     i++;
    }
   if( (k<R[0].ve)&&( abs(R[k+1].me)==abs(R[k].me) ) )
     //если кольцо сост. более чем из одной ветки  //если 2 кольца из одной ветки с одним одинак. концом, то
     {if( (R[k+1].me>0)&&(R[k].me)>0 ){            //обраб. лишнее кольцо, хотя особого ущерба от этого нет
       Lo=L_vect(V1[R[k+1].ve].XB-V1[R[k].ve].XB, V1[R[k+1].ve].YB-V1[R[k].ve].YB, V1[R[k+1].ve].ZB-V1[R[k].ve].ZB);
       LoR=L_vect(V1[R[k+1].ve].XB-V1[R[k].ve].XB, V1[R[k+1].ve].YB-V1[R[k].ve].YB, 0); LoZ=fabs(V1[R[k+1].ve].ZB-V1[R[k].ve].ZB);}
      if( (R[k+1].me>0)&&(R[k].me)<0 ){
       Lo=L_vect(V1[R[k+1].ve].XB-V1[R[k].ve].XE, V1[R[k+1].ve].YB-V1[R[k].ve].YE, V1[R[k+1].ve].ZB-V1[R[k].ve].ZE);
        LoR=L_vect(V1[R[k+1].ve].XB-V1[R[k].ve].XE, V1[R[k+1].ve].YB-V1[R[k].ve].YE, 0); LoZ=fabs(V1[R[k+1].ve].ZB-V1[R[k].ve].ZE);}
      if( (R[k+1].me<0)&&(R[k].me)>0 ){
       Lo=L_vect(V1[R[k+1].ve].XE-V1[R[k].ve].XB, V1[R[k+1].ve].YE-V1[R[k].ve].YB, V1[R[k+1].ve].ZE-V1[R[k].ve].ZB);
       LoR=L_vect(V1[R[k+1].ve].XE-V1[R[k].ve].XB, V1[R[k+1].ve].YE-V1[R[k].ve].YB, 0); LoZ=fabs(V1[R[k+1].ve].ZE-V1[R[k].ve].ZB);}
      if( (R[k+1].me<0)&&(R[k].me)<0 ){
       Lo=L_vect(V1[R[k+1].ve].XE-V1[R[k].ve].XE, V1[R[k+1].ve].YE-V1[R[k].ve].YE, V1[R[k+1].ve].ZE-V1[R[k].ve].ZE);
       LoR=L_vect(V1[R[k+1].ve].XE-V1[R[k].ve].XE, V1[R[k+1].ve].YE-V1[R[k].ve].YE, 0); LoZ=fabs(V1[R[k+1].ve].ZE-V1[R[k].ve].ZE);}
      L=vet_ring(k,mode,j); j=j+VRN[i];
      if(mode==0) {VRN[i]=VRN[0]; info1(k,L,Lo,LoR,LoZ);} //запись массива числа веток в кольце
       else if(Lo/L > maxEr) {maxEr=Lo/L; maxErCir=i;}  //нах. max невязки
      i++;
     }
   k++;
  }
 if(mode==0) VRN[0]=i-1;  //запись числа колец в VRN[0]
  else {
   VR[0]=maxErCir;    //запись номера кольца с max относит. невязкой
   if(maxEr>0.1)
    {state=-state;} // info3(VR[0],maxEr); } //меняем state!
   }
}
//--------------------------------------------------------------------------
float maxEr()    //запис. в VR[0] номер кольца с max отн. невязкой
{                //возвр. max абс. невязку
 int ko=1; char sign = 0;  //номер начальной ветки кольца, напр-е ветки
 float L,dL,dx,dy,dz;
 float maxErr=0;      //max относит. невязка (ошибка)
 float maxErA=0;      //max абс. невязка (ошибка)
 int   maxErCir = 0;      //номер кольца с max относит. невязкой
 for(int k=1; k<=VRN[0]; k++) //перебираем кольца
 {
  dx=0;dy=0;dz=0;L=0;
  for(int i=ko; i<ko+VRN[k]; i++) //перебираем ветки кольца
   {if(VR[i]>0) sign=1; else sign=-1;  // cout<<'\n'<< sign <<" ";
    dx=dx-sign*V1[abs(VR[i])].X(); //невязки
    dy=dy-sign*V1[abs(VR[i])].Y();
    dz=dz-sign*V1[abs(VR[i])].Z();
    L =L + V1[abs(VR[i])].vx(4);       //длина кольца
   }
  dL=L_vect(dx,dy,dz);
  ko=ko+VRN[k];
  if(dL/L > maxErr) {maxErr=dL/L; maxErCir=k;}    //нах. max отн. невязки
  if(dL > maxErA) maxErA=dL;                      //нах. max абс. невязки
//  cout<<maxErr<<" "<<maxErCir<<'\n'; getch();
//  cout<<'\n'<< dx << " "<< dy << " "<< dz << " "<< L; getch();
 }
 VR[0]=maxErCir;
 return maxErA;
}
//--------------------------------------------------------------------------
void corrV(int k) //разброс невязки k-го кольца по веткам
{
 int ko=1; int sign = 0;
 float L,Lxy,Lz,dx,dy,dz;
 float Li,Lixy,Liz;
 dx=0;dy=0;dz=0;L=0;Lxy=0;Lz=0;
 for(int i=1;i<k;i++) ko=ko+VRN[i]; //номер 1-й ветки k-го кольца в VR[]
// cout<<'\n'<< ko <<" ";
 for(int i=ko; i<ko+VRN[k]; i++)     //перебираем ветки кольца, вычисл. его хар-ки
  {if(VR[i]>0) sign=1; else sign=-1;  // cout<<'\n'<< sign <<" ";
   dx=dx-sign*V1[abs(VR[i])].X(); //невязки
   dy=dy-sign*V1[abs(VR[i])].Y();
   dz=dz-sign*V1[abs(VR[i])].Z();
   L =L +V1[abs(VR[i])].vx(7);       //длина кольца
   Lxy=Lxy+V1[abs(VR[i])].vx(8);     //длина в плане
   Lz=Lz+V1[abs(VR[i])].vx(9);       //сумма верт. проекций
//   cout<<'\n'<< dx << " "<< dy << " "<< dz << " "<< L <<'\n'; getch();
  }
 for(int i=ko; i<ko+VRN[k]; i++)     //разброс невязки по веткам
  {
   Li=V1[abs(VR[i])].vx(7); Lixy=V1[abs(VR[i])].vx(8); Liz=V1[abs(VR[i])].vx(9);
   if(VR[i]>0) sign=1; else sign=-1;
   V1[abs(VR[i])].ZE = V1[abs(VR[i])].ZE + sign*dz*(Li+Liz-Lz*Li/L)/L;
   V1[abs(VR[i])].XE = V1[abs(VR[i])].XE + sign*dx*(Li+Lixy-Lxy*Li/L)/L;
   V1[abs(VR[i])].YE = V1[abs(VR[i])].YE + sign*dy*(Li+Lixy-Lxy*Li/L)/L;
  }
}
//--------------------------------------------------------------------------
void corrP() //разброс невязки по пикетам
{
 float L,Li,Lxy,Lz,Lixy,Liz;
 for(int p=0; p<pik+fxp; p++)     //разброс невязки по пикетам
  {
   if((p==0)||(P1[p].Vet!=P1[p-1].Vet))
    {L=V1[P1[p].Vet].vx(7); Lxy=V1[P1[p].Vet].vx(8); Lz=V1[P1[p].Vet].vx(9);}
   Li=ldexp(L_vect(P1[p].L,P1[p].An,P1[p].Az),P1[p].prz1);
   Lixy=ldexp(L_vect(P1[p].L,P1[p].An,0),P1[p].prz1);
   Liz=ldexp(abs(P1[p].Az),P1[p].prz1);
   if(L!=0){
    P1[p].L = P1[p].L  - V1[P1[p].Vet].XE*(Li+Lixy-Lxy*Li/L)/L;
    P1[p].An= P1[p].An - V1[P1[p].Vet].YE*(Li+Lixy-Lxy*Li/L)/L;
    P1[p].Az= P1[p].Az - V1[P1[p].Vet].ZE*(Li+Liz-Lz*Li/L)/L;
    }
  }
}
//--------------------------------------------------------------------------
/* void correction()
{        Zc=Zc + dZ*(abs(Z2/Kk)+(L/Kk)*(1-Z_o/L_o))/L_o;
         Xc=Xc + dXY*(XY/Kk+(L/Kk)*(1-XY_o/L_o))/L_o);
         Z=Z+Zc;
         if(dXY>0.1)  //>0.1см - чтобы не разделить случайно на 0}
          { X=X+Xc*dX/(dXY); Y=Y+Xc*dY/(dXY); }
}*/
//--------------------------------------------------------------------------
//                  СТАДИИ ОБРАБОТКИ КОЛЕЦ
//1. Нах. т. нач. и конца кольца
//2. Нах. веток кольца
//3. Запись веток кольца
//4. Нах. кольца с max невязкой и выдача информации  | цикл
//5. Разброс невязки по веткам                       | цикл
//6. Разброс невязки по пикетам
//--------------------------------------------------------------------------

//************************ ПРОСМОТР МАССИВОВ *******************************

//**************************************************************************
//state
//  0 - построчная обработка без ошибок
// -1 - -//- с ошибками
//  2 - все ветки привязаны
// -2 - не все ветки привязаны, кольца не обрабатываем
//  3 - нет колец
//  4 - есть кольца
//  5 - без обработки колец
//  6 - выдать метки колец
//  7 - обработать кольца
//  8 - выдали метки колец
// -8 -  -//-, но была слишком большая ошибка в кольце
//  9 - обработка колец
//  10 - обработали, выходим
//  11 - если выходим, не построив (по exit)
// -12 - осталась невязка > 10см
//  12 - выходим с невязкой > 10см
//  13 - выходим, строя рельеф, пещеры нет
//**************************************************************************
void CurrState()
{
 wchar_t buffer[80];
 if(abs(state)<2)
  {
   if(!connection())
    {state=-2; Walls_Connection(); if(TransForm->Visible==false) TransForm->ShowModal(); return; }
   else state=2;
   if( !Walls_Connection()) // привязка изм. до стен к нитке
    {if(TransForm->Visible==false) TransForm->ShowModal(); return;}
  }
 if(state==2)
  {
   if(R) {delete[] R; R=NULL;}  //меток т. замыкания колец и соотв. им веток колец
    R = new ring[vet+1];  //число колец не > числа веток
   if(find_rings()) state=4;
    else state=3;
  }
 if(state==4)      //только если все ветки привязаны и есть кольца
  {
   if(VRN) delete[] VRN;      // массив VR веток всех колец
   VRN = new int[R[0].ve+2];  // хотя R[0].ve может в 2 раза прев. наст. число колец
   circle(0);       //находим кольца и выдаем невязки, заполняем VRN
   TransForm->Label1->Visible=true;
   TransForm->BitBtn2->Visible=true;
   TransForm->RadioGroup1->Visible=true;
   TransForm->DataLength.lps=VRN[0]; //число колец
   info2(VRN[0]);   //запрос об обработке колец
   if(TransForm->Visible==false) //покажем форму:
    { TransForm->ShowModal(); }
  }
 if((abs(state)==6)||(abs(state)==7))  //если "да" на запрос об обработке колец
   {
    VRN[VRN[0]+1]=0;                //находим число веток во всех кольцах
    for(int i=1; i<=VRN[0]; i++) VRN[VRN[0]+1]=VRN[VRN[0]+1]+VRN[i];

    if(VR) delete[] VR;
    VR = new int[VRN[VRN[0]+1]+1];  //создаем массив VR веток всех колец
    circle(1);                      //заполняем массив VR  //WievVRN(); WievVR();
    if(abs(state)==6)
     {for(int i=1;i<=VRN[0];i++) {AddLine(L""); infoCir(i);} //просмотр колец
      if(state==6) state=8; else state=-8;}
    if(abs(state)==7) {state=9; } if(state==-7) state=-8;
//    if((state)==7) {state=9; } if(state==-7) state=-8;
   }
// AddLine(L"ab");
 if(state==9)
 {
    TransForm->Label1->Visible=false;
    TransForm->BitBtn2->Visible=false;
    TransForm->RadioGroup1->Visible=false;
    for(int k=1;k<=vet;k++) {V1[k].Shift();}
    int i=0; UnicodeString dotstr = L"."; int j =AddLine(dotstr);
    do {corrV(VR[0]); i++;
        TransForm->Memo1->Lines->Insert(j, i);
        TransForm->Memo1->Lines->Delete(j+1);
       } //разброс невязок по веткам
    while((maxEr()>0.1)&&(i<1000));
    corrP();                           //разброс невязок по пикетам
    if(i==1000) {
   //  AddLine(L"max оставшаяся невязка "+ UnicodeString(maxEr()));
     state=-12;
     swprintf(buffer,L"max оставшаяся невязка кольца %.2f м", maxEr() );
     AddLine(UnicodeString (buffer));
     TransForm->Label1->Caption=L"   продолжить";
     TransForm->Label1->Visible=true;
     TransForm->BitBtn2->Visible=true;
     }
    connection();
 }
 if((state==-2)||(state==3)||(state==5)||(state==9)||(state==12))
 {
   if(fix_label.Length()!=0) fix();
   write_data();
//   write_file();     //!!!!!!!!!!!!!!!!!!!!!!!!!!!! незаменим для отладки !!!
   state=10;
   TransForm->Close();
 }
 if(state==13) {write_data(); TransForm->Close();}
}
//-------------------------- preprocessor-----------------------------------
bool Pre()              // прибл. подсчет числа пикетов, съемок, фикс. точек
{
 pre_pik=0;  // число пикетов
 pre_srv=0;  // число съемок
 pre_pnt=0;  // число фикс. точек
 pre_wal=0;  // число точек стен

 char pre_wal0=0; // переменная для стен

 TStringList* fdat = new TStringList();
 wchar_t* stroka;                // текущая строка обрабатываемого файла
 wchar_t *slovo;
 const wchar_t *separators = L"\t\v\r\n ";  //сепараторы - табуляции, конец строки и пробел
 wchar_t *err_symb = NULL;           //для обраб. ошибок в числ. данных
 float floatN; //число измерений до стен на пикете
 UnicodeString CavePath=cave_file;   // путь к заглавному файлу проекта
 int fils=0;

 if(FileList!=NULL) delete FileList;
 FileList = new TStringList();
 FileList->Add(L"");

 if(wcschr(cave_file,L'\\'))  {
   FileList->Strings[0]=wcsrchr(cave_file,L'\\')+1; //запись (без пути) в массив файлов
   CavePath.Delete( CavePath.LastDelimiter(L"\\")+1, CavePath.Length() ); } // запись пути
  else FileList->Strings[0]=cave_file;             //запись (без пути) в массив файлов
 fils=1;
 char k=0;
 while(k<fils)
 {
  if(k!=0)               // присоед. текущей директории
	wcscpy( cave_file,((CavePath + FileList->Strings[k])).c_str() );
  LoadFromFileUTF8Test(fdat, cave_file);
  if(fdat->Count == 0)                        // Проверка ошибки
   {
    if(k==0) {error(61,L"",cave_file);  return false;}  //exit(EXIT_FAILURE);
   }
  else
  {
   pre_srv++;        // если новый файл без декларирования съемки

   for (int ln = 0; ln < fdat->Count; ln++, line_number++) // считывание
   {                                                  // и обработка строки
	stroka = (*fdat)[ln].c_str();
    if(wcschr(stroka,L'%')) wcscpy(wcschr(stroka,L'%'),L"");// отсекаем комментарий
    if(wcschr(stroka,L';')) wcscpy(wcschr(stroka,L';'),L"");//  -//-
    if( (comment==1) && wcschr(stroka,L'}') )    //если конец комментария
      {wcscpy(stroka,wcschr(stroka,L'}')+1); comment=0;}
    if(wcschr(stroka,L'{'))                      //если начало комментария
       {if(!wcschr(stroka,L'}')) {wcscpy(wcschr(stroka,L'{'),L"");  comment=1;}
         else  wcscpy( wcschr(stroka,L'{'), (wcschr(stroka,L'}')+1) );}
    if(comment==0)
    {
     pre_pik++;                       // каждая строка - потенц. пикет
     pre_wal0=0;
     slovo = wcstok(stroka,separators);
     while (slovo!=NULL)              // разбивка на отдельные слова
     {
      if (wcscmp(slovo, L"#exit")==0) ex_prz=1;
      if (wcscmp(slovo, L"#point")==0) pre_pnt++;
      if (wcscmp(slovo, L"#point_UTM")==0) pre_pnt++;
      if (wcscmp(slovo, L"#survey")==0) pre_srv++;
      if(!wcsncmp(slovo,L"#walls[",7))
       {floatN=wcstod(op_par(slovo),&err_symb);
        if(!*err_symb && floatN>0 && floatN<101) pre_wal=pre_wal+int(floatN);
       }
      if ( (wcscmp(slovo, L"-")==0)&&(pre_wal0==0) ) {pre_wal++; pre_wal0=1;}
      if (wcscmp(slovo, L"#include")==0)
      {slovo = wcstok(NULL,separators);
        UnicodeString slovo1 = slovo;
        UnicodeString PartPath = cave_file;  // нах. разницу между путями
        PartPath.Delete(1, CavePath.Length());
        PartPath.Delete(PartPath.LastDelimiter(L"\\")+1, PartPath.Length() );
        slovo1.Delete( 1, slovo1.LastDelimiter(L".") );
        if ( slovo1.CompareIC(L"cav")*slovo1.CompareIC(L"dat")==0 ) // отбор пещерных файлов
        {
         if( FileList->IndexOf(slovo)<0 ) {FileList->Add(PartPath+UnicodeString(slovo)); fils = fils+1;}
        }
      }
      slovo = wcstok(NULL,separators);
     }
    }
    if(ex_prz) break;                               // выход по #exit
   }
  }
  k++;
  comment=0; ex_prz=0;
 }
 if(FileList!=NULL) delete FileList; FileList=NULL;
  delete fdat;
 //new P, surv, FixPnt - создаются в InitData
 return true;
}
//**************************************************************************
//---------------------------------------------------------------------------
__fastcall TTransForm::TTransForm(TComponent* Owner)
        : TForm(Owner)
{

}
//---------------------------------------------------------------------------
bool RunBuild(wchar_t* FileName1)
{
  wcscpy(cave_file, FileName1);
  Pre();     // прибл. подсчет числа пикетов, съемок, фикс. точек
  wcscpy(cave_file, FileName1);
  InitData();// инициализируем все данные перед работой
  AddLine(L"Processing " + UnicodeString(cave_file) +"          [ESC=EXIT]\ \r\n");
  UnicodeString slovo1 = cave_file;
  slovo1.Delete( 1, slovo1.LastDelimiter(L".") );
  if( (slovo1.CompareIC(L"cav"))*(slovo1.CompareIC(L"dat"))*(slovo1.CompareIC(L"cvx"))!=0 )
    {error(57,stroka0,slovo1.c_str()); state=11; TransForm->ShowModal(); }
  else
  {
   if (slovo1.CompareIC(L"cvx")==0) {cvx=true;}  //------- если файл поверхности
   bool Flag=read_data();
   if(!Flag) {state=11; TransForm->ShowModal();} // если возникли проблемы со считыванием
   if(state==-1)TransForm->ShowModal();
   if((pik+Srf.ncols)==0) state=11;    // если не было пикетов и рельефа
   if((state==0)&&(pik!=0)) { state=1; CurrState(); } // если есть пикеты и все ОК
   if((pik==0)&&(Srf.ncols!=0)) {state=13 ;write_data(); } // если есть рельеф и все ОК
  }

  if(BifStation!=NULL) delete [] BifStation; BifStation=NULL;
  if(FileList!=NULL) delete FileList; FileList=NULL;
  if(PrefList!=NULL){
   for (int i=0; i<PrefList->Count; i++)
    {FileParam* FilePar = (FileParam*)PrefList->Objects[i];  delete FilePar; }
   delete PrefList; }
  PrefList=NULL;
  if(R!=NULL) delete [] R; R=NULL;
  if(VR!=NULL) delete [] VR; VR=NULL;
  if(VRN!=NULL) delete [] VRN; VRN=NULL;
  TransForm->Close();
  if(state==11) return false; //если выходим не построив (по exit)
  return true;
}

//********************* Initialization data & components *********************
//---------------------------------------------------------------------------
bool InitData()
{//инициализируем все данные перед работой

   pik = 0;             // номер текущего пикета (число пикетов)
   vet = 1;             // номер текущей ветки (число веток)
   srv = 1;             // текущее число съемок (число съемок)
   fxp = 0;             // текущее фикс. точек (число фикс. точек)
   fil = 0;             // текущее число includ-файлов (число includ-файлов)
   wal = 0;             // номер текущей стены (число изерений до стен)
   com_num=0;           // текущее число комментариев
   wcom_num=0;          // текущее число комментариев к стенам
   lrud=0;              // число данных lrud
   //-----
   line_number = 0;     // номер текущей строки текущего файла
   color=2; sur_color=8;// текущий цвет и цвет пов. топо (Green и DarkGray по умолч.)
   dup_color=8;         // цвет duplicate топо
   corrL=1;             // перем. плавной коррекции длины и азимута
   corrLabs=0;          // переменная для обгрызанных рулеток
   truth=0;             // перем. достоверности съемки
   new_survey=0;        // флаг для новой съемки
   magnit=0;            // магнитное склонение

   angle=1;             // перекл. угол наклона/перепад
   comment=0;           // признак комментария
   razvertka=1;         // признак развертки
   state=0;             // состояние
   cvx=false;           // флаг файла, содержащего поверхность
   cav=false;           // флаг наличия пещерных пикетов
   walls_from_to=true;  // флаг к какому пикету стены
   z_survey = false;
   lr_normal = false;
   ignore_walls = false;
   only_convex_walls = false;

 //  memset(R, 0,sizeof(ring )*max_rin);  //до 50 колец   4x50 = 400 байт
 //  memset(VRN, 0,sizeof(int)*max_rin);  //массив числа веток в кольце  2x50 = 200 байт

 FirstError.line=0; FirstError.item=0; FirstError.num=0;
 cave_name=L"";
 region_name=L"";
 survey_prefix=L"";
 curr_prefix=L"";
 full_prefix=L".";
 fix_label=L"";
 AscFile=L"";
 TransForm->txtLoops=L""; //обнуление информационной строки

 Srf.ncols=0; Srf.nrows=0;
 Srf.xllcorner=0; Srf.yllcorner=0; Srf.xllcorner_wgs=0; Srf.yllcorner_wgs=0;
 Srf.xcellsize=0; Srf.ycellsize=0; Srf.convergence=0;
 Srf.flag=false;

 stroka0=NULL;
 if(R!=NULL) delete [] R; R=NULL;
 if(VR!=NULL) delete [] VR; VR=NULL;
 if(VRN!=NULL) delete [] VRN; VRN=NULL;
 if(Srf.Z!=NULL) delete [] Srf.Z; Srf.Z=NULL;
 if(Srf.X!=NULL) delete [] Srf.X; Srf.X=NULL;
 if(Srf.Y!=NULL) delete [] Srf.Y; Srf.Y=NULL;
 if(FixPnt!=NULL) delete [] FixPnt; FixPnt=NULL;
 if(localCoordinateSystem!=NULL) delete localCoordinateSystem; localCoordinateSystem=NULL;
 if(pre_pnt!=0) FixPnt = new FixPoint[pre_pnt];
  for(int i=0; i<pre_pnt; i++) FixPnt[i].MemSet0();    //обнуление FixPoint
 if(Survey1!=NULL) delete [] Survey1; Survey1=NULL;
 Survey1 = new Survey[pre_srv+1];
  for(int i=0; i<pre_srv; i++) Survey1[i].MemSet0();//обнуление съемок
 if(P1!=NULL) delete [] P1; P1=NULL;
 P1 = new Piket[pre_pik+1];
  for(int i=0; i<pre_pik; i++) P1[i].MemSet0();     //обнуление пикетов
 if(Wall1!=NULL) delete [] Wall1; Wall1=NULL;
 if(pre_wal!=0) {Wall1 = new Wall[pre_wal+1];
  for(int i=0; i<pre_wal; i++) Wall1[i].MemSet0();} //обнуление расст. до стен
 if(StationList!=NULL) delete StationList;
 StationList = new TStringList();
 StationList->Add(L"");  // т.к.0 зарезервирован за пустой меткой
 if(WallStationList!=NULL) delete WallStationList;
 WallStationList = new TStringList();
 if(FileList!=NULL) delete FileList;
 FileList = new TStringList();
 FileList->Add(L"");
 if(PrefList!=NULL){
  for (int i=0; i<PrefList->Count; i++)
   {FileParam* FilePar = (FileParam*)PrefList->Objects[i];  delete FilePar; }
  delete PrefList; }
 PrefList = new TStringList();
 PrefList->Add(L"");

 Equates.clear();

 return true;
}
//---------------------------------------------------------------------------
void __fastcall TTransForm::ComponentsInit()
{
 state=0;
 Memo1->Clear();
 RadioGroup1->ItemIndex=0;
 RadioGroup1->Visible=false;
 Label1->Caption=L"просмотр колец";
 Label1->Visible=false;
 BitBtn2->Visible=false;
 TransForm->WindowState=wsNormal;
 TransForm->IncFiles=L"";
 TransForm->Comment=L"";
 TransForm->WallComment=L"";
 TransForm->DataLength.lps=0;
}
//****************************** Buttons ************************************
//-------------------------------------OK------------------------------------

void __fastcall TTransForm::BitBtn1Click(TObject *Sender)
{        //OK=continue button
  {
  if (pik==0) {state=13; CurrState(); return;}
//  if ((state==-1)||(state==-2)) {CurrState(); return;}
  if ((state==-1)||(state==-2)||(state==2)) {CurrState(); return;}
  if (state==4)  {
    if(RadioGroup1->ItemIndex==0) state=7; //обработка колец
      else state=5;  //не обрабатывать кольца
    CurrState(); return;}
  if ((state==-8)||(state==-7)) {state=8;}
  if (state==8) {state=9; CurrState(); }    //обработка колец
  if (state==-12) {state=12; CurrState();}  // показ оставшейся невязки
  if (state==11) TransForm->Close();
  }
}
//----------------------------------Exit-------------------------------------
void __fastcall TTransForm::Button1Click(TObject *Sender)
{        //exit button
   state=11;
   TransForm->Close();
}
//------------------------------ Exit&Edit ----------------------------------
void LoadErrorFile()
{
	  LoadFromFileUTF8Test(EditForm->RichEdit1->Lines, FirstError.file);
      EditForm->SetFileName(FirstError.file);
      EditForm->RichEdit1->CaretPos=Point(0,FirstError.line);
      EditForm->RichEdit1->SelLength =
      EditForm->RichEdit1->Lines->Strings[FirstError.line].Length();
}
//--------------------------------Exit&Edit----------------------------------
void __fastcall TTransForm::Button2Click(TObject *Sender)
{    //exit & edit
  if(EditForm->Visible)
   {
   if((EditForm->edit_flag==0)||
      (EditForm->FFileName==EditForm->OpenDialog1->FileName) ||
      (EditForm->Confirm()==2))
    {EditForm->edit_flag=0;}
   if(FirstError.num!=0) //если есть ошибки - открываем первый файл с ошибками
    {
     if(EditForm->Confirm()==2) //если редактир. файл не сохранен
      { EditForm->Close(); if(EditForm->Visible==false) LoadErrorFile(); }
     else LoadErrorFile();
    }
   }
  else  //если окно edit закрыто
   {
   EditForm->edit_flag=0;
   if(FirstError.num!=0) //если есть ошибки - открываем первый файл с ошибками
    {
     LoadErrorFile();
    }
    else   //открываем "главный" файл
	{
	 LoadFromFileUTF8Test(EditForm->RichEdit1->Lines, MainForm1->CurrFileName);
	 EditForm->SetFileName(MainForm1->CurrFileName);
     EditForm->OpenDialog1->FileName=MainForm1->CurrFileName;
    }
   }
   TransForm->Close();
   state=11;
   EditForm->Show();
}
//------------------------------Show-loops-----------------------------------

void __fastcall TTransForm::BitBtn2Click(TObject *Sender)
{
 if (state==4){ state=6; CurrState(); state=4; //показ меток колец
     TransForm->BitBtn2->Visible=false;
     TransForm->Label1->Visible=false;
     TransForm->Memo1->SetFocus();}
 if (state==-12) { state=9; CurrState();}   // прод. обраб. колец
}

//---------------------------Key events--------------------------------------
void __fastcall TTransForm::Memo1KeyDown(TObject *Sender, WORD &Key,
      TShiftState Shift)
{
  if ((Key==27)&&(state!=8)) {TransForm->Close();  state=11; return;}   //выход по esc
  if (state==-1) {CurrState(); return;}
  if (state==-2) {CurrState(); return;}
  if (state==4)
  {
   switch(Key)
   {
    case L' '       : state=5; break;   //выход по пробелу
    case VK_RETURN : state=7; break;   //обработка колец
    default        : return;
   }
   CurrState(); return;
   }
   if ((state==-8)||(state==-7)) {
   switch(Key){
	case L' '       : state=5; CurrState(); break;   //выход по пробелу
    case VK_RETURN : state=8; break;   //обработка колец
    default    : return;
    }
   }
   if ((state==8)&&(Key==VK_RETURN)) {state=9; CurrState(); }
   if ((state==-12)&&(Key==VK_RETURN)) {state=12; CurrState(); }
   if (state==9) {CurrState();  }
}
//---------------------------------------------------------------------------
void __fastcall TTransForm::FormCloseQuery(TObject *Sender, bool &CanClose)
{
if(state<10) state=11;
}
void LoadFromFileUTF8Test(TStrings * list, UnicodeString fileName) {
	list->LoadFromFile(fileName);
	if (list->Encoding != TEncoding::UTF8) {
		const wchar_t markerCodes[] = {
			0x2122,
			0x00A6,
			0x0408,
			0x0459,
			0x2022,
			0x045C,
			0x201C,
			0x0401,
			0x00A9,
			0x2014,
			0x00A4,
			0x00AB,
			0x2019,
			0x0452,
			0x045F,
			0x00A0,
			0x045B,
			0x203A,
			0x201D,
			0x0407,
			0x00A7,
			0x040E,
			0x045A,
			0x0098,
			0x045E,
			0x00AC,
			0x2018,
			0x00AE,

			0x2116,
			0x2020,
			0x0453,
			0x0454,
			0x00B5,
			0x0405,
			0x0456,
			0x20AC,
			0x2030,
			0x00B7,
			0x2026,
			0x0409,
			0x201E,
			0x2039,
			0x0406,
			0x00B0,
			0x0457,
			0x0402,
			0x0455,
			0x00BB,
			0x0491,
			0x00B6,
			0x040C,
			0x040F,
			0x2021,
			0x0403,
			0x0458,
			0x0451,
			0x201A,
			0x040A,
			0x00B1,
			0x040B,

			'\0'
			};


	  {
		auto ptr = list->Text.c_str();
		while (ptr = wcschr(ptr, 0x0420)) {
			if (wcschr(markerCodes, *(ptr+1))) {
				list->LoadFromFile(fileName, TEncoding::UTF8);
				return;
			} else {
				ptr++;
			}
		}
	  }
	  {
		auto ptr = list->Text.c_str();
		while (ptr = wcschr(ptr, 0x0421)) {
			if (wcschr(markerCodes, *(ptr+1))) {
				list->LoadFromFile(fileName, TEncoding::UTF8);
				return;
			} else {
				ptr++;
			}
		}
	  }
	}
}
//---------------------------------------------------------------------------
