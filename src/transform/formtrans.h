﻿//---------------------------------------------------------------------------

#ifndef formtransH
#define formtransH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Buttons.hpp>
#include <ExtCtrls.hpp>     
#include <CaveData.h>  
#include <vector>
#include <map>
//---------------------------------------------------------------------------

#define cave_file_Len 255
/*
#define max_pik=2700; //max число пикетов
#define max_sur=70;   //max число съемок
*/
//для передачи в MainForm1:
//----------------------------- длины масивов -------------------------------
struct DLength {int pik, vet, vet0, met, srv, lps, fil, com, sur, dup, equ, fxp, wal, wcom;
                float declin; int lrud;};
//---------------------------------------------------------------------------
class TTransForm : public TForm
{
__published:	// IDE-managed Components
        TMemo *Memo1;
        TPanel *Panel1;
        TButton *Button1;
        TButton *Button2;
        TBitBtn *BitBtn1;
        TRadioGroup *RadioGroup1;
        TBitBtn *BitBtn2;
        TLabel *Label1;
        void __fastcall Memo1KeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);
        void __fastcall BitBtn1Click(TObject *Sender);
        void __fastcall Button1Click(TObject *Sender);
        void __fastcall Button2Click(TObject *Sender);
        void __fastcall BitBtn2Click(TObject *Sender);
        void __fastcall FormCloseQuery(TObject *Sender, bool &CanClose);
private:	// User declarations
public:		// User declarations
        __fastcall TTransForm(TComponent* Owner);
        void __fastcall ComponentsInit();
        UnicodeString Comment;
        UnicodeString WallComment;
        UnicodeString IncFiles;      // пути к файлам -CavePath
        DLength DataLength;       // длины массивов
        UnicodeString txtLoops;      // текст для сохранения в txt
};

//--------------------------- класс пикетов ------------------------------
class Piket         //3x4+2+2=16 байт  (если 14 байт, то 4.5 тыс. пикетов в 64K)
{ public:
    float L;                     //потом переводим
    float An;                    //в
    float Az;                    //декартовы
    float left,right,up,down;    // параметры пикета
    unsigned char col = 0; //цвет
    unsigned char prz = 0; //вх. признаки точки  //int prz; //временно для отладки
    unsigned char prz2 = 0;
    signed char prz1 = 0;  //точность
    int  Vet = 0;          //номер текущей ветки
    unsigned char Srv = 0; //номер текущей съемки
    int Com = 0;           //номер комментария
    int line = 0;          //номер строки в файле данных
    int proj = 0;          //переменная разреза-развертки
    int metBeg = 0;              //номера меток начала
    int metEnd = 0;              //и конца ветки

    void MemSet0();//обнуление всех данных

    void deCart(char mode); //перевод данных пикета в декартовы коорд.
    float pr_xy();     //разрез-развертка по горизонтали
    void control();    //проверка и коррекция текущего пикета
};

//-------------------------- Класс веток -----------------------------------
class Vetka
{
   public:
     int metBeg = 0;              //номера меток начала
     int metEnd = 0;              //и конца ветки
     int pikBeg = 0;              //номера пикетов начала
     int pikEnd = 0;              //номера меток начала
     float XB,YB,ZB,XE,YE,ZE; //коорд. начала и конца веток
     float XYB;               //коорд. начала проекции для развертки
     char con = 0;                //признаки соединения с др. ветками
     float X(){return XE-XB;};
     float Y(){return YE-YB;};
     float Z(){return ZE-ZB;};

     void  MemSet0();//обнуление всех данных
     void  Shift();
     float vx(char k);
     void  SetPr_xy();
};
//---------------------------- класс съемок ---------------------------------
class Survey
{
  public:
    UnicodeString    team;
    UnicodeString    title;
    UnicodeString    date;
    unsigned char file = 0;
    //wchar_t prefix[label_Len];
    void MemSet0() {team=L""; title=L""; date=L""; file=0;}; //обнуление всех данных
};
//---------------------------- класс стен ---------------------------------
class Wall
{
  public:
    int met = 0;
    double L;                     //потом переводим
    double An;                    //в
    double Az;                    //декартовы
    unsigned char col = 0;            //цвет    
    unsigned char Srv = 0;            //номер текущей съемки
    int line = 0;                     //номер строки в файле данных
    int com = 0;
    bool beg = false;                     // 0 - пикет на стену, 1 - от стены
	bool ignoreAt3d = false;
    void MemSet0() {met=0; L=0; An=0; Az=0; beg=0; col=0; Srv=0; com=0; line=0;}; //обнуление всех данных
    void deCart(char mode); //перевод данных пикета в декартовы коорд.
    void control();    //проверка и коррекция текущего пикета
};
//---------------------------- класс рельефа ---------------------------------
class Surface
{
  public:
    bool flag;
    int ncols = 0;
	int nrows = 0;
    double xllcorner, yllcorner, xllcorner_wgs, yllcorner_wgs;
    double xcellsize, ycellsize, convergence;
    long *X,*Y,*Z;
    int utmZone {0};
    bool utmNorth {true};
};
//---------------------------- класс фикс. точек ---------------------------------
enum FIX_POINT_MODE {
    FPM_UNKNOWN = -1,
    FPM_WGS84 = 0,
    FPM_UTM = 1
};

class FixPoint
{
  public:
    UnicodeString label;
    UnicodeString com;
    FIX_POINT_MODE sourceCS {FPM_UNKNOWN};

    double utmX {0}, utmY {0}, utmH {0};
    int utmZone {0};
    bool utmNorth {true};
    
    double wgsX {0}, wgsY {0}, wgsH {0};

    double localX {0}, localY {0}, localZ {0};

    char prz {0};
    void  MemSet0(){ *this = FixPoint(); }
                                                      //обнуление всех данных
};
//---------------------------------------------------------------------------
struct ring {int me = 0; int ve = 0;};   //метка(+/- начало/конец) и номера веток
struct FError {int line = 0; wchar_t file[cave_file_Len]; char item = 0; int num = 0;};
                                                //данные о месте 1-й ошибки
bool RunBuild(wchar_t* FileName1);
//---------------------------------------------------------------------------
namespace GeographicLib {
    class LocalCartesian; 
}

void LoadFromFileUTF8Test(TStrings * list, UnicodeString fileName);
//---------------------------------------------------------------------------
extern PACKAGE TTransForm *TransForm;
extern PACKAGE Piket *P1;
extern PACKAGE Vetka *V1;
extern PACKAGE Survey *Survey1;   // массив съемок
extern PACKAGE Surface Srf;       // рельеф поверхности
extern PACKAGE FixPoint *FixPnt;  // массив фикс. точек
extern PACKAGE Wall *Wall1;     // массив стен
extern PACKAGE TStringList* StationList;    // имена (метки) пикетов
extern PACKAGE EquatesArrayType Equates; 
extern PACKAGE GeographicLib::LocalCartesian* localCoordinateSystem;
//---------------------------------------------------------------------------
#endif
