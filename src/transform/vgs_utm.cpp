﻿//---------------------------------------------------------------------------
// Функция получает параметры : Север в метрах, Восток в метрах, номер зоны проекции
// Функция выдает значения : Широта в градусах, Долгота в градусах.
// Номер зоны = 31 + целая часть(Долгота_в_градусах/6) , Бзыбь - 37 зона.
//
// Пример вызова функции : utm_to_wgs(North, East, zona, &Latitude, &Longitude);

void utm_to_wgs(long double N, long double E, long double zona,
                long double *Lat, long double *Long)
{
  long double pi,Es,Ns,Ef,Nf,K0,fi,
  a, f_1,f,e2,e,e_s2,n,n2,n3,n4,ros,nus,sigma,fis,secs,
  T1,T2,T3,T4,T5,
  ts,ts2,ts3,ts4,ts5,ts6,
  x,x2,x3,x4,x5,x6,x7,
  psis,psis2,psis3,psis4,
  L1,L2,L3,L4,
  B,B1,B2,B3,B4,Bcentr;

  pi=3.141592653590;
  Nf=0;
  Ef=500000;
  K0=0.9996;
  a=6378137;
  f_1=298.257222101;
  f=1/f_1;
  e2=f*(2-f);
  e=sqrt(e2);
  e_s2=e2/(1-e2);
  n=f/(2-f);
  n2=n*n;
  n3=n2*n;
  n4=n2*n2;

  Ns=N-Nf;
  Es=E-Ef;

  sigma=Ns/(K0*a*(1-n)*(1-n2)*(1+9*n2/4+225*n4/64));

  T1=sigma;
  T2=(3*n/2-27*n3/32)*sin(2*sigma);
  T3=(21*n2/16-55*n4/32)*sin(4*sigma);
  T4=(151*n3/96)*sin(6*sigma);
  T5=(1097*n4/512)*sin(8*sigma);
  fis=T1+T2+T3+T4+T5;

  ros=a*(1-e2)/pow((double)(1-e2*sin(fis)*sin(fis)),1.5);
  nus=a/pow((double)(1-e2*sin(fis)*sin(fis)),0.5);

  ts=tan(fis);
  ts2=ts*ts;
  ts3=ts*ts2;
  ts4=ts*ts3;
  ts5=ts*ts4;
  ts6=ts*ts5;
  secs=1/cos(fis);

  psis=nus/ros;
  psis2=psis*psis;
  psis3=psis*psis2;
  psis4=psis*psis3;

  x=Es/(K0*nus);
  x2=x*x;
  x3=x*x2;
  x4=x*x3;
  x5=x*x4;
  x6=x*x5;
  x7=x*x6;

  L1=-1;
  L2=-4*psis2+9*psis*(1-ts2)+12*ts2;
  L3=-(8*psis4*(11-24*ts2)-12*psis3*(21-71*ts2)+15*psis2*(15-98*ts2+15*ts4)+
       180*psis*(5*ts2-3*ts4)+360*ts4);
  L4=1385+3633*ts2+4095*ts4+1575*ts6;
  fi=fis+ts*Es*(L1*x/2+L2*x3/24+L3*x5/720+L4*x7/40320)/(K0*ros);

  Bcentr=pi*(zona*6-177-6)/180;
  B1=1;
  B2=-(psis+2*ts2);
  B3=-4*psis3*(1-6*ts2)+psis2*(9-68*ts2)+72*psis*ts2+24*ts4;
  B4=-(61+662*ts2+1320*ts4+720*ts6);
  B=Bcentr+(x*B1+x3*B2/6+x5*B3/120+x7*B4/5040)*secs;

  *Lat=180*fi/pi;
  *Long=180*B/pi;
}

//---------------------------------------------------------------------------
// Функция получает параметры : Широта в градусах, Долгота в градусах, номер зоны проекции.
// Функция выдает значения : Север в метрах, Восток в метрах.
// Номер зоны = 31 + целая часть(Долгота_в_градусах/6) , Бзыбь - 37 зона.
//
// Пример вызова функции : utm_to_wgs(Latitude, Longitude, zona, &North, &East);

void wgs_to_utm(long double Lat, long double Long, long double zona,
                long double *N, long double *E)
{
  long double pi,Es,Ns,Ef,Nf,K0,CentrMer,dLong,
  a, f_1,f,e2,e,e4,e6,e_s2,n,ro,nu,fi,
  T,T1,T2,T3,T4,
  A0,A2,A4,A6,
  psi,psi2,psi3,psi4,
  tfi,tfi2,tfi4,tfi6,
  cfi,cfi2,cfi3,cfi4,cfi5,cfi6,cfi7,sfi,
  omega,omega2,omega3,omega4,omega5,omega6,omega7;

  pi=3.141592653590;
  Nf=0;
  Ef=500000;
  K0=0.9996;
  a=6378137;
  f_1=298.257222101;
  f=1/f_1;
  e2=f*(2-f);
  e=sqrt(e2);
  e4=e2*e2;
  e6=e4*e2;
  e_s2=e2/(1-e2);
  CentrMer=3+6*(zona-31);
  dLong=Long-CentrMer;
  fi=pi*Lat/180;
  sfi=sin(fi);

  ro=a*(1-e2)/pow((double)(1-e2*sfi*sfi),1.5);
  nu=a/pow((double)(1-e2*sfi*sfi),0.5);

  A0=1-e2/4-3*e4/64-5*e6/256;
  A2=-3*(e2+e4/4+15*e6/128)/8;
  A4=15*(e4+3*e6/4)/256;
  A6=-35*e6/3072;

  T=a*(A0*fi+A2*sin(2*fi)+A4*sin(4*fi)+A6*sin(6*fi));

  psi=nu/ro;
  psi2=psi*psi;
  psi3=psi2*psi;
  psi4=psi3*psi;

  omega=pi*dLong/180;
  omega2=omega*omega;
  omega3=omega2*omega;
  omega4=omega3*omega;
  omega5=omega4*omega;
  omega6=omega5*omega;
  omega7=omega6*omega;

  tfi=tan(fi);
  tfi2=tfi*tfi;
  tfi4=tfi2*tfi2;
  tfi6=tfi4*tfi2;

  cfi=cos(fi);
  cfi2=cfi*cfi;
  cfi3=cfi2*cfi;
  cfi4=cfi3*cfi;
  cfi5=cfi4*cfi;
  cfi6=cfi5*cfi;
  cfi7=cfi6*cfi;

  *E=Ef+K0*nu*omega*cfi*( 1 + omega2*cfi2*(psi-tfi2)/6
              +omega4*cfi4*(4*psi3*(1-6*tfi2)+psi2*(1+8*tfi2)-2*psi*tfi2+tfi4)/120
              +omega6*cfi6*(61-479*tfi2+179*tfi4-tfi6)/5040 );

  *N=Nf+K0*T+K0*nu*sfi*omega2*cfi*( 0.5 + omega2*cfi2*(4*psi2+psi-tfi2)/24
        + omega4*cfi4*(8*psi4*(11-24*tfi2)-28*psi3*(1-6*tfi2)+psi2*(1-32*tfi2)-2*psi*tfi2+tfi4)/720
        + omega6*cfi6*(1385-3111*tfi2+543*tfi4-tfi6)/40320) ;

}
//---------------------------------------------------------------------------
/*
void __fastcall TForm1::Button1Click(TObject *Sender)
{
  Ee=Edit2->Text.ToDouble();
  Nn=Edit1->Text.ToDouble();
  zona_=Edit5->Text.ToDouble();

  utm_to_wgs(Nn,Ee,zona_,&Lat_,&Long_);

  flo=Lat_;  swprintf(str1,L"%.8f",flo); Edit3->Text=str1;
  flo=Long_; swprintf(str1,L"%.8f",flo); Edit4->Text=str1;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button2Click(TObject *Sender)
{
  Lat_=Edit6->Text.ToDouble();
  Long_=Edit7->Text.ToDouble();
  zona_=Edit5->Text.ToDouble();

  wgs_to_utm(Lat_,Long_,zona_,&Nn,&Ee);

  flo=Nn;  swprintf(str1,L"%.8f",flo); Edit8->Text=str1;
  flo=Ee; swprintf(str1,L"%.8f",flo); Edit9->Text=str1;
}
//---------------------------------------------------------------------------
*/
